<?php
 header("Access-Control-Allow-Origin: *");
	session_start();
	$ch = curl_init('https://api.nt.digital/state/list');	
	curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json","charset:UTF-8","Authorization: Bearer ". $_SESSION['token_login']));
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_POST, 0);
	curl_setopt($ch, CURLOPT_TIMEOUT, 30);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLINFO_HEADER_OUT, false);	
	$return = curl_exec($ch);
	echo $return;
	curl_close($ch);
