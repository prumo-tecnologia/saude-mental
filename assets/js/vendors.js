/*! jQuery v3.2.0 | (c) JS Foundation and other contributors | jquery.org/license */
!(function (a, b) {
  "use strict";
  "object" == typeof module && "object" == typeof module.exports
    ? (module.exports = a.document
        ? b(a, !0)
        : function (a) {
            if (!a.document)
              throw new Error("jQuery requires a window with a document");
            return b(a);
          })
    : b(a);
})("undefined" != typeof window ? window : this, function (a, b) {
  "use strict";
  var c = [],
    d = a.document,
    e = Object.getPrototypeOf,
    f = c.slice,
    g = c.concat,
    h = c.push,
    i = c.indexOf,
    j = {},
    k = j.toString,
    l = j.hasOwnProperty,
    m = l.toString,
    n = m.call(Object),
    o = {};
  function p(a, b) {
    b = b || d;
    var c = b.createElement("script");
    (c.text = a), b.head.appendChild(c).parentNode.removeChild(c);
  }
  var q = "3.2.0",
    r = function (a, b) {
      return new r.fn.init(a, b);
    },
    s = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,
    t = /^-ms-/,
    u = /-([a-z])/g,
    v = function (a, b) {
      return b.toUpperCase();
    };
  (r.fn = r.prototype = {
    jquery: q,
    constructor: r,
    length: 0,
    toArray: function () {
      return f.call(this);
    },
    get: function (a) {
      return null == a ? f.call(this) : a < 0 ? this[a + this.length] : this[a];
    },
    pushStack: function (a) {
      var b = r.merge(this.constructor(), a);
      return (b.prevObject = this), b;
    },
    each: function (a) {
      return r.each(this, a);
    },
    map: function (a) {
      return this.pushStack(
        r.map(this, function (b, c) {
          return a.call(b, c, b);
        })
      );
    },
    slice: function () {
      return this.pushStack(f.apply(this, arguments));
    },
    first: function () {
      return this.eq(0);
    },
    last: function () {
      return this.eq(-1);
    },
    eq: function (a) {
      var b = this.length,
        c = +a + (a < 0 ? b : 0);
      return this.pushStack(c >= 0 && c < b ? [this[c]] : []);
    },
    end: function () {
      return this.prevObject || this.constructor();
    },
    push: h,
    sort: c.sort,
    splice: c.splice,
  }),
    (r.extend = r.fn.extend = function () {
      var a,
        b,
        c,
        d,
        e,
        f,
        g = arguments[0] || {},
        h = 1,
        i = arguments.length,
        j = !1;
      for (
        "boolean" == typeof g && ((j = g), (g = arguments[h] || {}), h++),
          "object" == typeof g || r.isFunction(g) || (g = {}),
          h === i && ((g = this), h--);
        h < i;
        h++
      )
        if (null != (a = arguments[h]))
          for (b in a)
            (c = g[b]),
              (d = a[b]),
              g !== d &&
                (j && d && (r.isPlainObject(d) || (e = Array.isArray(d)))
                  ? (e
                      ? ((e = !1), (f = c && Array.isArray(c) ? c : []))
                      : (f = c && r.isPlainObject(c) ? c : {}),
                    (g[b] = r.extend(j, f, d)))
                  : void 0 !== d && (g[b] = d));
      return g;
    }),
    r.extend({
      expando: "jQuery" + (q + Math.random()).replace(/\D/g, ""),
      isReady: !0,
      error: function (a) {
        throw new Error(a);
      },
      noop: function () {},
      isFunction: function (a) {
        return "function" === r.type(a);
      },
      isWindow: function (a) {
        return null != a && a === a.window;
      },
      isNumeric: function (a) {
        var b = r.type(a);
        return ("number" === b || "string" === b) && !isNaN(a - parseFloat(a));
      },
      isPlainObject: function (a) {
        var b, c;
        return (
          !(!a || "[object Object]" !== k.call(a)) &&
          (!(b = e(a)) ||
            ((c = l.call(b, "constructor") && b.constructor),
            "function" == typeof c && m.call(c) === n))
        );
      },
      isEmptyObject: function (a) {
        var b;
        for (b in a) return !1;
        return !0;
      },
      type: function (a) {
        return null == a
          ? a + ""
          : "object" == typeof a || "function" == typeof a
          ? j[k.call(a)] || "object"
          : typeof a;
      },
      globalEval: function (a) {
        p(a);
      },
      camelCase: function (a) {
        return a.replace(t, "ms-").replace(u, v);
      },
      each: function (a, b) {
        var c,
          d = 0;
        if (w(a)) {
          for (c = a.length; d < c; d++)
            if (b.call(a[d], d, a[d]) === !1) break;
        } else for (d in a) if (b.call(a[d], d, a[d]) === !1) break;
        return a;
      },
      trim: function (a) {
        return null == a ? "" : (a + "").replace(s, "");
      },
      makeArray: function (a, b) {
        var c = b || [];
        return (
          null != a &&
            (w(Object(a))
              ? r.merge(c, "string" == typeof a ? [a] : a)
              : h.call(c, a)),
          c
        );
      },
      inArray: function (a, b, c) {
        return null == b ? -1 : i.call(b, a, c);
      },
      merge: function (a, b) {
        for (var c = +b.length, d = 0, e = a.length; d < c; d++) a[e++] = b[d];
        return (a.length = e), a;
      },
      grep: function (a, b, c) {
        for (var d, e = [], f = 0, g = a.length, h = !c; f < g; f++)
          (d = !b(a[f], f)), d !== h && e.push(a[f]);
        return e;
      },
      map: function (a, b, c) {
        var d,
          e,
          f = 0,
          h = [];
        if (w(a))
          for (d = a.length; f < d; f++)
            (e = b(a[f], f, c)), null != e && h.push(e);
        else for (f in a) (e = b(a[f], f, c)), null != e && h.push(e);
        return g.apply([], h);
      },
      guid: 1,
      proxy: function (a, b) {
        var c, d, e;
        if (
          ("string" == typeof b && ((c = a[b]), (b = a), (a = c)),
          r.isFunction(a))
        )
          return (
            (d = f.call(arguments, 2)),
            (e = function () {
              return a.apply(b || this, d.concat(f.call(arguments)));
            }),
            (e.guid = a.guid = a.guid || r.guid++),
            e
          );
      },
      now: Date.now,
      support: o,
    }),
    "function" == typeof Symbol && (r.fn[Symbol.iterator] = c[Symbol.iterator]),
    r.each(
      "Boolean Number String Function Array Date RegExp Object Error Symbol".split(
        " "
      ),
      function (a, b) {
        j["[object " + b + "]"] = b.toLowerCase();
      }
    );
  function w(a) {
    var b = !!a && "length" in a && a.length,
      c = r.type(a);
    return (
      "function" !== c &&
      !r.isWindow(a) &&
      ("array" === c ||
        0 === b ||
        ("number" == typeof b && b > 0 && b - 1 in a))
    );
  }
  var x = (function (a) {
    var b,
      c,
      d,
      e,
      f,
      g,
      h,
      i,
      j,
      k,
      l,
      m,
      n,
      o,
      p,
      q,
      r,
      s,
      t,
      u = "sizzle" + 1 * new Date(),
      v = a.document,
      w = 0,
      x = 0,
      y = ha(),
      z = ha(),
      A = ha(),
      B = function (a, b) {
        return a === b && (l = !0), 0;
      },
      C = {}.hasOwnProperty,
      D = [],
      E = D.pop,
      F = D.push,
      G = D.push,
      H = D.slice,
      I = function (a, b) {
        for (var c = 0, d = a.length; c < d; c++) if (a[c] === b) return c;
        return -1;
      },
      J =
        "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",
      K = "[\\x20\\t\\r\\n\\f]",
      L = "(?:\\\\.|[\\w-]|[^\0-\\xa0])+",
      M =
        "\\[" +
        K +
        "*(" +
        L +
        ")(?:" +
        K +
        "*([*^$|!~]?=)" +
        K +
        "*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" +
        L +
        "))|)" +
        K +
        "*\\]",
      N =
        ":(" +
        L +
        ")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|" +
        M +
        ")*)|.*)\\)|)",
      O = new RegExp(K + "+", "g"),
      P = new RegExp("^" + K + "+|((?:^|[^\\\\])(?:\\\\.)*)" + K + "+$", "g"),
      Q = new RegExp("^" + K + "*," + K + "*"),
      R = new RegExp("^" + K + "*([>+~]|" + K + ")" + K + "*"),
      S = new RegExp("=" + K + "*([^\\]'\"]*?)" + K + "*\\]", "g"),
      T = new RegExp(N),
      U = new RegExp("^" + L + "$"),
      V = {
        ID: new RegExp("^#(" + L + ")"),
        CLASS: new RegExp("^\\.(" + L + ")"),
        TAG: new RegExp("^(" + L + "|[*])"),
        ATTR: new RegExp("^" + M),
        PSEUDO: new RegExp("^" + N),
        CHILD: new RegExp(
          "^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" +
            K +
            "*(even|odd|(([+-]|)(\\d*)n|)" +
            K +
            "*(?:([+-]|)" +
            K +
            "*(\\d+)|))" +
            K +
            "*\\)|)",
          "i"
        ),
        bool: new RegExp("^(?:" + J + ")$", "i"),
        needsContext: new RegExp(
          "^" +
            K +
            "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" +
            K +
            "*((?:-\\d)?\\d*)" +
            K +
            "*\\)|)(?=[^-]|$)",
          "i"
        ),
      },
      W = /^(?:input|select|textarea|button)$/i,
      X = /^h\d$/i,
      Y = /^[^{]+\{\s*\[native \w/,
      Z = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,
      $ = /[+~]/,
      _ = new RegExp("\\\\([\\da-f]{1,6}" + K + "?|(" + K + ")|.)", "ig"),
      aa = function (a, b, c) {
        var d = "0x" + b - 65536;
        return d !== d || c
          ? b
          : d < 0
          ? String.fromCharCode(d + 65536)
          : String.fromCharCode((d >> 10) | 55296, (1023 & d) | 56320);
      },
      ba = /([\0-\x1f\x7f]|^-?\d)|^-$|[^\0-\x1f\x7f-\uFFFF\w-]/g,
      ca = function (a, b) {
        return b
          ? "\0" === a
            ? "\ufffd"
            : a.slice(0, -1) +
              "\\" +
              a.charCodeAt(a.length - 1).toString(16) +
              " "
          : "\\" + a;
      },
      da = function () {
        m();
      },
      ea = ta(
        function (a) {
          return a.disabled === !0 && ("form" in a || "label" in a);
        },
        { dir: "parentNode", next: "legend" }
      );
    try {
      G.apply((D = H.call(v.childNodes)), v.childNodes),
        D[v.childNodes.length].nodeType;
    } catch (fa) {
      G = {
        apply: D.length
          ? function (a, b) {
              F.apply(a, H.call(b));
            }
          : function (a, b) {
              var c = a.length,
                d = 0;
              while ((a[c++] = b[d++]));
              a.length = c - 1;
            },
      };
    }
    function ga(a, b, d, e) {
      var f,
        h,
        j,
        k,
        l,
        o,
        r,
        s = b && b.ownerDocument,
        w = b ? b.nodeType : 9;
      if (
        ((d = d || []),
        "string" != typeof a || !a || (1 !== w && 9 !== w && 11 !== w))
      )
        return d;
      if (
        !e &&
        ((b ? b.ownerDocument || b : v) !== n && m(b), (b = b || n), p)
      ) {
        if (11 !== w && (l = Z.exec(a)))
          if ((f = l[1])) {
            if (9 === w) {
              if (!(j = b.getElementById(f))) return d;
              if (j.id === f) return d.push(j), d;
            } else if (s && (j = s.getElementById(f)) && t(b, j) && j.id === f)
              return d.push(j), d;
          } else {
            if (l[2]) return G.apply(d, b.getElementsByTagName(a)), d;
            if (
              (f = l[3]) &&
              c.getElementsByClassName &&
              b.getElementsByClassName
            )
              return G.apply(d, b.getElementsByClassName(f)), d;
          }
        if (c.qsa && !A[a + " "] && (!q || !q.test(a))) {
          if (1 !== w) (s = b), (r = a);
          else if ("object" !== b.nodeName.toLowerCase()) {
            (k = b.getAttribute("id"))
              ? (k = k.replace(ba, ca))
              : b.setAttribute("id", (k = u)),
              (o = g(a)),
              (h = o.length);
            while (h--) o[h] = "#" + k + " " + sa(o[h]);
            (r = o.join(",")), (s = ($.test(a) && qa(b.parentNode)) || b);
          }
          if (r)
            try {
              return G.apply(d, s.querySelectorAll(r)), d;
            } catch (x) {
            } finally {
              k === u && b.removeAttribute("id");
            }
        }
      }
      return i(a.replace(P, "$1"), b, d, e);
    }
    function ha() {
      var a = [];
      function b(c, e) {
        return (
          a.push(c + " ") > d.cacheLength && delete b[a.shift()],
          (b[c + " "] = e)
        );
      }
      return b;
    }
    function ia(a) {
      return (a[u] = !0), a;
    }
    function ja(a) {
      var b = n.createElement("fieldset");
      try {
        return !!a(b);
      } catch (c) {
        return !1;
      } finally {
        b.parentNode && b.parentNode.removeChild(b), (b = null);
      }
    }
    function ka(a, b) {
      var c = a.split("|"),
        e = c.length;
      while (e--) d.attrHandle[c[e]] = b;
    }
    function la(a, b) {
      var c = b && a,
        d =
          c &&
          1 === a.nodeType &&
          1 === b.nodeType &&
          a.sourceIndex - b.sourceIndex;
      if (d) return d;
      if (c) while ((c = c.nextSibling)) if (c === b) return -1;
      return a ? 1 : -1;
    }
    function ma(a) {
      return function (b) {
        var c = b.nodeName.toLowerCase();
        return "input" === c && b.type === a;
      };
    }
    function na(a) {
      return function (b) {
        var c = b.nodeName.toLowerCase();
        return ("input" === c || "button" === c) && b.type === a;
      };
    }
    function oa(a) {
      return function (b) {
        return "form" in b
          ? b.parentNode && b.disabled === !1
            ? "label" in b
              ? "label" in b.parentNode
                ? b.parentNode.disabled === a
                : b.disabled === a
              : b.isDisabled === a || (b.isDisabled !== !a && ea(b) === a)
            : b.disabled === a
          : "label" in b && b.disabled === a;
      };
    }
    function pa(a) {
      return ia(function (b) {
        return (
          (b = +b),
          ia(function (c, d) {
            var e,
              f = a([], c.length, b),
              g = f.length;
            while (g--) c[(e = f[g])] && (c[e] = !(d[e] = c[e]));
          })
        );
      });
    }
    function qa(a) {
      return a && "undefined" != typeof a.getElementsByTagName && a;
    }
    (c = ga.support = {}),
      (f = ga.isXML = function (a) {
        var b = a && (a.ownerDocument || a).documentElement;
        return !!b && "HTML" !== b.nodeName;
      }),
      (m = ga.setDocument = function (a) {
        var b,
          e,
          g = a ? a.ownerDocument || a : v;
        return g !== n && 9 === g.nodeType && g.documentElement
          ? ((n = g),
            (o = n.documentElement),
            (p = !f(n)),
            v !== n &&
              (e = n.defaultView) &&
              e.top !== e &&
              (e.addEventListener
                ? e.addEventListener("unload", da, !1)
                : e.attachEvent && e.attachEvent("onunload", da)),
            (c.attributes = ja(function (a) {
              return (a.className = "i"), !a.getAttribute("className");
            })),
            (c.getElementsByTagName = ja(function (a) {
              return (
                a.appendChild(n.createComment("")),
                !a.getElementsByTagName("*").length
              );
            })),
            (c.getElementsByClassName = Y.test(n.getElementsByClassName)),
            (c.getById = ja(function (a) {
              return (
                (o.appendChild(a).id = u),
                !n.getElementsByName || !n.getElementsByName(u).length
              );
            })),
            c.getById
              ? ((d.filter.ID = function (a) {
                  var b = a.replace(_, aa);
                  return function (a) {
                    return a.getAttribute("id") === b;
                  };
                }),
                (d.find.ID = function (a, b) {
                  if ("undefined" != typeof b.getElementById && p) {
                    var c = b.getElementById(a);
                    return c ? [c] : [];
                  }
                }))
              : ((d.filter.ID = function (a) {
                  var b = a.replace(_, aa);
                  return function (a) {
                    var c =
                      "undefined" != typeof a.getAttributeNode &&
                      a.getAttributeNode("id");
                    return c && c.value === b;
                  };
                }),
                (d.find.ID = function (a, b) {
                  if ("undefined" != typeof b.getElementById && p) {
                    var c,
                      d,
                      e,
                      f = b.getElementById(a);
                    if (f) {
                      if (((c = f.getAttributeNode("id")), c && c.value === a))
                        return [f];
                      (e = b.getElementsByName(a)), (d = 0);
                      while ((f = e[d++]))
                        if (
                          ((c = f.getAttributeNode("id")), c && c.value === a)
                        )
                          return [f];
                    }
                    return [];
                  }
                })),
            (d.find.TAG = c.getElementsByTagName
              ? function (a, b) {
                  return "undefined" != typeof b.getElementsByTagName
                    ? b.getElementsByTagName(a)
                    : c.qsa
                    ? b.querySelectorAll(a)
                    : void 0;
                }
              : function (a, b) {
                  var c,
                    d = [],
                    e = 0,
                    f = b.getElementsByTagName(a);
                  if ("*" === a) {
                    while ((c = f[e++])) 1 === c.nodeType && d.push(c);
                    return d;
                  }
                  return f;
                }),
            (d.find.CLASS =
              c.getElementsByClassName &&
              function (a, b) {
                if ("undefined" != typeof b.getElementsByClassName && p)
                  return b.getElementsByClassName(a);
              }),
            (r = []),
            (q = []),
            (c.qsa = Y.test(n.querySelectorAll)) &&
              (ja(function (a) {
                (o.appendChild(a).innerHTML =
                  "<a id='" +
                  u +
                  "'></a><select id='" +
                  u +
                  "-\r\\' msallowcapture=''><option selected=''></option></select>"),
                  a.querySelectorAll("[msallowcapture^='']").length &&
                    q.push("[*^$]=" + K + "*(?:''|\"\")"),
                  a.querySelectorAll("[selected]").length ||
                    q.push("\\[" + K + "*(?:value|" + J + ")"),
                  a.querySelectorAll("[id~=" + u + "-]").length || q.push("~="),
                  a.querySelectorAll(":checked").length || q.push(":checked"),
                  a.querySelectorAll("a#" + u + "+*").length ||
                    q.push(".#.+[+~]");
              }),
              ja(function (a) {
                a.innerHTML =
                  "<a href='' disabled='disabled'></a><select disabled='disabled'><option/></select>";
                var b = n.createElement("input");
                b.setAttribute("type", "hidden"),
                  a.appendChild(b).setAttribute("name", "D"),
                  a.querySelectorAll("[name=d]").length &&
                    q.push("name" + K + "*[*^$|!~]?="),
                  2 !== a.querySelectorAll(":enabled").length &&
                    q.push(":enabled", ":disabled"),
                  (o.appendChild(a).disabled = !0),
                  2 !== a.querySelectorAll(":disabled").length &&
                    q.push(":enabled", ":disabled"),
                  a.querySelectorAll("*,:x"),
                  q.push(",.*:");
              })),
            (c.matchesSelector = Y.test(
              (s =
                o.matches ||
                o.webkitMatchesSelector ||
                o.mozMatchesSelector ||
                o.oMatchesSelector ||
                o.msMatchesSelector)
            )) &&
              ja(function (a) {
                (c.disconnectedMatch = s.call(a, "*")),
                  s.call(a, "[s!='']:x"),
                  r.push("!=", N);
              }),
            (q = q.length && new RegExp(q.join("|"))),
            (r = r.length && new RegExp(r.join("|"))),
            (b = Y.test(o.compareDocumentPosition)),
            (t =
              b || Y.test(o.contains)
                ? function (a, b) {
                    var c = 9 === a.nodeType ? a.documentElement : a,
                      d = b && b.parentNode;
                    return (
                      a === d ||
                      !(
                        !d ||
                        1 !== d.nodeType ||
                        !(c.contains
                          ? c.contains(d)
                          : a.compareDocumentPosition &&
                            16 & a.compareDocumentPosition(d))
                      )
                    );
                  }
                : function (a, b) {
                    if (b) while ((b = b.parentNode)) if (b === a) return !0;
                    return !1;
                  }),
            (B = b
              ? function (a, b) {
                  if (a === b) return (l = !0), 0;
                  var d =
                    !a.compareDocumentPosition - !b.compareDocumentPosition;
                  return d
                    ? d
                    : ((d =
                        (a.ownerDocument || a) === (b.ownerDocument || b)
                          ? a.compareDocumentPosition(b)
                          : 1),
                      1 & d ||
                      (!c.sortDetached && b.compareDocumentPosition(a) === d)
                        ? a === n || (a.ownerDocument === v && t(v, a))
                          ? -1
                          : b === n || (b.ownerDocument === v && t(v, b))
                          ? 1
                          : k
                          ? I(k, a) - I(k, b)
                          : 0
                        : 4 & d
                        ? -1
                        : 1);
                }
              : function (a, b) {
                  if (a === b) return (l = !0), 0;
                  var c,
                    d = 0,
                    e = a.parentNode,
                    f = b.parentNode,
                    g = [a],
                    h = [b];
                  if (!e || !f)
                    return a === n
                      ? -1
                      : b === n
                      ? 1
                      : e
                      ? -1
                      : f
                      ? 1
                      : k
                      ? I(k, a) - I(k, b)
                      : 0;
                  if (e === f) return la(a, b);
                  c = a;
                  while ((c = c.parentNode)) g.unshift(c);
                  c = b;
                  while ((c = c.parentNode)) h.unshift(c);
                  while (g[d] === h[d]) d++;
                  return d
                    ? la(g[d], h[d])
                    : g[d] === v
                    ? -1
                    : h[d] === v
                    ? 1
                    : 0;
                }),
            n)
          : n;
      }),
      (ga.matches = function (a, b) {
        return ga(a, null, null, b);
      }),
      (ga.matchesSelector = function (a, b) {
        if (
          ((a.ownerDocument || a) !== n && m(a),
          (b = b.replace(S, "='$1']")),
          c.matchesSelector &&
            p &&
            !A[b + " "] &&
            (!r || !r.test(b)) &&
            (!q || !q.test(b)))
        )
          try {
            var d = s.call(a, b);
            if (
              d ||
              c.disconnectedMatch ||
              (a.document && 11 !== a.document.nodeType)
            )
              return d;
          } catch (e) {}
        return ga(b, n, null, [a]).length > 0;
      }),
      (ga.contains = function (a, b) {
        return (a.ownerDocument || a) !== n && m(a), t(a, b);
      }),
      (ga.attr = function (a, b) {
        (a.ownerDocument || a) !== n && m(a);
        var e = d.attrHandle[b.toLowerCase()],
          f = e && C.call(d.attrHandle, b.toLowerCase()) ? e(a, b, !p) : void 0;
        return void 0 !== f
          ? f
          : c.attributes || !p
          ? a.getAttribute(b)
          : (f = a.getAttributeNode(b)) && f.specified
          ? f.value
          : null;
      }),
      (ga.escape = function (a) {
        return (a + "").replace(ba, ca);
      }),
      (ga.error = function (a) {
        throw new Error("Syntax error, unrecognized expression: " + a);
      }),
      (ga.uniqueSort = function (a) {
        var b,
          d = [],
          e = 0,
          f = 0;
        if (
          ((l = !c.detectDuplicates),
          (k = !c.sortStable && a.slice(0)),
          a.sort(B),
          l)
        ) {
          while ((b = a[f++])) b === a[f] && (e = d.push(f));
          while (e--) a.splice(d[e], 1);
        }
        return (k = null), a;
      }),
      (e = ga.getText = function (a) {
        var b,
          c = "",
          d = 0,
          f = a.nodeType;
        if (f) {
          if (1 === f || 9 === f || 11 === f) {
            if ("string" == typeof a.textContent) return a.textContent;
            for (a = a.firstChild; a; a = a.nextSibling) c += e(a);
          } else if (3 === f || 4 === f) return a.nodeValue;
        } else while ((b = a[d++])) c += e(b);
        return c;
      }),
      (d = ga.selectors = {
        cacheLength: 50,
        createPseudo: ia,
        match: V,
        attrHandle: {},
        find: {},
        relative: {
          ">": { dir: "parentNode", first: !0 },
          " ": { dir: "parentNode" },
          "+": { dir: "previousSibling", first: !0 },
          "~": { dir: "previousSibling" },
        },
        preFilter: {
          ATTR: function (a) {
            return (
              (a[1] = a[1].replace(_, aa)),
              (a[3] = (a[3] || a[4] || a[5] || "").replace(_, aa)),
              "~=" === a[2] && (a[3] = " " + a[3] + " "),
              a.slice(0, 4)
            );
          },
          CHILD: function (a) {
            return (
              (a[1] = a[1].toLowerCase()),
              "nth" === a[1].slice(0, 3)
                ? (a[3] || ga.error(a[0]),
                  (a[4] = +(a[4]
                    ? a[5] + (a[6] || 1)
                    : 2 * ("even" === a[3] || "odd" === a[3]))),
                  (a[5] = +(a[7] + a[8] || "odd" === a[3])))
                : a[3] && ga.error(a[0]),
              a
            );
          },
          PSEUDO: function (a) {
            var b,
              c = !a[6] && a[2];
            return V.CHILD.test(a[0])
              ? null
              : (a[3]
                  ? (a[2] = a[4] || a[5] || "")
                  : c &&
                    T.test(c) &&
                    (b = g(c, !0)) &&
                    (b = c.indexOf(")", c.length - b) - c.length) &&
                    ((a[0] = a[0].slice(0, b)), (a[2] = c.slice(0, b))),
                a.slice(0, 3));
          },
        },
        filter: {
          TAG: function (a) {
            var b = a.replace(_, aa).toLowerCase();
            return "*" === a
              ? function () {
                  return !0;
                }
              : function (a) {
                  return a.nodeName && a.nodeName.toLowerCase() === b;
                };
          },
          CLASS: function (a) {
            var b = y[a + " "];
            return (
              b ||
              ((b = new RegExp("(^|" + K + ")" + a + "(" + K + "|$)")) &&
                y(a, function (a) {
                  return b.test(
                    ("string" == typeof a.className && a.className) ||
                      ("undefined" != typeof a.getAttribute &&
                        a.getAttribute("class")) ||
                      ""
                  );
                }))
            );
          },
          ATTR: function (a, b, c) {
            return function (d) {
              var e = ga.attr(d, a);
              return null == e
                ? "!=" === b
                : !b ||
                    ((e += ""),
                    "=" === b
                      ? e === c
                      : "!=" === b
                      ? e !== c
                      : "^=" === b
                      ? c && 0 === e.indexOf(c)
                      : "*=" === b
                      ? c && e.indexOf(c) > -1
                      : "$=" === b
                      ? c && e.slice(-c.length) === c
                      : "~=" === b
                      ? (" " + e.replace(O, " ") + " ").indexOf(c) > -1
                      : "|=" === b &&
                        (e === c || e.slice(0, c.length + 1) === c + "-"));
            };
          },
          CHILD: function (a, b, c, d, e) {
            var f = "nth" !== a.slice(0, 3),
              g = "last" !== a.slice(-4),
              h = "of-type" === b;
            return 1 === d && 0 === e
              ? function (a) {
                  return !!a.parentNode;
                }
              : function (b, c, i) {
                  var j,
                    k,
                    l,
                    m,
                    n,
                    o,
                    p = f !== g ? "nextSibling" : "previousSibling",
                    q = b.parentNode,
                    r = h && b.nodeName.toLowerCase(),
                    s = !i && !h,
                    t = !1;
                  if (q) {
                    if (f) {
                      while (p) {
                        m = b;
                        while ((m = m[p]))
                          if (
                            h
                              ? m.nodeName.toLowerCase() === r
                              : 1 === m.nodeType
                          )
                            return !1;
                        o = p = "only" === a && !o && "nextSibling";
                      }
                      return !0;
                    }
                    if (((o = [g ? q.firstChild : q.lastChild]), g && s)) {
                      (m = q),
                        (l = m[u] || (m[u] = {})),
                        (k = l[m.uniqueID] || (l[m.uniqueID] = {})),
                        (j = k[a] || []),
                        (n = j[0] === w && j[1]),
                        (t = n && j[2]),
                        (m = n && q.childNodes[n]);
                      while ((m = (++n && m && m[p]) || (t = n = 0) || o.pop()))
                        if (1 === m.nodeType && ++t && m === b) {
                          k[a] = [w, n, t];
                          break;
                        }
                    } else if (
                      (s &&
                        ((m = b),
                        (l = m[u] || (m[u] = {})),
                        (k = l[m.uniqueID] || (l[m.uniqueID] = {})),
                        (j = k[a] || []),
                        (n = j[0] === w && j[1]),
                        (t = n)),
                      t === !1)
                    )
                      while ((m = (++n && m && m[p]) || (t = n = 0) || o.pop()))
                        if (
                          (h
                            ? m.nodeName.toLowerCase() === r
                            : 1 === m.nodeType) &&
                          ++t &&
                          (s &&
                            ((l = m[u] || (m[u] = {})),
                            (k = l[m.uniqueID] || (l[m.uniqueID] = {})),
                            (k[a] = [w, t])),
                          m === b)
                        )
                          break;
                    return (t -= e), t === d || (t % d === 0 && t / d >= 0);
                  }
                };
          },
          PSEUDO: function (a, b) {
            var c,
              e =
                d.pseudos[a] ||
                d.setFilters[a.toLowerCase()] ||
                ga.error("unsupported pseudo: " + a);
            return e[u]
              ? e(b)
              : e.length > 1
              ? ((c = [a, a, "", b]),
                d.setFilters.hasOwnProperty(a.toLowerCase())
                  ? ia(function (a, c) {
                      var d,
                        f = e(a, b),
                        g = f.length;
                      while (g--) (d = I(a, f[g])), (a[d] = !(c[d] = f[g]));
                    })
                  : function (a) {
                      return e(a, 0, c);
                    })
              : e;
          },
        },
        pseudos: {
          not: ia(function (a) {
            var b = [],
              c = [],
              d = h(a.replace(P, "$1"));
            return d[u]
              ? ia(function (a, b, c, e) {
                  var f,
                    g = d(a, null, e, []),
                    h = a.length;
                  while (h--) (f = g[h]) && (a[h] = !(b[h] = f));
                })
              : function (a, e, f) {
                  return (b[0] = a), d(b, null, f, c), (b[0] = null), !c.pop();
                };
          }),
          has: ia(function (a) {
            return function (b) {
              return ga(a, b).length > 0;
            };
          }),
          contains: ia(function (a) {
            return (
              (a = a.replace(_, aa)),
              function (b) {
                return (b.textContent || b.innerText || e(b)).indexOf(a) > -1;
              }
            );
          }),
          lang: ia(function (a) {
            return (
              U.test(a || "") || ga.error("unsupported lang: " + a),
              (a = a.replace(_, aa).toLowerCase()),
              function (b) {
                var c;
                do
                  if (
                    (c = p
                      ? b.lang
                      : b.getAttribute("xml:lang") || b.getAttribute("lang"))
                  )
                    return (
                      (c = c.toLowerCase()), c === a || 0 === c.indexOf(a + "-")
                    );
                while ((b = b.parentNode) && 1 === b.nodeType);
                return !1;
              }
            );
          }),
          target: function (b) {
            var c = a.location && a.location.hash;
            return c && c.slice(1) === b.id;
          },
          root: function (a) {
            return a === o;
          },
          focus: function (a) {
            return (
              a === n.activeElement &&
              (!n.hasFocus || n.hasFocus()) &&
              !!(a.type || a.href || ~a.tabIndex)
            );
          },
          enabled: oa(!1),
          disabled: oa(!0),
          checked: function (a) {
            var b = a.nodeName.toLowerCase();
            return (
              ("input" === b && !!a.checked) || ("option" === b && !!a.selected)
            );
          },
          selected: function (a) {
            return (
              a.parentNode && a.parentNode.selectedIndex, a.selected === !0
            );
          },
          empty: function (a) {
            for (a = a.firstChild; a; a = a.nextSibling)
              if (a.nodeType < 6) return !1;
            return !0;
          },
          parent: function (a) {
            return !d.pseudos.empty(a);
          },
          header: function (a) {
            return X.test(a.nodeName);
          },
          input: function (a) {
            return W.test(a.nodeName);
          },
          button: function (a) {
            var b = a.nodeName.toLowerCase();
            return ("input" === b && "button" === a.type) || "button" === b;
          },
          text: function (a) {
            var b;
            return (
              "input" === a.nodeName.toLowerCase() &&
              "text" === a.type &&
              (null == (b = a.getAttribute("type")) ||
                "text" === b.toLowerCase())
            );
          },
          first: pa(function () {
            return [0];
          }),
          last: pa(function (a, b) {
            return [b - 1];
          }),
          eq: pa(function (a, b, c) {
            return [c < 0 ? c + b : c];
          }),
          even: pa(function (a, b) {
            for (var c = 0; c < b; c += 2) a.push(c);
            return a;
          }),
          odd: pa(function (a, b) {
            for (var c = 1; c < b; c += 2) a.push(c);
            return a;
          }),
          lt: pa(function (a, b, c) {
            for (var d = c < 0 ? c + b : c; --d >= 0; ) a.push(d);
            return a;
          }),
          gt: pa(function (a, b, c) {
            for (var d = c < 0 ? c + b : c; ++d < b; ) a.push(d);
            return a;
          }),
        },
      }),
      (d.pseudos.nth = d.pseudos.eq);
    for (b in { radio: !0, checkbox: !0, file: !0, password: !0, image: !0 })
      d.pseudos[b] = ma(b);
    for (b in { submit: !0, reset: !0 }) d.pseudos[b] = na(b);
    function ra() {}
    (ra.prototype = d.filters = d.pseudos),
      (d.setFilters = new ra()),
      (g = ga.tokenize = function (a, b) {
        var c,
          e,
          f,
          g,
          h,
          i,
          j,
          k = z[a + " "];
        if (k) return b ? 0 : k.slice(0);
        (h = a), (i = []), (j = d.preFilter);
        while (h) {
          (c && !(e = Q.exec(h))) ||
            (e && (h = h.slice(e[0].length) || h), i.push((f = []))),
            (c = !1),
            (e = R.exec(h)) &&
              ((c = e.shift()),
              f.push({ value: c, type: e[0].replace(P, " ") }),
              (h = h.slice(c.length)));
          for (g in d.filter)
            !(e = V[g].exec(h)) ||
              (j[g] && !(e = j[g](e))) ||
              ((c = e.shift()),
              f.push({ value: c, type: g, matches: e }),
              (h = h.slice(c.length)));
          if (!c) break;
        }
        return b ? h.length : h ? ga.error(a) : z(a, i).slice(0);
      });
    function sa(a) {
      for (var b = 0, c = a.length, d = ""; b < c; b++) d += a[b].value;
      return d;
    }
    function ta(a, b, c) {
      var d = b.dir,
        e = b.next,
        f = e || d,
        g = c && "parentNode" === f,
        h = x++;
      return b.first
        ? function (b, c, e) {
            while ((b = b[d])) if (1 === b.nodeType || g) return a(b, c, e);
            return !1;
          }
        : function (b, c, i) {
            var j,
              k,
              l,
              m = [w, h];
            if (i) {
              while ((b = b[d]))
                if ((1 === b.nodeType || g) && a(b, c, i)) return !0;
            } else
              while ((b = b[d]))
                if (1 === b.nodeType || g)
                  if (
                    ((l = b[u] || (b[u] = {})),
                    (k = l[b.uniqueID] || (l[b.uniqueID] = {})),
                    e && e === b.nodeName.toLowerCase())
                  )
                    b = b[d] || b;
                  else {
                    if ((j = k[f]) && j[0] === w && j[1] === h)
                      return (m[2] = j[2]);
                    if (((k[f] = m), (m[2] = a(b, c, i)))) return !0;
                  }
            return !1;
          };
    }
    function ua(a) {
      return a.length > 1
        ? function (b, c, d) {
            var e = a.length;
            while (e--) if (!a[e](b, c, d)) return !1;
            return !0;
          }
        : a[0];
    }
    function va(a, b, c) {
      for (var d = 0, e = b.length; d < e; d++) ga(a, b[d], c);
      return c;
    }
    function wa(a, b, c, d, e) {
      for (var f, g = [], h = 0, i = a.length, j = null != b; h < i; h++)
        (f = a[h]) && ((c && !c(f, d, e)) || (g.push(f), j && b.push(h)));
      return g;
    }
    function xa(a, b, c, d, e, f) {
      return (
        d && !d[u] && (d = xa(d)),
        e && !e[u] && (e = xa(e, f)),
        ia(function (f, g, h, i) {
          var j,
            k,
            l,
            m = [],
            n = [],
            o = g.length,
            p = f || va(b || "*", h.nodeType ? [h] : h, []),
            q = !a || (!f && b) ? p : wa(p, m, a, h, i),
            r = c ? (e || (f ? a : o || d) ? [] : g) : q;
          if ((c && c(q, r, h, i), d)) {
            (j = wa(r, n)), d(j, [], h, i), (k = j.length);
            while (k--) (l = j[k]) && (r[n[k]] = !(q[n[k]] = l));
          }
          if (f) {
            if (e || a) {
              if (e) {
                (j = []), (k = r.length);
                while (k--) (l = r[k]) && j.push((q[k] = l));
                e(null, (r = []), j, i);
              }
              k = r.length;
              while (k--)
                (l = r[k]) &&
                  (j = e ? I(f, l) : m[k]) > -1 &&
                  (f[j] = !(g[j] = l));
            }
          } else (r = wa(r === g ? r.splice(o, r.length) : r)), e ? e(null, g, r, i) : G.apply(g, r);
        })
      );
    }
    function ya(a) {
      for (
        var b,
          c,
          e,
          f = a.length,
          g = d.relative[a[0].type],
          h = g || d.relative[" "],
          i = g ? 1 : 0,
          k = ta(
            function (a) {
              return a === b;
            },
            h,
            !0
          ),
          l = ta(
            function (a) {
              return I(b, a) > -1;
            },
            h,
            !0
          ),
          m = [
            function (a, c, d) {
              var e =
                (!g && (d || c !== j)) ||
                ((b = c).nodeType ? k(a, c, d) : l(a, c, d));
              return (b = null), e;
            },
          ];
        i < f;
        i++
      )
        if ((c = d.relative[a[i].type])) m = [ta(ua(m), c)];
        else {
          if (((c = d.filter[a[i].type].apply(null, a[i].matches)), c[u])) {
            for (e = ++i; e < f; e++) if (d.relative[a[e].type]) break;
            return xa(
              i > 1 && ua(m),
              i > 1 &&
                sa(
                  a
                    .slice(0, i - 1)
                    .concat({ value: " " === a[i - 2].type ? "*" : "" })
                ).replace(P, "$1"),
              c,
              i < e && ya(a.slice(i, e)),
              e < f && ya((a = a.slice(e))),
              e < f && sa(a)
            );
          }
          m.push(c);
        }
      return ua(m);
    }
    function za(a, b) {
      var c = b.length > 0,
        e = a.length > 0,
        f = function (f, g, h, i, k) {
          var l,
            o,
            q,
            r = 0,
            s = "0",
            t = f && [],
            u = [],
            v = j,
            x = f || (e && d.find.TAG("*", k)),
            y = (w += null == v ? 1 : Math.random() || 0.1),
            z = x.length;
          for (
            k && (j = g === n || g || k);
            s !== z && null != (l = x[s]);
            s++
          ) {
            if (e && l) {
              (o = 0), g || l.ownerDocument === n || (m(l), (h = !p));
              while ((q = a[o++]))
                if (q(l, g || n, h)) {
                  i.push(l);
                  break;
                }
              k && (w = y);
            }
            c && ((l = !q && l) && r--, f && t.push(l));
          }
          if (((r += s), c && s !== r)) {
            o = 0;
            while ((q = b[o++])) q(t, u, g, h);
            if (f) {
              if (r > 0) while (s--) t[s] || u[s] || (u[s] = E.call(i));
              u = wa(u);
            }
            G.apply(i, u),
              k && !f && u.length > 0 && r + b.length > 1 && ga.uniqueSort(i);
          }
          return k && ((w = y), (j = v)), t;
        };
      return c ? ia(f) : f;
    }
    return (
      (h = ga.compile = function (a, b) {
        var c,
          d = [],
          e = [],
          f = A[a + " "];
        if (!f) {
          b || (b = g(a)), (c = b.length);
          while (c--) (f = ya(b[c])), f[u] ? d.push(f) : e.push(f);
          (f = A(a, za(e, d))), (f.selector = a);
        }
        return f;
      }),
      (i = ga.select = function (a, b, c, e) {
        var f,
          i,
          j,
          k,
          l,
          m = "function" == typeof a && a,
          n = !e && g((a = m.selector || a));
        if (((c = c || []), 1 === n.length)) {
          if (
            ((i = n[0] = n[0].slice(0)),
            i.length > 2 &&
              "ID" === (j = i[0]).type &&
              9 === b.nodeType &&
              p &&
              d.relative[i[1].type])
          ) {
            if (
              ((b = (d.find.ID(j.matches[0].replace(_, aa), b) || [])[0]), !b)
            )
              return c;
            m && (b = b.parentNode), (a = a.slice(i.shift().value.length));
          }
          f = V.needsContext.test(a) ? 0 : i.length;
          while (f--) {
            if (((j = i[f]), d.relative[(k = j.type)])) break;
            if (
              (l = d.find[k]) &&
              (e = l(
                j.matches[0].replace(_, aa),
                ($.test(i[0].type) && qa(b.parentNode)) || b
              ))
            ) {
              if ((i.splice(f, 1), (a = e.length && sa(i)), !a))
                return G.apply(c, e), c;
              break;
            }
          }
        }
        return (
          (m || h(a, n))(
            e,
            b,
            !p,
            c,
            !b || ($.test(a) && qa(b.parentNode)) || b
          ),
          c
        );
      }),
      (c.sortStable = u.split("").sort(B).join("") === u),
      (c.detectDuplicates = !!l),
      m(),
      (c.sortDetached = ja(function (a) {
        return 1 & a.compareDocumentPosition(n.createElement("fieldset"));
      })),
      ja(function (a) {
        return (
          (a.innerHTML = "<a href='#'></a>"),
          "#" === a.firstChild.getAttribute("href")
        );
      }) ||
        ka("type|href|height|width", function (a, b, c) {
          if (!c) return a.getAttribute(b, "type" === b.toLowerCase() ? 1 : 2);
        }),
      (c.attributes &&
        ja(function (a) {
          return (
            (a.innerHTML = "<input/>"),
            a.firstChild.setAttribute("value", ""),
            "" === a.firstChild.getAttribute("value")
          );
        })) ||
        ka("value", function (a, b, c) {
          if (!c && "input" === a.nodeName.toLowerCase()) return a.defaultValue;
        }),
      ja(function (a) {
        return null == a.getAttribute("disabled");
      }) ||
        ka(J, function (a, b, c) {
          var d;
          if (!c)
            return a[b] === !0
              ? b.toLowerCase()
              : (d = a.getAttributeNode(b)) && d.specified
              ? d.value
              : null;
        }),
      ga
    );
  })(a);
  (r.find = x),
    (r.expr = x.selectors),
    (r.expr[":"] = r.expr.pseudos),
    (r.uniqueSort = r.unique = x.uniqueSort),
    (r.text = x.getText),
    (r.isXMLDoc = x.isXML),
    (r.contains = x.contains),
    (r.escapeSelector = x.escape);
  var y = function (a, b, c) {
      var d = [],
        e = void 0 !== c;
      while ((a = a[b]) && 9 !== a.nodeType)
        if (1 === a.nodeType) {
          if (e && r(a).is(c)) break;
          d.push(a);
        }
      return d;
    },
    z = function (a, b) {
      for (var c = []; a; a = a.nextSibling)
        1 === a.nodeType && a !== b && c.push(a);
      return c;
    },
    A = r.expr.match.needsContext;
  function B(a, b) {
    return a.nodeName && a.nodeName.toLowerCase() === b.toLowerCase();
  }
  var C = /^<([a-z][^\/\0>:\x20\t\r\n\f]*)[\x20\t\r\n\f]*\/?>(?:<\/\1>|)$/i,
    D = /^.[^:#\[\.,]*$/;
  function E(a, b, c) {
    return r.isFunction(b)
      ? r.grep(a, function (a, d) {
          return !!b.call(a, d, a) !== c;
        })
      : b.nodeType
      ? r.grep(a, function (a) {
          return (a === b) !== c;
        })
      : "string" != typeof b
      ? r.grep(a, function (a) {
          return i.call(b, a) > -1 !== c;
        })
      : D.test(b)
      ? r.filter(b, a, c)
      : ((b = r.filter(b, a)),
        r.grep(a, function (a) {
          return i.call(b, a) > -1 !== c && 1 === a.nodeType;
        }));
  }
  (r.filter = function (a, b, c) {
    var d = b[0];
    return (
      c && (a = ":not(" + a + ")"),
      1 === b.length && 1 === d.nodeType
        ? r.find.matchesSelector(d, a)
          ? [d]
          : []
        : r.find.matches(
            a,
            r.grep(b, function (a) {
              return 1 === a.nodeType;
            })
          )
    );
  }),
    r.fn.extend({
      find: function (a) {
        var b,
          c,
          d = this.length,
          e = this;
        if ("string" != typeof a)
          return this.pushStack(
            r(a).filter(function () {
              for (b = 0; b < d; b++) if (r.contains(e[b], this)) return !0;
            })
          );
        for (c = this.pushStack([]), b = 0; b < d; b++) r.find(a, e[b], c);
        return d > 1 ? r.uniqueSort(c) : c;
      },
      filter: function (a) {
        return this.pushStack(E(this, a || [], !1));
      },
      not: function (a) {
        return this.pushStack(E(this, a || [], !0));
      },
      is: function (a) {
        return !!E(this, "string" == typeof a && A.test(a) ? r(a) : a || [], !1)
          .length;
      },
    });
  var F,
    G = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]+))$/,
    H = (r.fn.init = function (a, b, c) {
      var e, f;
      if (!a) return this;
      if (((c = c || F), "string" == typeof a)) {
        if (
          ((e =
            "<" === a[0] && ">" === a[a.length - 1] && a.length >= 3
              ? [null, a, null]
              : G.exec(a)),
          !e || (!e[1] && b))
        )
          return !b || b.jquery
            ? (b || c).find(a)
            : this.constructor(b).find(a);
        if (e[1]) {
          if (
            ((b = b instanceof r ? b[0] : b),
            r.merge(
              this,
              r.parseHTML(e[1], b && b.nodeType ? b.ownerDocument || b : d, !0)
            ),
            C.test(e[1]) && r.isPlainObject(b))
          )
            for (e in b)
              r.isFunction(this[e]) ? this[e](b[e]) : this.attr(e, b[e]);
          return this;
        }
        return (
          (f = d.getElementById(e[2])),
          f && ((this[0] = f), (this.length = 1)),
          this
        );
      }
      return a.nodeType
        ? ((this[0] = a), (this.length = 1), this)
        : r.isFunction(a)
        ? void 0 !== c.ready
          ? c.ready(a)
          : a(r)
        : r.makeArray(a, this);
    });
  (H.prototype = r.fn), (F = r(d));
  var I = /^(?:parents|prev(?:Until|All))/,
    J = { children: !0, contents: !0, next: !0, prev: !0 };
  r.fn.extend({
    has: function (a) {
      var b = r(a, this),
        c = b.length;
      return this.filter(function () {
        for (var a = 0; a < c; a++) if (r.contains(this, b[a])) return !0;
      });
    },
    closest: function (a, b) {
      var c,
        d = 0,
        e = this.length,
        f = [],
        g = "string" != typeof a && r(a);
      if (!A.test(a))
        for (; d < e; d++)
          for (c = this[d]; c && c !== b; c = c.parentNode)
            if (
              c.nodeType < 11 &&
              (g
                ? g.index(c) > -1
                : 1 === c.nodeType && r.find.matchesSelector(c, a))
            ) {
              f.push(c);
              break;
            }
      return this.pushStack(f.length > 1 ? r.uniqueSort(f) : f);
    },
    index: function (a) {
      return a
        ? "string" == typeof a
          ? i.call(r(a), this[0])
          : i.call(this, a.jquery ? a[0] : a)
        : this[0] && this[0].parentNode
        ? this.first().prevAll().length
        : -1;
    },
    add: function (a, b) {
      return this.pushStack(r.uniqueSort(r.merge(this.get(), r(a, b))));
    },
    addBack: function (a) {
      return this.add(null == a ? this.prevObject : this.prevObject.filter(a));
    },
  });
  function K(a, b) {
    while ((a = a[b]) && 1 !== a.nodeType);
    return a;
  }
  r.each(
    {
      parent: function (a) {
        var b = a.parentNode;
        return b && 11 !== b.nodeType ? b : null;
      },
      parents: function (a) {
        return y(a, "parentNode");
      },
      parentsUntil: function (a, b, c) {
        return y(a, "parentNode", c);
      },
      next: function (a) {
        return K(a, "nextSibling");
      },
      prev: function (a) {
        return K(a, "previousSibling");
      },
      nextAll: function (a) {
        return y(a, "nextSibling");
      },
      prevAll: function (a) {
        return y(a, "previousSibling");
      },
      nextUntil: function (a, b, c) {
        return y(a, "nextSibling", c);
      },
      prevUntil: function (a, b, c) {
        return y(a, "previousSibling", c);
      },
      siblings: function (a) {
        return z((a.parentNode || {}).firstChild, a);
      },
      children: function (a) {
        return z(a.firstChild);
      },
      contents: function (a) {
        return B(a, "iframe")
          ? a.contentDocument
          : (B(a, "template") && (a = a.content || a),
            r.merge([], a.childNodes));
      },
    },
    function (a, b) {
      r.fn[a] = function (c, d) {
        var e = r.map(this, b, c);
        return (
          "Until" !== a.slice(-5) && (d = c),
          d && "string" == typeof d && (e = r.filter(d, e)),
          this.length > 1 &&
            (J[a] || r.uniqueSort(e), I.test(a) && e.reverse()),
          this.pushStack(e)
        );
      };
    }
  );
  var L = /[^\x20\t\r\n\f]+/g;
  function M(a) {
    var b = {};
    return (
      r.each(a.match(L) || [], function (a, c) {
        b[c] = !0;
      }),
      b
    );
  }
  r.Callbacks = function (a) {
    a = "string" == typeof a ? M(a) : r.extend({}, a);
    var b,
      c,
      d,
      e,
      f = [],
      g = [],
      h = -1,
      i = function () {
        for (e = e || a.once, d = b = !0; g.length; h = -1) {
          c = g.shift();
          while (++h < f.length)
            f[h].apply(c[0], c[1]) === !1 &&
              a.stopOnFalse &&
              ((h = f.length), (c = !1));
        }
        a.memory || (c = !1), (b = !1), e && (f = c ? [] : "");
      },
      j = {
        add: function () {
          return (
            f &&
              (c && !b && ((h = f.length - 1), g.push(c)),
              (function d(b) {
                r.each(b, function (b, c) {
                  r.isFunction(c)
                    ? (a.unique && j.has(c)) || f.push(c)
                    : c && c.length && "string" !== r.type(c) && d(c);
                });
              })(arguments),
              c && !b && i()),
            this
          );
        },
        remove: function () {
          return (
            r.each(arguments, function (a, b) {
              var c;
              while ((c = r.inArray(b, f, c)) > -1)
                f.splice(c, 1), c <= h && h--;
            }),
            this
          );
        },
        has: function (a) {
          return a ? r.inArray(a, f) > -1 : f.length > 0;
        },
        empty: function () {
          return f && (f = []), this;
        },
        disable: function () {
          return (e = g = []), (f = c = ""), this;
        },
        disabled: function () {
          return !f;
        },
        lock: function () {
          return (e = g = []), c || b || (f = c = ""), this;
        },
        locked: function () {
          return !!e;
        },
        fireWith: function (a, c) {
          return (
            e ||
              ((c = c || []),
              (c = [a, c.slice ? c.slice() : c]),
              g.push(c),
              b || i()),
            this
          );
        },
        fire: function () {
          return j.fireWith(this, arguments), this;
        },
        fired: function () {
          return !!d;
        },
      };
    return j;
  };
  function N(a) {
    return a;
  }
  function O(a) {
    throw a;
  }
  function P(a, b, c, d) {
    var e;
    try {
      a && r.isFunction((e = a.promise))
        ? e.call(a).done(b).fail(c)
        : a && r.isFunction((e = a.then))
        ? e.call(a, b, c)
        : b.apply(void 0, [a].slice(d));
    } catch (a) {
      c.apply(void 0, [a]);
    }
  }
  r.extend({
    Deferred: function (b) {
      var c = [
          [
            "notify",
            "progress",
            r.Callbacks("memory"),
            r.Callbacks("memory"),
            2,
          ],
          [
            "resolve",
            "done",
            r.Callbacks("once memory"),
            r.Callbacks("once memory"),
            0,
            "resolved",
          ],
          [
            "reject",
            "fail",
            r.Callbacks("once memory"),
            r.Callbacks("once memory"),
            1,
            "rejected",
          ],
        ],
        d = "pending",
        e = {
          state: function () {
            return d;
          },
          always: function () {
            return f.done(arguments).fail(arguments), this;
          },
          catch: function (a) {
            return e.then(null, a);
          },
          pipe: function () {
            var a = arguments;
            return r
              .Deferred(function (b) {
                r.each(c, function (c, d) {
                  var e = r.isFunction(a[d[4]]) && a[d[4]];
                  f[d[1]](function () {
                    var a = e && e.apply(this, arguments);
                    a && r.isFunction(a.promise)
                      ? a
                          .promise()
                          .progress(b.notify)
                          .done(b.resolve)
                          .fail(b.reject)
                      : b[d[0] + "With"](this, e ? [a] : arguments);
                  });
                }),
                  (a = null);
              })
              .promise();
          },
          then: function (b, d, e) {
            var f = 0;
            function g(b, c, d, e) {
              return function () {
                var h = this,
                  i = arguments,
                  j = function () {
                    var a, j;
                    if (!(b < f)) {
                      if (((a = d.apply(h, i)), a === c.promise()))
                        throw new TypeError("Thenable self-resolution");
                      (j =
                        a &&
                        ("object" == typeof a || "function" == typeof a) &&
                        a.then),
                        r.isFunction(j)
                          ? e
                            ? j.call(a, g(f, c, N, e), g(f, c, O, e))
                            : (f++,
                              j.call(
                                a,
                                g(f, c, N, e),
                                g(f, c, O, e),
                                g(f, c, N, c.notifyWith)
                              ))
                          : (d !== N && ((h = void 0), (i = [a])),
                            (e || c.resolveWith)(h, i));
                    }
                  },
                  k = e
                    ? j
                    : function () {
                        try {
                          j();
                        } catch (a) {
                          r.Deferred.exceptionHook &&
                            r.Deferred.exceptionHook(a, k.stackTrace),
                            b + 1 >= f &&
                              (d !== O && ((h = void 0), (i = [a])),
                              c.rejectWith(h, i));
                        }
                      };
                b
                  ? k()
                  : (r.Deferred.getStackHook &&
                      (k.stackTrace = r.Deferred.getStackHook()),
                    a.setTimeout(k));
              };
            }
            return r
              .Deferred(function (a) {
                c[0][3].add(g(0, a, r.isFunction(e) ? e : N, a.notifyWith)),
                  c[1][3].add(g(0, a, r.isFunction(b) ? b : N)),
                  c[2][3].add(g(0, a, r.isFunction(d) ? d : O));
              })
              .promise();
          },
          promise: function (a) {
            return null != a ? r.extend(a, e) : e;
          },
        },
        f = {};
      return (
        r.each(c, function (a, b) {
          var g = b[2],
            h = b[5];
          (e[b[1]] = g.add),
            h &&
              g.add(
                function () {
                  d = h;
                },
                c[3 - a][2].disable,
                c[0][2].lock
              ),
            g.add(b[3].fire),
            (f[b[0]] = function () {
              return (
                f[b[0] + "With"](this === f ? void 0 : this, arguments), this
              );
            }),
            (f[b[0] + "With"] = g.fireWith);
        }),
        e.promise(f),
        b && b.call(f, f),
        f
      );
    },
    when: function (a) {
      var b = arguments.length,
        c = b,
        d = Array(c),
        e = f.call(arguments),
        g = r.Deferred(),
        h = function (a) {
          return function (c) {
            (d[a] = this),
              (e[a] = arguments.length > 1 ? f.call(arguments) : c),
              --b || g.resolveWith(d, e);
          };
        };
      if (
        b <= 1 &&
        (P(a, g.done(h(c)).resolve, g.reject, !b),
        "pending" === g.state() || r.isFunction(e[c] && e[c].then))
      )
        return g.then();
      while (c--) P(e[c], h(c), g.reject);
      return g.promise();
    },
  });
  var Q = /^(Eval|Internal|Range|Reference|Syntax|Type|URI)Error$/;
  (r.Deferred.exceptionHook = function (b, c) {
    a.console &&
      a.console.warn &&
      b &&
      Q.test(b.name) &&
      a.console.warn("jQuery.Deferred exception: " + b.message, b.stack, c);
  }),
    (r.readyException = function (b) {
      a.setTimeout(function () {
        throw b;
      });
    });
  var R = r.Deferred();
  (r.fn.ready = function (a) {
    return (
      R.then(a)["catch"](function (a) {
        r.readyException(a);
      }),
      this
    );
  }),
    r.extend({
      isReady: !1,
      readyWait: 1,
      ready: function (a) {
        (a === !0 ? --r.readyWait : r.isReady) ||
          ((r.isReady = !0),
          (a !== !0 && --r.readyWait > 0) || R.resolveWith(d, [r]));
      },
    }),
    (r.ready.then = R.then);
  function S() {
    d.removeEventListener("DOMContentLoaded", S),
      a.removeEventListener("load", S),
      r.ready();
  }
  "complete" === d.readyState ||
  ("loading" !== d.readyState && !d.documentElement.doScroll)
    ? a.setTimeout(r.ready)
    : (d.addEventListener("DOMContentLoaded", S),
      a.addEventListener("load", S));
  var T = function (a, b, c, d, e, f, g) {
      var h = 0,
        i = a.length,
        j = null == c;
      if ("object" === r.type(c)) {
        e = !0;
        for (h in c) T(a, b, h, c[h], !0, f, g);
      } else if (
        void 0 !== d &&
        ((e = !0),
        r.isFunction(d) || (g = !0),
        j &&
          (g
            ? (b.call(a, d), (b = null))
            : ((j = b),
              (b = function (a, b, c) {
                return j.call(r(a), c);
              }))),
        b)
      )
        for (; h < i; h++) b(a[h], c, g ? d : d.call(a[h], h, b(a[h], c)));
      return e ? a : j ? b.call(a) : i ? b(a[0], c) : f;
    },
    U = function (a) {
      return 1 === a.nodeType || 9 === a.nodeType || !+a.nodeType;
    };
  function V() {
    this.expando = r.expando + V.uid++;
  }
  (V.uid = 1),
    (V.prototype = {
      cache: function (a) {
        var b = a[this.expando];
        return (
          b ||
            ((b = {}),
            U(a) &&
              (a.nodeType
                ? (a[this.expando] = b)
                : Object.defineProperty(a, this.expando, {
                    value: b,
                    configurable: !0,
                  }))),
          b
        );
      },
      set: function (a, b, c) {
        var d,
          e = this.cache(a);
        if ("string" == typeof b) e[r.camelCase(b)] = c;
        else for (d in b) e[r.camelCase(d)] = b[d];
        return e;
      },
      get: function (a, b) {
        return void 0 === b
          ? this.cache(a)
          : a[this.expando] && a[this.expando][r.camelCase(b)];
      },
      access: function (a, b, c) {
        return void 0 === b || (b && "string" == typeof b && void 0 === c)
          ? this.get(a, b)
          : (this.set(a, b, c), void 0 !== c ? c : b);
      },
      remove: function (a, b) {
        var c,
          d = a[this.expando];
        if (void 0 !== d) {
          if (void 0 !== b) {
            Array.isArray(b)
              ? (b = b.map(r.camelCase))
              : ((b = r.camelCase(b)), (b = b in d ? [b] : b.match(L) || [])),
              (c = b.length);
            while (c--) delete d[b[c]];
          }
          (void 0 === b || r.isEmptyObject(d)) &&
            (a.nodeType ? (a[this.expando] = void 0) : delete a[this.expando]);
        }
      },
      hasData: function (a) {
        var b = a[this.expando];
        return void 0 !== b && !r.isEmptyObject(b);
      },
    });
  var W = new V(),
    X = new V(),
    Y = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,
    Z = /[A-Z]/g;
  function $(a) {
    return (
      "true" === a ||
      ("false" !== a &&
        ("null" === a
          ? null
          : a === +a + ""
          ? +a
          : Y.test(a)
          ? JSON.parse(a)
          : a))
    );
  }
  function _(a, b, c) {
    var d;
    if (void 0 === c && 1 === a.nodeType)
      if (
        ((d = "data-" + b.replace(Z, "-$&").toLowerCase()),
        (c = a.getAttribute(d)),
        "string" == typeof c)
      ) {
        try {
          c = $(c);
        } catch (e) {}
        X.set(a, b, c);
      } else c = void 0;
    return c;
  }
  r.extend({
    hasData: function (a) {
      return X.hasData(a) || W.hasData(a);
    },
    data: function (a, b, c) {
      return X.access(a, b, c);
    },
    removeData: function (a, b) {
      X.remove(a, b);
    },
    _data: function (a, b, c) {
      return W.access(a, b, c);
    },
    _removeData: function (a, b) {
      W.remove(a, b);
    },
  }),
    r.fn.extend({
      data: function (a, b) {
        var c,
          d,
          e,
          f = this[0],
          g = f && f.attributes;
        if (void 0 === a) {
          if (
            this.length &&
            ((e = X.get(f)), 1 === f.nodeType && !W.get(f, "hasDataAttrs"))
          ) {
            c = g.length;
            while (c--)
              g[c] &&
                ((d = g[c].name),
                0 === d.indexOf("data-") &&
                  ((d = r.camelCase(d.slice(5))), _(f, d, e[d])));
            W.set(f, "hasDataAttrs", !0);
          }
          return e;
        }
        return "object" == typeof a
          ? this.each(function () {
              X.set(this, a);
            })
          : T(
              this,
              function (b) {
                var c;
                if (f && void 0 === b) {
                  if (((c = X.get(f, a)), void 0 !== c)) return c;
                  if (((c = _(f, a)), void 0 !== c)) return c;
                } else
                  this.each(function () {
                    X.set(this, a, b);
                  });
              },
              null,
              b,
              arguments.length > 1,
              null,
              !0
            );
      },
      removeData: function (a) {
        return this.each(function () {
          X.remove(this, a);
        });
      },
    }),
    r.extend({
      queue: function (a, b, c) {
        var d;
        if (a)
          return (
            (b = (b || "fx") + "queue"),
            (d = W.get(a, b)),
            c &&
              (!d || Array.isArray(c)
                ? (d = W.access(a, b, r.makeArray(c)))
                : d.push(c)),
            d || []
          );
      },
      dequeue: function (a, b) {
        b = b || "fx";
        var c = r.queue(a, b),
          d = c.length,
          e = c.shift(),
          f = r._queueHooks(a, b),
          g = function () {
            r.dequeue(a, b);
          };
        "inprogress" === e && ((e = c.shift()), d--),
          e &&
            ("fx" === b && c.unshift("inprogress"),
            delete f.stop,
            e.call(a, g, f)),
          !d && f && f.empty.fire();
      },
      _queueHooks: function (a, b) {
        var c = b + "queueHooks";
        return (
          W.get(a, c) ||
          W.access(a, c, {
            empty: r.Callbacks("once memory").add(function () {
              W.remove(a, [b + "queue", c]);
            }),
          })
        );
      },
    }),
    r.fn.extend({
      queue: function (a, b) {
        var c = 2;
        return (
          "string" != typeof a && ((b = a), (a = "fx"), c--),
          arguments.length < c
            ? r.queue(this[0], a)
            : void 0 === b
            ? this
            : this.each(function () {
                var c = r.queue(this, a, b);
                r._queueHooks(this, a),
                  "fx" === a && "inprogress" !== c[0] && r.dequeue(this, a);
              })
        );
      },
      dequeue: function (a) {
        return this.each(function () {
          r.dequeue(this, a);
        });
      },
      clearQueue: function (a) {
        return this.queue(a || "fx", []);
      },
      promise: function (a, b) {
        var c,
          d = 1,
          e = r.Deferred(),
          f = this,
          g = this.length,
          h = function () {
            --d || e.resolveWith(f, [f]);
          };
        "string" != typeof a && ((b = a), (a = void 0)), (a = a || "fx");
        while (g--)
          (c = W.get(f[g], a + "queueHooks")),
            c && c.empty && (d++, c.empty.add(h));
        return h(), e.promise(b);
      },
    });
  var aa = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,
    ba = new RegExp("^(?:([+-])=|)(" + aa + ")([a-z%]*)$", "i"),
    ca = ["Top", "Right", "Bottom", "Left"],
    da = function (a, b) {
      return (
        (a = b || a),
        "none" === a.style.display ||
          ("" === a.style.display &&
            r.contains(a.ownerDocument, a) &&
            "none" === r.css(a, "display"))
      );
    },
    ea = function (a, b, c, d) {
      var e,
        f,
        g = {};
      for (f in b) (g[f] = a.style[f]), (a.style[f] = b[f]);
      e = c.apply(a, d || []);
      for (f in b) a.style[f] = g[f];
      return e;
    };
  function fa(a, b, c, d) {
    var e,
      f = 1,
      g = 20,
      h = d
        ? function () {
            return d.cur();
          }
        : function () {
            return r.css(a, b, "");
          },
      i = h(),
      j = (c && c[3]) || (r.cssNumber[b] ? "" : "px"),
      k = (r.cssNumber[b] || ("px" !== j && +i)) && ba.exec(r.css(a, b));
    if (k && k[3] !== j) {
      (j = j || k[3]), (c = c || []), (k = +i || 1);
      do (f = f || ".5"), (k /= f), r.style(a, b, k + j);
      while (f !== (f = h() / i) && 1 !== f && --g);
    }
    return (
      c &&
        ((k = +k || +i || 0),
        (e = c[1] ? k + (c[1] + 1) * c[2] : +c[2]),
        d && ((d.unit = j), (d.start = k), (d.end = e))),
      e
    );
  }
  var ga = {};
  function ha(a) {
    var b,
      c = a.ownerDocument,
      d = a.nodeName,
      e = ga[d];
    return e
      ? e
      : ((b = c.body.appendChild(c.createElement(d))),
        (e = r.css(b, "display")),
        b.parentNode.removeChild(b),
        "none" === e && (e = "block"),
        (ga[d] = e),
        e);
  }
  function ia(a, b) {
    for (var c, d, e = [], f = 0, g = a.length; f < g; f++)
      (d = a[f]),
        d.style &&
          ((c = d.style.display),
          b
            ? ("none" === c &&
                ((e[f] = W.get(d, "display") || null),
                e[f] || (d.style.display = "")),
              "" === d.style.display && da(d) && (e[f] = ha(d)))
            : "none" !== c && ((e[f] = "none"), W.set(d, "display", c)));
    for (f = 0; f < g; f++) null != e[f] && (a[f].style.display = e[f]);
    return a;
  }
  r.fn.extend({
    show: function () {
      return ia(this, !0);
    },
    hide: function () {
      return ia(this);
    },
    toggle: function (a) {
      return "boolean" == typeof a
        ? a
          ? this.show()
          : this.hide()
        : this.each(function () {
            da(this) ? r(this).show() : r(this).hide();
          });
    },
  });
  var ja = /^(?:checkbox|radio)$/i,
    ka = /<([a-z][^\/\0>\x20\t\r\n\f]+)/i,
    la = /^$|\/(?:java|ecma)script/i,
    ma = {
      option: [1, "<select multiple='multiple'>", "</select>"],
      thead: [1, "<table>", "</table>"],
      col: [2, "<table><colgroup>", "</colgroup></table>"],
      tr: [2, "<table><tbody>", "</tbody></table>"],
      td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
      _default: [0, "", ""],
    };
  (ma.optgroup = ma.option),
    (ma.tbody = ma.tfoot = ma.colgroup = ma.caption = ma.thead),
    (ma.th = ma.td);
  function na(a, b) {
    var c;
    return (
      (c =
        "undefined" != typeof a.getElementsByTagName
          ? a.getElementsByTagName(b || "*")
          : "undefined" != typeof a.querySelectorAll
          ? a.querySelectorAll(b || "*")
          : []),
      void 0 === b || (b && B(a, b)) ? r.merge([a], c) : c
    );
  }
  function oa(a, b) {
    for (var c = 0, d = a.length; c < d; c++)
      W.set(a[c], "globalEval", !b || W.get(b[c], "globalEval"));
  }
  var pa = /<|&#?\w+;/;
  function qa(a, b, c, d, e) {
    for (
      var f,
        g,
        h,
        i,
        j,
        k,
        l = b.createDocumentFragment(),
        m = [],
        n = 0,
        o = a.length;
      n < o;
      n++
    )
      if (((f = a[n]), f || 0 === f))
        if ("object" === r.type(f)) r.merge(m, f.nodeType ? [f] : f);
        else if (pa.test(f)) {
          (g = g || l.appendChild(b.createElement("div"))),
            (h = (ka.exec(f) || ["", ""])[1].toLowerCase()),
            (i = ma[h] || ma._default),
            (g.innerHTML = i[1] + r.htmlPrefilter(f) + i[2]),
            (k = i[0]);
          while (k--) g = g.lastChild;
          r.merge(m, g.childNodes), (g = l.firstChild), (g.textContent = "");
        } else m.push(b.createTextNode(f));
    (l.textContent = ""), (n = 0);
    while ((f = m[n++]))
      if (d && r.inArray(f, d) > -1) e && e.push(f);
      else if (
        ((j = r.contains(f.ownerDocument, f)),
        (g = na(l.appendChild(f), "script")),
        j && oa(g),
        c)
      ) {
        k = 0;
        while ((f = g[k++])) la.test(f.type || "") && c.push(f);
      }
    return l;
  }
  !(function () {
    var a = d.createDocumentFragment(),
      b = a.appendChild(d.createElement("div")),
      c = d.createElement("input");
    c.setAttribute("type", "radio"),
      c.setAttribute("checked", "checked"),
      c.setAttribute("name", "t"),
      b.appendChild(c),
      (o.checkClone = b.cloneNode(!0).cloneNode(!0).lastChild.checked),
      (b.innerHTML = "<textarea>x</textarea>"),
      (o.noCloneChecked = !!b.cloneNode(!0).lastChild.defaultValue);
  })();
  var ra = d.documentElement,
    sa = /^key/,
    ta = /^(?:mouse|pointer|contextmenu|drag|drop)|click/,
    ua = /^([^.]*)(?:\.(.+)|)/;
  function va() {
    return !0;
  }
  function wa() {
    return !1;
  }
  function xa() {
    try {
      return d.activeElement;
    } catch (a) {}
  }
  function ya(a, b, c, d, e, f) {
    var g, h;
    if ("object" == typeof b) {
      "string" != typeof c && ((d = d || c), (c = void 0));
      for (h in b) ya(a, h, c, d, b[h], f);
      return a;
    }
    if (
      (null == d && null == e
        ? ((e = c), (d = c = void 0))
        : null == e &&
          ("string" == typeof c
            ? ((e = d), (d = void 0))
            : ((e = d), (d = c), (c = void 0))),
      e === !1)
    )
      e = wa;
    else if (!e) return a;
    return (
      1 === f &&
        ((g = e),
        (e = function (a) {
          return r().off(a), g.apply(this, arguments);
        }),
        (e.guid = g.guid || (g.guid = r.guid++))),
      a.each(function () {
        r.event.add(this, b, e, d, c);
      })
    );
  }
  (r.event = {
    global: {},
    add: function (a, b, c, d, e) {
      var f,
        g,
        h,
        i,
        j,
        k,
        l,
        m,
        n,
        o,
        p,
        q = W.get(a);
      if (q) {
        c.handler && ((f = c), (c = f.handler), (e = f.selector)),
          e && r.find.matchesSelector(ra, e),
          c.guid || (c.guid = r.guid++),
          (i = q.events) || (i = q.events = {}),
          (g = q.handle) ||
            (g = q.handle = function (b) {
              return "undefined" != typeof r && r.event.triggered !== b.type
                ? r.event.dispatch.apply(a, arguments)
                : void 0;
            }),
          (b = (b || "").match(L) || [""]),
          (j = b.length);
        while (j--)
          (h = ua.exec(b[j]) || []),
            (n = p = h[1]),
            (o = (h[2] || "").split(".").sort()),
            n &&
              ((l = r.event.special[n] || {}),
              (n = (e ? l.delegateType : l.bindType) || n),
              (l = r.event.special[n] || {}),
              (k = r.extend(
                {
                  type: n,
                  origType: p,
                  data: d,
                  handler: c,
                  guid: c.guid,
                  selector: e,
                  needsContext: e && r.expr.match.needsContext.test(e),
                  namespace: o.join("."),
                },
                f
              )),
              (m = i[n]) ||
                ((m = i[n] = []),
                (m.delegateCount = 0),
                (l.setup && l.setup.call(a, d, o, g) !== !1) ||
                  (a.addEventListener && a.addEventListener(n, g))),
              l.add &&
                (l.add.call(a, k), k.handler.guid || (k.handler.guid = c.guid)),
              e ? m.splice(m.delegateCount++, 0, k) : m.push(k),
              (r.event.global[n] = !0));
      }
    },
    remove: function (a, b, c, d, e) {
      var f,
        g,
        h,
        i,
        j,
        k,
        l,
        m,
        n,
        o,
        p,
        q = W.hasData(a) && W.get(a);
      if (q && (i = q.events)) {
        (b = (b || "").match(L) || [""]), (j = b.length);
        while (j--)
          if (
            ((h = ua.exec(b[j]) || []),
            (n = p = h[1]),
            (o = (h[2] || "").split(".").sort()),
            n)
          ) {
            (l = r.event.special[n] || {}),
              (n = (d ? l.delegateType : l.bindType) || n),
              (m = i[n] || []),
              (h =
                h[2] &&
                new RegExp("(^|\\.)" + o.join("\\.(?:.*\\.|)") + "(\\.|$)")),
              (g = f = m.length);
            while (f--)
              (k = m[f]),
                (!e && p !== k.origType) ||
                  (c && c.guid !== k.guid) ||
                  (h && !h.test(k.namespace)) ||
                  (d && d !== k.selector && ("**" !== d || !k.selector)) ||
                  (m.splice(f, 1),
                  k.selector && m.delegateCount--,
                  l.remove && l.remove.call(a, k));
            g &&
              !m.length &&
              ((l.teardown && l.teardown.call(a, o, q.handle) !== !1) ||
                r.removeEvent(a, n, q.handle),
              delete i[n]);
          } else for (n in i) r.event.remove(a, n + b[j], c, d, !0);
        r.isEmptyObject(i) && W.remove(a, "handle events");
      }
    },
    dispatch: function (a) {
      var b = r.event.fix(a),
        c,
        d,
        e,
        f,
        g,
        h,
        i = new Array(arguments.length),
        j = (W.get(this, "events") || {})[b.type] || [],
        k = r.event.special[b.type] || {};
      for (i[0] = b, c = 1; c < arguments.length; c++) i[c] = arguments[c];
      if (
        ((b.delegateTarget = this),
        !k.preDispatch || k.preDispatch.call(this, b) !== !1)
      ) {
        (h = r.event.handlers.call(this, b, j)), (c = 0);
        while ((f = h[c++]) && !b.isPropagationStopped()) {
          (b.currentTarget = f.elem), (d = 0);
          while ((g = f.handlers[d++]) && !b.isImmediatePropagationStopped())
            (b.rnamespace && !b.rnamespace.test(g.namespace)) ||
              ((b.handleObj = g),
              (b.data = g.data),
              (e = (
                (r.event.special[g.origType] || {}).handle || g.handler
              ).apply(f.elem, i)),
              void 0 !== e &&
                (b.result = e) === !1 &&
                (b.preventDefault(), b.stopPropagation()));
        }
        return k.postDispatch && k.postDispatch.call(this, b), b.result;
      }
    },
    handlers: function (a, b) {
      var c,
        d,
        e,
        f,
        g,
        h = [],
        i = b.delegateCount,
        j = a.target;
      if (i && j.nodeType && !("click" === a.type && a.button >= 1))
        for (; j !== this; j = j.parentNode || this)
          if (1 === j.nodeType && ("click" !== a.type || j.disabled !== !0)) {
            for (f = [], g = {}, c = 0; c < i; c++)
              (d = b[c]),
                (e = d.selector + " "),
                void 0 === g[e] &&
                  (g[e] = d.needsContext
                    ? r(e, this).index(j) > -1
                    : r.find(e, this, null, [j]).length),
                g[e] && f.push(d);
            f.length && h.push({ elem: j, handlers: f });
          }
      return (
        (j = this), i < b.length && h.push({ elem: j, handlers: b.slice(i) }), h
      );
    },
    addProp: function (a, b) {
      Object.defineProperty(r.Event.prototype, a, {
        enumerable: !0,
        configurable: !0,
        get: r.isFunction(b)
          ? function () {
              if (this.originalEvent) return b(this.originalEvent);
            }
          : function () {
              if (this.originalEvent) return this.originalEvent[a];
            },
        set: function (b) {
          Object.defineProperty(this, a, {
            enumerable: !0,
            configurable: !0,
            writable: !0,
            value: b,
          });
        },
      });
    },
    fix: function (a) {
      return a[r.expando] ? a : new r.Event(a);
    },
    special: {
      load: { noBubble: !0 },
      focus: {
        trigger: function () {
          if (this !== xa() && this.focus) return this.focus(), !1;
        },
        delegateType: "focusin",
      },
      blur: {
        trigger: function () {
          if (this === xa() && this.blur) return this.blur(), !1;
        },
        delegateType: "focusout",
      },
      click: {
        trigger: function () {
          if (ja.test(this.type) && this.click && B(this, "input"))
            return this.click(), !1;
        },
        _default: function (a) {
          return B(a.target, "a");
        },
      },
      beforeunload: {
        postDispatch: function (a) {
          void 0 !== a.result &&
            a.originalEvent &&
            (a.originalEvent.returnValue = a.result);
        },
      },
    },
  }),
    (r.removeEvent = function (a, b, c) {
      a.removeEventListener && a.removeEventListener(b, c);
    }),
    (r.Event = function (a, b) {
      return this instanceof r.Event
        ? (a && a.type
            ? ((this.originalEvent = a),
              (this.type = a.type),
              (this.isDefaultPrevented =
                a.defaultPrevented ||
                (void 0 === a.defaultPrevented && a.returnValue === !1)
                  ? va
                  : wa),
              (this.target =
                a.target && 3 === a.target.nodeType
                  ? a.target.parentNode
                  : a.target),
              (this.currentTarget = a.currentTarget),
              (this.relatedTarget = a.relatedTarget))
            : (this.type = a),
          b && r.extend(this, b),
          (this.timeStamp = (a && a.timeStamp) || r.now()),
          void (this[r.expando] = !0))
        : new r.Event(a, b);
    }),
    (r.Event.prototype = {
      constructor: r.Event,
      isDefaultPrevented: wa,
      isPropagationStopped: wa,
      isImmediatePropagationStopped: wa,
      isSimulated: !1,
      preventDefault: function () {
        var a = this.originalEvent;
        (this.isDefaultPrevented = va),
          a && !this.isSimulated && a.preventDefault();
      },
      stopPropagation: function () {
        var a = this.originalEvent;
        (this.isPropagationStopped = va),
          a && !this.isSimulated && a.stopPropagation();
      },
      stopImmediatePropagation: function () {
        var a = this.originalEvent;
        (this.isImmediatePropagationStopped = va),
          a && !this.isSimulated && a.stopImmediatePropagation(),
          this.stopPropagation();
      },
    }),
    r.each(
      {
        altKey: !0,
        bubbles: !0,
        cancelable: !0,
        changedTouches: !0,
        ctrlKey: !0,
        detail: !0,
        eventPhase: !0,
        metaKey: !0,
        pageX: !0,
        pageY: !0,
        shiftKey: !0,
        view: !0,
        char: !0,
        charCode: !0,
        key: !0,
        keyCode: !0,
        button: !0,
        buttons: !0,
        clientX: !0,
        clientY: !0,
        offsetX: !0,
        offsetY: !0,
        pointerId: !0,
        pointerType: !0,
        screenX: !0,
        screenY: !0,
        targetTouches: !0,
        toElement: !0,
        touches: !0,
        which: function (a) {
          var b = a.button;
          return null == a.which && sa.test(a.type)
            ? null != a.charCode
              ? a.charCode
              : a.keyCode
            : !a.which && void 0 !== b && ta.test(a.type)
            ? 1 & b
              ? 1
              : 2 & b
              ? 3
              : 4 & b
              ? 2
              : 0
            : a.which;
        },
      },
      r.event.addProp
    ),
    r.each(
      {
        mouseenter: "mouseover",
        mouseleave: "mouseout",
        pointerenter: "pointerover",
        pointerleave: "pointerout",
      },
      function (a, b) {
        r.event.special[a] = {
          delegateType: b,
          bindType: b,
          handle: function (a) {
            var c,
              d = this,
              e = a.relatedTarget,
              f = a.handleObj;
            return (
              (e && (e === d || r.contains(d, e))) ||
                ((a.type = f.origType),
                (c = f.handler.apply(this, arguments)),
                (a.type = b)),
              c
            );
          },
        };
      }
    ),
    r.fn.extend({
      on: function (a, b, c, d) {
        return ya(this, a, b, c, d);
      },
      one: function (a, b, c, d) {
        return ya(this, a, b, c, d, 1);
      },
      off: function (a, b, c) {
        var d, e;
        if (a && a.preventDefault && a.handleObj)
          return (
            (d = a.handleObj),
            r(a.delegateTarget).off(
              d.namespace ? d.origType + "." + d.namespace : d.origType,
              d.selector,
              d.handler
            ),
            this
          );
        if ("object" == typeof a) {
          for (e in a) this.off(e, b, a[e]);
          return this;
        }
        return (
          (b !== !1 && "function" != typeof b) || ((c = b), (b = void 0)),
          c === !1 && (c = wa),
          this.each(function () {
            r.event.remove(this, a, c, b);
          })
        );
      },
    });
  var za = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([a-z][^\/\0>\x20\t\r\n\f]*)[^>]*)\/>/gi,
    Aa = /<script|<style|<link/i,
    Ba = /checked\s*(?:[^=]|=\s*.checked.)/i,
    Ca = /^true\/(.*)/,
    Da = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g;
  function Ea(a, b) {
    return B(a, "table") && B(11 !== b.nodeType ? b : b.firstChild, "tr")
      ? r(">tbody", a)[0] || a
      : a;
  }
  function Fa(a) {
    return (a.type = (null !== a.getAttribute("type")) + "/" + a.type), a;
  }
  function Ga(a) {
    var b = Ca.exec(a.type);
    return b ? (a.type = b[1]) : a.removeAttribute("type"), a;
  }
  function Ha(a, b) {
    var c, d, e, f, g, h, i, j;
    if (1 === b.nodeType) {
      if (
        W.hasData(a) &&
        ((f = W.access(a)), (g = W.set(b, f)), (j = f.events))
      ) {
        delete g.handle, (g.events = {});
        for (e in j)
          for (c = 0, d = j[e].length; c < d; c++) r.event.add(b, e, j[e][c]);
      }
      X.hasData(a) && ((h = X.access(a)), (i = r.extend({}, h)), X.set(b, i));
    }
  }
  function Ia(a, b) {
    var c = b.nodeName.toLowerCase();
    "input" === c && ja.test(a.type)
      ? (b.checked = a.checked)
      : ("input" !== c && "textarea" !== c) ||
        (b.defaultValue = a.defaultValue);
  }
  function Ja(a, b, c, d) {
    b = g.apply([], b);
    var e,
      f,
      h,
      i,
      j,
      k,
      l = 0,
      m = a.length,
      n = m - 1,
      q = b[0],
      s = r.isFunction(q);
    if (s || (m > 1 && "string" == typeof q && !o.checkClone && Ba.test(q)))
      return a.each(function (e) {
        var f = a.eq(e);
        s && (b[0] = q.call(this, e, f.html())), Ja(f, b, c, d);
      });
    if (
      m &&
      ((e = qa(b, a[0].ownerDocument, !1, a, d)),
      (f = e.firstChild),
      1 === e.childNodes.length && (e = f),
      f || d)
    ) {
      for (h = r.map(na(e, "script"), Fa), i = h.length; l < m; l++)
        (j = e),
          l !== n &&
            ((j = r.clone(j, !0, !0)), i && r.merge(h, na(j, "script"))),
          c.call(a[l], j, l);
      if (i)
        for (k = h[h.length - 1].ownerDocument, r.map(h, Ga), l = 0; l < i; l++)
          (j = h[l]),
            la.test(j.type || "") &&
              !W.access(j, "globalEval") &&
              r.contains(k, j) &&
              (j.src
                ? r._evalUrl && r._evalUrl(j.src)
                : p(j.textContent.replace(Da, ""), k));
    }
    return a;
  }
  function Ka(a, b, c) {
    for (var d, e = b ? r.filter(b, a) : a, f = 0; null != (d = e[f]); f++)
      c || 1 !== d.nodeType || r.cleanData(na(d)),
        d.parentNode &&
          (c && r.contains(d.ownerDocument, d) && oa(na(d, "script")),
          d.parentNode.removeChild(d));
    return a;
  }
  r.extend({
    htmlPrefilter: function (a) {
      return a.replace(za, "<$1></$2>");
    },
    clone: function (a, b, c) {
      var d,
        e,
        f,
        g,
        h = a.cloneNode(!0),
        i = r.contains(a.ownerDocument, a);
      if (
        !(
          o.noCloneChecked ||
          (1 !== a.nodeType && 11 !== a.nodeType) ||
          r.isXMLDoc(a)
        )
      )
        for (g = na(h), f = na(a), d = 0, e = f.length; d < e; d++)
          Ia(f[d], g[d]);
      if (b)
        if (c)
          for (f = f || na(a), g = g || na(h), d = 0, e = f.length; d < e; d++)
            Ha(f[d], g[d]);
        else Ha(a, h);
      return (
        (g = na(h, "script")), g.length > 0 && oa(g, !i && na(a, "script")), h
      );
    },
    cleanData: function (a) {
      for (var b, c, d, e = r.event.special, f = 0; void 0 !== (c = a[f]); f++)
        if (U(c)) {
          if ((b = c[W.expando])) {
            if (b.events)
              for (d in b.events)
                e[d] ? r.event.remove(c, d) : r.removeEvent(c, d, b.handle);
            c[W.expando] = void 0;
          }
          c[X.expando] && (c[X.expando] = void 0);
        }
    },
  }),
    r.fn.extend({
      detach: function (a) {
        return Ka(this, a, !0);
      },
      remove: function (a) {
        return Ka(this, a);
      },
      text: function (a) {
        return T(
          this,
          function (a) {
            return void 0 === a
              ? r.text(this)
              : this.empty().each(function () {
                  (1 !== this.nodeType &&
                    11 !== this.nodeType &&
                    9 !== this.nodeType) ||
                    (this.textContent = a);
                });
          },
          null,
          a,
          arguments.length
        );
      },
      append: function () {
        return Ja(this, arguments, function (a) {
          if (
            1 === this.nodeType ||
            11 === this.nodeType ||
            9 === this.nodeType
          ) {
            var b = Ea(this, a);
            b.appendChild(a);
          }
        });
      },
      prepend: function () {
        return Ja(this, arguments, function (a) {
          if (
            1 === this.nodeType ||
            11 === this.nodeType ||
            9 === this.nodeType
          ) {
            var b = Ea(this, a);
            b.insertBefore(a, b.firstChild);
          }
        });
      },
      before: function () {
        return Ja(this, arguments, function (a) {
          this.parentNode && this.parentNode.insertBefore(a, this);
        });
      },
      after: function () {
        return Ja(this, arguments, function (a) {
          this.parentNode && this.parentNode.insertBefore(a, this.nextSibling);
        });
      },
      empty: function () {
        for (var a, b = 0; null != (a = this[b]); b++)
          1 === a.nodeType && (r.cleanData(na(a, !1)), (a.textContent = ""));
        return this;
      },
      clone: function (a, b) {
        return (
          (a = null != a && a),
          (b = null == b ? a : b),
          this.map(function () {
            return r.clone(this, a, b);
          })
        );
      },
      html: function (a) {
        return T(
          this,
          function (a) {
            var b = this[0] || {},
              c = 0,
              d = this.length;
            if (void 0 === a && 1 === b.nodeType) return b.innerHTML;
            if (
              "string" == typeof a &&
              !Aa.test(a) &&
              !ma[(ka.exec(a) || ["", ""])[1].toLowerCase()]
            ) {
              a = r.htmlPrefilter(a);
              try {
                for (; c < d; c++)
                  (b = this[c] || {}),
                    1 === b.nodeType &&
                      (r.cleanData(na(b, !1)), (b.innerHTML = a));
                b = 0;
              } catch (e) {}
            }
            b && this.empty().append(a);
          },
          null,
          a,
          arguments.length
        );
      },
      replaceWith: function () {
        var a = [];
        return Ja(
          this,
          arguments,
          function (b) {
            var c = this.parentNode;
            r.inArray(this, a) < 0 &&
              (r.cleanData(na(this)), c && c.replaceChild(b, this));
          },
          a
        );
      },
    }),
    r.each(
      {
        appendTo: "append",
        prependTo: "prepend",
        insertBefore: "before",
        insertAfter: "after",
        replaceAll: "replaceWith",
      },
      function (a, b) {
        r.fn[a] = function (a) {
          for (var c, d = [], e = r(a), f = e.length - 1, g = 0; g <= f; g++)
            (c = g === f ? this : this.clone(!0)),
              r(e[g])[b](c),
              h.apply(d, c.get());
          return this.pushStack(d);
        };
      }
    );
  var La = /^margin/,
    Ma = new RegExp("^(" + aa + ")(?!px)[a-z%]+$", "i"),
    Na = function (b) {
      var c = b.ownerDocument.defaultView;
      return (c && c.opener) || (c = a), c.getComputedStyle(b);
    };
  !(function () {
    function b() {
      if (i) {
        (i.style.cssText =
          "box-sizing:border-box;position:relative;display:block;margin:auto;border:1px;padding:1px;top:1%;width:50%"),
          (i.innerHTML = ""),
          ra.appendChild(h);
        var b = a.getComputedStyle(i);
        (c = "1%" !== b.top),
          (g = "2px" === b.marginLeft),
          (e = "4px" === b.width),
          (i.style.marginRight = "50%"),
          (f = "4px" === b.marginRight),
          ra.removeChild(h),
          (i = null);
      }
    }
    var c,
      e,
      f,
      g,
      h = d.createElement("div"),
      i = d.createElement("div");
    i.style &&
      ((i.style.backgroundClip = "content-box"),
      (i.cloneNode(!0).style.backgroundClip = ""),
      (o.clearCloneStyle = "content-box" === i.style.backgroundClip),
      (h.style.cssText =
        "border:0;width:8px;height:0;top:0;left:-9999px;padding:0;margin-top:1px;position:absolute"),
      h.appendChild(i),
      r.extend(o, {
        pixelPosition: function () {
          return b(), c;
        },
        boxSizingReliable: function () {
          return b(), e;
        },
        pixelMarginRight: function () {
          return b(), f;
        },
        reliableMarginLeft: function () {
          return b(), g;
        },
      }));
  })();
  function Oa(a, b, c) {
    var d,
      e,
      f,
      g,
      h = a.style;
    return (
      (c = c || Na(a)),
      c &&
        ((g = c.getPropertyValue(b) || c[b]),
        "" !== g || r.contains(a.ownerDocument, a) || (g = r.style(a, b)),
        !o.pixelMarginRight() &&
          Ma.test(g) &&
          La.test(b) &&
          ((d = h.width),
          (e = h.minWidth),
          (f = h.maxWidth),
          (h.minWidth = h.maxWidth = h.width = g),
          (g = c.width),
          (h.width = d),
          (h.minWidth = e),
          (h.maxWidth = f))),
      void 0 !== g ? g + "" : g
    );
  }
  function Pa(a, b) {
    return {
      get: function () {
        return a()
          ? void delete this.get
          : (this.get = b).apply(this, arguments);
      },
    };
  }
  var Qa = /^(none|table(?!-c[ea]).+)/,
    Ra = /^--/,
    Sa = { position: "absolute", visibility: "hidden", display: "block" },
    Ta = { letterSpacing: "0", fontWeight: "400" },
    Ua = ["Webkit", "Moz", "ms"],
    Va = d.createElement("div").style;
  function Wa(a) {
    if (a in Va) return a;
    var b = a[0].toUpperCase() + a.slice(1),
      c = Ua.length;
    while (c--) if (((a = Ua[c] + b), a in Va)) return a;
  }
  function Xa(a) {
    var b = r.cssProps[a];
    return b || (b = r.cssProps[a] = Wa(a) || a), b;
  }
  function Ya(a, b, c) {
    var d = ba.exec(b);
    return d ? Math.max(0, d[2] - (c || 0)) + (d[3] || "px") : b;
  }
  function Za(a, b, c, d, e) {
    var f,
      g = 0;
    for (
      f = c === (d ? "border" : "content") ? 4 : "width" === b ? 1 : 0;
      f < 4;
      f += 2
    )
      "margin" === c && (g += r.css(a, c + ca[f], !0, e)),
        d
          ? ("content" === c && (g -= r.css(a, "padding" + ca[f], !0, e)),
            "margin" !== c &&
              (g -= r.css(a, "border" + ca[f] + "Width", !0, e)))
          : ((g += r.css(a, "padding" + ca[f], !0, e)),
            "padding" !== c &&
              (g += r.css(a, "border" + ca[f] + "Width", !0, e)));
    return g;
  }
  function $a(a, b, c) {
    var d,
      e = Na(a),
      f = Oa(a, b, e),
      g = "border-box" === r.css(a, "boxSizing", !1, e);
    return Ma.test(f)
      ? f
      : ((d = g && (o.boxSizingReliable() || f === a.style[b])),
        (f = parseFloat(f) || 0),
        f + Za(a, b, c || (g ? "border" : "content"), d, e) + "px");
  }
  r.extend({
    cssHooks: {
      opacity: {
        get: function (a, b) {
          if (b) {
            var c = Oa(a, "opacity");
            return "" === c ? "1" : c;
          }
        },
      },
    },
    cssNumber: {
      animationIterationCount: !0,
      columnCount: !0,
      fillOpacity: !0,
      flexGrow: !0,
      flexShrink: !0,
      fontWeight: !0,
      lineHeight: !0,
      opacity: !0,
      order: !0,
      orphans: !0,
      widows: !0,
      zIndex: !0,
      zoom: !0,
    },
    cssProps: { float: "cssFloat" },
    style: function (a, b, c, d) {
      if (a && 3 !== a.nodeType && 8 !== a.nodeType && a.style) {
        var e,
          f,
          g,
          h = r.camelCase(b),
          i = Ra.test(b),
          j = a.style;
        return (
          i || (b = Xa(h)),
          (g = r.cssHooks[b] || r.cssHooks[h]),
          void 0 === c
            ? g && "get" in g && void 0 !== (e = g.get(a, !1, d))
              ? e
              : j[b]
            : ((f = typeof c),
              "string" === f &&
                (e = ba.exec(c)) &&
                e[1] &&
                ((c = fa(a, b, e)), (f = "number")),
              null != c &&
                c === c &&
                ("number" === f &&
                  (c += (e && e[3]) || (r.cssNumber[h] ? "" : "px")),
                o.clearCloneStyle ||
                  "" !== c ||
                  0 !== b.indexOf("background") ||
                  (j[b] = "inherit"),
                (g && "set" in g && void 0 === (c = g.set(a, c, d))) ||
                  (i ? j.setProperty(b, c) : (j[b] = c))),
              void 0)
        );
      }
    },
    css: function (a, b, c, d) {
      var e,
        f,
        g,
        h = r.camelCase(b),
        i = Ra.test(b);
      return (
        i || (b = Xa(h)),
        (g = r.cssHooks[b] || r.cssHooks[h]),
        g && "get" in g && (e = g.get(a, !0, c)),
        void 0 === e && (e = Oa(a, b, d)),
        "normal" === e && b in Ta && (e = Ta[b]),
        "" === c || c
          ? ((f = parseFloat(e)), c === !0 || isFinite(f) ? f || 0 : e)
          : e
      );
    },
  }),
    r.each(["height", "width"], function (a, b) {
      r.cssHooks[b] = {
        get: function (a, c, d) {
          if (c)
            return !Qa.test(r.css(a, "display")) ||
              (a.getClientRects().length && a.getBoundingClientRect().width)
              ? $a(a, b, d)
              : ea(a, Sa, function () {
                  return $a(a, b, d);
                });
        },
        set: function (a, c, d) {
          var e,
            f = d && Na(a),
            g =
              d &&
              Za(a, b, d, "border-box" === r.css(a, "boxSizing", !1, f), f);
          return (
            g &&
              (e = ba.exec(c)) &&
              "px" !== (e[3] || "px") &&
              ((a.style[b] = c), (c = r.css(a, b))),
            Ya(a, c, g)
          );
        },
      };
    }),
    (r.cssHooks.marginLeft = Pa(o.reliableMarginLeft, function (a, b) {
      if (b)
        return (
          (parseFloat(Oa(a, "marginLeft")) ||
            a.getBoundingClientRect().left -
              ea(a, { marginLeft: 0 }, function () {
                return a.getBoundingClientRect().left;
              })) + "px"
        );
    })),
    r.each({ margin: "", padding: "", border: "Width" }, function (a, b) {
      (r.cssHooks[a + b] = {
        expand: function (c) {
          for (
            var d = 0, e = {}, f = "string" == typeof c ? c.split(" ") : [c];
            d < 4;
            d++
          )
            e[a + ca[d] + b] = f[d] || f[d - 2] || f[0];
          return e;
        },
      }),
        La.test(a) || (r.cssHooks[a + b].set = Ya);
    }),
    r.fn.extend({
      css: function (a, b) {
        return T(
          this,
          function (a, b, c) {
            var d,
              e,
              f = {},
              g = 0;
            if (Array.isArray(b)) {
              for (d = Na(a), e = b.length; g < e; g++)
                f[b[g]] = r.css(a, b[g], !1, d);
              return f;
            }
            return void 0 !== c ? r.style(a, b, c) : r.css(a, b);
          },
          a,
          b,
          arguments.length > 1
        );
      },
    });
  function _a(a, b, c, d, e) {
    return new _a.prototype.init(a, b, c, d, e);
  }
  (r.Tween = _a),
    (_a.prototype = {
      constructor: _a,
      init: function (a, b, c, d, e, f) {
        (this.elem = a),
          (this.prop = c),
          (this.easing = e || r.easing._default),
          (this.options = b),
          (this.start = this.now = this.cur()),
          (this.end = d),
          (this.unit = f || (r.cssNumber[c] ? "" : "px"));
      },
      cur: function () {
        var a = _a.propHooks[this.prop];
        return a && a.get ? a.get(this) : _a.propHooks._default.get(this);
      },
      run: function (a) {
        var b,
          c = _a.propHooks[this.prop];
        return (
          this.options.duration
            ? (this.pos = b = r.easing[this.easing](
                a,
                this.options.duration * a,
                0,
                1,
                this.options.duration
              ))
            : (this.pos = b = a),
          (this.now = (this.end - this.start) * b + this.start),
          this.options.step &&
            this.options.step.call(this.elem, this.now, this),
          c && c.set ? c.set(this) : _a.propHooks._default.set(this),
          this
        );
      },
    }),
    (_a.prototype.init.prototype = _a.prototype),
    (_a.propHooks = {
      _default: {
        get: function (a) {
          var b;
          return 1 !== a.elem.nodeType ||
            (null != a.elem[a.prop] && null == a.elem.style[a.prop])
            ? a.elem[a.prop]
            : ((b = r.css(a.elem, a.prop, "")), b && "auto" !== b ? b : 0);
        },
        set: function (a) {
          r.fx.step[a.prop]
            ? r.fx.step[a.prop](a)
            : 1 !== a.elem.nodeType ||
              (null == a.elem.style[r.cssProps[a.prop]] && !r.cssHooks[a.prop])
            ? (a.elem[a.prop] = a.now)
            : r.style(a.elem, a.prop, a.now + a.unit);
        },
      },
    }),
    (_a.propHooks.scrollTop = _a.propHooks.scrollLeft = {
      set: function (a) {
        a.elem.nodeType && a.elem.parentNode && (a.elem[a.prop] = a.now);
      },
    }),
    (r.easing = {
      linear: function (a) {
        return a;
      },
      swing: function (a) {
        return 0.5 - Math.cos(a * Math.PI) / 2;
      },
      _default: "swing",
    }),
    (r.fx = _a.prototype.init),
    (r.fx.step = {});
  var ab,
    bb,
    cb = /^(?:toggle|show|hide)$/,
    db = /queueHooks$/;
  function eb() {
    bb &&
      (d.hidden === !1 && a.requestAnimationFrame
        ? a.requestAnimationFrame(eb)
        : a.setTimeout(eb, r.fx.interval),
      r.fx.tick());
  }
  function fb() {
    return (
      a.setTimeout(function () {
        ab = void 0;
      }),
      (ab = r.now())
    );
  }
  function gb(a, b) {
    var c,
      d = 0,
      e = { height: a };
    for (b = b ? 1 : 0; d < 4; d += 2 - b)
      (c = ca[d]), (e["margin" + c] = e["padding" + c] = a);
    return b && (e.opacity = e.width = a), e;
  }
  function hb(a, b, c) {
    for (
      var d,
        e = (kb.tweeners[b] || []).concat(kb.tweeners["*"]),
        f = 0,
        g = e.length;
      f < g;
      f++
    )
      if ((d = e[f].call(c, b, a))) return d;
  }
  function ib(a, b, c) {
    var d,
      e,
      f,
      g,
      h,
      i,
      j,
      k,
      l = "width" in b || "height" in b,
      m = this,
      n = {},
      o = a.style,
      p = a.nodeType && da(a),
      q = W.get(a, "fxshow");
    c.queue ||
      ((g = r._queueHooks(a, "fx")),
      null == g.unqueued &&
        ((g.unqueued = 0),
        (h = g.empty.fire),
        (g.empty.fire = function () {
          g.unqueued || h();
        })),
      g.unqueued++,
      m.always(function () {
        m.always(function () {
          g.unqueued--, r.queue(a, "fx").length || g.empty.fire();
        });
      }));
    for (d in b)
      if (((e = b[d]), cb.test(e))) {
        if (
          (delete b[d], (f = f || "toggle" === e), e === (p ? "hide" : "show"))
        ) {
          if ("show" !== e || !q || void 0 === q[d]) continue;
          p = !0;
        }
        n[d] = (q && q[d]) || r.style(a, d);
      }
    if (((i = !r.isEmptyObject(b)), i || !r.isEmptyObject(n))) {
      l &&
        1 === a.nodeType &&
        ((c.overflow = [o.overflow, o.overflowX, o.overflowY]),
        (j = q && q.display),
        null == j && (j = W.get(a, "display")),
        (k = r.css(a, "display")),
        "none" === k &&
          (j
            ? (k = j)
            : (ia([a], !0),
              (j = a.style.display || j),
              (k = r.css(a, "display")),
              ia([a]))),
        ("inline" === k || ("inline-block" === k && null != j)) &&
          "none" === r.css(a, "float") &&
          (i ||
            (m.done(function () {
              o.display = j;
            }),
            null == j && ((k = o.display), (j = "none" === k ? "" : k))),
          (o.display = "inline-block"))),
        c.overflow &&
          ((o.overflow = "hidden"),
          m.always(function () {
            (o.overflow = c.overflow[0]),
              (o.overflowX = c.overflow[1]),
              (o.overflowY = c.overflow[2]);
          })),
        (i = !1);
      for (d in n)
        i ||
          (q
            ? "hidden" in q && (p = q.hidden)
            : (q = W.access(a, "fxshow", { display: j })),
          f && (q.hidden = !p),
          p && ia([a], !0),
          m.done(function () {
            p || ia([a]), W.remove(a, "fxshow");
            for (d in n) r.style(a, d, n[d]);
          })),
          (i = hb(p ? q[d] : 0, d, m)),
          d in q || ((q[d] = i.start), p && ((i.end = i.start), (i.start = 0)));
    }
  }
  function jb(a, b) {
    var c, d, e, f, g;
    for (c in a)
      if (
        ((d = r.camelCase(c)),
        (e = b[d]),
        (f = a[c]),
        Array.isArray(f) && ((e = f[1]), (f = a[c] = f[0])),
        c !== d && ((a[d] = f), delete a[c]),
        (g = r.cssHooks[d]),
        g && "expand" in g)
      ) {
        (f = g.expand(f)), delete a[d];
        for (c in f) c in a || ((a[c] = f[c]), (b[c] = e));
      } else b[d] = e;
  }
  function kb(a, b, c) {
    var d,
      e,
      f = 0,
      g = kb.prefilters.length,
      h = r.Deferred().always(function () {
        delete i.elem;
      }),
      i = function () {
        if (e) return !1;
        for (
          var b = ab || fb(),
            c = Math.max(0, j.startTime + j.duration - b),
            d = c / j.duration || 0,
            f = 1 - d,
            g = 0,
            i = j.tweens.length;
          g < i;
          g++
        )
          j.tweens[g].run(f);
        return (
          h.notifyWith(a, [j, f, c]),
          f < 1 && i
            ? c
            : (i || h.notifyWith(a, [j, 1, 0]), h.resolveWith(a, [j]), !1)
        );
      },
      j = h.promise({
        elem: a,
        props: r.extend({}, b),
        opts: r.extend(!0, { specialEasing: {}, easing: r.easing._default }, c),
        originalProperties: b,
        originalOptions: c,
        startTime: ab || fb(),
        duration: c.duration,
        tweens: [],
        createTween: function (b, c) {
          var d = r.Tween(
            a,
            j.opts,
            b,
            c,
            j.opts.specialEasing[b] || j.opts.easing
          );
          return j.tweens.push(d), d;
        },
        stop: function (b) {
          var c = 0,
            d = b ? j.tweens.length : 0;
          if (e) return this;
          for (e = !0; c < d; c++) j.tweens[c].run(1);
          return (
            b
              ? (h.notifyWith(a, [j, 1, 0]), h.resolveWith(a, [j, b]))
              : h.rejectWith(a, [j, b]),
            this
          );
        },
      }),
      k = j.props;
    for (jb(k, j.opts.specialEasing); f < g; f++)
      if ((d = kb.prefilters[f].call(j, a, k, j.opts)))
        return (
          r.isFunction(d.stop) &&
            (r._queueHooks(j.elem, j.opts.queue).stop = r.proxy(d.stop, d)),
          d
        );
    return (
      r.map(k, hb, j),
      r.isFunction(j.opts.start) && j.opts.start.call(a, j),
      j
        .progress(j.opts.progress)
        .done(j.opts.done, j.opts.complete)
        .fail(j.opts.fail)
        .always(j.opts.always),
      r.fx.timer(r.extend(i, { elem: a, anim: j, queue: j.opts.queue })),
      j
    );
  }
  (r.Animation = r.extend(kb, {
    tweeners: {
      "*": [
        function (a, b) {
          var c = this.createTween(a, b);
          return fa(c.elem, a, ba.exec(b), c), c;
        },
      ],
    },
    tweener: function (a, b) {
      r.isFunction(a) ? ((b = a), (a = ["*"])) : (a = a.match(L));
      for (var c, d = 0, e = a.length; d < e; d++)
        (c = a[d]),
          (kb.tweeners[c] = kb.tweeners[c] || []),
          kb.tweeners[c].unshift(b);
    },
    prefilters: [ib],
    prefilter: function (a, b) {
      b ? kb.prefilters.unshift(a) : kb.prefilters.push(a);
    },
  })),
    (r.speed = function (a, b, c) {
      var d =
        a && "object" == typeof a
          ? r.extend({}, a)
          : {
              complete: c || (!c && b) || (r.isFunction(a) && a),
              duration: a,
              easing: (c && b) || (b && !r.isFunction(b) && b),
            };
      return (
        r.fx.off
          ? (d.duration = 0)
          : "number" != typeof d.duration &&
            (d.duration in r.fx.speeds
              ? (d.duration = r.fx.speeds[d.duration])
              : (d.duration = r.fx.speeds._default)),
        (null != d.queue && d.queue !== !0) || (d.queue = "fx"),
        (d.old = d.complete),
        (d.complete = function () {
          r.isFunction(d.old) && d.old.call(this),
            d.queue && r.dequeue(this, d.queue);
        }),
        d
      );
    }),
    r.fn.extend({
      fadeTo: function (a, b, c, d) {
        return this.filter(da)
          .css("opacity", 0)
          .show()
          .end()
          .animate({ opacity: b }, a, c, d);
      },
      animate: function (a, b, c, d) {
        var e = r.isEmptyObject(a),
          f = r.speed(b, c, d),
          g = function () {
            var b = kb(this, r.extend({}, a), f);
            (e || W.get(this, "finish")) && b.stop(!0);
          };
        return (
          (g.finish = g),
          e || f.queue === !1 ? this.each(g) : this.queue(f.queue, g)
        );
      },
      stop: function (a, b, c) {
        var d = function (a) {
          var b = a.stop;
          delete a.stop, b(c);
        };
        return (
          "string" != typeof a && ((c = b), (b = a), (a = void 0)),
          b && a !== !1 && this.queue(a || "fx", []),
          this.each(function () {
            var b = !0,
              e = null != a && a + "queueHooks",
              f = r.timers,
              g = W.get(this);
            if (e) g[e] && g[e].stop && d(g[e]);
            else for (e in g) g[e] && g[e].stop && db.test(e) && d(g[e]);
            for (e = f.length; e--; )
              f[e].elem !== this ||
                (null != a && f[e].queue !== a) ||
                (f[e].anim.stop(c), (b = !1), f.splice(e, 1));
            (!b && c) || r.dequeue(this, a);
          })
        );
      },
      finish: function (a) {
        return (
          a !== !1 && (a = a || "fx"),
          this.each(function () {
            var b,
              c = W.get(this),
              d = c[a + "queue"],
              e = c[a + "queueHooks"],
              f = r.timers,
              g = d ? d.length : 0;
            for (
              c.finish = !0,
                r.queue(this, a, []),
                e && e.stop && e.stop.call(this, !0),
                b = f.length;
              b--;

            )
              f[b].elem === this &&
                f[b].queue === a &&
                (f[b].anim.stop(!0), f.splice(b, 1));
            for (b = 0; b < g; b++)
              d[b] && d[b].finish && d[b].finish.call(this);
            delete c.finish;
          })
        );
      },
    }),
    r.each(["toggle", "show", "hide"], function (a, b) {
      var c = r.fn[b];
      r.fn[b] = function (a, d, e) {
        return null == a || "boolean" == typeof a
          ? c.apply(this, arguments)
          : this.animate(gb(b, !0), a, d, e);
      };
    }),
    r.each(
      {
        slideDown: gb("show"),
        slideUp: gb("hide"),
        slideToggle: gb("toggle"),
        fadeIn: { opacity: "show" },
        fadeOut: { opacity: "hide" },
        fadeToggle: { opacity: "toggle" },
      },
      function (a, b) {
        r.fn[a] = function (a, c, d) {
          return this.animate(b, a, c, d);
        };
      }
    ),
    (r.timers = []),
    (r.fx.tick = function () {
      var a,
        b = 0,
        c = r.timers;
      for (ab = r.now(); b < c.length; b++)
        (a = c[b]), a() || c[b] !== a || c.splice(b--, 1);
      c.length || r.fx.stop(), (ab = void 0);
    }),
    (r.fx.timer = function (a) {
      r.timers.push(a), r.fx.start();
    }),
    (r.fx.interval = 13),
    (r.fx.start = function () {
      bb || ((bb = !0), eb());
    }),
    (r.fx.stop = function () {
      bb = null;
    }),
    (r.fx.speeds = { slow: 600, fast: 200, _default: 400 }),
    (r.fn.delay = function (b, c) {
      return (
        (b = r.fx ? r.fx.speeds[b] || b : b),
        (c = c || "fx"),
        this.queue(c, function (c, d) {
          var e = a.setTimeout(c, b);
          d.stop = function () {
            a.clearTimeout(e);
          };
        })
      );
    }),
    (function () {
      var a = d.createElement("input"),
        b = d.createElement("select"),
        c = b.appendChild(d.createElement("option"));
      (a.type = "checkbox"),
        (o.checkOn = "" !== a.value),
        (o.optSelected = c.selected),
        (a = d.createElement("input")),
        (a.value = "t"),
        (a.type = "radio"),
        (o.radioValue = "t" === a.value);
    })();
  var lb,
    mb = r.expr.attrHandle;
  r.fn.extend({
    attr: function (a, b) {
      return T(this, r.attr, a, b, arguments.length > 1);
    },
    removeAttr: function (a) {
      return this.each(function () {
        r.removeAttr(this, a);
      });
    },
  }),
    r.extend({
      attr: function (a, b, c) {
        var d,
          e,
          f = a.nodeType;
        if (3 !== f && 8 !== f && 2 !== f)
          return "undefined" == typeof a.getAttribute
            ? r.prop(a, b, c)
            : ((1 === f && r.isXMLDoc(a)) ||
                (e =
                  r.attrHooks[b.toLowerCase()] ||
                  (r.expr.match.bool.test(b) ? lb : void 0)),
              void 0 !== c
                ? null === c
                  ? void r.removeAttr(a, b)
                  : e && "set" in e && void 0 !== (d = e.set(a, c, b))
                  ? d
                  : (a.setAttribute(b, c + ""), c)
                : e && "get" in e && null !== (d = e.get(a, b))
                ? d
                : ((d = r.find.attr(a, b)), null == d ? void 0 : d));
      },
      attrHooks: {
        type: {
          set: function (a, b) {
            if (!o.radioValue && "radio" === b && B(a, "input")) {
              var c = a.value;
              return a.setAttribute("type", b), c && (a.value = c), b;
            }
          },
        },
      },
      removeAttr: function (a, b) {
        var c,
          d = 0,
          e = b && b.match(L);
        if (e && 1 === a.nodeType) while ((c = e[d++])) a.removeAttribute(c);
      },
    }),
    (lb = {
      set: function (a, b, c) {
        return b === !1 ? r.removeAttr(a, c) : a.setAttribute(c, c), c;
      },
    }),
    r.each(r.expr.match.bool.source.match(/\w+/g), function (a, b) {
      var c = mb[b] || r.find.attr;
      mb[b] = function (a, b, d) {
        var e,
          f,
          g = b.toLowerCase();
        return (
          d ||
            ((f = mb[g]),
            (mb[g] = e),
            (e = null != c(a, b, d) ? g : null),
            (mb[g] = f)),
          e
        );
      };
    });
  var nb = /^(?:input|select|textarea|button)$/i,
    ob = /^(?:a|area)$/i;
  r.fn.extend({
    prop: function (a, b) {
      return T(this, r.prop, a, b, arguments.length > 1);
    },
    removeProp: function (a) {
      return this.each(function () {
        delete this[r.propFix[a] || a];
      });
    },
  }),
    r.extend({
      prop: function (a, b, c) {
        var d,
          e,
          f = a.nodeType;
        if (3 !== f && 8 !== f && 2 !== f)
          return (
            (1 === f && r.isXMLDoc(a)) ||
              ((b = r.propFix[b] || b), (e = r.propHooks[b])),
            void 0 !== c
              ? e && "set" in e && void 0 !== (d = e.set(a, c, b))
                ? d
                : (a[b] = c)
              : e && "get" in e && null !== (d = e.get(a, b))
              ? d
              : a[b]
          );
      },
      propHooks: {
        tabIndex: {
          get: function (a) {
            var b = r.find.attr(a, "tabindex");
            return b
              ? parseInt(b, 10)
              : nb.test(a.nodeName) || (ob.test(a.nodeName) && a.href)
              ? 0
              : -1;
          },
        },
      },
      propFix: { for: "htmlFor", class: "className" },
    }),
    o.optSelected ||
      (r.propHooks.selected = {
        get: function (a) {
          var b = a.parentNode;
          return b && b.parentNode && b.parentNode.selectedIndex, null;
        },
        set: function (a) {
          var b = a.parentNode;
          b && (b.selectedIndex, b.parentNode && b.parentNode.selectedIndex);
        },
      }),
    r.each(
      [
        "tabIndex",
        "readOnly",
        "maxLength",
        "cellSpacing",
        "cellPadding",
        "rowSpan",
        "colSpan",
        "useMap",
        "frameBorder",
        "contentEditable",
      ],
      function () {
        r.propFix[this.toLowerCase()] = this;
      }
    );
  function pb(a) {
    var b = a.match(L) || [];
    return b.join(" ");
  }
  function qb(a) {
    return (a.getAttribute && a.getAttribute("class")) || "";
  }
  r.fn.extend({
    addClass: function (a) {
      var b,
        c,
        d,
        e,
        f,
        g,
        h,
        i = 0;
      if (r.isFunction(a))
        return this.each(function (b) {
          r(this).addClass(a.call(this, b, qb(this)));
        });
      if ("string" == typeof a && a) {
        b = a.match(L) || [];
        while ((c = this[i++]))
          if (((e = qb(c)), (d = 1 === c.nodeType && " " + pb(e) + " "))) {
            g = 0;
            while ((f = b[g++])) d.indexOf(" " + f + " ") < 0 && (d += f + " ");
            (h = pb(d)), e !== h && c.setAttribute("class", h);
          }
      }
      return this;
    },
    removeClass: function (a) {
      var b,
        c,
        d,
        e,
        f,
        g,
        h,
        i = 0;
      if (r.isFunction(a))
        return this.each(function (b) {
          r(this).removeClass(a.call(this, b, qb(this)));
        });
      if (!arguments.length) return this.attr("class", "");
      if ("string" == typeof a && a) {
        b = a.match(L) || [];
        while ((c = this[i++]))
          if (((e = qb(c)), (d = 1 === c.nodeType && " " + pb(e) + " "))) {
            g = 0;
            while ((f = b[g++]))
              while (d.indexOf(" " + f + " ") > -1)
                d = d.replace(" " + f + " ", " ");
            (h = pb(d)), e !== h && c.setAttribute("class", h);
          }
      }
      return this;
    },
    toggleClass: function (a, b) {
      var c = typeof a;
      return "boolean" == typeof b && "string" === c
        ? b
          ? this.addClass(a)
          : this.removeClass(a)
        : r.isFunction(a)
        ? this.each(function (c) {
            r(this).toggleClass(a.call(this, c, qb(this), b), b);
          })
        : this.each(function () {
            var b, d, e, f;
            if ("string" === c) {
              (d = 0), (e = r(this)), (f = a.match(L) || []);
              while ((b = f[d++]))
                e.hasClass(b) ? e.removeClass(b) : e.addClass(b);
            } else (void 0 !== a && "boolean" !== c) || ((b = qb(this)), b && W.set(this, "__className__", b), this.setAttribute && this.setAttribute("class", b || a === !1 ? "" : W.get(this, "__className__") || ""));
          });
    },
    hasClass: function (a) {
      var b,
        c,
        d = 0;
      b = " " + a + " ";
      while ((c = this[d++]))
        if (1 === c.nodeType && (" " + pb(qb(c)) + " ").indexOf(b) > -1)
          return !0;
      return !1;
    },
  });
  var rb = /\r/g;
  r.fn.extend({
    val: function (a) {
      var b,
        c,
        d,
        e = this[0];
      {
        if (arguments.length)
          return (
            (d = r.isFunction(a)),
            this.each(function (c) {
              var e;
              1 === this.nodeType &&
                ((e = d ? a.call(this, c, r(this).val()) : a),
                null == e
                  ? (e = "")
                  : "number" == typeof e
                  ? (e += "")
                  : Array.isArray(e) &&
                    (e = r.map(e, function (a) {
                      return null == a ? "" : a + "";
                    })),
                (b =
                  r.valHooks[this.type] ||
                  r.valHooks[this.nodeName.toLowerCase()]),
                (b && "set" in b && void 0 !== b.set(this, e, "value")) ||
                  (this.value = e));
            })
          );
        if (e)
          return (
            (b = r.valHooks[e.type] || r.valHooks[e.nodeName.toLowerCase()]),
            b && "get" in b && void 0 !== (c = b.get(e, "value"))
              ? c
              : ((c = e.value),
                "string" == typeof c ? c.replace(rb, "") : null == c ? "" : c)
          );
      }
    },
  }),
    r.extend({
      valHooks: {
        option: {
          get: function (a) {
            var b = r.find.attr(a, "value");
            return null != b ? b : pb(r.text(a));
          },
        },
        select: {
          get: function (a) {
            var b,
              c,
              d,
              e = a.options,
              f = a.selectedIndex,
              g = "select-one" === a.type,
              h = g ? null : [],
              i = g ? f + 1 : e.length;
            for (d = f < 0 ? i : g ? f : 0; d < i; d++)
              if (
                ((c = e[d]),
                (c.selected || d === f) &&
                  !c.disabled &&
                  (!c.parentNode.disabled || !B(c.parentNode, "optgroup")))
              ) {
                if (((b = r(c).val()), g)) return b;
                h.push(b);
              }
            return h;
          },
          set: function (a, b) {
            var c,
              d,
              e = a.options,
              f = r.makeArray(b),
              g = e.length;
            while (g--)
              (d = e[g]),
                (d.selected = r.inArray(r.valHooks.option.get(d), f) > -1) &&
                  (c = !0);
            return c || (a.selectedIndex = -1), f;
          },
        },
      },
    }),
    r.each(["radio", "checkbox"], function () {
      (r.valHooks[this] = {
        set: function (a, b) {
          if (Array.isArray(b))
            return (a.checked = r.inArray(r(a).val(), b) > -1);
        },
      }),
        o.checkOn ||
          (r.valHooks[this].get = function (a) {
            return null === a.getAttribute("value") ? "on" : a.value;
          });
    });
  var sb = /^(?:focusinfocus|focusoutblur)$/;
  r.extend(r.event, {
    trigger: function (b, c, e, f) {
      var g,
        h,
        i,
        j,
        k,
        m,
        n,
        o = [e || d],
        p = l.call(b, "type") ? b.type : b,
        q = l.call(b, "namespace") ? b.namespace.split(".") : [];
      if (
        ((h = i = e = e || d),
        3 !== e.nodeType &&
          8 !== e.nodeType &&
          !sb.test(p + r.event.triggered) &&
          (p.indexOf(".") > -1 &&
            ((q = p.split(".")), (p = q.shift()), q.sort()),
          (k = p.indexOf(":") < 0 && "on" + p),
          (b = b[r.expando] ? b : new r.Event(p, "object" == typeof b && b)),
          (b.isTrigger = f ? 2 : 3),
          (b.namespace = q.join(".")),
          (b.rnamespace = b.namespace
            ? new RegExp("(^|\\.)" + q.join("\\.(?:.*\\.|)") + "(\\.|$)")
            : null),
          (b.result = void 0),
          b.target || (b.target = e),
          (c = null == c ? [b] : r.makeArray(c, [b])),
          (n = r.event.special[p] || {}),
          f || !n.trigger || n.trigger.apply(e, c) !== !1))
      ) {
        if (!f && !n.noBubble && !r.isWindow(e)) {
          for (
            j = n.delegateType || p, sb.test(j + p) || (h = h.parentNode);
            h;
            h = h.parentNode
          )
            o.push(h), (i = h);
          i === (e.ownerDocument || d) &&
            o.push(i.defaultView || i.parentWindow || a);
        }
        g = 0;
        while ((h = o[g++]) && !b.isPropagationStopped())
          (b.type = g > 1 ? j : n.bindType || p),
            (m = (W.get(h, "events") || {})[b.type] && W.get(h, "handle")),
            m && m.apply(h, c),
            (m = k && h[k]),
            m &&
              m.apply &&
              U(h) &&
              ((b.result = m.apply(h, c)),
              b.result === !1 && b.preventDefault());
        return (
          (b.type = p),
          f ||
            b.isDefaultPrevented() ||
            (n._default && n._default.apply(o.pop(), c) !== !1) ||
            !U(e) ||
            (k &&
              r.isFunction(e[p]) &&
              !r.isWindow(e) &&
              ((i = e[k]),
              i && (e[k] = null),
              (r.event.triggered = p),
              e[p](),
              (r.event.triggered = void 0),
              i && (e[k] = i))),
          b.result
        );
      }
    },
    simulate: function (a, b, c) {
      var d = r.extend(new r.Event(), c, { type: a, isSimulated: !0 });
      r.event.trigger(d, null, b);
    },
  }),
    r.fn.extend({
      trigger: function (a, b) {
        return this.each(function () {
          r.event.trigger(a, b, this);
        });
      },
      triggerHandler: function (a, b) {
        var c = this[0];
        if (c) return r.event.trigger(a, b, c, !0);
      },
    }),
    r.each(
      "blur focus focusin focusout resize scroll click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup contextmenu".split(
        " "
      ),
      function (a, b) {
        r.fn[b] = function (a, c) {
          return arguments.length > 0
            ? this.on(b, null, a, c)
            : this.trigger(b);
        };
      }
    ),
    r.fn.extend({
      hover: function (a, b) {
        return this.mouseenter(a).mouseleave(b || a);
      },
    }),
    (o.focusin = "onfocusin" in a),
    o.focusin ||
      r.each({ focus: "focusin", blur: "focusout" }, function (a, b) {
        var c = function (a) {
          r.event.simulate(b, a.target, r.event.fix(a));
        };
        r.event.special[b] = {
          setup: function () {
            var d = this.ownerDocument || this,
              e = W.access(d, b);
            e || d.addEventListener(a, c, !0), W.access(d, b, (e || 0) + 1);
          },
          teardown: function () {
            var d = this.ownerDocument || this,
              e = W.access(d, b) - 1;
            e
              ? W.access(d, b, e)
              : (d.removeEventListener(a, c, !0), W.remove(d, b));
          },
        };
      });
  var tb = a.location,
    ub = r.now(),
    vb = /\?/;
  r.parseXML = function (b) {
    var c;
    if (!b || "string" != typeof b) return null;
    try {
      c = new a.DOMParser().parseFromString(b, "text/xml");
    } catch (d) {
      c = void 0;
    }
    return (
      (c && !c.getElementsByTagName("parsererror").length) ||
        r.error("Invalid XML: " + b),
      c
    );
  };
  var wb = /\[\]$/,
    xb = /\r?\n/g,
    yb = /^(?:submit|button|image|reset|file)$/i,
    zb = /^(?:input|select|textarea|keygen)/i;
  function Ab(a, b, c, d) {
    var e;
    if (Array.isArray(b))
      r.each(b, function (b, e) {
        c || wb.test(a)
          ? d(a, e)
          : Ab(
              a + "[" + ("object" == typeof e && null != e ? b : "") + "]",
              e,
              c,
              d
            );
      });
    else if (c || "object" !== r.type(b)) d(a, b);
    else for (e in b) Ab(a + "[" + e + "]", b[e], c, d);
  }
  (r.param = function (a, b) {
    var c,
      d = [],
      e = function (a, b) {
        var c = r.isFunction(b) ? b() : b;
        d[d.length] =
          encodeURIComponent(a) + "=" + encodeURIComponent(null == c ? "" : c);
      };
    if (Array.isArray(a) || (a.jquery && !r.isPlainObject(a)))
      r.each(a, function () {
        e(this.name, this.value);
      });
    else for (c in a) Ab(c, a[c], b, e);
    return d.join("&");
  }),
    r.fn.extend({
      serialize: function () {
        return r.param(this.serializeArray());
      },
      serializeArray: function () {
        return this.map(function () {
          var a = r.prop(this, "elements");
          return a ? r.makeArray(a) : this;
        })
          .filter(function () {
            var a = this.type;
            return (
              this.name &&
              !r(this).is(":disabled") &&
              zb.test(this.nodeName) &&
              !yb.test(a) &&
              (this.checked || !ja.test(a))
            );
          })
          .map(function (a, b) {
            var c = r(this).val();
            return null == c
              ? null
              : Array.isArray(c)
              ? r.map(c, function (a) {
                  return { name: b.name, value: a.replace(xb, "\r\n") };
                })
              : { name: b.name, value: c.replace(xb, "\r\n") };
          })
          .get();
      },
    });
  var Bb = /%20/g,
    Cb = /#.*$/,
    Db = /([?&])_=[^&]*/,
    Eb = /^(.*?):[ \t]*([^\r\n]*)$/gm,
    Fb = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/,
    Gb = /^(?:GET|HEAD)$/,
    Hb = /^\/\//,
    Ib = {},
    Jb = {},
    Kb = "*/".concat("*"),
    Lb = d.createElement("a");
  Lb.href = tb.href;
  function Mb(a) {
    return function (b, c) {
      "string" != typeof b && ((c = b), (b = "*"));
      var d,
        e = 0,
        f = b.toLowerCase().match(L) || [];
      if (r.isFunction(c))
        while ((d = f[e++]))
          "+" === d[0]
            ? ((d = d.slice(1) || "*"), (a[d] = a[d] || []).unshift(c))
            : (a[d] = a[d] || []).push(c);
    };
  }
  function Nb(a, b, c, d) {
    var e = {},
      f = a === Jb;
    function g(h) {
      var i;
      return (
        (e[h] = !0),
        r.each(a[h] || [], function (a, h) {
          var j = h(b, c, d);
          return "string" != typeof j || f || e[j]
            ? f
              ? !(i = j)
              : void 0
            : (b.dataTypes.unshift(j), g(j), !1);
        }),
        i
      );
    }
    return g(b.dataTypes[0]) || (!e["*"] && g("*"));
  }
  function Ob(a, b) {
    var c,
      d,
      e = r.ajaxSettings.flatOptions || {};
    for (c in b) void 0 !== b[c] && ((e[c] ? a : d || (d = {}))[c] = b[c]);
    return d && r.extend(!0, a, d), a;
  }
  function Pb(a, b, c) {
    var d,
      e,
      f,
      g,
      h = a.contents,
      i = a.dataTypes;
    while ("*" === i[0])
      i.shift(),
        void 0 === d && (d = a.mimeType || b.getResponseHeader("Content-Type"));
    if (d)
      for (e in h)
        if (h[e] && h[e].test(d)) {
          i.unshift(e);
          break;
        }
    if (i[0] in c) f = i[0];
    else {
      for (e in c) {
        if (!i[0] || a.converters[e + " " + i[0]]) {
          f = e;
          break;
        }
        g || (g = e);
      }
      f = f || g;
    }
    if (f) return f !== i[0] && i.unshift(f), c[f];
  }
  function Qb(a, b, c, d) {
    var e,
      f,
      g,
      h,
      i,
      j = {},
      k = a.dataTypes.slice();
    if (k[1]) for (g in a.converters) j[g.toLowerCase()] = a.converters[g];
    f = k.shift();
    while (f)
      if (
        (a.responseFields[f] && (c[a.responseFields[f]] = b),
        !i && d && a.dataFilter && (b = a.dataFilter(b, a.dataType)),
        (i = f),
        (f = k.shift()))
      )
        if ("*" === f) f = i;
        else if ("*" !== i && i !== f) {
          if (((g = j[i + " " + f] || j["* " + f]), !g))
            for (e in j)
              if (
                ((h = e.split(" ")),
                h[1] === f && (g = j[i + " " + h[0]] || j["* " + h[0]]))
              ) {
                g === !0
                  ? (g = j[e])
                  : j[e] !== !0 && ((f = h[0]), k.unshift(h[1]));
                break;
              }
          if (g !== !0)
            if (g && a["throws"]) b = g(b);
            else
              try {
                b = g(b);
              } catch (l) {
                return {
                  state: "parsererror",
                  error: g ? l : "No conversion from " + i + " to " + f,
                };
              }
        }
    return { state: "success", data: b };
  }
  r.extend({
    active: 0,
    lastModified: {},
    etag: {},
    ajaxSettings: {
      url: tb.href,
      type: "GET",
      isLocal: Fb.test(tb.protocol),
      global: !0,
      processData: !0,
      async: !0,
      contentType: "application/x-www-form-urlencoded; charset=UTF-8",
      accepts: {
        "*": Kb,
        text: "text/plain",
        html: "text/html",
        xml: "application/xml, text/xml",
        json: "application/json, text/javascript",
      },
      contents: { xml: /\bxml\b/, html: /\bhtml/, json: /\bjson\b/ },
      responseFields: {
        xml: "responseXML",
        text: "responseText",
        json: "responseJSON",
      },
      converters: {
        "* text": String,
        "text html": !0,
        "text json": JSON.parse,
        "text xml": r.parseXML,
      },
      flatOptions: { url: !0, context: !0 },
    },
    ajaxSetup: function (a, b) {
      return b ? Ob(Ob(a, r.ajaxSettings), b) : Ob(r.ajaxSettings, a);
    },
    ajaxPrefilter: Mb(Ib),
    ajaxTransport: Mb(Jb),
    ajax: function (b, c) {
      "object" == typeof b && ((c = b), (b = void 0)), (c = c || {});
      var e,
        f,
        g,
        h,
        i,
        j,
        k,
        l,
        m,
        n,
        o = r.ajaxSetup({}, c),
        p = o.context || o,
        q = o.context && (p.nodeType || p.jquery) ? r(p) : r.event,
        s = r.Deferred(),
        t = r.Callbacks("once memory"),
        u = o.statusCode || {},
        v = {},
        w = {},
        x = "canceled",
        y = {
          readyState: 0,
          getResponseHeader: function (a) {
            var b;
            if (k) {
              if (!h) {
                h = {};
                while ((b = Eb.exec(g))) h[b[1].toLowerCase()] = b[2];
              }
              b = h[a.toLowerCase()];
            }
            return null == b ? null : b;
          },
          getAllResponseHeaders: function () {
            return k ? g : null;
          },
          setRequestHeader: function (a, b) {
            return (
              null == k &&
                ((a = w[a.toLowerCase()] = w[a.toLowerCase()] || a),
                (v[a] = b)),
              this
            );
          },
          overrideMimeType: function (a) {
            return null == k && (o.mimeType = a), this;
          },
          statusCode: function (a) {
            var b;
            if (a)
              if (k) y.always(a[y.status]);
              else for (b in a) u[b] = [u[b], a[b]];
            return this;
          },
          abort: function (a) {
            var b = a || x;
            return e && e.abort(b), A(0, b), this;
          },
        };
      if (
        (s.promise(y),
        (o.url = ((b || o.url || tb.href) + "").replace(
          Hb,
          tb.protocol + "//"
        )),
        (o.type = c.method || c.type || o.method || o.type),
        (o.dataTypes = (o.dataType || "*").toLowerCase().match(L) || [""]),
        null == o.crossDomain)
      ) {
        j = d.createElement("a");
        try {
          (j.href = o.url),
            (j.href = j.href),
            (o.crossDomain =
              Lb.protocol + "//" + Lb.host != j.protocol + "//" + j.host);
        } catch (z) {
          o.crossDomain = !0;
        }
      }
      if (
        (o.data &&
          o.processData &&
          "string" != typeof o.data &&
          (o.data = r.param(o.data, o.traditional)),
        Nb(Ib, o, c, y),
        k)
      )
        return y;
      (l = r.event && o.global),
        l && 0 === r.active++ && r.event.trigger("ajaxStart"),
        (o.type = o.type.toUpperCase()),
        (o.hasContent = !Gb.test(o.type)),
        (f = o.url.replace(Cb, "")),
        o.hasContent
          ? o.data &&
            o.processData &&
            0 ===
              (o.contentType || "").indexOf(
                "application/x-www-form-urlencoded"
              ) &&
            (o.data = o.data.replace(Bb, "+"))
          : ((n = o.url.slice(f.length)),
            o.data && ((f += (vb.test(f) ? "&" : "?") + o.data), delete o.data),
            o.cache === !1 &&
              ((f = f.replace(Db, "$1")),
              (n = (vb.test(f) ? "&" : "?") + "_=" + ub++ + n)),
            (o.url = f + n)),
        o.ifModified &&
          (r.lastModified[f] &&
            y.setRequestHeader("If-Modified-Since", r.lastModified[f]),
          r.etag[f] && y.setRequestHeader("If-None-Match", r.etag[f])),
        ((o.data && o.hasContent && o.contentType !== !1) || c.contentType) &&
          y.setRequestHeader("Content-Type", o.contentType),
        y.setRequestHeader(
          "Accept",
          o.dataTypes[0] && o.accepts[o.dataTypes[0]]
            ? o.accepts[o.dataTypes[0]] +
                ("*" !== o.dataTypes[0] ? ", " + Kb + "; q=0.01" : "")
            : o.accepts["*"]
        );
      for (m in o.headers) y.setRequestHeader(m, o.headers[m]);
      if (o.beforeSend && (o.beforeSend.call(p, y, o) === !1 || k))
        return y.abort();
      if (
        ((x = "abort"),
        t.add(o.complete),
        y.done(o.success),
        y.fail(o.error),
        (e = Nb(Jb, o, c, y)))
      ) {
        if (((y.readyState = 1), l && q.trigger("ajaxSend", [y, o]), k))
          return y;
        o.async &&
          o.timeout > 0 &&
          (i = a.setTimeout(function () {
            y.abort("timeout");
          }, o.timeout));
        try {
          (k = !1), e.send(v, A);
        } catch (z) {
          if (k) throw z;
          A(-1, z);
        }
      } else A(-1, "No Transport");
      function A(b, c, d, h) {
        var j,
          m,
          n,
          v,
          w,
          x = c;
        k ||
          ((k = !0),
          i && a.clearTimeout(i),
          (e = void 0),
          (g = h || ""),
          (y.readyState = b > 0 ? 4 : 0),
          (j = (b >= 200 && b < 300) || 304 === b),
          d && (v = Pb(o, y, d)),
          (v = Qb(o, v, y, j)),
          j
            ? (o.ifModified &&
                ((w = y.getResponseHeader("Last-Modified")),
                w && (r.lastModified[f] = w),
                (w = y.getResponseHeader("etag")),
                w && (r.etag[f] = w)),
              204 === b || "HEAD" === o.type
                ? (x = "nocontent")
                : 304 === b
                ? (x = "notmodified")
                : ((x = v.state), (m = v.data), (n = v.error), (j = !n)))
            : ((n = x), (!b && x) || ((x = "error"), b < 0 && (b = 0))),
          (y.status = b),
          (y.statusText = (c || x) + ""),
          j ? s.resolveWith(p, [m, x, y]) : s.rejectWith(p, [y, x, n]),
          y.statusCode(u),
          (u = void 0),
          l && q.trigger(j ? "ajaxSuccess" : "ajaxError", [y, o, j ? m : n]),
          t.fireWith(p, [y, x]),
          l &&
            (q.trigger("ajaxComplete", [y, o]),
            --r.active || r.event.trigger("ajaxStop")));
      }
      return y;
    },
    getJSON: function (a, b, c) {
      return r.get(a, b, c, "json");
    },
    getScript: function (a, b) {
      return r.get(a, void 0, b, "script");
    },
  }),
    r.each(["get", "post"], function (a, b) {
      r[b] = function (a, c, d, e) {
        return (
          r.isFunction(c) && ((e = e || d), (d = c), (c = void 0)),
          r.ajax(
            r.extend(
              { url: a, type: b, dataType: e, data: c, success: d },
              r.isPlainObject(a) && a
            )
          )
        );
      };
    }),
    (r._evalUrl = function (a) {
      return r.ajax({
        url: a,
        type: "GET",
        dataType: "script",
        cache: !0,
        async: !1,
        global: !1,
        throws: !0,
      });
    }),
    r.fn.extend({
      wrapAll: function (a) {
        var b;
        return (
          this[0] &&
            (r.isFunction(a) && (a = a.call(this[0])),
            (b = r(a, this[0].ownerDocument).eq(0).clone(!0)),
            this[0].parentNode && b.insertBefore(this[0]),
            b
              .map(function () {
                var a = this;
                while (a.firstElementChild) a = a.firstElementChild;
                return a;
              })
              .append(this)),
          this
        );
      },
      wrapInner: function (a) {
        return r.isFunction(a)
          ? this.each(function (b) {
              r(this).wrapInner(a.call(this, b));
            })
          : this.each(function () {
              var b = r(this),
                c = b.contents();
              c.length ? c.wrapAll(a) : b.append(a);
            });
      },
      wrap: function (a) {
        var b = r.isFunction(a);
        return this.each(function (c) {
          r(this).wrapAll(b ? a.call(this, c) : a);
        });
      },
      unwrap: function (a) {
        return (
          this.parent(a)
            .not("body")
            .each(function () {
              r(this).replaceWith(this.childNodes);
            }),
          this
        );
      },
    }),
    (r.expr.pseudos.hidden = function (a) {
      return !r.expr.pseudos.visible(a);
    }),
    (r.expr.pseudos.visible = function (a) {
      return !!(a.offsetWidth || a.offsetHeight || a.getClientRects().length);
    }),
    (r.ajaxSettings.xhr = function () {
      try {
        return new a.XMLHttpRequest();
      } catch (b) {}
    });
  var Rb = { 0: 200, 1223: 204 },
    Sb = r.ajaxSettings.xhr();
  (o.cors = !!Sb && "withCredentials" in Sb),
    (o.ajax = Sb = !!Sb),
    r.ajaxTransport(function (b) {
      var c, d;
      if (o.cors || (Sb && !b.crossDomain))
        return {
          send: function (e, f) {
            var g,
              h = b.xhr();
            if (
              (h.open(b.type, b.url, b.async, b.username, b.password),
              b.xhrFields)
            )
              for (g in b.xhrFields) h[g] = b.xhrFields[g];
            b.mimeType && h.overrideMimeType && h.overrideMimeType(b.mimeType),
              b.crossDomain ||
                e["X-Requested-With"] ||
                (e["X-Requested-With"] = "XMLHttpRequest");
            for (g in e) h.setRequestHeader(g, e[g]);
            (c = function (a) {
              return function () {
                c &&
                  ((c = d = h.onload = h.onerror = h.onabort = h.onreadystatechange = null),
                  "abort" === a
                    ? h.abort()
                    : "error" === a
                    ? "number" != typeof h.status
                      ? f(0, "error")
                      : f(h.status, h.statusText)
                    : f(
                        Rb[h.status] || h.status,
                        h.statusText,
                        "text" !== (h.responseType || "text") ||
                          "string" != typeof h.responseText
                          ? { binary: h.response }
                          : { text: h.responseText },
                        h.getAllResponseHeaders()
                      ));
              };
            }),
              (h.onload = c()),
              (d = h.onerror = c("error")),
              void 0 !== h.onabort
                ? (h.onabort = d)
                : (h.onreadystatechange = function () {
                    4 === h.readyState &&
                      a.setTimeout(function () {
                        c && d();
                      });
                  }),
              (c = c("abort"));
            try {
              h.send((b.hasContent && b.data) || null);
            } catch (i) {
              if (c) throw i;
            }
          },
          abort: function () {
            c && c();
          },
        };
    }),
    r.ajaxPrefilter(function (a) {
      a.crossDomain && (a.contents.script = !1);
    }),
    r.ajaxSetup({
      accepts: {
        script:
          "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript",
      },
      contents: { script: /\b(?:java|ecma)script\b/ },
      converters: {
        "text script": function (a) {
          return r.globalEval(a), a;
        },
      },
    }),
    r.ajaxPrefilter("script", function (a) {
      void 0 === a.cache && (a.cache = !1), a.crossDomain && (a.type = "GET");
    }),
    r.ajaxTransport("script", function (a) {
      if (a.crossDomain) {
        var b, c;
        return {
          send: function (e, f) {
            (b = r("<script>")
              .prop({ charset: a.scriptCharset, src: a.url })
              .on(
                "load error",
                (c = function (a) {
                  b.remove(),
                    (c = null),
                    a && f("error" === a.type ? 404 : 200, a.type);
                })
              )),
              d.head.appendChild(b[0]);
          },
          abort: function () {
            c && c();
          },
        };
      }
    });
  var Tb = [],
    Ub = /(=)\?(?=&|$)|\?\?/;
  r.ajaxSetup({
    jsonp: "callback",
    jsonpCallback: function () {
      var a = Tb.pop() || r.expando + "_" + ub++;
      return (this[a] = !0), a;
    },
  }),
    r.ajaxPrefilter("json jsonp", function (b, c, d) {
      var e,
        f,
        g,
        h =
          b.jsonp !== !1 &&
          (Ub.test(b.url)
            ? "url"
            : "string" == typeof b.data &&
              0 ===
                (b.contentType || "").indexOf(
                  "application/x-www-form-urlencoded"
                ) &&
              Ub.test(b.data) &&
              "data");
      if (h || "jsonp" === b.dataTypes[0])
        return (
          (e = b.jsonpCallback = r.isFunction(b.jsonpCallback)
            ? b.jsonpCallback()
            : b.jsonpCallback),
          h
            ? (b[h] = b[h].replace(Ub, "$1" + e))
            : b.jsonp !== !1 &&
              (b.url += (vb.test(b.url) ? "&" : "?") + b.jsonp + "=" + e),
          (b.converters["script json"] = function () {
            return g || r.error(e + " was not called"), g[0];
          }),
          (b.dataTypes[0] = "json"),
          (f = a[e]),
          (a[e] = function () {
            g = arguments;
          }),
          d.always(function () {
            void 0 === f ? r(a).removeProp(e) : (a[e] = f),
              b[e] && ((b.jsonpCallback = c.jsonpCallback), Tb.push(e)),
              g && r.isFunction(f) && f(g[0]),
              (g = f = void 0);
          }),
          "script"
        );
    }),
    (o.createHTMLDocument = (function () {
      var a = d.implementation.createHTMLDocument("").body;
      return (
        (a.innerHTML = "<form></form><form></form>"), 2 === a.childNodes.length
      );
    })()),
    (r.parseHTML = function (a, b, c) {
      if ("string" != typeof a) return [];
      "boolean" == typeof b && ((c = b), (b = !1));
      var e, f, g;
      return (
        b ||
          (o.createHTMLDocument
            ? ((b = d.implementation.createHTMLDocument("")),
              (e = b.createElement("base")),
              (e.href = d.location.href),
              b.head.appendChild(e))
            : (b = d)),
        (f = C.exec(a)),
        (g = !c && []),
        f
          ? [b.createElement(f[1])]
          : ((f = qa([a], b, g)),
            g && g.length && r(g).remove(),
            r.merge([], f.childNodes))
      );
    }),
    (r.fn.load = function (a, b, c) {
      var d,
        e,
        f,
        g = this,
        h = a.indexOf(" ");
      return (
        h > -1 && ((d = pb(a.slice(h))), (a = a.slice(0, h))),
        r.isFunction(b)
          ? ((c = b), (b = void 0))
          : b && "object" == typeof b && (e = "POST"),
        g.length > 0 &&
          r
            .ajax({ url: a, type: e || "GET", dataType: "html", data: b })
            .done(function (a) {
              (f = arguments),
                g.html(d ? r("<div>").append(r.parseHTML(a)).find(d) : a);
            })
            .always(
              c &&
                function (a, b) {
                  g.each(function () {
                    c.apply(this, f || [a.responseText, b, a]);
                  });
                }
            ),
        this
      );
    }),
    r.each(
      [
        "ajaxStart",
        "ajaxStop",
        "ajaxComplete",
        "ajaxError",
        "ajaxSuccess",
        "ajaxSend",
      ],
      function (a, b) {
        r.fn[b] = function (a) {
          return this.on(b, a);
        };
      }
    ),
    (r.expr.pseudos.animated = function (a) {
      return r.grep(r.timers, function (b) {
        return a === b.elem;
      }).length;
    }),
    (r.offset = {
      setOffset: function (a, b, c) {
        var d,
          e,
          f,
          g,
          h,
          i,
          j,
          k = r.css(a, "position"),
          l = r(a),
          m = {};
        "static" === k && (a.style.position = "relative"),
          (h = l.offset()),
          (f = r.css(a, "top")),
          (i = r.css(a, "left")),
          (j =
            ("absolute" === k || "fixed" === k) &&
            (f + i).indexOf("auto") > -1),
          j
            ? ((d = l.position()), (g = d.top), (e = d.left))
            : ((g = parseFloat(f) || 0), (e = parseFloat(i) || 0)),
          r.isFunction(b) && (b = b.call(a, c, r.extend({}, h))),
          null != b.top && (m.top = b.top - h.top + g),
          null != b.left && (m.left = b.left - h.left + e),
          "using" in b ? b.using.call(a, m) : l.css(m);
      },
    }),
    r.fn.extend({
      offset: function (a) {
        if (arguments.length)
          return void 0 === a
            ? this
            : this.each(function (b) {
                r.offset.setOffset(this, a, b);
              });
        var b,
          c,
          d,
          e,
          f = this[0];
        if (f)
          return f.getClientRects().length
            ? ((d = f.getBoundingClientRect()),
              (b = f.ownerDocument),
              (c = b.documentElement),
              (e = b.defaultView),
              {
                top: d.top + e.pageYOffset - c.clientTop,
                left: d.left + e.pageXOffset - c.clientLeft,
              })
            : { top: 0, left: 0 };
      },
      position: function () {
        if (this[0]) {
          var a,
            b,
            c = this[0],
            d = { top: 0, left: 0 };
          return (
            "fixed" === r.css(c, "position")
              ? (b = c.getBoundingClientRect())
              : ((a = this.offsetParent()),
                (b = this.offset()),
                B(a[0], "html") || (d = a.offset()),
                (d = {
                  top: d.top + r.css(a[0], "borderTopWidth", !0),
                  left: d.left + r.css(a[0], "borderLeftWidth", !0),
                })),
            {
              top: b.top - d.top - r.css(c, "marginTop", !0),
              left: b.left - d.left - r.css(c, "marginLeft", !0),
            }
          );
        }
      },
      offsetParent: function () {
        return this.map(function () {
          var a = this.offsetParent;
          while (a && "static" === r.css(a, "position")) a = a.offsetParent;
          return a || ra;
        });
      },
    }),
    r.each({ scrollLeft: "pageXOffset", scrollTop: "pageYOffset" }, function (
      a,
      b
    ) {
      var c = "pageYOffset" === b;
      r.fn[a] = function (d) {
        return T(
          this,
          function (a, d, e) {
            var f;
            return (
              r.isWindow(a) ? (f = a) : 9 === a.nodeType && (f = a.defaultView),
              void 0 === e
                ? f
                  ? f[b]
                  : a[d]
                : void (f
                    ? f.scrollTo(c ? f.pageXOffset : e, c ? e : f.pageYOffset)
                    : (a[d] = e))
            );
          },
          a,
          d,
          arguments.length
        );
      };
    }),
    r.each(["top", "left"], function (a, b) {
      r.cssHooks[b] = Pa(o.pixelPosition, function (a, c) {
        if (c)
          return (c = Oa(a, b)), Ma.test(c) ? r(a).position()[b] + "px" : c;
      });
    }),
    r.each({ Height: "height", Width: "width" }, function (a, b) {
      r.each({ padding: "inner" + a, content: b, "": "outer" + a }, function (
        c,
        d
      ) {
        r.fn[d] = function (e, f) {
          var g = arguments.length && (c || "boolean" != typeof e),
            h = c || (e === !0 || f === !0 ? "margin" : "border");
          return T(
            this,
            function (b, c, e) {
              var f;
              return r.isWindow(b)
                ? 0 === d.indexOf("outer")
                  ? b["inner" + a]
                  : b.document.documentElement["client" + a]
                : 9 === b.nodeType
                ? ((f = b.documentElement),
                  Math.max(
                    b.body["scroll" + a],
                    f["scroll" + a],
                    b.body["offset" + a],
                    f["offset" + a],
                    f["client" + a]
                  ))
                : void 0 === e
                ? r.css(b, c, h)
                : r.style(b, c, e, h);
            },
            b,
            g ? e : void 0,
            g
          );
        };
      });
    }),
    r.fn.extend({
      bind: function (a, b, c) {
        return this.on(a, null, b, c);
      },
      unbind: function (a, b) {
        return this.off(a, null, b);
      },
      delegate: function (a, b, c, d) {
        return this.on(b, a, c, d);
      },
      undelegate: function (a, b, c) {
        return 1 === arguments.length
          ? this.off(a, "**")
          : this.off(b, a || "**", c);
      },
      holdReady: function (a) {
        a ? r.readyWait++ : r.ready(!0);
      },
    }),
    (r.isArray = Array.isArray),
    (r.parseJSON = JSON.parse),
    (r.nodeName = B),
    "function" == typeof define &&
      define.amd &&
      define("jquery", [], function () {
        return r;
      });
  var Vb = a.jQuery,
    Wb = a.$;
  return (
    (r.noConflict = function (b) {
      return a.$ === r && (a.$ = Wb), b && a.jQuery === r && (a.jQuery = Vb), r;
    }),
    b || (a.jQuery = a.$ = r),
    r
  );
});

/**
 * jquery.mask.js
 * @version: v1.14.15
 * @author: Igor Escobar
 *
 * Created by Igor Escobar on 2012-03-10. Please report any bug at github.com/igorescobar/jQuery-Mask-Plugin
 *
 * Copyright (c) 2012 Igor Escobar http://igorescobar.com
 *
 * The MIT License (http://www.opensource.org/licenses/mit-license.php)
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

/* jshint laxbreak: true */
/* jshint maxcomplexity:17 */
/* global define */

// UMD (Universal Module Definition) patterns for JavaScript modules that work everywhere.
// https://github.com/umdjs/umd/blob/master/templates/jqueryPlugin.js
(function (factory, jQuery, Zepto) {
  if (typeof define === "function" && define.amd) {
    define(["jquery"], factory);
  } else if (typeof exports === "object") {
    module.exports = factory(require("jquery"));
  } else {
    factory(jQuery || Zepto);
  }
})(
  function ($) {
    "use strict";

    var Mask = function (el, mask, options) {
      var p = {
        invalid: [],
        getCaret: function () {
          try {
            var sel,
              pos = 0,
              ctrl = el.get(0),
              dSel = document.selection,
              cSelStart = ctrl.selectionStart;

            // IE Support
            if (dSel && navigator.appVersion.indexOf("MSIE 10") === -1) {
              sel = dSel.createRange();
              sel.moveStart("character", -p.val().length);
              pos = sel.text.length;
            }
            // Firefox support
            else if (cSelStart || cSelStart === "0") {
              pos = cSelStart;
            }

            return pos;
          } catch (e) {}
        },
        setCaret: function (pos) {
          try {
            if (el.is(":focus")) {
              var range,
                ctrl = el.get(0);

              // Firefox, WebKit, etc..
              if (ctrl.setSelectionRange) {
                ctrl.setSelectionRange(pos, pos);
              } else {
                // IE
                range = ctrl.createTextRange();
                range.collapse(true);
                range.moveEnd("character", pos);
                range.moveStart("character", pos);
                range.select();
              }
            }
          } catch (e) {}
        },
        events: function () {
          el.on("keydown.mask", function (e) {
            el.data("mask-keycode", e.keyCode || e.which);
            el.data("mask-previus-value", el.val());
            el.data("mask-previus-caret-pos", p.getCaret());
            p.maskDigitPosMapOld = p.maskDigitPosMap;
          })
            .on(
              $.jMaskGlobals.useInput ? "input.mask" : "keyup.mask",
              p.behaviour
            )
            .on("paste.mask drop.mask", function () {
              setTimeout(function () {
                el.keydown().keyup();
              }, 100);
            })
            .on("change.mask", function () {
              el.data("changed", true);
            })
            .on("blur.mask", function () {
              if (oldValue !== p.val() && !el.data("changed")) {
                el.trigger("change");
              }
              el.data("changed", false);
            })
            // it's very important that this callback remains in this position
            // otherwhise oldValue it's going to work buggy
            .on("blur.mask", function () {
              oldValue = p.val();
            })
            // select all text on focus
            .on("focus.mask", function (e) {
              if (options.selectOnFocus === true) {
                $(e.target).select();
              }
            })
            // clear the value if it not complete the mask
            .on("focusout.mask", function () {
              if (options.clearIfNotMatch && !regexMask.test(p.val())) {
                p.val("");
              }
            });
        },
        getRegexMask: function () {
          var maskChunks = [],
            translation,
            pattern,
            optional,
            recursive,
            oRecursive,
            r;

          for (var i = 0; i < mask.length; i++) {
            translation = jMask.translation[mask.charAt(i)];

            if (translation) {
              pattern = translation.pattern
                .toString()
                .replace(/.{1}$|^.{1}/g, "");
              optional = translation.optional;
              recursive = translation.recursive;

              if (recursive) {
                maskChunks.push(mask.charAt(i));
                oRecursive = { digit: mask.charAt(i), pattern: pattern };
              } else {
                maskChunks.push(
                  !optional && !recursive ? pattern : pattern + "?"
                );
              }
            } else {
              maskChunks.push(
                mask.charAt(i).replace(/[-\/\\^$*+?.()|[\]{}]/g, "\\$&")
              );
            }
          }

          r = maskChunks.join("");

          if (oRecursive) {
            r = r
              .replace(
                new RegExp(
                  "(" + oRecursive.digit + "(.*" + oRecursive.digit + ")?)"
                ),
                "($1)?"
              )
              .replace(new RegExp(oRecursive.digit, "g"), oRecursive.pattern);
          }

          return new RegExp(r);
        },
        destroyEvents: function () {
          el.off(
            [
              "input",
              "keydown",
              "keyup",
              "paste",
              "drop",
              "blur",
              "focusout",
              "",
            ].join(".mask ")
          );
        },
        val: function (v) {
          var isInput = el.is("input"),
            method = isInput ? "val" : "text",
            r;

          if (arguments.length > 0) {
            if (el[method]() !== v) {
              el[method](v);
            }
            r = el;
          } else {
            r = el[method]();
          }

          return r;
        },
        calculateCaretPosition: function () {
          var oldVal = el.data("mask-previus-value") || "",
            newVal = p.getMasked(),
            caretPosNew = p.getCaret();
          if (oldVal !== newVal) {
            var caretPosOld = el.data("mask-previus-caret-pos") || 0,
              newValL = newVal.length,
              oldValL = oldVal.length,
              maskDigitsBeforeCaret = 0,
              maskDigitsAfterCaret = 0,
              maskDigitsBeforeCaretAll = 0,
              maskDigitsBeforeCaretAllOld = 0,
              i = 0;

            for (i = caretPosNew; i < newValL; i++) {
              if (!p.maskDigitPosMap[i]) {
                break;
              }
              maskDigitsAfterCaret++;
            }

            for (i = caretPosNew - 1; i >= 0; i--) {
              if (!p.maskDigitPosMap[i]) {
                break;
              }
              maskDigitsBeforeCaret++;
            }

            for (i = caretPosNew - 1; i >= 0; i--) {
              if (p.maskDigitPosMap[i]) {
                maskDigitsBeforeCaretAll++;
              }
            }

            for (i = caretPosOld - 1; i >= 0; i--) {
              if (p.maskDigitPosMapOld[i]) {
                maskDigitsBeforeCaretAllOld++;
              }
            }

            // if the cursor is at the end keep it there
            if (caretPosNew > oldValL) {
              caretPosNew = newValL * 10;
            } else if (caretPosOld >= caretPosNew && caretPosOld !== oldValL) {
              if (!p.maskDigitPosMapOld[caretPosNew]) {
                var caretPos = caretPosNew;
                caretPosNew -=
                  maskDigitsBeforeCaretAllOld - maskDigitsBeforeCaretAll;
                caretPosNew -= maskDigitsBeforeCaret;
                if (p.maskDigitPosMap[caretPosNew]) {
                  caretPosNew = caretPos;
                }
              }
            } else if (caretPosNew > caretPosOld) {
              caretPosNew +=
                maskDigitsBeforeCaretAll - maskDigitsBeforeCaretAllOld;
              caretPosNew += maskDigitsAfterCaret;
            }
          }
          return caretPosNew;
        },
        behaviour: function (e) {
          e = e || window.event;
          p.invalid = [];

          var keyCode = el.data("mask-keycode");

          if ($.inArray(keyCode, jMask.byPassKeys) === -1) {
            var newVal = p.getMasked(),
              caretPos = p.getCaret();

            // this is a compensation to devices/browsers that don't compensate
            // caret positioning the right way
            setTimeout(function () {
              p.setCaret(p.calculateCaretPosition());
            }, $.jMaskGlobals.keyStrokeCompensation);

            p.val(newVal);
            p.setCaret(caretPos);
            return p.callbacks(e);
          }
        },
        getMasked: function (skipMaskChars, val) {
          var buf = [],
            value = val === undefined ? p.val() : val + "",
            m = 0,
            maskLen = mask.length,
            v = 0,
            valLen = value.length,
            offset = 1,
            addMethod = "push",
            resetPos = -1,
            maskDigitCount = 0,
            maskDigitPosArr = [],
            lastMaskChar,
            check;

          if (options.reverse) {
            addMethod = "unshift";
            offset = -1;
            lastMaskChar = 0;
            m = maskLen - 1;
            v = valLen - 1;
            check = function () {
              return m > -1 && v > -1;
            };
          } else {
            lastMaskChar = maskLen - 1;
            check = function () {
              return m < maskLen && v < valLen;
            };
          }

          var lastUntranslatedMaskChar;
          while (check()) {
            var maskDigit = mask.charAt(m),
              valDigit = value.charAt(v),
              translation = jMask.translation[maskDigit];

            if (translation) {
              if (valDigit.match(translation.pattern)) {
                buf[addMethod](valDigit);
                if (translation.recursive) {
                  if (resetPos === -1) {
                    resetPos = m;
                  } else if (m === lastMaskChar && m !== resetPos) {
                    m = resetPos - offset;
                  }

                  if (lastMaskChar === resetPos) {
                    m -= offset;
                  }
                }
                m += offset;
              } else if (valDigit === lastUntranslatedMaskChar) {
                // matched the last untranslated (raw) mask character that we encountered
                // likely an insert offset the mask character from the last entry; fall
                // through and only increment v
                maskDigitCount--;
                lastUntranslatedMaskChar = undefined;
              } else if (translation.optional) {
                m += offset;
                v -= offset;
              } else if (translation.fallback) {
                buf[addMethod](translation.fallback);
                m += offset;
                v -= offset;
              } else {
                p.invalid.push({ p: v, v: valDigit, e: translation.pattern });
              }
              v += offset;
            } else {
              if (!skipMaskChars) {
                buf[addMethod](maskDigit);
              }

              if (valDigit === maskDigit) {
                maskDigitPosArr.push(v);
                v += offset;
              } else {
                lastUntranslatedMaskChar = maskDigit;
                maskDigitPosArr.push(v + maskDigitCount);
                maskDigitCount++;
              }

              m += offset;
            }
          }

          var lastMaskCharDigit = mask.charAt(lastMaskChar);
          if (maskLen === valLen + 1 && !jMask.translation[lastMaskCharDigit]) {
            buf.push(lastMaskCharDigit);
          }

          var newVal = buf.join("");
          p.mapMaskdigitPositions(newVal, maskDigitPosArr, valLen);
          return newVal;
        },
        mapMaskdigitPositions: function (newVal, maskDigitPosArr, valLen) {
          var maskDiff = options.reverse ? newVal.length - valLen : 0;
          p.maskDigitPosMap = {};
          for (var i = 0; i < maskDigitPosArr.length; i++) {
            p.maskDigitPosMap[maskDigitPosArr[i] + maskDiff] = 1;
          }
        },
        callbacks: function (e) {
          var val = p.val(),
            changed = val !== oldValue,
            defaultArgs = [val, e, el, options],
            callback = function (name, criteria, args) {
              if (typeof options[name] === "function" && criteria) {
                options[name].apply(this, args);
              }
            };

          callback("onChange", changed === true, defaultArgs);
          callback("onKeyPress", changed === true, defaultArgs);
          callback("onComplete", val.length === mask.length, defaultArgs);
          callback("onInvalid", p.invalid.length > 0, [
            val,
            e,
            el,
            p.invalid,
            options,
          ]);
        },
      };

      el = $(el);
      var jMask = this,
        oldValue = p.val(),
        regexMask;

      mask =
        typeof mask === "function"
          ? mask(p.val(), undefined, el, options)
          : mask;

      // public methods
      jMask.mask = mask;
      jMask.options = options;
      jMask.remove = function () {
        var caret = p.getCaret();
        if (jMask.options.placeholder) {
          el.removeAttr("placeholder");
        }
        if (el.data("mask-maxlength")) {
          el.removeAttr("maxlength");
        }
        p.destroyEvents();
        p.val(jMask.getCleanVal());
        p.setCaret(caret);
        return el;
      };

      // get value without mask
      jMask.getCleanVal = function () {
        return p.getMasked(true);
      };

      // get masked value without the value being in the input or element
      jMask.getMaskedVal = function (val) {
        return p.getMasked(false, val);
      };

      jMask.init = function (onlyMask) {
        onlyMask = onlyMask || false;
        options = options || {};

        jMask.clearIfNotMatch = $.jMaskGlobals.clearIfNotMatch;
        jMask.byPassKeys = $.jMaskGlobals.byPassKeys;
        jMask.translation = $.extend(
          {},
          $.jMaskGlobals.translation,
          options.translation
        );

        jMask = $.extend(true, {}, jMask, options);

        regexMask = p.getRegexMask();

        if (onlyMask) {
          p.events();
          p.val(p.getMasked());
        } else {
          if (options.placeholder) {
            el.attr("placeholder", options.placeholder);
          }

          // this is necessary, otherwise if the user submit the form
          // and then press the "back" button, the autocomplete will erase
          // the data. Works fine on IE9+, FF, Opera, Safari.
          if (el.data("mask")) {
            el.attr("autocomplete", "off");
          }

          // detect if is necessary let the user type freely.
          // for is a lot faster than forEach.
          for (var i = 0, maxlength = true; i < mask.length; i++) {
            var translation = jMask.translation[mask.charAt(i)];
            if (translation && translation.recursive) {
              maxlength = false;
              break;
            }
          }

          if (maxlength) {
            el.attr("maxlength", mask.length).data("mask-maxlength", true);
          }

          p.destroyEvents();
          p.events();

          var caret = p.getCaret();
          p.val(p.getMasked());
          p.setCaret(caret);
        }
      };

      jMask.init(!el.is("input"));
    };

    $.maskWatchers = {};
    var HTMLAttributes = function () {
        var input = $(this),
          options = {},
          prefix = "data-mask-",
          mask = input.attr("data-mask");

        if (input.attr(prefix + "reverse")) {
          options.reverse = true;
        }

        if (input.attr(prefix + "clearifnotmatch")) {
          options.clearIfNotMatch = true;
        }

        if (input.attr(prefix + "selectonfocus") === "true") {
          options.selectOnFocus = true;
        }

        if (notSameMaskObject(input, mask, options)) {
          return input.data("mask", new Mask(this, mask, options));
        }
      },
      notSameMaskObject = function (field, mask, options) {
        options = options || {};
        var maskObject = $(field).data("mask"),
          stringify = JSON.stringify,
          value = $(field).val() || $(field).text();
        try {
          if (typeof mask === "function") {
            mask = mask(value);
          }
          return (
            typeof maskObject !== "object" ||
            stringify(maskObject.options) !== stringify(options) ||
            maskObject.mask !== mask
          );
        } catch (e) {}
      },
      eventSupported = function (eventName) {
        var el = document.createElement("div"),
          isSupported;

        eventName = "on" + eventName;
        isSupported = eventName in el;

        if (!isSupported) {
          el.setAttribute(eventName, "return;");
          isSupported = typeof el[eventName] === "function";
        }
        el = null;

        return isSupported;
      };

    $.fn.mask = function (mask, options) {
      options = options || {};
      var selector = this.selector,
        globals = $.jMaskGlobals,
        interval = globals.watchInterval,
        watchInputs = options.watchInputs || globals.watchInputs,
        maskFunction = function () {
          if (notSameMaskObject(this, mask, options)) {
            return $(this).data("mask", new Mask(this, mask, options));
          }
        };

      $(this).each(maskFunction);

      if (selector && selector !== "" && watchInputs) {
        clearInterval($.maskWatchers[selector]);
        $.maskWatchers[selector] = setInterval(function () {
          $(document).find(selector).each(maskFunction);
        }, interval);
      }
      return this;
    };

    $.fn.masked = function (val) {
      return this.data("mask").getMaskedVal(val);
    };

    $.fn.unmask = function () {
      clearInterval($.maskWatchers[this.selector]);
      delete $.maskWatchers[this.selector];
      return this.each(function () {
        var dataMask = $(this).data("mask");
        if (dataMask) {
          dataMask.remove().removeData("mask");
        }
      });
    };

    $.fn.cleanVal = function () {
      return this.data("mask").getCleanVal();
    };

    $.applyDataMask = function (selector) {
      selector = selector || $.jMaskGlobals.maskElements;
      var $selector = selector instanceof $ ? selector : $(selector);
      $selector.filter($.jMaskGlobals.dataMaskAttr).each(HTMLAttributes);
    };

    var globals = {
      maskElements: "input,td,span,div",
      dataMaskAttr: "*[data-mask]",
      dataMask: true,
      watchInterval: 300,
      watchInputs: true,
      keyStrokeCompensation: 10,
      // old versions of chrome dont work great with input event
      useInput:
        !/Chrome\/[2-4][0-9]|SamsungBrowser/.test(window.navigator.userAgent) &&
        eventSupported("input"),
      watchDataMask: false,
      byPassKeys: [9, 16, 17, 18, 36, 37, 38, 39, 40, 91],
      translation: {
        0: { pattern: /\d/ },
        9: { pattern: /\d/, optional: true },
        "#": { pattern: /\d/, recursive: true },
        A: { pattern: /[a-zA-Z0-9]/ },
        S: { pattern: /[a-zA-Z]/ },
      },
    };

    $.jMaskGlobals = $.jMaskGlobals || {};
    globals = $.jMaskGlobals = $.extend(true, {}, globals, $.jMaskGlobals);

    // looking for inputs with data-mask attribute
    if (globals.dataMask) {
      $.applyDataMask();
    }

    setInterval(function () {
      if ($.jMaskGlobals.watchDataMask) {
        $.applyDataMask();
      }
    }, globals.watchInterval);
  },
  window.jQuery,
  window.Zepto
);

typeof navigator === "object" &&
  (function (global, factory) {
    typeof exports === "object" && typeof module !== "undefined"
      ? (module.exports = factory())
      : typeof define === "function" && define.amd
      ? define("Plyr", factory)
      : (global.Plyr = factory());
  })(this, function () {
    "use strict";

    // Polyfill for creating CustomEvents on IE9/10/11

    // code pulled from:
    // https://github.com/d4tocchini/customevent-polyfill
    // https://developer.mozilla.org/en-US/docs/Web/API/CustomEvent#Polyfill

    (function () {
      if (typeof window === "undefined") {
        return;
      }

      try {
        var ce = new window.CustomEvent("test", { cancelable: true });
        ce.preventDefault();
        if (ce.defaultPrevented !== true) {
          // IE has problems with .preventDefault() on custom events
          // http://stackoverflow.com/questions/23349191
          throw new Error("Could not prevent default");
        }
      } catch (e) {
        var CustomEvent = function (event, params) {
          var evt, origPrevent;
          params = params || {
            bubbles: false,
            cancelable: false,
            detail: undefined,
          };

          evt = document.createEvent("CustomEvent");
          evt.initCustomEvent(
            event,
            params.bubbles,
            params.cancelable,
            params.detail
          );
          origPrevent = evt.preventDefault;
          evt.preventDefault = function () {
            origPrevent.call(this);
            try {
              Object.defineProperty(this, "defaultPrevented", {
                get: function () {
                  return true;
                },
              });
            } catch (e) {
              this.defaultPrevented = true;
            }
          };
          return evt;
        };

        CustomEvent.prototype = window.Event.prototype;
        window.CustomEvent = CustomEvent; // expose definition to window
      }
    })();

    var commonjsGlobal =
      typeof window !== "undefined"
        ? window
        : typeof global !== "undefined"
        ? global
        : typeof self !== "undefined"
        ? self
        : {};

    function createCommonjsModule(fn, module) {
      return (
        (module = { exports: {} }), fn(module, module.exports), module.exports
      );
    }

    (function (global) {
      /**
       * Polyfill URLSearchParams
       *
       * Inspired from : https://github.com/WebReflection/url-search-params/blob/master/src/url-search-params.js
       */

      var checkIfIteratorIsSupported = function () {
        try {
          return !!Symbol.iterator;
        } catch (error) {
          return false;
        }
      };

      var iteratorSupported = checkIfIteratorIsSupported();

      var createIterator = function (items) {
        var iterator = {
          next: function () {
            var value = items.shift();
            return { done: value === void 0, value: value };
          },
        };

        if (iteratorSupported) {
          iterator[Symbol.iterator] = function () {
            return iterator;
          };
        }

        return iterator;
      };

      /**
       * Search param name and values should be encoded according to https://url.spec.whatwg.org/#urlencoded-serializing
       * encodeURIComponent() produces the same result except encoding spaces as `%20` instead of `+`.
       */
      var serializeParam = function (value) {
        return encodeURIComponent(value).replace(/%20/g, "+");
      };

      var deserializeParam = function (value) {
        return decodeURIComponent(value).replace(/\+/g, " ");
      };

      var polyfillURLSearchParams = function () {
        var URLSearchParams = function (searchString) {
          Object.defineProperty(this, "_entries", {
            writable: true,
            value: {},
          });

          if (typeof searchString === "string") {
            if (searchString !== "") {
              this._fromString(searchString);
            }
          } else if (searchString instanceof URLSearchParams) {
            var _this = this;
            searchString.forEach(function (value, name) {
              _this.append(name, value);
            });
          }
        };

        var proto = URLSearchParams.prototype;

        proto.append = function (name, value) {
          if (name in this._entries) {
            this._entries[name].push(value.toString());
          } else {
            this._entries[name] = [value.toString()];
          }
        };

        proto.delete = function (name) {
          delete this._entries[name];
        };

        proto.get = function (name) {
          return name in this._entries ? this._entries[name][0] : null;
        };

        proto.getAll = function (name) {
          return name in this._entries ? this._entries[name].slice(0) : [];
        };

        proto.has = function (name) {
          return name in this._entries;
        };

        proto.set = function (name, value) {
          this._entries[name] = [value.toString()];
        };

        proto.forEach = function (callback, thisArg) {
          var entries;
          for (var name in this._entries) {
            if (this._entries.hasOwnProperty(name)) {
              entries = this._entries[name];
              for (var i = 0; i < entries.length; i++) {
                callback.call(thisArg, entries[i], name, this);
              }
            }
          }
        };

        proto.keys = function () {
          var items = [];
          this.forEach(function (value, name) {
            items.push(name);
          });
          return createIterator(items);
        };

        proto.values = function () {
          var items = [];
          this.forEach(function (value) {
            items.push(value);
          });
          return createIterator(items);
        };

        proto.entries = function () {
          var items = [];
          this.forEach(function (value, name) {
            items.push([name, value]);
          });
          return createIterator(items);
        };

        if (iteratorSupported) {
          proto[Symbol.iterator] = proto.entries;
        }

        proto.toString = function () {
          var searchArray = [];
          this.forEach(function (value, name) {
            searchArray.push(
              serializeParam(name) + "=" + serializeParam(value)
            );
          });
          return searchArray.join("&");
        };

        Object.defineProperty(proto, "_fromString", {
          enumerable: false,
          configurable: false,
          writable: false,
          value: function (searchString) {
            this._entries = {};
            searchString = searchString.replace(/^\?/, "");
            var attributes = searchString.split("&");
            var attribute;
            for (var i = 0; i < attributes.length; i++) {
              attribute = attributes[i].split("=");
              this.append(
                deserializeParam(attribute[0]),
                attribute.length > 1 ? deserializeParam(attribute[1]) : ""
              );
            }
          },
        });

        global.URLSearchParams = URLSearchParams;
      };

      if (
        !("URLSearchParams" in global) ||
        new URLSearchParams("?a=1").toString() !== "a=1"
      ) {
        polyfillURLSearchParams();
      }

      if (typeof URLSearchParams.prototype.sort !== "function") {
        URLSearchParams.prototype.sort = function () {
          var _this = this;
          var items = [];
          this.forEach(function (value, name) {
            items.push([name, value]);
            if (!_this._entries) {
              _this.delete(name);
            }
          });
          items.sort(function (a, b) {
            if (a[0] < b[0]) {
              return -1;
            } else if (a[0] > b[0]) {
              return +1;
            } else {
              return 0;
            }
          });
          if (_this._entries) {
            // force reset because IE keeps keys index
            _this._entries = {};
          }
          for (var i = 0; i < items.length; i++) {
            this.append(items[i][0], items[i][1]);
          }
        };
      }

      // HTMLAnchorElement
    })(
      typeof commonjsGlobal !== "undefined"
        ? commonjsGlobal
        : typeof window !== "undefined"
        ? window
        : typeof self !== "undefined"
        ? self
        : commonjsGlobal
    );

    (function (global) {
      /**
       * Polyfill URL
       *
       * Inspired from : https://github.com/arv/DOM-URL-Polyfill/blob/master/src/url.js
       */

      var checkIfURLIsSupported = function () {
        try {
          var u = new URL("b", "http://a");
          u.pathname = "c%20d";
          return u.href === "http://a/c%20d" && u.searchParams;
        } catch (e) {
          return false;
        }
      };

      var polyfillURL = function () {
        var _URL = global.URL;

        var URL = function (url, base) {
          if (typeof url !== "string") url = String(url);

          // Only create another document if the base is different from current location.
          var doc = document,
            baseElement;
          if (
            base &&
            (global.location === void 0 || base !== global.location.href)
          ) {
            doc = document.implementation.createHTMLDocument("");
            baseElement = doc.createElement("base");
            baseElement.href = base;
            doc.head.appendChild(baseElement);
            try {
              if (baseElement.href.indexOf(base) !== 0)
                throw new Error(baseElement.href);
            } catch (err) {
              throw new Error(
                "URL unable to set base " + base + " due to " + err
              );
            }
          }

          var anchorElement = doc.createElement("a");
          anchorElement.href = url;
          if (baseElement) {
            doc.body.appendChild(anchorElement);
            anchorElement.href = anchorElement.href; // force href to refresh
          }

          if (anchorElement.protocol === ":" || !/:/.test(anchorElement.href)) {
            throw new TypeError("Invalid URL");
          }

          Object.defineProperty(this, "_anchorElement", {
            value: anchorElement,
          });

          // create a linked searchParams which reflect its changes on URL
          var searchParams = new URLSearchParams(this.search);
          var enableSearchUpdate = true;
          var enableSearchParamsUpdate = true;
          var _this = this;
          ["append", "delete", "set"].forEach(function (methodName) {
            var method = searchParams[methodName];
            searchParams[methodName] = function () {
              method.apply(searchParams, arguments);
              if (enableSearchUpdate) {
                enableSearchParamsUpdate = false;
                _this.search = searchParams.toString();
                enableSearchParamsUpdate = true;
              }
            };
          });

          Object.defineProperty(this, "searchParams", {
            value: searchParams,
            enumerable: true,
          });

          var search = void 0;
          Object.defineProperty(this, "_updateSearchParams", {
            enumerable: false,
            configurable: false,
            writable: false,
            value: function () {
              if (this.search !== search) {
                search = this.search;
                if (enableSearchParamsUpdate) {
                  enableSearchUpdate = false;
                  this.searchParams._fromString(this.search);
                  enableSearchUpdate = true;
                }
              }
            },
          });
        };

        var proto = URL.prototype;

        var linkURLWithAnchorAttribute = function (attributeName) {
          Object.defineProperty(proto, attributeName, {
            get: function () {
              return this._anchorElement[attributeName];
            },
            set: function (value) {
              this._anchorElement[attributeName] = value;
            },
            enumerable: true,
          });
        };

        ["hash", "host", "hostname", "port", "protocol"].forEach(function (
          attributeName
        ) {
          linkURLWithAnchorAttribute(attributeName);
        });

        Object.defineProperty(proto, "search", {
          get: function () {
            return this._anchorElement["search"];
          },
          set: function (value) {
            this._anchorElement["search"] = value;
            this._updateSearchParams();
          },
          enumerable: true,
        });

        Object.defineProperties(proto, {
          toString: {
            get: function () {
              var _this = this;
              return function () {
                return _this.href;
              };
            },
          },

          href: {
            get: function () {
              return this._anchorElement.href.replace(/\?$/, "");
            },
            set: function (value) {
              this._anchorElement.href = value;
              this._updateSearchParams();
            },
            enumerable: true,
          },

          pathname: {
            get: function () {
              return this._anchorElement.pathname.replace(/(^\/?)/, "/");
            },
            set: function (value) {
              this._anchorElement.pathname = value;
            },
            enumerable: true,
          },

          origin: {
            get: function () {
              // get expected port from protocol
              var expectedPort = { "http:": 80, "https:": 443, "ftp:": 21 }[
                this._anchorElement.protocol
              ];
              // add port to origin if, expected port is different than actual port
              // and it is not empty f.e http://foo:8080
              // 8080 != 80 && 8080 != ''
              var addPortToOrigin =
                this._anchorElement.port != expectedPort &&
                this._anchorElement.port !== "";

              return (
                this._anchorElement.protocol +
                "//" +
                this._anchorElement.hostname +
                (addPortToOrigin ? ":" + this._anchorElement.port : "")
              );
            },
            enumerable: true,
          },

          password: {
            // TODO
            get: function () {
              return "";
            },
            set: function (value) {},
            enumerable: true,
          },

          username: {
            // TODO
            get: function () {
              return "";
            },
            set: function (value) {},
            enumerable: true,
          },
        });

        URL.createObjectURL = function (blob) {
          return _URL.createObjectURL.apply(_URL, arguments);
        };

        URL.revokeObjectURL = function (url) {
          return _URL.revokeObjectURL.apply(_URL, arguments);
        };

        global.URL = URL;
      };

      if (!checkIfURLIsSupported()) {
        polyfillURL();
      }

      if (global.location !== void 0 && !("origin" in global.location)) {
        var getOrigin = function () {
          return (
            global.location.protocol +
            "//" +
            global.location.hostname +
            (global.location.port ? ":" + global.location.port : "")
          );
        };

        try {
          Object.defineProperty(global.location, "origin", {
            get: getOrigin,
            enumerable: true,
          });
        } catch (e) {
          setInterval(function () {
            global.location.origin = getOrigin();
          }, 100);
        }
      }
    })(
      typeof commonjsGlobal !== "undefined"
        ? commonjsGlobal
        : typeof window !== "undefined"
        ? window
        : typeof self !== "undefined"
        ? self
        : commonjsGlobal
    );

    var _aFunction = function (it) {
      if (typeof it != "function") throw TypeError(it + " is not a function!");
      return it;
    };

    // optional / simple context binding

    var _ctx = function (fn, that, length) {
      _aFunction(fn);
      if (that === undefined) return fn;
      switch (length) {
        case 1:
          return function (a) {
            return fn.call(that, a);
          };
        case 2:
          return function (a, b) {
            return fn.call(that, a, b);
          };
        case 3:
          return function (a, b, c) {
            return fn.call(that, a, b, c);
          };
      }
      return function (/* ...args */) {
        return fn.apply(that, arguments);
      };
    };

    var _global = createCommonjsModule(function (module) {
      // https://github.com/zloirock/core-js/issues/86#issuecomment-115759028
      var global = (module.exports =
        typeof window != "undefined" && window.Math == Math
          ? window
          : typeof self != "undefined" && self.Math == Math
          ? self
          : // eslint-disable-next-line no-new-func
            Function("return this")());
      if (typeof __g == "number") __g = global; // eslint-disable-line no-undef
    });

    var _core = createCommonjsModule(function (module) {
      var core = (module.exports = { version: "2.5.7" });
      if (typeof __e == "number") __e = core; // eslint-disable-line no-undef
    });
    var _core_1 = _core.version;

    var _isObject = function (it) {
      return typeof it === "object" ? it !== null : typeof it === "function";
    };

    var _anObject = function (it) {
      if (!_isObject(it)) throw TypeError(it + " is not an object!");
      return it;
    };

    var _fails = function (exec) {
      try {
        return !!exec();
      } catch (e) {
        return true;
      }
    };

    // Thank's IE8 for his funny defineProperty
    var _descriptors = !_fails(function () {
      return (
        Object.defineProperty({}, "a", {
          get: function () {
            return 7;
          },
        }).a != 7
      );
    });

    var document$1 = _global.document;
    // typeof document.createElement is 'object' in old IE
    var is = _isObject(document$1) && _isObject(document$1.createElement);
    var _domCreate = function (it) {
      return is ? document$1.createElement(it) : {};
    };

    var _ie8DomDefine =
      !_descriptors &&
      !_fails(function () {
        return (
          Object.defineProperty(_domCreate("div"), "a", {
            get: function () {
              return 7;
            },
          }).a != 7
        );
      });

    // 7.1.1 ToPrimitive(input [, PreferredType])

    // instead of the ES6 spec version, we didn't implement @@toPrimitive case
    // and the second argument - flag - preferred type is a string
    var _toPrimitive = function (it, S) {
      if (!_isObject(it)) return it;
      var fn, val;
      if (
        S &&
        typeof (fn = it.toString) == "function" &&
        !_isObject((val = fn.call(it)))
      )
        return val;
      if (
        typeof (fn = it.valueOf) == "function" &&
        !_isObject((val = fn.call(it)))
      )
        return val;
      if (
        !S &&
        typeof (fn = it.toString) == "function" &&
        !_isObject((val = fn.call(it)))
      )
        return val;
      throw TypeError("Can't convert object to primitive value");
    };

    var dP = Object.defineProperty;

    var f = _descriptors
      ? Object.defineProperty
      : function defineProperty(O, P, Attributes) {
          _anObject(O);
          P = _toPrimitive(P, true);
          _anObject(Attributes);
          if (_ie8DomDefine)
            try {
              return dP(O, P, Attributes);
            } catch (e) {
              /* empty */
            }
          if ("get" in Attributes || "set" in Attributes)
            throw TypeError("Accessors not supported!");
          if ("value" in Attributes) O[P] = Attributes.value;
          return O;
        };

    var _objectDp = {
      f: f,
    };

    var _propertyDesc = function (bitmap, value) {
      return {
        enumerable: !(bitmap & 1),
        configurable: !(bitmap & 2),
        writable: !(bitmap & 4),
        value: value,
      };
    };

    var _hide = _descriptors
      ? function (object, key, value) {
          return _objectDp.f(object, key, _propertyDesc(1, value));
        }
      : function (object, key, value) {
          object[key] = value;
          return object;
        };

    var hasOwnProperty = {}.hasOwnProperty;
    var _has = function (it, key) {
      return hasOwnProperty.call(it, key);
    };

    var id = 0;
    var px = Math.random();
    var _uid = function (key) {
      return "Symbol(".concat(
        key === undefined ? "" : key,
        ")_",
        (++id + px).toString(36)
      );
    };

    var _redefine = createCommonjsModule(function (module) {
      var SRC = _uid("src");
      var TO_STRING = "toString";
      var $toString = Function[TO_STRING];
      var TPL = ("" + $toString).split(TO_STRING);

      _core.inspectSource = function (it) {
        return $toString.call(it);
      };

      (module.exports = function (O, key, val, safe) {
        var isFunction = typeof val == "function";
        if (isFunction) _has(val, "name") || _hide(val, "name", key);
        if (O[key] === val) return;
        if (isFunction)
          _has(val, SRC) ||
            _hide(val, SRC, O[key] ? "" + O[key] : TPL.join(String(key)));
        if (O === _global) {
          O[key] = val;
        } else if (!safe) {
          delete O[key];
          _hide(O, key, val);
        } else if (O[key]) {
          O[key] = val;
        } else {
          _hide(O, key, val);
        }
        // add fake Function#toString for correct work wrapped methods / constructors with methods like LoDash isNative
      })(Function.prototype, TO_STRING, function toString() {
        return (typeof this == "function" && this[SRC]) || $toString.call(this);
      });
    });

    var PROTOTYPE = "prototype";

    var $export = function (type, name, source) {
      var IS_FORCED = type & $export.F;
      var IS_GLOBAL = type & $export.G;
      var IS_STATIC = type & $export.S;
      var IS_PROTO = type & $export.P;
      var IS_BIND = type & $export.B;
      var target = IS_GLOBAL
        ? _global
        : IS_STATIC
        ? _global[name] || (_global[name] = {})
        : (_global[name] || {})[PROTOTYPE];
      var exports = IS_GLOBAL ? _core : _core[name] || (_core[name] = {});
      var expProto = exports[PROTOTYPE] || (exports[PROTOTYPE] = {});
      var key, own, out, exp;
      if (IS_GLOBAL) source = name;
      for (key in source) {
        // contains in native
        own = !IS_FORCED && target && target[key] !== undefined;
        // export native or passed
        out = (own ? target : source)[key];
        // bind timers to global for call from export context
        exp =
          IS_BIND && own
            ? _ctx(out, _global)
            : IS_PROTO && typeof out == "function"
            ? _ctx(Function.call, out)
            : out;
        // extend global
        if (target) _redefine(target, key, out, type & $export.U);
        // export
        if (exports[key] != out) _hide(exports, key, exp);
        if (IS_PROTO && expProto[key] != out) expProto[key] = out;
      }
    };
    _global.core = _core;
    // type bitmap
    $export.F = 1; // forced
    $export.G = 2; // global
    $export.S = 4; // static
    $export.P = 8; // proto
    $export.B = 16; // bind
    $export.W = 32; // wrap
    $export.U = 64; // safe
    $export.R = 128; // real proto method for `library`
    var _export = $export;

    // 7.2.1 RequireObjectCoercible(argument)
    var _defined = function (it) {
      if (it == undefined) throw TypeError("Can't call method on  " + it);
      return it;
    };

    // 7.1.13 ToObject(argument)

    var _toObject = function (it) {
      return Object(_defined(it));
    };

    // call something on iterator step with safe closing on error

    var _iterCall = function (iterator, fn, value, entries) {
      try {
        return entries ? fn(_anObject(value)[0], value[1]) : fn(value);
        // 7.4.6 IteratorClose(iterator, completion)
      } catch (e) {
        var ret = iterator["return"];
        if (ret !== undefined) _anObject(ret.call(iterator));
        throw e;
      }
    };

    var _iterators = {};

    var _library = false;

    var _shared = createCommonjsModule(function (module) {
      var SHARED = "__core-js_shared__";
      var store = _global[SHARED] || (_global[SHARED] = {});

      (module.exports = function (key, value) {
        return store[key] || (store[key] = value !== undefined ? value : {});
      })("versions", []).push({
        version: _core.version,
        mode: "global",
        copyright: "© 2018 Denis Pushkarev (zloirock.ru)",
      });
    });

    var _wks = createCommonjsModule(function (module) {
      var store = _shared("wks");

      var Symbol = _global.Symbol;
      var USE_SYMBOL = typeof Symbol == "function";

      var $exports = (module.exports = function (name) {
        return (
          store[name] ||
          (store[name] =
            (USE_SYMBOL && Symbol[name]) ||
            (USE_SYMBOL ? Symbol : _uid)("Symbol." + name))
        );
      });

      $exports.store = store;
    });

    // check on default Array iterator

    var ITERATOR = _wks("iterator");
    var ArrayProto = Array.prototype;

    var _isArrayIter = function (it) {
      return (
        it !== undefined &&
        (_iterators.Array === it || ArrayProto[ITERATOR] === it)
      );
    };

    // 7.1.4 ToInteger
    var ceil = Math.ceil;
    var floor = Math.floor;
    var _toInteger = function (it) {
      return isNaN((it = +it)) ? 0 : (it > 0 ? floor : ceil)(it);
    };

    // 7.1.15 ToLength

    var min = Math.min;
    var _toLength = function (it) {
      return it > 0 ? min(_toInteger(it), 0x1fffffffffffff) : 0; // pow(2, 53) - 1 == 9007199254740991
    };

    var _createProperty = function (object, index, value) {
      if (index in object) _objectDp.f(object, index, _propertyDesc(0, value));
      else object[index] = value;
    };

    var toString = {}.toString;

    var _cof = function (it) {
      return toString.call(it).slice(8, -1);
    };

    // getting tag from 19.1.3.6 Object.prototype.toString()

    var TAG = _wks("toStringTag");
    // ES3 wrong here
    var ARG =
      _cof(
        (function () {
          return arguments;
        })()
      ) == "Arguments";

    // fallback for IE11 Script Access Denied error
    var tryGet = function (it, key) {
      try {
        return it[key];
      } catch (e) {
        /* empty */
      }
    };

    var _classof = function (it) {
      var O, T, B;
      return it === undefined
        ? "Undefined"
        : it === null
        ? "Null"
        : // @@toStringTag case
        typeof (T = tryGet((O = Object(it)), TAG)) == "string"
        ? T
        : // builtinTag case
        ARG
        ? _cof(O)
        : // ES3 arguments fallback
        (B = _cof(O)) == "Object" && typeof O.callee == "function"
        ? "Arguments"
        : B;
    };

    var ITERATOR$1 = _wks("iterator");

    var core_getIteratorMethod = (_core.getIteratorMethod = function (it) {
      if (it != undefined)
        return it[ITERATOR$1] || it["@@iterator"] || _iterators[_classof(it)];
    });

    var ITERATOR$2 = _wks("iterator");
    var SAFE_CLOSING = false;

    try {
      var riter = [7][ITERATOR$2]();
      riter["return"] = function () {
        SAFE_CLOSING = true;
      };
    } catch (e) {
      /* empty */
    }

    var _iterDetect = function (exec, skipClosing) {
      if (!skipClosing && !SAFE_CLOSING) return false;
      var safe = false;
      try {
        var arr = [7];
        var iter = arr[ITERATOR$2]();
        iter.next = function () {
          return { done: (safe = true) };
        };
        arr[ITERATOR$2] = function () {
          return iter;
        };
        exec(arr);
      } catch (e) {
        /* empty */
      }
      return safe;
    };

    _export(_export.S + _export.F * !_iterDetect(function (iter) {}), "Array", {
      // 22.1.2.1 Array.from(arrayLike, mapfn = undefined, thisArg = undefined)
      from: function from(
        arrayLike /* , mapfn = undefined, thisArg = undefined */
      ) {
        var O = _toObject(arrayLike);
        var C = typeof this == "function" ? this : Array;
        var aLen = arguments.length;
        var mapfn = aLen > 1 ? arguments[1] : undefined;
        var mapping = mapfn !== undefined;
        var index = 0;
        var iterFn = core_getIteratorMethod(O);
        var length, result, step, iterator;
        if (mapping)
          mapfn = _ctx(mapfn, aLen > 2 ? arguments[2] : undefined, 2);
        // if object isn't iterable or it's array with default iterator - use simple case
        if (iterFn != undefined && !(C == Array && _isArrayIter(iterFn))) {
          for (
            iterator = iterFn.call(O), result = new C();
            !(step = iterator.next()).done;
            index++
          ) {
            _createProperty(
              result,
              index,
              mapping
                ? _iterCall(iterator, mapfn, [step.value, index], true)
                : step.value
            );
          }
        } else {
          length = _toLength(O.length);
          for (result = new C(length); length > index; index++) {
            _createProperty(
              result,
              index,
              mapping ? mapfn(O[index], index) : O[index]
            );
          }
        }
        result.length = index;
        return result;
      },
    });

    // fallback for non-array-like ES3 and non-enumerable old V8 strings

    // eslint-disable-next-line no-prototype-builtins
    var _iobject = Object("z").propertyIsEnumerable(0)
      ? Object
      : function (it) {
          return _cof(it) == "String" ? it.split("") : Object(it);
        };

    // 7.2.2 IsArray(argument)

    var _isArray =
      Array.isArray ||
      function isArray(arg) {
        return _cof(arg) == "Array";
      };

    var SPECIES = _wks("species");

    var _arraySpeciesConstructor = function (original) {
      var C;
      if (_isArray(original)) {
        C = original.constructor;
        // cross-realm fallback
        if (typeof C == "function" && (C === Array || _isArray(C.prototype)))
          C = undefined;
        if (_isObject(C)) {
          C = C[SPECIES];
          if (C === null) C = undefined;
        }
      }
      return C === undefined ? Array : C;
    };

    // 9.4.2.3 ArraySpeciesCreate(originalArray, length)

    var _arraySpeciesCreate = function (original, length) {
      return new (_arraySpeciesConstructor(original))(length);
    };

    // 0 -> Array#forEach
    // 1 -> Array#map
    // 2 -> Array#filter
    // 3 -> Array#some
    // 4 -> Array#every
    // 5 -> Array#find
    // 6 -> Array#findIndex

    var _arrayMethods = function (TYPE, $create) {
      var IS_MAP = TYPE == 1;
      var IS_FILTER = TYPE == 2;
      var IS_SOME = TYPE == 3;
      var IS_EVERY = TYPE == 4;
      var IS_FIND_INDEX = TYPE == 6;
      var NO_HOLES = TYPE == 5 || IS_FIND_INDEX;
      var create = $create || _arraySpeciesCreate;
      return function ($this, callbackfn, that) {
        var O = _toObject($this);
        var self = _iobject(O);
        var f = _ctx(callbackfn, that, 3);
        var length = _toLength(self.length);
        var index = 0;
        var result = IS_MAP
          ? create($this, length)
          : IS_FILTER
          ? create($this, 0)
          : undefined;
        var val, res;
        for (; length > index; index++)
          if (NO_HOLES || index in self) {
            val = self[index];
            res = f(val, index, O);
            if (TYPE) {
              if (IS_MAP) result[index] = res;
              // map
              else if (res)
                switch (TYPE) {
                  case 3:
                    return true; // some
                  case 5:
                    return val; // find
                  case 6:
                    return index; // findIndex
                  case 2:
                    result.push(val); // filter
                }
              else if (IS_EVERY) return false; // every
            }
          }
        return IS_FIND_INDEX ? -1 : IS_SOME || IS_EVERY ? IS_EVERY : result;
      };
    };

    // 22.1.3.31 Array.prototype[@@unscopables]
    var UNSCOPABLES = _wks("unscopables");
    var ArrayProto$1 = Array.prototype;
    if (ArrayProto$1[UNSCOPABLES] == undefined)
      _hide(ArrayProto$1, UNSCOPABLES, {});
    var _addToUnscopables = function (key) {
      ArrayProto$1[UNSCOPABLES][key] = true;
    };

    // 22.1.3.8 Array.prototype.find(predicate, thisArg = undefined)

    var $find = _arrayMethods(5);
    var KEY = "find";
    var forced = true;
    // Shouldn't skip holes
    if (KEY in [])
      Array(1)[KEY](function () {
        forced = false;
      });
    _export(_export.P + _export.F * forced, "Array", {
      find: function find(callbackfn /* , that = undefined */) {
        return $find(
          this,
          callbackfn,
          arguments.length > 1 ? arguments[1] : undefined
        );
      },
    });
    _addToUnscopables(KEY);

    var f$1 = {}.propertyIsEnumerable;

    var _objectPie = {
      f: f$1,
    };

    // to indexed object, toObject with fallback for non-array-like ES3 strings

    var _toIobject = function (it) {
      return _iobject(_defined(it));
    };

    var gOPD = Object.getOwnPropertyDescriptor;

    var f$2 = _descriptors
      ? gOPD
      : function getOwnPropertyDescriptor(O, P) {
          O = _toIobject(O);
          P = _toPrimitive(P, true);
          if (_ie8DomDefine)
            try {
              return gOPD(O, P);
            } catch (e) {
              /* empty */
            }
          if (_has(O, P)) return _propertyDesc(!_objectPie.f.call(O, P), O[P]);
        };

    var _objectGopd = {
      f: f$2,
    };

    // Works with __proto__ only. Old v8 can't work with null proto objects.
    /* eslint-disable no-proto */

    var check = function (O, proto) {
      _anObject(O);
      if (!_isObject(proto) && proto !== null)
        throw TypeError(proto + ": can't set as prototype!");
    };
    var _setProto = {
      set:
        Object.setPrototypeOf ||
        ("__proto__" in {} // eslint-disable-line
          ? (function (test, buggy, set) {
              try {
                set = _ctx(
                  Function.call,
                  _objectGopd.f(Object.prototype, "__proto__").set,
                  2
                );
                set(test, []);
                buggy = !(test instanceof Array);
              } catch (e) {
                buggy = true;
              }
              return function setPrototypeOf(O, proto) {
                check(O, proto);
                if (buggy) O.__proto__ = proto;
                else set(O, proto);
                return O;
              };
            })({}, false)
          : undefined),
      check: check,
    };

    var setPrototypeOf = _setProto.set;
    var _inheritIfRequired = function (that, target, C) {
      var S = target.constructor;
      var P;
      if (
        S !== C &&
        typeof S == "function" &&
        (P = S.prototype) !== C.prototype &&
        _isObject(P) &&
        setPrototypeOf
      ) {
        setPrototypeOf(that, P);
      }
      return that;
    };

    var max = Math.max;
    var min$1 = Math.min;
    var _toAbsoluteIndex = function (index, length) {
      index = _toInteger(index);
      return index < 0 ? max(index + length, 0) : min$1(index, length);
    };

    // false -> Array#indexOf
    // true  -> Array#includes

    var _arrayIncludes = function (IS_INCLUDES) {
      return function ($this, el, fromIndex) {
        var O = _toIobject($this);
        var length = _toLength(O.length);
        var index = _toAbsoluteIndex(fromIndex, length);
        var value;
        // Array#includes uses SameValueZero equality algorithm
        // eslint-disable-next-line no-self-compare
        if (IS_INCLUDES && el != el)
          while (length > index) {
            value = O[index++];
            // eslint-disable-next-line no-self-compare
            if (value != value) return true;
            // Array#indexOf ignores holes, Array#includes - not
          }
        else
          for (; length > index; index++)
            if (IS_INCLUDES || index in O) {
              if (O[index] === el) return IS_INCLUDES || index || 0;
            }
        return !IS_INCLUDES && -1;
      };
    };

    var shared = _shared("keys");

    var _sharedKey = function (key) {
      return shared[key] || (shared[key] = _uid(key));
    };

    var arrayIndexOf = _arrayIncludes(false);
    var IE_PROTO = _sharedKey("IE_PROTO");

    var _objectKeysInternal = function (object, names) {
      var O = _toIobject(object);
      var i = 0;
      var result = [];
      var key;
      for (key in O) if (key != IE_PROTO) _has(O, key) && result.push(key);
      // Don't enum bug & hidden keys
      while (names.length > i)
        if (_has(O, (key = names[i++]))) {
          ~arrayIndexOf(result, key) || result.push(key);
        }
      return result;
    };

    // IE 8- don't enum bug keys
    var _enumBugKeys = "constructor,hasOwnProperty,isPrototypeOf,propertyIsEnumerable,toLocaleString,toString,valueOf".split(
      ","
    );

    // 19.1.2.7 / 15.2.3.4 Object.getOwnPropertyNames(O)

    var hiddenKeys = _enumBugKeys.concat("length", "prototype");

    var f$3 =
      Object.getOwnPropertyNames ||
      function getOwnPropertyNames(O) {
        return _objectKeysInternal(O, hiddenKeys);
      };

    var _objectGopn = {
      f: f$3,
    };

    var _stringWs =
      "\x09\x0A\x0B\x0C\x0D\x20\xA0\u1680\u180E\u2000\u2001\u2002\u2003" +
      "\u2004\u2005\u2006\u2007\u2008\u2009\u200A\u202F\u205F\u3000\u2028\u2029\uFEFF";

    var space = "[" + _stringWs + "]";
    var non = "\u200b\u0085";
    var ltrim = RegExp("^" + space + space + "*");
    var rtrim = RegExp(space + space + "*$");

    var exporter = function (KEY, exec, ALIAS) {
      var exp = {};
      var FORCE = _fails(function () {
        return !!_stringWs[KEY]() || non[KEY]() != non;
      });
      var fn = (exp[KEY] = FORCE ? exec(trim) : _stringWs[KEY]);
      if (ALIAS) exp[ALIAS] = fn;
      _export(_export.P + _export.F * FORCE, "String", exp);
    };

    // 1 -> String#trimLeft
    // 2 -> String#trimRight
    // 3 -> String#trim
    var trim = (exporter.trim = function (string, TYPE) {
      string = String(_defined(string));
      if (TYPE & 1) string = string.replace(ltrim, "");
      if (TYPE & 2) string = string.replace(rtrim, "");
      return string;
    });

    var _stringTrim = exporter;

    // 19.1.2.14 / 15.2.3.14 Object.keys(O)

    var _objectKeys =
      Object.keys ||
      function keys(O) {
        return _objectKeysInternal(O, _enumBugKeys);
      };

    var _objectDps = _descriptors
      ? Object.defineProperties
      : function defineProperties(O, Properties) {
          _anObject(O);
          var keys = _objectKeys(Properties);
          var length = keys.length;
          var i = 0;
          var P;
          while (length > i) _objectDp.f(O, (P = keys[i++]), Properties[P]);
          return O;
        };

    var document$2 = _global.document;
    var _html = document$2 && document$2.documentElement;

    // 19.1.2.2 / 15.2.3.5 Object.create(O [, Properties])

    var IE_PROTO$1 = _sharedKey("IE_PROTO");
    var Empty = function () {
      /* empty */
    };
    var PROTOTYPE$1 = "prototype";

    // Create object with fake `null` prototype: use iframe Object with cleared prototype
    var createDict = function () {
      // Thrash, waste and sodomy: IE GC bug
      var iframe = _domCreate("iframe");
      var i = _enumBugKeys.length;
      var lt = "<";
      var gt = ">";
      var iframeDocument;
      iframe.style.display = "none";
      _html.appendChild(iframe);
      iframe.src = "javascript:"; // eslint-disable-line no-script-url
      // createDict = iframe.contentWindow.Object;
      // html.removeChild(iframe);
      iframeDocument = iframe.contentWindow.document;
      iframeDocument.open();
      iframeDocument.write(
        lt + "script" + gt + "document.F=Object" + lt + "/script" + gt
      );
      iframeDocument.close();
      createDict = iframeDocument.F;
      while (i--) delete createDict[PROTOTYPE$1][_enumBugKeys[i]];
      return createDict();
    };

    var _objectCreate =
      Object.create ||
      function create(O, Properties) {
        var result;
        if (O !== null) {
          Empty[PROTOTYPE$1] = _anObject(O);
          result = new Empty();
          Empty[PROTOTYPE$1] = null;
          // add "__proto__" for Object.getPrototypeOf polyfill
          result[IE_PROTO$1] = O;
        } else result = createDict();
        return Properties === undefined
          ? result
          : _objectDps(result, Properties);
      };

    var gOPN = _objectGopn.f;
    var gOPD$1 = _objectGopd.f;
    var dP$1 = _objectDp.f;
    var $trim = _stringTrim.trim;
    var NUMBER = "Number";
    var $Number = _global[NUMBER];
    var Base = $Number;
    var proto = $Number.prototype;
    // Opera ~12 has broken Object#toString
    var BROKEN_COF = _cof(_objectCreate(proto)) == NUMBER;
    var TRIM = "trim" in String.prototype;

    // 7.1.3 ToNumber(argument)
    var toNumber = function (argument) {
      var it = _toPrimitive(argument, false);
      if (typeof it == "string" && it.length > 2) {
        it = TRIM ? it.trim() : $trim(it, 3);
        var first = it.charCodeAt(0);
        var third, radix, maxCode;
        if (first === 43 || first === 45) {
          third = it.charCodeAt(2);
          if (third === 88 || third === 120) return NaN; // Number('+0x1') should be NaN, old V8 fix
        } else if (first === 48) {
          switch (it.charCodeAt(1)) {
            case 66:
            case 98:
              radix = 2;
              maxCode = 49;
              break; // fast equal /^0b[01]+$/i
            case 79:
            case 111:
              radix = 8;
              maxCode = 55;
              break; // fast equal /^0o[0-7]+$/i
            default:
              return +it;
          }
          for (
            var digits = it.slice(2), i = 0, l = digits.length, code;
            i < l;
            i++
          ) {
            code = digits.charCodeAt(i);
            // parseInt parses a string to a first unavailable symbol
            // but ToNumber should return NaN if a string contains unavailable symbols
            if (code < 48 || code > maxCode) return NaN;
          }
          return parseInt(digits, radix);
        }
      }
      return +it;
    };

    if (!$Number(" 0o1") || !$Number("0b1") || $Number("+0x1")) {
      $Number = function Number(value) {
        var it = arguments.length < 1 ? 0 : value;
        var that = this;
        return that instanceof $Number &&
          // check on 1..constructor(foo) case
          (BROKEN_COF
            ? _fails(function () {
                proto.valueOf.call(that);
              })
            : _cof(that) != NUMBER)
          ? _inheritIfRequired(new Base(toNumber(it)), that, $Number)
          : toNumber(it);
      };
      for (
        var keys = _descriptors
            ? gOPN(Base)
            : // ES3:
              (
                "MAX_VALUE,MIN_VALUE,NaN,NEGATIVE_INFINITY,POSITIVE_INFINITY," +
                // ES6 (in case, if modules with ES6 Number statics required before):
                "EPSILON,isFinite,isInteger,isNaN,isSafeInteger,MAX_SAFE_INTEGER," +
                "MIN_SAFE_INTEGER,parseFloat,parseInt,isInteger"
              ).split(","),
          j = 0,
          key;
        keys.length > j;
        j++
      ) {
        if (_has(Base, (key = keys[j])) && !_has($Number, key)) {
          dP$1($Number, key, gOPD$1(Base, key));
        }
      }
      $Number.prototype = proto;
      proto.constructor = $Number;
      _redefine(_global, NUMBER, $Number);
    }

    // most Object methods by ES6 should accept primitives

    var _objectSap = function (KEY, exec) {
      var fn = (_core.Object || {})[KEY] || Object[KEY];
      var exp = {};
      exp[KEY] = exec(fn);
      _export(
        _export.S +
          _export.F *
            _fails(function () {
              fn(1);
            }),
        "Object",
        exp
      );
    };

    // 19.1.2.14 Object.keys(O)

    _objectSap("keys", function () {
      return function keys(it) {
        return _objectKeys(_toObject(it));
      };
    });

    // 7.2.8 IsRegExp(argument)

    var MATCH = _wks("match");
    var _isRegexp = function (it) {
      var isRegExp;
      return (
        _isObject(it) &&
        ((isRegExp = it[MATCH]) !== undefined
          ? !!isRegExp
          : _cof(it) == "RegExp")
      );
    };

    // helper for String#{startsWith, endsWith, includes}

    var _stringContext = function (that, searchString, NAME) {
      if (_isRegexp(searchString))
        throw TypeError("String#" + NAME + " doesn't accept regex!");
      return String(_defined(that));
    };

    var MATCH$1 = _wks("match");
    var _failsIsRegexp = function (KEY) {
      var re = /./;
      try {
        "/./"[KEY](re);
      } catch (e) {
        try {
          re[MATCH$1] = false;
          return !"/./"[KEY](re);
        } catch (f) {
          /* empty */
        }
      }
      return true;
    };

    var INCLUDES = "includes";

    _export(_export.P + _export.F * _failsIsRegexp(INCLUDES), "String", {
      includes: function includes(searchString /* , position = 0 */) {
        return !!~_stringContext(this, searchString, INCLUDES).indexOf(
          searchString,
          arguments.length > 1 ? arguments[1] : undefined
        );
      },
    });

    // https://github.com/tc39/Array.prototype.includes

    var $includes = _arrayIncludes(true);

    _export(_export.P, "Array", {
      includes: function includes(el /* , fromIndex = 0 */) {
        return $includes(
          this,
          el,
          arguments.length > 1 ? arguments[1] : undefined
        );
      },
    });

    _addToUnscopables("includes");

    var _fixReWks = function (KEY, length, exec) {
      var SYMBOL = _wks(KEY);
      var fns = exec(_defined, SYMBOL, ""[KEY]);
      var strfn = fns[0];
      var rxfn = fns[1];
      if (
        _fails(function () {
          var O = {};
          O[SYMBOL] = function () {
            return 7;
          };
          return ""[KEY](O) != 7;
        })
      ) {
        _redefine(String.prototype, KEY, strfn);
        _hide(
          RegExp.prototype,
          SYMBOL,
          length == 2
            ? // 21.2.5.8 RegExp.prototype[@@replace](string, replaceValue)
              // 21.2.5.11 RegExp.prototype[@@split](string, limit)
              function (string, arg) {
                return rxfn.call(string, this, arg);
              }
            : // 21.2.5.6 RegExp.prototype[@@match](string)
              // 21.2.5.9 RegExp.prototype[@@search](string)
              function (string) {
                return rxfn.call(string, this);
              }
        );
      }
    };

    // @@search logic
    _fixReWks("search", 1, function (defined, SEARCH, $search) {
      // 21.1.3.15 String.prototype.search(regexp)
      return [
        function search(regexp) {
          var O = defined(this);
          var fn = regexp == undefined ? undefined : regexp[SEARCH];
          return fn !== undefined
            ? fn.call(regexp, O)
            : new RegExp(regexp)[SEARCH](String(O));
        },
        $search,
      ];
    });

    // 21.2.5.3 get RegExp.prototype.flags

    var _flags = function () {
      var that = _anObject(this);
      var result = "";
      if (that.global) result += "g";
      if (that.ignoreCase) result += "i";
      if (that.multiline) result += "m";
      if (that.unicode) result += "u";
      if (that.sticky) result += "y";
      return result;
    };

    // 21.2.5.3 get RegExp.prototype.flags()
    if (_descriptors && /./g.flags != "g")
      _objectDp.f(RegExp.prototype, "flags", {
        configurable: true,
        get: _flags,
      });

    var TO_STRING = "toString";
    var $toString = /./[TO_STRING];

    var define = function (fn) {
      _redefine(RegExp.prototype, TO_STRING, fn, true);
    };

    // 21.2.5.14 RegExp.prototype.toString()
    if (
      _fails(function () {
        return $toString.call({ source: "a", flags: "b" }) != "/a/b";
      })
    ) {
      define(function toString() {
        var R = _anObject(this);
        return "/".concat(
          R.source,
          "/",
          "flags" in R
            ? R.flags
            : !_descriptors && R instanceof RegExp
            ? _flags.call(R)
            : undefined
        );
      });
      // FF44- RegExp#toString has a wrong name
    } else if ($toString.name != TO_STRING) {
      define(function toString() {
        return $toString.call(this);
      });
    }

    var _iterStep = function (done, value) {
      return { value: value, done: !!done };
    };

    var def = _objectDp.f;

    var TAG$1 = _wks("toStringTag");

    var _setToStringTag = function (it, tag, stat) {
      if (it && !_has((it = stat ? it : it.prototype), TAG$1))
        def(it, TAG$1, { configurable: true, value: tag });
    };

    var IteratorPrototype = {};

    // 25.1.2.1.1 %IteratorPrototype%[@@iterator]()
    _hide(IteratorPrototype, _wks("iterator"), function () {
      return this;
    });

    var _iterCreate = function (Constructor, NAME, next) {
      Constructor.prototype = _objectCreate(IteratorPrototype, {
        next: _propertyDesc(1, next),
      });
      _setToStringTag(Constructor, NAME + " Iterator");
    };

    // 19.1.2.9 / 15.2.3.2 Object.getPrototypeOf(O)

    var IE_PROTO$2 = _sharedKey("IE_PROTO");
    var ObjectProto = Object.prototype;

    var _objectGpo =
      Object.getPrototypeOf ||
      function (O) {
        O = _toObject(O);
        if (_has(O, IE_PROTO$2)) return O[IE_PROTO$2];
        if (typeof O.constructor == "function" && O instanceof O.constructor) {
          return O.constructor.prototype;
        }
        return O instanceof Object ? ObjectProto : null;
      };

    var ITERATOR$3 = _wks("iterator");
    var BUGGY = !([].keys && "next" in [].keys()); // Safari has buggy iterators w/o `next`
    var FF_ITERATOR = "@@iterator";
    var KEYS = "keys";
    var VALUES = "values";

    var returnThis = function () {
      return this;
    };

    var _iterDefine = function (
      Base,
      NAME,
      Constructor,
      next,
      DEFAULT,
      IS_SET,
      FORCED
    ) {
      _iterCreate(Constructor, NAME, next);
      var getMethod = function (kind) {
        if (!BUGGY && kind in proto) return proto[kind];
        switch (kind) {
          case KEYS:
            return function keys() {
              return new Constructor(this, kind);
            };
          case VALUES:
            return function values() {
              return new Constructor(this, kind);
            };
        }
        return function entries() {
          return new Constructor(this, kind);
        };
      };
      var TAG = NAME + " Iterator";
      var DEF_VALUES = DEFAULT == VALUES;
      var VALUES_BUG = false;
      var proto = Base.prototype;
      var $native =
        proto[ITERATOR$3] || proto[FF_ITERATOR] || (DEFAULT && proto[DEFAULT]);
      var $default = $native || getMethod(DEFAULT);
      var $entries = DEFAULT
        ? !DEF_VALUES
          ? $default
          : getMethod("entries")
        : undefined;
      var $anyNative = NAME == "Array" ? proto.entries || $native : $native;
      var methods, key, IteratorPrototype;
      // Fix native
      if ($anyNative) {
        IteratorPrototype = _objectGpo($anyNative.call(new Base()));
        if (IteratorPrototype !== Object.prototype && IteratorPrototype.next) {
          // Set @@toStringTag to native iterators
          _setToStringTag(IteratorPrototype, TAG, true);
          // fix for some old engines
          if (typeof IteratorPrototype[ITERATOR$3] != "function")
            _hide(IteratorPrototype, ITERATOR$3, returnThis);
        }
      }
      // fix Array#{values, @@iterator}.name in V8 / FF
      if (DEF_VALUES && $native && $native.name !== VALUES) {
        VALUES_BUG = true;
        $default = function values() {
          return $native.call(this);
        };
      }
      // Define iterator
      if (BUGGY || VALUES_BUG || !proto[ITERATOR$3]) {
        _hide(proto, ITERATOR$3, $default);
      }
      // Plug for library
      _iterators[NAME] = $default;
      _iterators[TAG] = returnThis;
      if (DEFAULT) {
        methods = {
          values: DEF_VALUES ? $default : getMethod(VALUES),
          keys: IS_SET ? $default : getMethod(KEYS),
          entries: $entries,
        };
        if (FORCED)
          for (key in methods) {
            if (!(key in proto)) _redefine(proto, key, methods[key]);
          }
        else
          _export(_export.P + _export.F * (BUGGY || VALUES_BUG), NAME, methods);
      }
      return methods;
    };

    // 22.1.3.4 Array.prototype.entries()
    // 22.1.3.13 Array.prototype.keys()
    // 22.1.3.29 Array.prototype.values()
    // 22.1.3.30 Array.prototype[@@iterator]()
    var es6_array_iterator = _iterDefine(
      Array,
      "Array",
      function (iterated, kind) {
        this._t = _toIobject(iterated); // target
        this._i = 0; // next index
        this._k = kind; // kind
        // 22.1.5.2.1 %ArrayIteratorPrototype%.next()
      },
      function () {
        var O = this._t;
        var kind = this._k;
        var index = this._i++;
        if (!O || index >= O.length) {
          this._t = undefined;
          return _iterStep(1);
        }
        if (kind == "keys") return _iterStep(0, index);
        if (kind == "values") return _iterStep(0, O[index]);
        return _iterStep(0, [index, O[index]]);
      },
      "values"
    );

    // argumentsList[@@iterator] is %ArrayProto_values% (9.4.4.6, 9.4.4.7)
    _iterators.Arguments = _iterators.Array;

    _addToUnscopables("keys");
    _addToUnscopables("values");
    _addToUnscopables("entries");

    var ITERATOR$4 = _wks("iterator");
    var TO_STRING_TAG = _wks("toStringTag");
    var ArrayValues = _iterators.Array;

    var DOMIterables = {
      CSSRuleList: true, // TODO: Not spec compliant, should be false.
      CSSStyleDeclaration: false,
      CSSValueList: false,
      ClientRectList: false,
      DOMRectList: false,
      DOMStringList: false,
      DOMTokenList: true,
      DataTransferItemList: false,
      FileList: false,
      HTMLAllCollection: false,
      HTMLCollection: false,
      HTMLFormElement: false,
      HTMLSelectElement: false,
      MediaList: true, // TODO: Not spec compliant, should be false.
      MimeTypeArray: false,
      NamedNodeMap: false,
      NodeList: true,
      PaintRequestList: false,
      Plugin: false,
      PluginArray: false,
      SVGLengthList: false,
      SVGNumberList: false,
      SVGPathSegList: false,
      SVGPointList: false,
      SVGStringList: false,
      SVGTransformList: false,
      SourceBufferList: false,
      StyleSheetList: true, // TODO: Not spec compliant, should be false.
      TextTrackCueList: false,
      TextTrackList: false,
      TouchList: false,
    };

    for (
      var collections = _objectKeys(DOMIterables), i = 0;
      i < collections.length;
      i++
    ) {
      var NAME = collections[i];
      var explicit = DOMIterables[NAME];
      var Collection = _global[NAME];
      var proto$1 = Collection && Collection.prototype;
      var key$1;
      if (proto$1) {
        if (!proto$1[ITERATOR$4]) _hide(proto$1, ITERATOR$4, ArrayValues);
        if (!proto$1[TO_STRING_TAG]) _hide(proto$1, TO_STRING_TAG, NAME);
        _iterators[NAME] = ArrayValues;
        if (explicit)
          for (key$1 in es6_array_iterator)
            if (!proto$1[key$1])
              _redefine(proto$1, key$1, es6_array_iterator[key$1], true);
      }
    }

    // true  -> String#at
    // false -> String#codePointAt
    var _stringAt = function (TO_STRING) {
      return function (that, pos) {
        var s = String(_defined(that));
        var i = _toInteger(pos);
        var l = s.length;
        var a, b;
        if (i < 0 || i >= l) return TO_STRING ? "" : undefined;
        a = s.charCodeAt(i);
        return a < 0xd800 ||
          a > 0xdbff ||
          i + 1 === l ||
          (b = s.charCodeAt(i + 1)) < 0xdc00 ||
          b > 0xdfff
          ? TO_STRING
            ? s.charAt(i)
            : a
          : TO_STRING
          ? s.slice(i, i + 2)
          : ((a - 0xd800) << 10) + (b - 0xdc00) + 0x10000;
      };
    };

    var $at = _stringAt(true);

    // 21.1.3.27 String.prototype[@@iterator]()
    _iterDefine(
      String,
      "String",
      function (iterated) {
        this._t = String(iterated); // target
        this._i = 0; // next index
        // 21.1.5.2.1 %StringIteratorPrototype%.next()
      },
      function () {
        var O = this._t;
        var index = this._i;
        var point;
        if (index >= O.length) return { value: undefined, done: true };
        point = $at(O, index);
        this._i += point.length;
        return { value: point, done: false };
      }
    );

    var _meta = createCommonjsModule(function (module) {
      var META = _uid("meta");

      var setDesc = _objectDp.f;
      var id = 0;
      var isExtensible =
        Object.isExtensible ||
        function () {
          return true;
        };
      var FREEZE = !_fails(function () {
        return isExtensible(Object.preventExtensions({}));
      });
      var setMeta = function (it) {
        setDesc(it, META, {
          value: {
            i: "O" + ++id, // object ID
            w: {}, // weak collections IDs
          },
        });
      };
      var fastKey = function (it, create) {
        // return primitive with prefix
        if (!_isObject(it))
          return typeof it == "symbol"
            ? it
            : (typeof it == "string" ? "S" : "P") + it;
        if (!_has(it, META)) {
          // can't set metadata to uncaught frozen object
          if (!isExtensible(it)) return "F";
          // not necessary to add metadata
          if (!create) return "E";
          // add missing metadata
          setMeta(it);
          // return object ID
        }
        return it[META].i;
      };
      var getWeak = function (it, create) {
        if (!_has(it, META)) {
          // can't set metadata to uncaught frozen object
          if (!isExtensible(it)) return true;
          // not necessary to add metadata
          if (!create) return false;
          // add missing metadata
          setMeta(it);
          // return hash weak collections IDs
        }
        return it[META].w;
      };
      // add metadata on freeze-family methods calling
      var onFreeze = function (it) {
        if (FREEZE && meta.NEED && isExtensible(it) && !_has(it, META))
          setMeta(it);
        return it;
      };
      var meta = (module.exports = {
        KEY: META,
        NEED: false,
        fastKey: fastKey,
        getWeak: getWeak,
        onFreeze: onFreeze,
      });
    });
    var _meta_1 = _meta.KEY;
    var _meta_2 = _meta.NEED;
    var _meta_3 = _meta.fastKey;
    var _meta_4 = _meta.getWeak;
    var _meta_5 = _meta.onFreeze;

    var f$4 = Object.getOwnPropertySymbols;

    var _objectGops = {
      f: f$4,
    };

    // 19.1.2.1 Object.assign(target, source, ...)

    var $assign = Object.assign;

    // should work with symbols and should have deterministic property order (V8 bug)
    var _objectAssign =
      !$assign ||
      _fails(function () {
        var A = {};
        var B = {};
        // eslint-disable-next-line no-undef
        var S = Symbol();
        var K = "abcdefghijklmnopqrst";
        A[S] = 7;
        K.split("").forEach(function (k) {
          B[k] = k;
        });
        return (
          $assign({}, A)[S] != 7 || Object.keys($assign({}, B)).join("") != K
        );
      })
        ? function assign(target, source) {
            // eslint-disable-line no-unused-vars
            var T = _toObject(target);
            var aLen = arguments.length;
            var index = 1;
            var getSymbols = _objectGops.f;
            var isEnum = _objectPie.f;
            while (aLen > index) {
              var S = _iobject(arguments[index++]);
              var keys = getSymbols
                ? _objectKeys(S).concat(getSymbols(S))
                : _objectKeys(S);
              var length = keys.length;
              var j = 0;
              var key;
              while (length > j)
                if (isEnum.call(S, (key = keys[j++]))) T[key] = S[key];
            }
            return T;
          }
        : $assign;

    var _redefineAll = function (target, src, safe) {
      for (var key in src) _redefine(target, key, src[key], safe);
      return target;
    };

    var _anInstance = function (it, Constructor, name, forbiddenField) {
      if (
        !(it instanceof Constructor) ||
        (forbiddenField !== undefined && forbiddenField in it)
      ) {
        throw TypeError(name + ": incorrect invocation!");
      }
      return it;
    };

    var _forOf = createCommonjsModule(function (module) {
      var BREAK = {};
      var RETURN = {};
      var exports = (module.exports = function (
        iterable,
        entries,
        fn,
        that,
        ITERATOR
      ) {
        var iterFn = ITERATOR
          ? function () {
              return iterable;
            }
          : core_getIteratorMethod(iterable);
        var f = _ctx(fn, that, entries ? 2 : 1);
        var index = 0;
        var length, step, iterator, result;
        if (typeof iterFn != "function")
          throw TypeError(iterable + " is not iterable!");
        // fast case for arrays with default iterator
        if (_isArrayIter(iterFn))
          for (length = _toLength(iterable.length); length > index; index++) {
            result = entries
              ? f(_anObject((step = iterable[index]))[0], step[1])
              : f(iterable[index]);
            if (result === BREAK || result === RETURN) return result;
          }
        else
          for (
            iterator = iterFn.call(iterable);
            !(step = iterator.next()).done;

          ) {
            result = _iterCall(iterator, f, step.value, entries);
            if (result === BREAK || result === RETURN) return result;
          }
      });
      exports.BREAK = BREAK;
      exports.RETURN = RETURN;
    });

    var _validateCollection = function (it, TYPE) {
      if (!_isObject(it) || it._t !== TYPE)
        throw TypeError("Incompatible receiver, " + TYPE + " required!");
      return it;
    };

    var getWeak = _meta.getWeak;

    var arrayFind = _arrayMethods(5);
    var arrayFindIndex = _arrayMethods(6);
    var id$1 = 0;

    // fallback for uncaught frozen keys
    var uncaughtFrozenStore = function (that) {
      return that._l || (that._l = new UncaughtFrozenStore());
    };
    var UncaughtFrozenStore = function () {
      this.a = [];
    };
    var findUncaughtFrozen = function (store, key) {
      return arrayFind(store.a, function (it) {
        return it[0] === key;
      });
    };
    UncaughtFrozenStore.prototype = {
      get: function (key) {
        var entry = findUncaughtFrozen(this, key);
        if (entry) return entry[1];
      },
      has: function (key) {
        return !!findUncaughtFrozen(this, key);
      },
      set: function (key, value) {
        var entry = findUncaughtFrozen(this, key);
        if (entry) entry[1] = value;
        else this.a.push([key, value]);
      },
      delete: function (key) {
        var index = arrayFindIndex(this.a, function (it) {
          return it[0] === key;
        });
        if (~index) this.a.splice(index, 1);
        return !!~index;
      },
    };

    var _collectionWeak = {
      getConstructor: function (wrapper, NAME, IS_MAP, ADDER) {
        var C = wrapper(function (that, iterable) {
          _anInstance(that, C, NAME, "_i");
          that._t = NAME; // collection type
          that._i = id$1++; // collection id
          that._l = undefined; // leak store for uncaught frozen objects
          if (iterable != undefined)
            _forOf(iterable, IS_MAP, that[ADDER], that);
        });
        _redefineAll(C.prototype, {
          // 23.3.3.2 WeakMap.prototype.delete(key)
          // 23.4.3.3 WeakSet.prototype.delete(value)
          delete: function (key) {
            if (!_isObject(key)) return false;
            var data = getWeak(key);
            if (data === true)
              return uncaughtFrozenStore(_validateCollection(this, NAME))[
                "delete"
              ](key);
            return data && _has(data, this._i) && delete data[this._i];
          },
          // 23.3.3.4 WeakMap.prototype.has(key)
          // 23.4.3.4 WeakSet.prototype.has(value)
          has: function has(key) {
            if (!_isObject(key)) return false;
            var data = getWeak(key);
            if (data === true)
              return uncaughtFrozenStore(_validateCollection(this, NAME)).has(
                key
              );
            return data && _has(data, this._i);
          },
        });
        return C;
      },
      def: function (that, key, value) {
        var data = getWeak(_anObject(key), true);
        if (data === true) uncaughtFrozenStore(that).set(key, value);
        else data[that._i] = value;
        return that;
      },
      ufstore: uncaughtFrozenStore,
    };

    var _collection = function (
      NAME,
      wrapper,
      methods,
      common,
      IS_MAP,
      IS_WEAK
    ) {
      var Base = _global[NAME];
      var C = Base;
      var ADDER = IS_MAP ? "set" : "add";
      var proto = C && C.prototype;
      var O = {};
      var fixMethod = function (KEY) {
        var fn = proto[KEY];
        _redefine(
          proto,
          KEY,
          KEY == "delete"
            ? function (a) {
                return IS_WEAK && !_isObject(a)
                  ? false
                  : fn.call(this, a === 0 ? 0 : a);
              }
            : KEY == "has"
            ? function has(a) {
                return IS_WEAK && !_isObject(a)
                  ? false
                  : fn.call(this, a === 0 ? 0 : a);
              }
            : KEY == "get"
            ? function get(a) {
                return IS_WEAK && !_isObject(a)
                  ? undefined
                  : fn.call(this, a === 0 ? 0 : a);
              }
            : KEY == "add"
            ? function add(a) {
                fn.call(this, a === 0 ? 0 : a);
                return this;
              }
            : function set(a, b) {
                fn.call(this, a === 0 ? 0 : a, b);
                return this;
              }
        );
      };
      if (
        typeof C != "function" ||
        !(
          IS_WEAK ||
          (proto.forEach &&
            !_fails(function () {
              new C().entries().next();
            }))
        )
      ) {
        // create collection constructor
        C = common.getConstructor(wrapper, NAME, IS_MAP, ADDER);
        _redefineAll(C.prototype, methods);
        _meta.NEED = true;
      } else {
        var instance = new C();
        // early implementations not supports chaining
        var HASNT_CHAINING = instance[ADDER](IS_WEAK ? {} : -0, 1) != instance;
        // V8 ~  Chromium 40- weak-collections throws on primitives, but should return false
        var THROWS_ON_PRIMITIVES = _fails(function () {
          instance.has(1);
        });
        // most early implementations doesn't supports iterables, most modern - not close it correctly
        var ACCEPT_ITERABLES = _iterDetect(function (iter) {
          new C(iter);
        }); // eslint-disable-line no-new
        // for early implementations -0 and +0 not the same
        var BUGGY_ZERO =
          !IS_WEAK &&
          _fails(function () {
            // V8 ~ Chromium 42- fails only with 5+ elements
            var $instance = new C();
            var index = 5;
            while (index--) $instance[ADDER](index, index);
            return !$instance.has(-0);
          });
        if (!ACCEPT_ITERABLES) {
          C = wrapper(function (target, iterable) {
            _anInstance(target, C, NAME);
            var that = _inheritIfRequired(new Base(), target, C);
            if (iterable != undefined)
              _forOf(iterable, IS_MAP, that[ADDER], that);
            return that;
          });
          C.prototype = proto;
          proto.constructor = C;
        }
        if (THROWS_ON_PRIMITIVES || BUGGY_ZERO) {
          fixMethod("delete");
          fixMethod("has");
          IS_MAP && fixMethod("get");
        }
        if (BUGGY_ZERO || HASNT_CHAINING) fixMethod(ADDER);
        // weak collections should not contains .clear method
        if (IS_WEAK && proto.clear) delete proto.clear;
      }

      _setToStringTag(C, NAME);

      O[NAME] = C;
      _export(_export.G + _export.W + _export.F * (C != Base), O);

      if (!IS_WEAK) common.setStrong(C, NAME, IS_MAP);

      return C;
    };

    var es6_weakMap = createCommonjsModule(function (module) {
      var each = _arrayMethods(0);

      var WEAK_MAP = "WeakMap";
      var getWeak = _meta.getWeak;
      var isExtensible = Object.isExtensible;
      var uncaughtFrozenStore = _collectionWeak.ufstore;
      var tmp = {};
      var InternalMap;

      var wrapper = function (get) {
        return function WeakMap() {
          return get(this, arguments.length > 0 ? arguments[0] : undefined);
        };
      };

      var methods = {
        // 23.3.3.3 WeakMap.prototype.get(key)
        get: function get(key) {
          if (_isObject(key)) {
            var data = getWeak(key);
            if (data === true)
              return uncaughtFrozenStore(
                _validateCollection(this, WEAK_MAP)
              ).get(key);
            return data ? data[this._i] : undefined;
          }
        },
        // 23.3.3.5 WeakMap.prototype.set(key, value)
        set: function set(key, value) {
          return _collectionWeak.def(
            _validateCollection(this, WEAK_MAP),
            key,
            value
          );
        },
      };

      // 23.3 WeakMap Objects
      var $WeakMap = (module.exports = _collection(
        WEAK_MAP,
        wrapper,
        methods,
        _collectionWeak,
        true,
        true
      ));

      // IE11 WeakMap frozen keys fix
      if (
        _fails(function () {
          return (
            new $WeakMap().set((Object.freeze || Object)(tmp), 7).get(tmp) != 7
          );
        })
      ) {
        InternalMap = _collectionWeak.getConstructor(wrapper, WEAK_MAP);
        _objectAssign(InternalMap.prototype, methods);
        _meta.NEED = true;
        each(["delete", "has", "get", "set"], function (key) {
          var proto = $WeakMap.prototype;
          var method = proto[key];
          _redefine(proto, key, function (a, b) {
            // store frozen objects on internal weakmap shim
            if (_isObject(a) && !isExtensible(a)) {
              if (!this._f) this._f = new InternalMap();
              var result = this._f[key](a, b);
              return key == "set" ? this : result;
              // store all the rest on native weakmap
            }
            return method.call(this, a, b);
          });
        });
      }
    });

    function _classCallCheck(instance, Constructor) {
      if (!(instance instanceof Constructor)) {
        throw new TypeError("Cannot call a class as a function");
      }
    }

    function _defineProperties(target, props) {
      for (var i = 0; i < props.length; i++) {
        var descriptor = props[i];
        descriptor.enumerable = descriptor.enumerable || false;
        descriptor.configurable = true;
        if ("value" in descriptor) descriptor.writable = true;
        Object.defineProperty(target, descriptor.key, descriptor);
      }
    }

    function _createClass(Constructor, protoProps, staticProps) {
      if (protoProps) _defineProperties(Constructor.prototype, protoProps);
      if (staticProps) _defineProperties(Constructor, staticProps);
      return Constructor;
    }

    function _defineProperty(obj, key, value) {
      if (key in obj) {
        Object.defineProperty(obj, key, {
          value: value,
          enumerable: true,
          configurable: true,
          writable: true,
        });
      } else {
        obj[key] = value;
      }

      return obj;
    }

    function _slicedToArray(arr, i) {
      return (
        _arrayWithHoles(arr) ||
        _iterableToArrayLimit(arr, i) ||
        _nonIterableRest()
      );
    }

    function _toConsumableArray(arr) {
      return (
        _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread()
      );
    }

    function _arrayWithoutHoles(arr) {
      if (Array.isArray(arr)) {
        for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++)
          arr2[i] = arr[i];

        return arr2;
      }
    }

    function _arrayWithHoles(arr) {
      if (Array.isArray(arr)) return arr;
    }

    function _iterableToArray(iter) {
      if (
        Symbol.iterator in Object(iter) ||
        Object.prototype.toString.call(iter) === "[object Arguments]"
      )
        return Array.from(iter);
    }

    function _iterableToArrayLimit(arr, i) {
      var _arr = [];
      var _n = true;
      var _d = false;
      var _e = undefined;

      try {
        for (
          var _i = arr[Symbol.iterator](), _s;
          !(_n = (_s = _i.next()).done);
          _n = true
        ) {
          _arr.push(_s.value);

          if (i && _arr.length === i) break;
        }
      } catch (err) {
        _d = true;
        _e = err;
      } finally {
        try {
          if (!_n && _i["return"] != null) _i["return"]();
        } finally {
          if (_d) throw _e;
        }
      }

      return _arr;
    }

    function _nonIterableSpread() {
      throw new TypeError("Invalid attempt to spread non-iterable instance");
    }

    function _nonIterableRest() {
      throw new TypeError(
        "Invalid attempt to destructure non-iterable instance"
      );
    }

    // 19.1.3.1 Object.assign(target, source)

    _export(_export.S + _export.F, "Object", { assign: _objectAssign });

    // @@split logic
    _fixReWks("split", 2, function (defined, SPLIT, $split) {
      var isRegExp = _isRegexp;
      var _split = $split;
      var $push = [].push;
      var $SPLIT = "split";
      var LENGTH = "length";
      var LAST_INDEX = "lastIndex";
      if (
        "abbc"[$SPLIT](/(b)*/)[1] == "c" ||
        "test"[$SPLIT](/(?:)/, -1)[LENGTH] != 4 ||
        "ab"[$SPLIT](/(?:ab)*/)[LENGTH] != 2 ||
        "."[$SPLIT](/(.?)(.?)/)[LENGTH] != 4 ||
        "."[$SPLIT](/()()/)[LENGTH] > 1 ||
        ""[$SPLIT](/.?/)[LENGTH]
      ) {
        var NPCG = /()??/.exec("")[1] === undefined; // nonparticipating capturing group
        // based on es5-shim implementation, need to rework it
        $split = function (separator, limit) {
          var string = String(this);
          if (separator === undefined && limit === 0) return [];
          // If `separator` is not a regex, use native split
          if (!isRegExp(separator))
            return _split.call(string, separator, limit);
          var output = [];
          var flags =
            (separator.ignoreCase ? "i" : "") +
            (separator.multiline ? "m" : "") +
            (separator.unicode ? "u" : "") +
            (separator.sticky ? "y" : "");
          var lastLastIndex = 0;
          var splitLimit = limit === undefined ? 4294967295 : limit >>> 0;
          // Make `global` and avoid `lastIndex` issues by working with a copy
          var separatorCopy = new RegExp(separator.source, flags + "g");
          var separator2, match, lastIndex, lastLength, i;
          // Doesn't need flags gy, but they don't hurt
          if (!NPCG)
            separator2 = new RegExp(
              "^" + separatorCopy.source + "$(?!\\s)",
              flags
            );
          while ((match = separatorCopy.exec(string))) {
            // `separatorCopy.lastIndex` is not reliable cross-browser
            lastIndex = match.index + match[0][LENGTH];
            if (lastIndex > lastLastIndex) {
              output.push(string.slice(lastLastIndex, match.index));
              // Fix browsers whose `exec` methods don't consistently return `undefined` for NPCG
              // eslint-disable-next-line no-loop-func
              if (!NPCG && match[LENGTH] > 1)
                match[0].replace(separator2, function () {
                  for (i = 1; i < arguments[LENGTH] - 2; i++)
                    if (arguments[i] === undefined) match[i] = undefined;
                });
              if (match[LENGTH] > 1 && match.index < string[LENGTH])
                $push.apply(output, match.slice(1));
              lastLength = match[0][LENGTH];
              lastLastIndex = lastIndex;
              if (output[LENGTH] >= splitLimit) break;
            }
            if (separatorCopy[LAST_INDEX] === match.index)
              separatorCopy[LAST_INDEX]++; // Avoid an infinite loop
          }
          if (lastLastIndex === string[LENGTH]) {
            if (lastLength || !separatorCopy.test("")) output.push("");
          } else output.push(string.slice(lastLastIndex));
          return output[LENGTH] > splitLimit
            ? output.slice(0, splitLimit)
            : output;
        };
        // Chakra, V8
      } else if ("0"[$SPLIT](undefined, 0)[LENGTH]) {
        $split = function (separator, limit) {
          return separator === undefined && limit === 0
            ? []
            : _split.call(this, separator, limit);
        };
      }
      // 21.1.3.17 String.prototype.split(separator, limit)
      return [
        function split(separator, limit) {
          var O = defined(this);
          var fn = separator == undefined ? undefined : separator[SPLIT];
          return fn !== undefined
            ? fn.call(separator, O, limit)
            : $split.call(String(O), separator, limit);
        },
        $split,
      ];
    });

    var isEnum = _objectPie.f;
    var _objectToArray = function (isEntries) {
      return function (it) {
        var O = _toIobject(it);
        var keys = _objectKeys(O);
        var length = keys.length;
        var i = 0;
        var result = [];
        var key;
        while (length > i)
          if (isEnum.call(O, (key = keys[i++]))) {
            result.push(isEntries ? [key, O[key]] : O[key]);
          }
        return result;
      };
    };

    // https://github.com/tc39/proposal-object-values-entries

    var $entries = _objectToArray(true);

    _export(_export.S, "Object", {
      entries: function entries(it) {
        return $entries(it);
      },
    });

    // https://github.com/tc39/proposal-object-values-entries

    var $values = _objectToArray(false);

    _export(_export.S, "Object", {
      values: function values(it) {
        return $values(it);
      },
    });

    // @@replace logic
    _fixReWks("replace", 2, function (defined, REPLACE, $replace) {
      // 21.1.3.14 String.prototype.replace(searchValue, replaceValue)
      return [
        function replace(searchValue, replaceValue) {
          var O = defined(this);
          var fn = searchValue == undefined ? undefined : searchValue[REPLACE];
          return fn !== undefined
            ? fn.call(searchValue, O, replaceValue)
            : $replace.call(String(O), searchValue, replaceValue);
        },
        $replace,
      ];
    });

    // 7.3.20 SpeciesConstructor(O, defaultConstructor)

    var SPECIES$1 = _wks("species");
    var _speciesConstructor = function (O, D) {
      var C = _anObject(O).constructor;
      var S;
      return C === undefined || (S = _anObject(C)[SPECIES$1]) == undefined
        ? D
        : _aFunction(S);
    };

    // fast apply, http://jsperf.lnkit.com/fast-apply/5
    var _invoke = function (fn, args, that) {
      var un = that === undefined;
      switch (args.length) {
        case 0:
          return un ? fn() : fn.call(that);
        case 1:
          return un ? fn(args[0]) : fn.call(that, args[0]);
        case 2:
          return un ? fn(args[0], args[1]) : fn.call(that, args[0], args[1]);
        case 3:
          return un
            ? fn(args[0], args[1], args[2])
            : fn.call(that, args[0], args[1], args[2]);
        case 4:
          return un
            ? fn(args[0], args[1], args[2], args[3])
            : fn.call(that, args[0], args[1], args[2], args[3]);
      }
      return fn.apply(that, args);
    };

    var process = _global.process;
    var setTask = _global.setImmediate;
    var clearTask = _global.clearImmediate;
    var MessageChannel = _global.MessageChannel;
    var Dispatch = _global.Dispatch;
    var counter = 0;
    var queue = {};
    var ONREADYSTATECHANGE = "onreadystatechange";
    var defer, channel, port;
    var run = function () {
      var id = +this;
      // eslint-disable-next-line no-prototype-builtins
      if (queue.hasOwnProperty(id)) {
        var fn = queue[id];
        delete queue[id];
        fn();
      }
    };
    var listener = function (event) {
      run.call(event.data);
    };
    // Node.js 0.9+ & IE10+ has setImmediate, otherwise:
    if (!setTask || !clearTask) {
      setTask = function setImmediate(fn) {
        var args = [];
        var i = 1;
        while (arguments.length > i) args.push(arguments[i++]);
        queue[++counter] = function () {
          // eslint-disable-next-line no-new-func
          _invoke(typeof fn == "function" ? fn : Function(fn), args);
        };
        defer(counter);
        return counter;
      };
      clearTask = function clearImmediate(id) {
        delete queue[id];
      };
      // Node.js 0.8-
      if (_cof(process) == "process") {
        defer = function (id) {
          process.nextTick(_ctx(run, id, 1));
        };
        // Sphere (JS game engine) Dispatch API
      } else if (Dispatch && Dispatch.now) {
        defer = function (id) {
          Dispatch.now(_ctx(run, id, 1));
        };
        // Browsers with MessageChannel, includes WebWorkers
      } else if (MessageChannel) {
        channel = new MessageChannel();
        port = channel.port2;
        channel.port1.onmessage = listener;
        defer = _ctx(port.postMessage, port, 1);
        // Browsers with postMessage, skip WebWorkers
        // IE8 has postMessage, but it's sync & typeof its postMessage is 'object'
      } else if (
        _global.addEventListener &&
        typeof postMessage == "function" &&
        !_global.importScripts
      ) {
        defer = function (id) {
          _global.postMessage(id + "", "*");
        };
        _global.addEventListener("message", listener, false);
        // IE8-
      } else if (ONREADYSTATECHANGE in _domCreate("script")) {
        defer = function (id) {
          _html.appendChild(_domCreate("script"))[
            ONREADYSTATECHANGE
          ] = function () {
            _html.removeChild(this);
            run.call(id);
          };
        };
        // Rest old browsers
      } else {
        defer = function (id) {
          setTimeout(_ctx(run, id, 1), 0);
        };
      }
    }
    var _task = {
      set: setTask,
      clear: clearTask,
    };

    var macrotask = _task.set;
    var Observer = _global.MutationObserver || _global.WebKitMutationObserver;
    var process$1 = _global.process;
    var Promise$1 = _global.Promise;
    var isNode = _cof(process$1) == "process";

    var _microtask = function () {
      var head, last, notify;

      var flush = function () {
        var parent, fn;
        if (isNode && (parent = process$1.domain)) parent.exit();
        while (head) {
          fn = head.fn;
          head = head.next;
          try {
            fn();
          } catch (e) {
            if (head) notify();
            else last = undefined;
            throw e;
          }
        }
        last = undefined;
        if (parent) parent.enter();
      };

      // Node.js
      if (isNode) {
        notify = function () {
          process$1.nextTick(flush);
        };
        // browsers with MutationObserver, except iOS Safari - https://github.com/zloirock/core-js/issues/339
      } else if (
        Observer &&
        !(_global.navigator && _global.navigator.standalone)
      ) {
        var toggle = true;
        var node = document.createTextNode("");
        new Observer(flush).observe(node, { characterData: true }); // eslint-disable-line no-new
        notify = function () {
          node.data = toggle = !toggle;
        };
        // environments with maybe non-completely correct, but existent Promise
      } else if (Promise$1 && Promise$1.resolve) {
        // Promise.resolve without an argument throws an error in LG WebOS 2
        var promise = Promise$1.resolve(undefined);
        notify = function () {
          promise.then(flush);
        };
        // for other environments - macrotask based on:
        // - setImmediate
        // - MessageChannel
        // - window.postMessag
        // - onreadystatechange
        // - setTimeout
      } else {
        notify = function () {
          // strange IE + webpack dev server bug - use .call(global)
          macrotask.call(_global, flush);
        };
      }

      return function (fn) {
        var task = { fn: fn, next: undefined };
        if (last) last.next = task;
        if (!head) {
          head = task;
          notify();
        }
        last = task;
      };
    };

    // 25.4.1.5 NewPromiseCapability(C)

    function PromiseCapability(C) {
      var resolve, reject;
      this.promise = new C(function ($$resolve, $$reject) {
        if (resolve !== undefined || reject !== undefined)
          throw TypeError("Bad Promise constructor");
        resolve = $$resolve;
        reject = $$reject;
      });
      this.resolve = _aFunction(resolve);
      this.reject = _aFunction(reject);
    }

    var f$5 = function (C) {
      return new PromiseCapability(C);
    };

    var _newPromiseCapability = {
      f: f$5,
    };

    var _perform = function (exec) {
      try {
        return { e: false, v: exec() };
      } catch (e) {
        return { e: true, v: e };
      }
    };

    var navigator$1 = _global.navigator;

    var _userAgent = (navigator$1 && navigator$1.userAgent) || "";

    var _promiseResolve = function (C, x) {
      _anObject(C);
      if (_isObject(x) && x.constructor === C) return x;
      var promiseCapability = _newPromiseCapability.f(C);
      var resolve = promiseCapability.resolve;
      resolve(x);
      return promiseCapability.promise;
    };

    var SPECIES$2 = _wks("species");

    var _setSpecies = function (KEY) {
      var C = _global[KEY];
      if (_descriptors && C && !C[SPECIES$2])
        _objectDp.f(C, SPECIES$2, {
          configurable: true,
          get: function () {
            return this;
          },
        });
    };

    var task = _task.set;
    var microtask = _microtask();

    var PROMISE = "Promise";
    var TypeError$1 = _global.TypeError;
    var process$2 = _global.process;
    var versions = process$2 && process$2.versions;
    var v8 = (versions && versions.v8) || "";
    var $Promise = _global[PROMISE];
    var isNode$1 = _classof(process$2) == "process";
    var empty = function () {
      /* empty */
    };
    var Internal, newGenericPromiseCapability, OwnPromiseCapability, Wrapper;
    var newPromiseCapability = (newGenericPromiseCapability =
      _newPromiseCapability.f);

    var USE_NATIVE = !!(function () {
      try {
        // correct subclassing with @@species support
        var promise = $Promise.resolve(1);
        var FakePromise = ((promise.constructor = {})[
          _wks("species")
        ] = function (exec) {
          exec(empty, empty);
        });
        // unhandled rejections tracking support, NodeJS Promise without it fails @@species test
        return (
          (isNode$1 || typeof PromiseRejectionEvent == "function") &&
          promise.then(empty) instanceof FakePromise &&
          // v8 6.6 (Node 10 and Chrome 66) have a bug with resolving custom thenables
          // https://bugs.chromium.org/p/chromium/issues/detail?id=830565
          // we can't detect it synchronously, so just check versions
          v8.indexOf("6.6") !== 0 &&
          _userAgent.indexOf("Chrome/66") === -1
        );
      } catch (e) {
        /* empty */
      }
    })();

    // helpers
    var isThenable = function (it) {
      var then;
      return _isObject(it) && typeof (then = it.then) == "function"
        ? then
        : false;
    };
    var notify = function (promise, isReject) {
      if (promise._n) return;
      promise._n = true;
      var chain = promise._c;
      microtask(function () {
        var value = promise._v;
        var ok = promise._s == 1;
        var i = 0;
        var run = function (reaction) {
          var handler = ok ? reaction.ok : reaction.fail;
          var resolve = reaction.resolve;
          var reject = reaction.reject;
          var domain = reaction.domain;
          var result, then, exited;
          try {
            if (handler) {
              if (!ok) {
                if (promise._h == 2) onHandleUnhandled(promise);
                promise._h = 1;
              }
              if (handler === true) result = value;
              else {
                if (domain) domain.enter();
                result = handler(value); // may throw
                if (domain) {
                  domain.exit();
                  exited = true;
                }
              }
              if (result === reaction.promise) {
                reject(TypeError$1("Promise-chain cycle"));
              } else if ((then = isThenable(result))) {
                then.call(result, resolve, reject);
              } else resolve(result);
            } else reject(value);
          } catch (e) {
            if (domain && !exited) domain.exit();
            reject(e);
          }
        };
        while (chain.length > i) run(chain[i++]); // variable length - can't use forEach
        promise._c = [];
        promise._n = false;
        if (isReject && !promise._h) onUnhandled(promise);
      });
    };
    var onUnhandled = function (promise) {
      task.call(_global, function () {
        var value = promise._v;
        var unhandled = isUnhandled(promise);
        var result, handler, console;
        if (unhandled) {
          result = _perform(function () {
            if (isNode$1) {
              process$2.emit("unhandledRejection", value, promise);
            } else if ((handler = _global.onunhandledrejection)) {
              handler({ promise: promise, reason: value });
            } else if ((console = _global.console) && console.error) {
              console.error("Unhandled promise rejection", value);
            }
          });
          // Browsers should not trigger `rejectionHandled` event if it was handled here, NodeJS - should
          promise._h = isNode$1 || isUnhandled(promise) ? 2 : 1;
        }
        promise._a = undefined;
        if (unhandled && result.e) throw result.v;
      });
    };
    var isUnhandled = function (promise) {
      return promise._h !== 1 && (promise._a || promise._c).length === 0;
    };
    var onHandleUnhandled = function (promise) {
      task.call(_global, function () {
        var handler;
        if (isNode$1) {
          process$2.emit("rejectionHandled", promise);
        } else if ((handler = _global.onrejectionhandled)) {
          handler({ promise: promise, reason: promise._v });
        }
      });
    };
    var $reject = function (value) {
      var promise = this;
      if (promise._d) return;
      promise._d = true;
      promise = promise._w || promise; // unwrap
      promise._v = value;
      promise._s = 2;
      if (!promise._a) promise._a = promise._c.slice();
      notify(promise, true);
    };
    var $resolve = function (value) {
      var promise = this;
      var then;
      if (promise._d) return;
      promise._d = true;
      promise = promise._w || promise; // unwrap
      try {
        if (promise === value)
          throw TypeError$1("Promise can't be resolved itself");
        if ((then = isThenable(value))) {
          microtask(function () {
            var wrapper = { _w: promise, _d: false }; // wrap
            try {
              then.call(
                value,
                _ctx($resolve, wrapper, 1),
                _ctx($reject, wrapper, 1)
              );
            } catch (e) {
              $reject.call(wrapper, e);
            }
          });
        } else {
          promise._v = value;
          promise._s = 1;
          notify(promise, false);
        }
      } catch (e) {
        $reject.call({ _w: promise, _d: false }, e); // wrap
      }
    };

    // constructor polyfill
    if (!USE_NATIVE) {
      // 25.4.3.1 Promise(executor)
      $Promise = function Promise(executor) {
        _anInstance(this, $Promise, PROMISE, "_h");
        _aFunction(executor);
        Internal.call(this);
        try {
          executor(_ctx($resolve, this, 1), _ctx($reject, this, 1));
        } catch (err) {
          $reject.call(this, err);
        }
      };
      // eslint-disable-next-line no-unused-vars
      Internal = function Promise(executor) {
        this._c = []; // <- awaiting reactions
        this._a = undefined; // <- checked in isUnhandled reactions
        this._s = 0; // <- state
        this._d = false; // <- done
        this._v = undefined; // <- value
        this._h = 0; // <- rejection state, 0 - default, 1 - handled, 2 - unhandled
        this._n = false; // <- notify
      };
      Internal.prototype = _redefineAll($Promise.prototype, {
        // 25.4.5.3 Promise.prototype.then(onFulfilled, onRejected)
        then: function then(onFulfilled, onRejected) {
          var reaction = newPromiseCapability(
            _speciesConstructor(this, $Promise)
          );
          reaction.ok = typeof onFulfilled == "function" ? onFulfilled : true;
          reaction.fail = typeof onRejected == "function" && onRejected;
          reaction.domain = isNode$1 ? process$2.domain : undefined;
          this._c.push(reaction);
          if (this._a) this._a.push(reaction);
          if (this._s) notify(this, false);
          return reaction.promise;
        },
        // 25.4.5.1 Promise.prototype.catch(onRejected)
        catch: function (onRejected) {
          return this.then(undefined, onRejected);
        },
      });
      OwnPromiseCapability = function () {
        var promise = new Internal();
        this.promise = promise;
        this.resolve = _ctx($resolve, promise, 1);
        this.reject = _ctx($reject, promise, 1);
      };
      _newPromiseCapability.f = newPromiseCapability = function (C) {
        return C === $Promise || C === Wrapper
          ? new OwnPromiseCapability(C)
          : newGenericPromiseCapability(C);
      };
    }

    _export(_export.G + _export.W + _export.F * !USE_NATIVE, {
      Promise: $Promise,
    });
    _setToStringTag($Promise, PROMISE);
    _setSpecies(PROMISE);
    Wrapper = _core[PROMISE];

    // statics
    _export(_export.S + _export.F * !USE_NATIVE, PROMISE, {
      // 25.4.4.5 Promise.reject(r)
      reject: function reject(r) {
        var capability = newPromiseCapability(this);
        var $$reject = capability.reject;
        $$reject(r);
        return capability.promise;
      },
    });
    _export(_export.S + _export.F * !USE_NATIVE, PROMISE, {
      // 25.4.4.6 Promise.resolve(x)
      resolve: function resolve(x) {
        return _promiseResolve(
          _library && this === Wrapper ? $Promise : this,
          x
        );
      },
    });
    _export(
      _export.S +
        _export.F *
          !(
            USE_NATIVE &&
            _iterDetect(function (iter) {
              $Promise.all(iter)["catch"](empty);
            })
          ),
      PROMISE,
      {
        // 25.4.4.1 Promise.all(iterable)
        all: function all(iterable) {
          var C = this;
          var capability = newPromiseCapability(C);
          var resolve = capability.resolve;
          var reject = capability.reject;
          var result = _perform(function () {
            var values = [];
            var index = 0;
            var remaining = 1;
            _forOf(iterable, false, function (promise) {
              var $index = index++;
              var alreadyCalled = false;
              values.push(undefined);
              remaining++;
              C.resolve(promise).then(function (value) {
                if (alreadyCalled) return;
                alreadyCalled = true;
                values[$index] = value;
                --remaining || resolve(values);
              }, reject);
            });
            --remaining || resolve(values);
          });
          if (result.e) reject(result.v);
          return capability.promise;
        },
        // 25.4.4.4 Promise.race(iterable)
        race: function race(iterable) {
          var C = this;
          var capability = newPromiseCapability(C);
          var reject = capability.reject;
          var result = _perform(function () {
            _forOf(iterable, false, function (promise) {
              C.resolve(promise).then(capability.resolve, reject);
            });
          });
          if (result.e) reject(result.v);
          return capability.promise;
        },
      }
    );

    var STARTS_WITH = "startsWith";
    var $startsWith = ""[STARTS_WITH];

    _export(_export.P + _export.F * _failsIsRegexp(STARTS_WITH), "String", {
      startsWith: function startsWith(searchString /* , position = 0 */) {
        var that = _stringContext(this, searchString, STARTS_WITH);
        var index = _toLength(
          Math.min(arguments.length > 1 ? arguments[1] : undefined, that.length)
        );
        var search = String(searchString);
        return $startsWith
          ? $startsWith.call(that, search, index)
          : that.slice(index, index + search.length) === search;
      },
    });

    // 20.1.2.4 Number.isNaN(number)

    _export(_export.S, "Number", {
      isNaN: function isNaN(number) {
        // eslint-disable-next-line no-self-compare
        return number != number;
      },
    });

    // ==========================================================================
    // Type checking utils
    // ==========================================================================
    var getConstructor = function getConstructor(input) {
      return input !== null && typeof input !== "undefined"
        ? input.constructor
        : null;
    };

    var instanceOf = function instanceOf(input, constructor) {
      return Boolean(input && constructor && input instanceof constructor);
    };

    var isNullOrUndefined = function isNullOrUndefined(input) {
      return input === null || typeof input === "undefined";
    };

    var isObject = function isObject(input) {
      return getConstructor(input) === Object;
    };

    var isNumber = function isNumber(input) {
      return getConstructor(input) === Number && !Number.isNaN(input);
    };

    var isString = function isString(input) {
      return getConstructor(input) === String;
    };

    var isBoolean = function isBoolean(input) {
      return getConstructor(input) === Boolean;
    };

    var isFunction = function isFunction(input) {
      return getConstructor(input) === Function;
    };

    var isArray = function isArray(input) {
      return Array.isArray(input);
    };

    var isWeakMap = function isWeakMap(input) {
      return instanceOf(input, WeakMap);
    };

    var isNodeList = function isNodeList(input) {
      return instanceOf(input, NodeList);
    };

    var isElement = function isElement(input) {
      return instanceOf(input, Element);
    };

    var isTextNode = function isTextNode(input) {
      return getConstructor(input) === Text;
    };

    var isEvent = function isEvent(input) {
      return instanceOf(input, Event);
    };

    var isKeyboardEvent = function isKeyboardEvent(input) {
      return instanceOf(input, KeyboardEvent);
    };

    var isCue = function isCue(input) {
      return (
        instanceOf(input, window.TextTrackCue) ||
        instanceOf(input, window.VTTCue)
      );
    };

    var isTrack = function isTrack(input) {
      return (
        instanceOf(input, TextTrack) ||
        (!isNullOrUndefined(input) && isString(input.kind))
      );
    };

    var isEmpty = function isEmpty(input) {
      return (
        isNullOrUndefined(input) ||
        ((isString(input) || isArray(input) || isNodeList(input)) &&
          !input.length) ||
        (isObject(input) && !Object.keys(input).length)
      );
    };

    var isUrl = function isUrl(input) {
      // Accept a URL object
      if (instanceOf(input, window.URL)) {
        return true;
      } // Must be string from here

      if (!isString(input)) {
        return false;
      } // Add the protocol if required

      var string = input;

      if (!input.startsWith("http://") || !input.startsWith("https://")) {
        string = "http://".concat(input);
      }

      try {
        return !isEmpty(new URL(string).hostname);
      } catch (e) {
        return false;
      }
    };

    var is$1 = {
      nullOrUndefined: isNullOrUndefined,
      object: isObject,
      number: isNumber,
      string: isString,
      boolean: isBoolean,
      function: isFunction,
      array: isArray,
      weakMap: isWeakMap,
      nodeList: isNodeList,
      element: isElement,
      textNode: isTextNode,
      event: isEvent,
      keyboardEvent: isKeyboardEvent,
      cue: isCue,
      track: isTrack,
      url: isUrl,
      empty: isEmpty,
    };

    // https://github.com/WICG/EventListenerOptions/blob/gh-pages/explainer.md
    // https://www.youtube.com/watch?v=NPM6172J22g

    var supportsPassiveListeners = (function () {
      // Test via a getter in the options object to see if the passive property is accessed
      var supported = false;

      try {
        var options = Object.defineProperty({}, "passive", {
          get: function get() {
            supported = true;
            return null;
          },
        });
        window.addEventListener("test", null, options);
        window.removeEventListener("test", null, options);
      } catch (e) {
        // Do nothing
      }

      return supported;
    })(); // Toggle event listener

    function toggleListener(element, event, callback) {
      var _this = this;

      var toggle =
        arguments.length > 3 && arguments[3] !== undefined
          ? arguments[3]
          : false;
      var passive =
        arguments.length > 4 && arguments[4] !== undefined
          ? arguments[4]
          : true;
      var capture =
        arguments.length > 5 && arguments[5] !== undefined
          ? arguments[5]
          : false;

      // Bail if no element, event, or callback
      if (
        !element ||
        !("addEventListener" in element) ||
        is$1.empty(event) ||
        !is$1.function(callback)
      ) {
        return;
      } // Allow multiple events

      var events = event.split(" "); // Build options
      // Default to just the capture boolean for browsers with no passive listener support

      var options = capture; // If passive events listeners are supported

      if (supportsPassiveListeners) {
        options = {
          // Whether the listener can be passive (i.e. default never prevented)
          passive: passive,
          // Whether the listener is a capturing listener or not
          capture: capture,
        };
      } // If a single node is passed, bind the event listener

      events.forEach(function (type) {
        if (_this && _this.eventListeners && toggle) {
          // Cache event listener
          _this.eventListeners.push({
            element: element,
            type: type,
            callback: callback,
            options: options,
          });
        }

        element[toggle ? "addEventListener" : "removeEventListener"](
          type,
          callback,
          options
        );
      });
    } // Bind event handler

    function on(element) {
      var events =
        arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "";
      var callback = arguments.length > 2 ? arguments[2] : undefined;
      var passive =
        arguments.length > 3 && arguments[3] !== undefined
          ? arguments[3]
          : true;
      var capture =
        arguments.length > 4 && arguments[4] !== undefined
          ? arguments[4]
          : false;
      toggleListener.call(
        this,
        element,
        events,
        callback,
        true,
        passive,
        capture
      );
    } // Unbind event handler

    function off(element) {
      var events =
        arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "";
      var callback = arguments.length > 2 ? arguments[2] : undefined;
      var passive =
        arguments.length > 3 && arguments[3] !== undefined
          ? arguments[3]
          : true;
      var capture =
        arguments.length > 4 && arguments[4] !== undefined
          ? arguments[4]
          : false;
      toggleListener.call(
        this,
        element,
        events,
        callback,
        false,
        passive,
        capture
      );
    } // Bind once-only event handler

    function once(element) {
      var events =
        arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "";
      var callback = arguments.length > 2 ? arguments[2] : undefined;
      var passive =
        arguments.length > 3 && arguments[3] !== undefined
          ? arguments[3]
          : true;
      var capture =
        arguments.length > 4 && arguments[4] !== undefined
          ? arguments[4]
          : false;

      function onceCallback() {
        off(element, events, onceCallback, passive, capture);

        for (
          var _len = arguments.length, args = new Array(_len), _key = 0;
          _key < _len;
          _key++
        ) {
          args[_key] = arguments[_key];
        }

        callback.apply(this, args);
      }

      toggleListener.call(
        this,
        element,
        events,
        onceCallback,
        true,
        passive,
        capture
      );
    } // Trigger event

    function triggerEvent(element) {
      var type =
        arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "";
      var bubbles =
        arguments.length > 2 && arguments[2] !== undefined
          ? arguments[2]
          : false;
      var detail =
        arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {};

      // Bail if no element
      if (!is$1.element(element) || is$1.empty(type)) {
        return;
      } // Create and dispatch the event

      var event = new CustomEvent(type, {
        bubbles: bubbles,
        detail: Object.assign({}, detail, {
          plyr: this,
        }),
      }); // Dispatch the event

      element.dispatchEvent(event);
    } // Unbind all cached event listeners

    function unbindListeners() {
      if (this && this.eventListeners) {
        this.eventListeners.forEach(function (item) {
          var element = item.element,
            type = item.type,
            callback = item.callback,
            options = item.options;
          element.removeEventListener(type, callback, options);
        });
        this.eventListeners = [];
      }
    } // Run method when / if player is ready

    function ready() {
      var _this2 = this;

      return new Promise(function (resolve) {
        return _this2.ready
          ? setTimeout(resolve, 0)
          : on.call(_this2, _this2.elements.container, "ready", resolve);
      }).then(function () {});
    }

    function wrap(elements, wrapper) {
      // Convert `elements` to an array, if necessary.
      var targets = elements.length ? elements : [elements]; // Loops backwards to prevent having to clone the wrapper on the
      // first element (see `child` below).

      Array.from(targets)
        .reverse()
        .forEach(function (element, index) {
          var child = index > 0 ? wrapper.cloneNode(true) : wrapper; // Cache the current parent and sibling.

          var parent = element.parentNode;
          var sibling = element.nextSibling; // Wrap the element (is automatically removed from its current
          // parent).

          child.appendChild(element); // If the element had a sibling, insert the wrapper before
          // the sibling to maintain the HTML structure; otherwise, just
          // append it to the parent.

          if (sibling) {
            parent.insertBefore(child, sibling);
          } else {
            parent.appendChild(child);
          }
        });
    } // Set attributes

    function setAttributes(element, attributes) {
      if (!is$1.element(element) || is$1.empty(attributes)) {
        return;
      } // Assume null and undefined attributes should be left out,
      // Setting them would otherwise convert them to "null" and "undefined"

      Object.entries(attributes)
        .filter(function (_ref) {
          var _ref2 = _slicedToArray(_ref, 2),
            value = _ref2[1];

          return !is$1.nullOrUndefined(value);
        })
        .forEach(function (_ref3) {
          var _ref4 = _slicedToArray(_ref3, 2),
            key = _ref4[0],
            value = _ref4[1];

          return element.setAttribute(key, value);
        });
    } // Create a DocumentFragment

    function createElement(type, attributes, text) {
      // Create a new <element>
      var element = document.createElement(type); // Set all passed attributes

      if (is$1.object(attributes)) {
        setAttributes(element, attributes);
      } // Add text node

      if (is$1.string(text)) {
        element.innerText = text;
      } // Return built element

      return element;
    } // Inaert an element after another

    function insertAfter(element, target) {
      if (!is$1.element(element) || !is$1.element(target)) {
        return;
      }

      target.parentNode.insertBefore(element, target.nextSibling);
    } // Insert a DocumentFragment

    function insertElement(type, parent, attributes, text) {
      if (!is$1.element(parent)) {
        return;
      }

      parent.appendChild(createElement(type, attributes, text));
    } // Remove element(s)

    function removeElement(element) {
      if (is$1.nodeList(element) || is$1.array(element)) {
        Array.from(element).forEach(removeElement);
        return;
      }

      if (!is$1.element(element) || !is$1.element(element.parentNode)) {
        return;
      }

      element.parentNode.removeChild(element);
    } // Remove all child elements

    function emptyElement(element) {
      if (!is$1.element(element)) {
        return;
      }

      var length = element.childNodes.length;

      while (length > 0) {
        element.removeChild(element.lastChild);
        length -= 1;
      }
    } // Replace element

    function replaceElement(newChild, oldChild) {
      if (
        !is$1.element(oldChild) ||
        !is$1.element(oldChild.parentNode) ||
        !is$1.element(newChild)
      ) {
        return null;
      }

      oldChild.parentNode.replaceChild(newChild, oldChild);
      return newChild;
    } // Get an attribute object from a string selector

    function getAttributesFromSelector(sel, existingAttributes) {
      // For example:
      // '.test' to { class: 'test' }
      // '#test' to { id: 'test' }
      // '[data-test="test"]' to { 'data-test': 'test' }
      if (!is$1.string(sel) || is$1.empty(sel)) {
        return {};
      }

      var attributes = {};
      var existing = existingAttributes;
      sel.split(",").forEach(function (s) {
        // Remove whitespace
        var selector = s.trim();
        var className = selector.replace(".", "");
        var stripped = selector.replace(/[[\]]/g, ""); // Get the parts and value

        var parts = stripped.split("=");
        var key = parts[0];
        var value = parts.length > 1 ? parts[1].replace(/["']/g, "") : ""; // Get the first character

        var start = selector.charAt(0);

        switch (start) {
          case ".":
            // Add to existing classname
            if (is$1.object(existing) && is$1.string(existing.class)) {
              existing.class += " ".concat(className);
            }

            attributes.class = className;
            break;

          case "#":
            // ID selector
            attributes.id = selector.replace("#", "");
            break;

          case "[":
            // Attribute selector
            attributes[key] = value;
            break;

          default:
            break;
        }
      });
      return attributes;
    } // Toggle hidden

    function toggleHidden(element, hidden) {
      if (!is$1.element(element)) {
        return;
      }

      var hide = hidden;

      if (!is$1.boolean(hide)) {
        hide = !element.hidden;
      }

      if (hide) {
        element.setAttribute("hidden", "");
      } else {
        element.removeAttribute("hidden");
      }
    } // Mirror Element.classList.toggle, with IE compatibility for "force" argument

    function toggleClass(element, className, force) {
      if (is$1.nodeList(element)) {
        return Array.from(element).map(function (e) {
          return toggleClass(e, className, force);
        });
      }

      if (is$1.element(element)) {
        var method = "toggle";

        if (typeof force !== "undefined") {
          method = force ? "add" : "remove";
        }

        element.classList[method](className);
        return element.classList.contains(className);
      }

      return false;
    } // Has class name

    function hasClass(element, className) {
      return is$1.element(element) && element.classList.contains(className);
    } // Element matches selector

    function matches(element, selector) {
      var prototype = {
        Element: Element,
      };

      function match() {
        return Array.from(document.querySelectorAll(selector)).includes(this);
      }

      var matches =
        prototype.matches ||
        prototype.webkitMatchesSelector ||
        prototype.mozMatchesSelector ||
        prototype.msMatchesSelector ||
        match;
      return matches.call(element, selector);
    } // Find all elements

    function getElements(selector) {
      return this.elements.container.querySelectorAll(selector);
    } // Find a single element

    function getElement(selector) {
      return this.elements.container.querySelector(selector);
    } // Trap focus inside container

    function trapFocus() {
      var element =
        arguments.length > 0 && arguments[0] !== undefined
          ? arguments[0]
          : null;
      var toggle =
        arguments.length > 1 && arguments[1] !== undefined
          ? arguments[1]
          : false;

      if (!is$1.element(element)) {
        return;
      }

      var focusable = getElements.call(
        this,
        "button:not(:disabled), input:not(:disabled), [tabindex]"
      );
      var first = focusable[0];
      var last = focusable[focusable.length - 1];

      var trap = function trap(event) {
        // Bail if not tab key or not fullscreen
        if (event.key !== "Tab" || event.keyCode !== 9) {
          return;
        } // Get the current focused element

        var focused = document.activeElement;

        if (focused === last && !event.shiftKey) {
          // Move focus to first element that can be tabbed if Shift isn't used
          first.focus();
          event.preventDefault();
        } else if (focused === first && event.shiftKey) {
          // Move focus to last element that can be tabbed if Shift is used
          last.focus();
          event.preventDefault();
        }
      };

      toggleListener.call(
        this,
        this.elements.container,
        "keydown",
        trap,
        toggle,
        false
      );
    } // Set focus and tab focus class

    function setFocus() {
      var element =
        arguments.length > 0 && arguments[0] !== undefined
          ? arguments[0]
          : null;
      var tabFocus =
        arguments.length > 1 && arguments[1] !== undefined
          ? arguments[1]
          : false;

      if (!is$1.element(element)) {
        return;
      } // Set regular focus

      element.focus(); // If we want to mimic keyboard focus via tab

      if (tabFocus) {
        toggleClass(element, this.config.classNames.tabFocus);
      }
    }

    var transitionEndEvent = (function () {
      var element = document.createElement("span");
      var events = {
        WebkitTransition: "webkitTransitionEnd",
        MozTransition: "transitionend",
        OTransition: "oTransitionEnd otransitionend",
        transition: "transitionend",
      };
      var type = Object.keys(events).find(function (event) {
        return element.style[event] !== undefined;
      });
      return is$1.string(type) ? events[type] : false;
    })(); // Force repaint of element

    function repaint(element) {
      setTimeout(function () {
        try {
          toggleHidden(element, true);
          element.offsetHeight; // eslint-disable-line

          toggleHidden(element, false);
        } catch (e) {
          // Do nothing
        }
      }, 0);
    }

    // ==========================================================================
    // Browser sniffing
    // Unfortunately, due to mixed support, UA sniffing is required
    // ==========================================================================
    var browser = {
      isIE:
        /* @cc_on!@ */
        !!document.documentMode,
      isWebkit:
        "WebkitAppearance" in document.documentElement.style &&
        !/Edge/.test(navigator.userAgent),
      isIPhone: /(iPhone|iPod)/gi.test(navigator.platform),
      isIos: /(iPad|iPhone|iPod)/gi.test(navigator.platform),
    };

    var defaultCodecs = {
      "audio/ogg": "vorbis",
      "audio/wav": "1",
      "video/webm": "vp8, vorbis",
      "video/mp4": "avc1.42E01E, mp4a.40.2",
      "video/ogg": "theora",
    }; // Check for feature support

    var support = {
      // Basic support
      audio: "canPlayType" in document.createElement("audio"),
      video: "canPlayType" in document.createElement("video"),
      // Check for support
      // Basic functionality vs full UI
      check: function check(type, provider, playsinline) {
        var canPlayInline =
          browser.isIPhone && playsinline && support.playsinline;
        var api = support[type] || provider !== "html5";
        var ui =
          api &&
          support.rangeInput &&
          (type !== "video" || !browser.isIPhone || canPlayInline);
        return {
          api: api,
          ui: ui,
        };
      },
      // Picture-in-picture support
      // Safari & Chrome only currently
      pip: (function () {
        if (browser.isIPhone) {
          return false;
        } // Safari
        // https://developer.apple.com/documentation/webkitjs/adding_picture_in_picture_to_your_safari_media_controls

        if (is$1.function(createElement("video").webkitSetPresentationMode)) {
          return true;
        } // Chrome
        // https://developers.google.com/web/updates/2018/10/watch-video-using-picture-in-picture

        if (
          document.pictureInPictureEnabled &&
          !createElement("video").disablePictureInPicture
        ) {
          return true;
        }

        return false;
      })(),
      // Airplay support
      // Safari only currently
      airplay: is$1.function(window.WebKitPlaybackTargetAvailabilityEvent),
      // Inline playback support
      // https://webkit.org/blog/6784/new-video-policies-for-ios/
      playsinline: "playsInline" in document.createElement("video"),
      // Check for mime type support against a player instance
      // Credits: http://diveintohtml5.info/everything.html
      // Related: http://www.leanbackplayer.com/test/h5mt.html
      mime: function mime(inputType) {
        var _inputType$split = inputType.split("/"),
          _inputType$split2 = _slicedToArray(_inputType$split, 1),
          mediaType = _inputType$split2[0];

        if (!this.isHTML5 || mediaType !== this.type) {
          return false;
        }

        var type;

        if (inputType && inputType.includes("codecs=")) {
          // Use input directly
          type = inputType;
        } else if (inputType === "audio/mpeg") {
          // Skip codec
          type = "audio/mpeg;";
        } else if (inputType in defaultCodecs) {
          // Use codec
          type = ""
            .concat(inputType, '; codecs="')
            .concat(defaultCodecs[inputType], '"');
        }

        try {
          return Boolean(
            type && this.media.canPlayType(type).replace(/no/, "")
          );
        } catch (err) {
          return false;
        }
      },
      // Check for textTracks support
      textTracks: "textTracks" in document.createElement("video"),
      // <input type="range"> Sliders
      rangeInput: (function () {
        var range = document.createElement("input");
        range.type = "range";
        return range.type === "range";
      })(),
      // Touch
      // NOTE: Remember a device can be mouse + touch enabled so we check on first touch event
      touch: "ontouchstart" in document.documentElement,
      // Detect transitions support
      transitions: transitionEndEvent !== false,
      // Reduced motion iOS & MacOS setting
      // https://webkit.org/blog/7551/responsive-design-for-motion/
      reducedMotion:
        "matchMedia" in window &&
        window.matchMedia("(prefers-reduced-motion)").matches,
    };

    var html5 = {
      getSources: function getSources() {
        var _this = this;

        if (!this.isHTML5) {
          return [];
        }

        var sources = Array.from(this.media.querySelectorAll("source")); // Filter out unsupported sources

        return sources.filter(function (source) {
          return support.mime.call(_this, source.getAttribute("type"));
        });
      },
      // Get quality levels
      getQualityOptions: function getQualityOptions() {
        // Get sizes from <source> elements
        return html5.getSources
          .call(this)
          .map(function (source) {
            return Number(source.getAttribute("size"));
          })
          .filter(Boolean);
      },
      extend: function extend() {
        if (!this.isHTML5) {
          return;
        }

        var player = this; // Quality

        Object.defineProperty(player.media, "quality", {
          get: function get() {
            // Get sources
            var sources = html5.getSources.call(player);
            var source = sources.find(function (source) {
              return source.getAttribute("src") === player.source;
            }); // Return size, if match is found

            return source && Number(source.getAttribute("size"));
          },
          set: function set(input) {
            // Get sources
            var sources = html5.getSources.call(player); // Get first match for requested size

            var source = sources.find(function (source) {
              return Number(source.getAttribute("size")) === input;
            }); // No matching source found

            if (!source) {
              return;
            } // Get current state

            var _player$media = player.media,
              currentTime = _player$media.currentTime,
              paused = _player$media.paused,
              preload = _player$media.preload,
              readyState = _player$media.readyState; // Set new source

            player.media.src = source.getAttribute("src"); // Prevent loading if preload="none" and the current source isn't loaded (#1044)

            if (preload !== "none" || readyState) {
              // Restore time
              player.once("loadedmetadata", function () {
                player.currentTime = currentTime; // Resume playing

                if (!paused) {
                  player.play();
                }
              }); // Load new source

              player.media.load();
            } // Trigger change event

            triggerEvent.call(player, player.media, "qualitychange", false, {
              quality: input,
            });
          },
        });
      },
      // Cancel current network requests
      // See https://github.com/sampotts/plyr/issues/174
      cancelRequests: function cancelRequests() {
        if (!this.isHTML5) {
          return;
        } // Remove child sources

        removeElement(html5.getSources.call(this)); // Set blank video src attribute
        // This is to prevent a MEDIA_ERR_SRC_NOT_SUPPORTED error
        // Info: http://stackoverflow.com/questions/32231579/how-to-properly-dispose-of-an-html5-video-and-close-socket-or-connection

        this.media.setAttribute("src", this.config.blankVideo); // Load the new empty source
        // This will cancel existing requests
        // See https://github.com/sampotts/plyr/issues/174

        this.media.load(); // Debugging

        this.debug.log("Cancelled network requests");
      },
    };

    // ==========================================================================

    function dedupe(array) {
      if (!is$1.array(array)) {
        return array;
      }

      return array.filter(function (item, index) {
        return array.indexOf(item) === index;
      });
    } // Get the closest value in an array

    function closest(array, value) {
      if (!is$1.array(array) || !array.length) {
        return null;
      }

      return array.reduce(function (prev, curr) {
        return Math.abs(curr - value) < Math.abs(prev - value) ? curr : prev;
      });
    }

    function cloneDeep(object) {
      return JSON.parse(JSON.stringify(object));
    } // Get a nested value in an object

    function getDeep(object, path) {
      return path.split(".").reduce(function (obj, key) {
        return obj && obj[key];
      }, object);
    } // Deep extend destination object with N more objects

    function extend() {
      var target =
        arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

      for (
        var _len = arguments.length,
          sources = new Array(_len > 1 ? _len - 1 : 0),
          _key = 1;
        _key < _len;
        _key++
      ) {
        sources[_key - 1] = arguments[_key];
      }

      if (!sources.length) {
        return target;
      }

      var source = sources.shift();

      if (!is$1.object(source)) {
        return target;
      }

      Object.keys(source).forEach(function (key) {
        if (is$1.object(source[key])) {
          if (!Object.keys(target).includes(key)) {
            Object.assign(target, _defineProperty({}, key, {}));
          }

          extend(target[key], source[key]);
        } else {
          Object.assign(target, _defineProperty({}, key, source[key]));
        }
      });
      return extend.apply(void 0, [target].concat(sources));
    }

    var dP$2 = _objectDp.f;
    var gOPN$1 = _objectGopn.f;

    var $RegExp = _global.RegExp;
    var Base$1 = $RegExp;
    var proto$2 = $RegExp.prototype;
    var re1 = /a/g;
    var re2 = /a/g;
    // "new" creates a new object, old webkit buggy here
    var CORRECT_NEW = new $RegExp(re1) !== re1;

    if (
      _descriptors &&
      (!CORRECT_NEW ||
        _fails(function () {
          re2[_wks("match")] = false;
          // RegExp constructor can alter flags and IsRegExp works correct with @@match
          return (
            $RegExp(re1) != re1 ||
            $RegExp(re2) == re2 ||
            $RegExp(re1, "i") != "/a/i"
          );
        }))
    ) {
      $RegExp = function RegExp(p, f) {
        var tiRE = this instanceof $RegExp;
        var piRE = _isRegexp(p);
        var fiU = f === undefined;
        return !tiRE && piRE && p.constructor === $RegExp && fiU
          ? p
          : _inheritIfRequired(
              CORRECT_NEW
                ? new Base$1(piRE && !fiU ? p.source : p, f)
                : Base$1(
                    (piRE = p instanceof $RegExp) ? p.source : p,
                    piRE && fiU ? _flags.call(p) : f
                  ),
              tiRE ? this : proto$2,
              $RegExp
            );
      };
      var proxy = function (key) {
        key in $RegExp ||
          dP$2($RegExp, key, {
            configurable: true,
            get: function () {
              return Base$1[key];
            },
            set: function (it) {
              Base$1[key] = it;
            },
          });
      };
      for (var keys$1 = gOPN$1(Base$1), i$1 = 0; keys$1.length > i$1; )
        proxy(keys$1[i$1++]);
      proto$2.constructor = $RegExp;
      $RegExp.prototype = proto$2;
      _redefine(_global, "RegExp", $RegExp);
    }

    _setSpecies("RegExp");

    function generateId(prefix) {
      return "".concat(prefix, "-").concat(Math.floor(Math.random() * 10000));
    } // Format string

    function format(input) {
      for (
        var _len = arguments.length,
          args = new Array(_len > 1 ? _len - 1 : 0),
          _key = 1;
        _key < _len;
        _key++
      ) {
        args[_key - 1] = arguments[_key];
      }

      if (is$1.empty(input)) {
        return input;
      }

      return input.toString().replace(/{(\d+)}/g, function (match, i) {
        return args[i].toString();
      });
    } // Get percentage

    function getPercentage(current, max) {
      if (
        current === 0 ||
        max === 0 ||
        Number.isNaN(current) ||
        Number.isNaN(max)
      ) {
        return 0;
      }

      return ((current / max) * 100).toFixed(2);
    } // Replace all occurances of a string in a string

    function replaceAll() {
      var input =
        arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "";
      var find =
        arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "";
      var replace =
        arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : "";
      return input.replace(
        new RegExp(
          find.toString().replace(/([.*+?^=!:${}()|[\]/\\])/g, "\\$1"),
          "g"
        ),
        replace.toString()
      );
    } // Convert to title case

    function toTitleCase() {
      var input =
        arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "";
      return input.toString().replace(/\w\S*/g, function (text) {
        return text.charAt(0).toUpperCase() + text.substr(1).toLowerCase();
      });
    } // Convert string to pascalCase

    function toPascalCase() {
      var input =
        arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "";
      var string = input.toString(); // Convert kebab case

      string = replaceAll(string, "-", " "); // Convert snake case

      string = replaceAll(string, "_", " "); // Convert to title case

      string = toTitleCase(string); // Convert to pascal case

      return replaceAll(string, " ", "");
    } // Convert string to pascalCase

    function toCamelCase() {
      var input =
        arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "";
      var string = input.toString(); // Convert to pascal case

      string = toPascalCase(string); // Convert first character to lowercase

      return string.charAt(0).toLowerCase() + string.slice(1);
    } // Remove HTML from a string

    function stripHTML(source) {
      var fragment = document.createDocumentFragment();
      var element = document.createElement("div");
      fragment.appendChild(element);
      element.innerHTML = source;
      return fragment.firstChild.innerText;
    } // Like outerHTML, but also works for DocumentFragment

    function getHTML(element) {
      var wrapper = document.createElement("div");
      wrapper.appendChild(element);
      return wrapper.innerHTML;
    }

    var resources = {
      pip: "PIP",
      airplay: "AirPlay",
      html5: "HTML5",
      vimeo: "Vimeo",
      youtube: "YouTube",
    };
    var i18n = {
      get: function get() {
        var key =
          arguments.length > 0 && arguments[0] !== undefined
            ? arguments[0]
            : "";
        var config =
          arguments.length > 1 && arguments[1] !== undefined
            ? arguments[1]
            : {};

        if (is$1.empty(key) || is$1.empty(config)) {
          return "";
        }

        var string = getDeep(config.i18n, key);

        if (is$1.empty(string)) {
          if (Object.keys(resources).includes(key)) {
            return resources[key];
          }

          return "";
        }

        var replace = {
          "{seektime}": config.seekTime,
          "{title}": config.title,
        };
        Object.entries(replace).forEach(function (_ref) {
          var _ref2 = _slicedToArray(_ref, 2),
            key = _ref2[0],
            value = _ref2[1];

          string = replaceAll(string, key, value);
        });
        return string;
      },
    };

    var Storage =
      /*#__PURE__*/
      (function () {
        function Storage(player) {
          _classCallCheck(this, Storage);

          this.enabled = player.config.storage.enabled;
          this.key = player.config.storage.key;
        } // Check for actual support (see if we can use it)

        _createClass(
          Storage,
          [
            {
              key: "get",
              value: function get(key) {
                if (!Storage.supported || !this.enabled) {
                  return null;
                }

                var store = window.localStorage.getItem(this.key);

                if (is$1.empty(store)) {
                  return null;
                }

                var json = JSON.parse(store);
                return is$1.string(key) && key.length ? json[key] : json;
              },
            },
            {
              key: "set",
              value: function set(object) {
                // Bail if we don't have localStorage support or it's disabled
                if (!Storage.supported || !this.enabled) {
                  return;
                } // Can only store objectst

                if (!is$1.object(object)) {
                  return;
                } // Get current storage

                var storage = this.get(); // Default to empty object

                if (is$1.empty(storage)) {
                  storage = {};
                } // Update the working copy of the values

                extend(storage, object); // Update storage

                window.localStorage.setItem(this.key, JSON.stringify(storage));
              },
            },
          ],
          [
            {
              key: "supported",
              get: function get() {
                try {
                  if (!("localStorage" in window)) {
                    return false;
                  }

                  var test = "___test"; // Try to use it (it might be disabled, e.g. user is in private mode)
                  // see: https://github.com/sampotts/plyr/issues/131

                  window.localStorage.setItem(test, test);
                  window.localStorage.removeItem(test);
                  return true;
                } catch (e) {
                  return false;
                }
              },
            },
          ]
        );

        return Storage;
      })();

    // ==========================================================================
    // Fetch wrapper
    // Using XHR to avoid issues with older browsers
    // ==========================================================================
    function fetch(url) {
      var responseType =
        arguments.length > 1 && arguments[1] !== undefined
          ? arguments[1]
          : "text";
      return new Promise(function (resolve, reject) {
        try {
          var request = new XMLHttpRequest(); // Check for CORS support

          if (!("withCredentials" in request)) {
            return;
          }

          request.addEventListener("load", function () {
            if (responseType === "text") {
              try {
                resolve(JSON.parse(request.responseText));
              } catch (e) {
                resolve(request.responseText);
              }
            } else {
              resolve(request.response);
            }
          });
          request.addEventListener("error", function () {
            throw new Error(request.status);
          });
          request.open("GET", url, true); // Set the required response type

          request.responseType = responseType;
          request.send();
        } catch (e) {
          reject(e);
        }
      });
    }

    // ==========================================================================

    function loadSprite(url, id) {
      if (!is$1.string(url)) {
        return;
      }

      var prefix = "cache";
      var hasId = is$1.string(id);
      var isCached = false;

      var exists = function exists() {
        return document.getElementById(id) !== null;
      };

      var update = function update(container, data) {
        container.innerHTML = data; // Check again incase of race condition

        if (hasId && exists()) {
          return;
        } // Inject the SVG to the body

        document.body.insertAdjacentElement("afterbegin", container);
      }; // Only load once if ID set

      if (!hasId || !exists()) {
        var useStorage = Storage.supported; // Create container

        var container = document.createElement("div");
        container.setAttribute("hidden", "");

        if (hasId) {
          container.setAttribute("id", id);
        } // Check in cache

        if (useStorage) {
          var cached = window.localStorage.getItem(
            "".concat(prefix, "-").concat(id)
          );
          isCached = cached !== null;

          if (isCached) {
            var data = JSON.parse(cached);
            update(container, data.content);
          }
        } // Get the sprite

        fetch(url)
          .then(function (result) {
            if (is$1.empty(result)) {
              return;
            }

            if (useStorage) {
              window.localStorage.setItem(
                "".concat(prefix, "-").concat(id),
                JSON.stringify({
                  content: result,
                })
              );
            }

            update(container, result);
          })
          .catch(function () {});
      }
    }

    // ==========================================================================

    var getHours = function getHours(value) {
      return parseInt((value / 60 / 60) % 60, 10);
    };
    var getMinutes = function getMinutes(value) {
      return parseInt((value / 60) % 60, 10);
    };
    var getSeconds = function getSeconds(value) {
      return parseInt(value % 60, 10);
    }; // Format time to UI friendly string

    function formatTime() {
      var time =
        arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
      var displayHours =
        arguments.length > 1 && arguments[1] !== undefined
          ? arguments[1]
          : false;
      var inverted =
        arguments.length > 2 && arguments[2] !== undefined
          ? arguments[2]
          : false;

      // Bail if the value isn't a number
      if (!is$1.number(time)) {
        return formatTime(null, displayHours, inverted);
      } // Format time component to add leading zero

      var format = function format(value) {
        return "0".concat(value).slice(-2);
      }; // Breakdown to hours, mins, secs

      var hours = getHours(time);
      var mins = getMinutes(time);
      var secs = getSeconds(time); // Do we need to display hours?

      if (displayHours || hours > 0) {
        hours = "".concat(hours, ":");
      } else {
        hours = "";
      } // Render

      return ""
        .concat(inverted && time > 0 ? "-" : "")
        .concat(hours)
        .concat(format(mins), ":")
        .concat(format(secs));
    }

    var controls = {
      // Get icon URL
      getIconUrl: function getIconUrl() {
        var url = new URL(this.config.iconUrl, window.location);
        var cors =
          url.host !== window.location.host ||
          (browser.isIE && !window.svg4everybody);
        return {
          url: this.config.iconUrl,
          cors: cors,
        };
      },
      // Find the UI controls
      findElements: function findElements() {
        try {
          this.elements.controls = getElement.call(
            this,
            this.config.selectors.controls.wrapper
          ); // Buttons

          this.elements.buttons = {
            play: getElements.call(this, this.config.selectors.buttons.play),
            pause: getElement.call(this, this.config.selectors.buttons.pause),
            restart: getElement.call(
              this,
              this.config.selectors.buttons.restart
            ),
            rewind: getElement.call(this, this.config.selectors.buttons.rewind),
            fastForward: getElement.call(
              this,
              this.config.selectors.buttons.fastForward
            ),
            mute: getElement.call(this, this.config.selectors.buttons.mute),
            pip: getElement.call(this, this.config.selectors.buttons.pip),
            airplay: getElement.call(
              this,
              this.config.selectors.buttons.airplay
            ),
            settings: getElement.call(
              this,
              this.config.selectors.buttons.settings
            ),
            captions: getElement.call(
              this,
              this.config.selectors.buttons.captions
            ),
            fullscreen: getElement.call(
              this,
              this.config.selectors.buttons.fullscreen
            ),
          }; // Progress

          this.elements.progress = getElement.call(
            this,
            this.config.selectors.progress
          ); // Inputs

          this.elements.inputs = {
            seek: getElement.call(this, this.config.selectors.inputs.seek),
            volume: getElement.call(this, this.config.selectors.inputs.volume),
          }; // Display

          this.elements.display = {
            buffer: getElement.call(this, this.config.selectors.display.buffer),
            currentTime: getElement.call(
              this,
              this.config.selectors.display.currentTime
            ),
            duration: getElement.call(
              this,
              this.config.selectors.display.duration
            ),
          }; // Seek tooltip

          if (is$1.element(this.elements.progress)) {
            this.elements.display.seekTooltip = this.elements.progress.querySelector(
              ".".concat(this.config.classNames.tooltip)
            );
          }

          return true;
        } catch (error) {
          // Log it
          this.debug.warn(
            "It looks like there is a problem with your custom controls HTML",
            error
          ); // Restore native video controls

          this.toggleNativeControls(true);
          return false;
        }
      },
      // Create <svg> icon
      createIcon: function createIcon(type, attributes) {
        var namespace = "http://www.w3.org/2000/svg";
        var iconUrl = controls.getIconUrl.call(this);
        var iconPath = ""
          .concat(!iconUrl.cors ? iconUrl.url : "", "#")
          .concat(this.config.iconPrefix); // Create <svg>

        var icon = document.createElementNS(namespace, "svg");
        setAttributes(
          icon,
          extend(attributes, {
            role: "presentation",
            focusable: "false",
          })
        ); // Create the <use> to reference sprite

        var use = document.createElementNS(namespace, "use");
        var path = "".concat(iconPath, "-").concat(type); // Set `href` attributes
        // https://github.com/sampotts/plyr/issues/460
        // https://developer.mozilla.org/en-US/docs/Web/SVG/Attribute/xlink:href

        if ("href" in use) {
          use.setAttributeNS("http://www.w3.org/1999/xlink", "href", path);
        } // Always set the older attribute even though it's "deprecated" (it'll be around for ages)

        use.setAttributeNS("http://www.w3.org/1999/xlink", "xlink:href", path); // Add <use> to <svg>

        icon.appendChild(use);
        return icon;
      },
      // Create hidden text label
      createLabel: function createLabel(key) {
        var attr =
          arguments.length > 1 && arguments[1] !== undefined
            ? arguments[1]
            : {};
        var text = i18n.get(key, this.config);
        var attributes = Object.assign({}, attr, {
          class: [attr.class, this.config.classNames.hidden]
            .filter(Boolean)
            .join(" "),
        });
        return createElement("span", attributes, text);
      },
      // Create a badge
      createBadge: function createBadge(text) {
        if (is$1.empty(text)) {
          return null;
        }

        var badge = createElement("span", {
          class: this.config.classNames.menu.value,
        });
        badge.appendChild(
          createElement(
            "span",
            {
              class: this.config.classNames.menu.badge,
            },
            text
          )
        );
        return badge;
      },
      // Create a <button>
      createButton: function createButton(buttonType, attr) {
        var attributes = Object.assign({}, attr);
        var type = toCamelCase(buttonType);
        var props = {
          element: "button",
          toggle: false,
          label: null,
          icon: null,
          labelPressed: null,
          iconPressed: null,
        };
        ["element", "icon", "label"].forEach(function (key) {
          if (Object.keys(attributes).includes(key)) {
            props[key] = attributes[key];
            delete attributes[key];
          }
        }); // Default to 'button' type to prevent form submission

        if (
          props.element === "button" &&
          !Object.keys(attributes).includes("type")
        ) {
          attributes.type = "button";
        } // Set class name

        if (Object.keys(attributes).includes("class")) {
          if (!attributes.class.includes(this.config.classNames.control)) {
            attributes.class += " ".concat(this.config.classNames.control);
          }
        } else {
          attributes.class = this.config.classNames.control;
        } // Large play button

        switch (buttonType) {
          case "play":
            props.toggle = true;
            props.label = "play";
            props.labelPressed = "pause";
            props.icon = "play";
            props.iconPressed = "pause";
            break;

          case "mute":
            props.toggle = true;
            props.label = "mute";
            props.labelPressed = "unmute";
            props.icon = "volume";
            props.iconPressed = "muted";
            break;

          case "captions":
            props.toggle = true;
            props.label = "enableCaptions";
            props.labelPressed = "disableCaptions";
            props.icon = "captions-off";
            props.iconPressed = "captions-on";
            break;

          case "fullscreen":
            props.toggle = true;
            props.label = "enterFullscreen";
            props.labelPressed = "exitFullscreen";
            props.icon = "enter-fullscreen";
            props.iconPressed = "exit-fullscreen";
            break;

          case "play-large":
            attributes.class += " ".concat(
              this.config.classNames.control,
              "--overlaid"
            );
            type = "play";
            props.label = "play";
            props.icon = "play";
            break;

          default:
            if (is$1.empty(props.label)) {
              props.label = type;
            }

            if (is$1.empty(props.icon)) {
              props.icon = buttonType;
            }
        }

        var button = createElement(props.element); // Setup toggle icon and labels

        if (props.toggle) {
          // Icon
          button.appendChild(
            controls.createIcon.call(this, props.iconPressed, {
              class: "icon--pressed",
            })
          );
          button.appendChild(
            controls.createIcon.call(this, props.icon, {
              class: "icon--not-pressed",
            })
          ); // Label/Tooltip

          button.appendChild(
            controls.createLabel.call(this, props.labelPressed, {
              class: "label--pressed",
            })
          );
          button.appendChild(
            controls.createLabel.call(this, props.label, {
              class: "label--not-pressed",
            })
          );
        } else {
          button.appendChild(controls.createIcon.call(this, props.icon));
          button.appendChild(controls.createLabel.call(this, props.label));
        } // Merge and set attributes

        extend(
          attributes,
          getAttributesFromSelector(
            this.config.selectors.buttons[type],
            attributes
          )
        );
        setAttributes(button, attributes); // We have multiple play buttons

        if (type === "play") {
          if (!is$1.array(this.elements.buttons[type])) {
            this.elements.buttons[type] = [];
          }

          this.elements.buttons[type].push(button);
        } else {
          this.elements.buttons[type] = button;
        }

        return button;
      },
      // Create an <input type='range'>
      createRange: function createRange(type, attributes) {
        // Seek input
        var input = createElement(
          "input",
          extend(
            getAttributesFromSelector(this.config.selectors.inputs[type]),
            {
              type: "range",
              min: 0,
              max: 100,
              step: 0.01,
              value: 0,
              autocomplete: "off",
              // A11y fixes for https://github.com/sampotts/plyr/issues/905
              role: "slider",
              "aria-label": i18n.get(type, this.config),
              "aria-valuemin": 0,
              "aria-valuemax": 100,
              "aria-valuenow": 0,
            },
            attributes
          )
        );
        this.elements.inputs[type] = input; // Set the fill for webkit now

        controls.updateRangeFill.call(this, input);
        return input;
      },
      // Create a <progress>
      createProgress: function createProgress(type, attributes) {
        var progress = createElement(
          "progress",
          extend(
            getAttributesFromSelector(this.config.selectors.display[type]),
            {
              min: 0,
              max: 100,
              value: 0,
              role: "presentation",
              "aria-hidden": true,
            },
            attributes
          )
        ); // Create the label inside

        if (type !== "volume") {
          progress.appendChild(createElement("span", null, "0"));
          var suffixKey = {
            played: "played",
            buffer: "buffered",
          }[type];
          var suffix = suffixKey ? i18n.get(suffixKey, this.config) : "";
          progress.innerText = "% ".concat(suffix.toLowerCase());
        }

        this.elements.display[type] = progress;
        return progress;
      },
      // Create time display
      createTime: function createTime(type) {
        var attributes = getAttributesFromSelector(
          this.config.selectors.display[type]
        );
        var container = createElement(
          "div",
          extend(attributes, {
            class: ""
              .concat(this.config.classNames.display.time, " ")
              .concat(attributes.class ? attributes.class : "")
              .trim(),
            "aria-label": i18n.get(type, this.config),
          }),
          "00:00"
        ); // Reference for updates

        this.elements.display[type] = container;
        return container;
      },
      // Bind keyboard shortcuts for a menu item
      // We have to bind to keyup otherwise Firefox triggers a click when a keydown event handler shifts focus
      // https://bugzilla.mozilla.org/show_bug.cgi?id=1220143
      bindMenuItemShortcuts: function bindMenuItemShortcuts(menuItem, type) {
        var _this = this;

        // Navigate through menus via arrow keys and space
        on(
          menuItem,
          "keydown keyup",
          function (event) {
            // We only care about space and ⬆️ ⬇️️ ➡️
            if (![32, 38, 39, 40].includes(event.which)) {
              return;
            } // Prevent play / seek

            event.preventDefault();
            event.stopPropagation(); // We're just here to prevent the keydown bubbling

            if (event.type === "keydown") {
              return;
            }

            var isRadioButton = matches(menuItem, '[role="menuitemradio"]'); // Show the respective menu

            if (!isRadioButton && [32, 39].includes(event.which)) {
              controls.showMenuPanel.call(_this, type, true);
            } else {
              var target;

              if (event.which !== 32) {
                if (
                  event.which === 40 ||
                  (isRadioButton && event.which === 39)
                ) {
                  target = menuItem.nextElementSibling;

                  if (!is$1.element(target)) {
                    target = menuItem.parentNode.firstElementChild;
                  }
                } else {
                  target = menuItem.previousElementSibling;

                  if (!is$1.element(target)) {
                    target = menuItem.parentNode.lastElementChild;
                  }
                }

                setFocus.call(_this, target, true);
              }
            }
          },
          false
        ); // Enter will fire a `click` event but we still need to manage focus
        // So we bind to keyup which fires after and set focus here

        on(menuItem, "keyup", function (event) {
          if (event.which !== 13) {
            return;
          }

          controls.focusFirstMenuItem.call(_this, null, true);
        });
      },
      // Create a settings menu item
      createMenuItem: function createMenuItem(_ref) {
        var _this2 = this;

        var value = _ref.value,
          list = _ref.list,
          type = _ref.type,
          title = _ref.title,
          _ref$badge = _ref.badge,
          badge = _ref$badge === void 0 ? null : _ref$badge,
          _ref$checked = _ref.checked,
          checked = _ref$checked === void 0 ? false : _ref$checked;
        var attributes = getAttributesFromSelector(
          this.config.selectors.inputs[type]
        );
        var menuItem = createElement(
          "button",
          extend(attributes, {
            type: "button",
            role: "menuitemradio",
            class: ""
              .concat(this.config.classNames.control, " ")
              .concat(attributes.class ? attributes.class : "")
              .trim(),
            "aria-checked": checked,
            value: value,
          })
        );
        var flex = createElement("span"); // We have to set as HTML incase of special characters

        flex.innerHTML = title;

        if (is$1.element(badge)) {
          flex.appendChild(badge);
        }

        menuItem.appendChild(flex); // Replicate radio button behaviour

        Object.defineProperty(menuItem, "checked", {
          enumerable: true,
          get: function get() {
            return menuItem.getAttribute("aria-checked") === "true";
          },
          set: function set(checked) {
            // Ensure exclusivity
            if (checked) {
              Array.from(menuItem.parentNode.children)
                .filter(function (node) {
                  return matches(node, '[role="menuitemradio"]');
                })
                .forEach(function (node) {
                  return node.setAttribute("aria-checked", "false");
                });
            }

            menuItem.setAttribute("aria-checked", checked ? "true" : "false");
          },
        });
        this.listeners.bind(
          menuItem,
          "click keyup",
          function (event) {
            if (is$1.keyboardEvent(event) && event.which !== 32) {
              return;
            }

            event.preventDefault();
            event.stopPropagation();
            menuItem.checked = true;

            switch (type) {
              case "language":
                _this2.currentTrack = Number(value);
                break;

              case "quality":
                _this2.quality = value;
                break;

              case "speed":
                _this2.speed = parseFloat(value);
                break;

              default:
                break;
            }

            controls.showMenuPanel.call(
              _this2,
              "home",
              is$1.keyboardEvent(event)
            );
          },
          type,
          false
        );
        controls.bindMenuItemShortcuts.call(this, menuItem, type);
        list.appendChild(menuItem);
      },
      // Format a time for display
      formatTime: function formatTime$$1() {
        var time =
          arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
        var inverted =
          arguments.length > 1 && arguments[1] !== undefined
            ? arguments[1]
            : false;

        // Bail if the value isn't a number
        if (!is$1.number(time)) {
          return time;
        } // Always display hours if duration is over an hour

        var forceHours = getHours(this.duration) > 0;
        return formatTime(time, forceHours, inverted);
      },
      // Update the displayed time
      updateTimeDisplay: function updateTimeDisplay() {
        var target =
          arguments.length > 0 && arguments[0] !== undefined
            ? arguments[0]
            : null;
        var time =
          arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
        var inverted =
          arguments.length > 2 && arguments[2] !== undefined
            ? arguments[2]
            : false;

        // Bail if there's no element to display or the value isn't a number
        if (!is$1.element(target) || !is$1.number(time)) {
          return;
        } // eslint-disable-next-line no-param-reassign

        target.innerText = controls.formatTime(time, inverted);
      },
      // Update volume UI and storage
      updateVolume: function updateVolume() {
        if (!this.supported.ui) {
          return;
        } // Update range

        if (is$1.element(this.elements.inputs.volume)) {
          controls.setRange.call(
            this,
            this.elements.inputs.volume,
            this.muted ? 0 : this.volume
          );
        } // Update mute state

        if (is$1.element(this.elements.buttons.mute)) {
          this.elements.buttons.mute.pressed = this.muted || this.volume === 0;
        }
      },
      // Update seek value and lower fill
      setRange: function setRange(target) {
        var value =
          arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;

        if (!is$1.element(target)) {
          return;
        } // eslint-disable-next-line

        target.value = value; // Webkit range fill

        controls.updateRangeFill.call(this, target);
      },
      // Update <progress> elements
      updateProgress: function updateProgress(event) {
        var _this3 = this;

        if (!this.supported.ui || !is$1.event(event)) {
          return;
        }

        var value = 0;

        var setProgress = function setProgress(target, input) {
          var value = is$1.number(input) ? input : 0;
          var progress = is$1.element(target)
            ? target
            : _this3.elements.display.buffer; // Update value and label

          if (is$1.element(progress)) {
            progress.value = value; // Update text label inside

            var label = progress.getElementsByTagName("span")[0];

            if (is$1.element(label)) {
              label.childNodes[0].nodeValue = value;
            }
          }
        };

        if (event) {
          switch (event.type) {
            // Video playing
            case "timeupdate":
            case "seeking":
            case "seeked":
              value = getPercentage(this.currentTime, this.duration); // Set seek range value only if it's a 'natural' time event

              if (event.type === "timeupdate") {
                controls.setRange.call(this, this.elements.inputs.seek, value);
              }

              break;
            // Check buffer status

            case "playing":
            case "progress":
              setProgress(this.elements.display.buffer, this.buffered * 100);
              break;

            default:
              break;
          }
        }
      },
      // Webkit polyfill for lower fill range
      updateRangeFill: function updateRangeFill(target) {
        // Get range from event if event passed
        var range = is$1.event(target) ? target.target : target; // Needs to be a valid <input type='range'>

        if (!is$1.element(range) || range.getAttribute("type") !== "range") {
          return;
        } // Set aria values for https://github.com/sampotts/plyr/issues/905

        if (matches(range, this.config.selectors.inputs.seek)) {
          range.setAttribute("aria-valuenow", this.currentTime);
          var currentTime = controls.formatTime(this.currentTime);
          var duration = controls.formatTime(this.duration);
          var format$$1 = i18n.get("seekLabel", this.config);
          range.setAttribute(
            "aria-valuetext",
            format$$1
              .replace("{currentTime}", currentTime)
              .replace("{duration}", duration)
          );
        } else if (matches(range, this.config.selectors.inputs.volume)) {
          var percent = range.value * 100;
          range.setAttribute("aria-valuenow", percent);
          range.setAttribute(
            "aria-valuetext",
            "".concat(percent.toFixed(1), "%")
          );
        } else {
          range.setAttribute("aria-valuenow", range.value);
        } // WebKit only

        if (!browser.isWebkit) {
          return;
        } // Set CSS custom property

        range.style.setProperty(
          "--value",
          "".concat((range.value / range.max) * 100, "%")
        );
      },
      // Update hover tooltip for seeking
      updateSeekTooltip: function updateSeekTooltip(event) {
        var _this4 = this;

        // Bail if setting not true
        if (
          !this.config.tooltips.seek ||
          !is$1.element(this.elements.inputs.seek) ||
          !is$1.element(this.elements.display.seekTooltip) ||
          this.duration === 0
        ) {
          return;
        } // Calculate percentage

        var percent = 0;
        var clientRect = this.elements.progress.getBoundingClientRect();
        var visible = "".concat(this.config.classNames.tooltip, "--visible");

        var toggle = function toggle(_toggle) {
          toggleClass(_this4.elements.display.seekTooltip, visible, _toggle);
        }; // Hide on touch

        if (this.touch) {
          toggle(false);
          return;
        } // Determine percentage, if already visible

        if (is$1.event(event)) {
          percent = (100 / clientRect.width) * (event.pageX - clientRect.left);
        } else if (hasClass(this.elements.display.seekTooltip, visible)) {
          percent = parseFloat(
            this.elements.display.seekTooltip.style.left,
            10
          );
        } else {
          return;
        } // Set bounds

        if (percent < 0) {
          percent = 0;
        } else if (percent > 100) {
          percent = 100;
        } // Display the time a click would seek to

        controls.updateTimeDisplay.call(
          this,
          this.elements.display.seekTooltip,
          (this.duration / 100) * percent
        ); // Set position

        this.elements.display.seekTooltip.style.left = "".concat(percent, "%"); // Show/hide the tooltip
        // If the event is a moues in/out and percentage is inside bounds

        if (
          is$1.event(event) &&
          ["mouseenter", "mouseleave"].includes(event.type)
        ) {
          toggle(event.type === "mouseenter");
        }
      },
      // Handle time change event
      timeUpdate: function timeUpdate(event) {
        // Only invert if only one time element is displayed and used for both duration and currentTime
        var invert =
          !is$1.element(this.elements.display.duration) &&
          this.config.invertTime; // Duration

        controls.updateTimeDisplay.call(
          this,
          this.elements.display.currentTime,
          invert ? this.duration - this.currentTime : this.currentTime,
          invert
        ); // Ignore updates while seeking

        if (event && event.type === "timeupdate" && this.media.seeking) {
          return;
        } // Playing progress

        controls.updateProgress.call(this, event);
      },
      // Show the duration on metadataloaded or durationchange events
      durationUpdate: function durationUpdate() {
        // Bail if no UI or durationchange event triggered after playing/seek when invertTime is false
        if (
          !this.supported.ui ||
          (!this.config.invertTime && this.currentTime)
        ) {
          return;
        } // If duration is the 2**32 (shaka), Infinity (HLS), DASH-IF (Number.MAX_SAFE_INTEGER || Number.MAX_VALUE) indicating live we hide the currentTime and progressbar.
        // https://github.com/video-dev/hls.js/blob/5820d29d3c4c8a46e8b75f1e3afa3e68c1a9a2db/src/controller/buffer-controller.js#L415
        // https://github.com/google/shaka-player/blob/4d889054631f4e1cf0fbd80ddd2b71887c02e232/lib/media/streaming_engine.js#L1062
        // https://github.com/Dash-Industry-Forum/dash.js/blob/69859f51b969645b234666800d4cb596d89c602d/src/dash/models/DashManifestModel.js#L338

        if (this.duration >= Math.pow(2, 32)) {
          toggleHidden(this.elements.display.currentTime, true);
          toggleHidden(this.elements.progress, true);
          return;
        } // Update ARIA values

        if (is$1.element(this.elements.inputs.seek)) {
          this.elements.inputs.seek.setAttribute(
            "aria-valuemax",
            this.duration
          );
        } // If there's a spot to display duration

        var hasDuration = is$1.element(this.elements.display.duration); // If there's only one time display, display duration there

        if (!hasDuration && this.config.displayDuration && this.paused) {
          controls.updateTimeDisplay.call(
            this,
            this.elements.display.currentTime,
            this.duration
          );
        } // If there's a duration element, update content

        if (hasDuration) {
          controls.updateTimeDisplay.call(
            this,
            this.elements.display.duration,
            this.duration
          );
        } // Update the tooltip (if visible)

        controls.updateSeekTooltip.call(this);
      },
      // Hide/show a tab
      toggleMenuButton: function toggleMenuButton(setting, toggle) {
        toggleHidden(this.elements.settings.buttons[setting], !toggle);
      },
      // Update the selected setting
      updateSetting: function updateSetting(setting, container, input) {
        var pane = this.elements.settings.panels[setting];
        var value = null;
        var list = container;

        if (setting === "captions") {
          value = this.currentTrack;
        } else {
          value = !is$1.empty(input) ? input : this[setting]; // Get default

          if (is$1.empty(value)) {
            value = this.config[setting].default;
          } // Unsupported value

          if (
            !is$1.empty(this.options[setting]) &&
            !this.options[setting].includes(value)
          ) {
            this.debug.warn(
              "Unsupported value of '".concat(value, "' for ").concat(setting)
            );
            return;
          } // Disabled value

          if (!this.config[setting].options.includes(value)) {
            this.debug.warn(
              "Disabled value of '".concat(value, "' for ").concat(setting)
            );
            return;
          }
        } // Get the list if we need to

        if (!is$1.element(list)) {
          list = pane && pane.querySelector('[role="menu"]');
        } // If there's no list it means it's not been rendered...

        if (!is$1.element(list)) {
          return;
        } // Update the label

        var label = this.elements.settings.buttons[setting].querySelector(
          ".".concat(this.config.classNames.menu.value)
        );
        label.innerHTML = controls.getLabel.call(this, setting, value); // Find the radio option and check it

        var target = list && list.querySelector('[value="'.concat(value, '"]'));

        if (is$1.element(target)) {
          target.checked = true;
        }
      },
      // Translate a value into a nice label
      getLabel: function getLabel(setting, value) {
        switch (setting) {
          case "speed":
            return value === 1
              ? i18n.get("normal", this.config)
              : "".concat(value, "&times;");

          case "quality":
            if (is$1.number(value)) {
              var label = i18n.get("qualityLabel.".concat(value), this.config);

              if (!label.length) {
                return "".concat(value, "p");
              }

              return label;
            }

            return toTitleCase(value);

          case "captions":
            return captions.getLabel.call(this);

          default:
            return null;
        }
      },
      // Set the quality menu
      setQualityMenu: function setQualityMenu(options) {
        var _this5 = this;

        // Menu required
        if (!is$1.element(this.elements.settings.panels.quality)) {
          return;
        }

        var type = "quality";
        var list = this.elements.settings.panels.quality.querySelector(
          '[role="menu"]'
        ); // Set options if passed and filter based on uniqueness and config

        if (is$1.array(options)) {
          this.options.quality = dedupe(options).filter(function (quality) {
            return _this5.config.quality.options.includes(quality);
          });
        } // Toggle the pane and tab

        var toggle =
          !is$1.empty(this.options.quality) && this.options.quality.length > 1;
        controls.toggleMenuButton.call(this, type, toggle); // Empty the menu

        emptyElement(list); // Check if we need to toggle the parent

        controls.checkMenu.call(this); // If we're hiding, nothing more to do

        if (!toggle) {
          return;
        } // Get the badge HTML for HD, 4K etc

        var getBadge = function getBadge(quality) {
          var label = i18n.get("qualityBadge.".concat(quality), _this5.config);

          if (!label.length) {
            return null;
          }

          return controls.createBadge.call(_this5, label);
        }; // Sort options by the config and then render options

        this.options.quality
          .sort(function (a, b) {
            var sorting = _this5.config.quality.options;
            return sorting.indexOf(a) > sorting.indexOf(b) ? 1 : -1;
          })
          .forEach(function (quality) {
            controls.createMenuItem.call(_this5, {
              value: quality,
              list: list,
              type: type,
              title: controls.getLabel.call(_this5, "quality", quality),
              badge: getBadge(quality),
            });
          });
        controls.updateSetting.call(this, type, list);
      },
      // Set the looping options

      /* setLoopMenu() {
        // Menu required
        if (!is.element(this.elements.settings.panels.loop)) {
            return;
        }
         const options = ['start', 'end', 'all', 'reset'];
        const list = this.elements.settings.panels.loop.querySelector('[role="menu"]');
         // Show the pane and tab
        toggleHidden(this.elements.settings.buttons.loop, false);
        toggleHidden(this.elements.settings.panels.loop, false);
         // Toggle the pane and tab
        const toggle = !is.empty(this.loop.options);
        controls.toggleMenuButton.call(this, 'loop', toggle);
         // Empty the menu
        emptyElement(list);
         options.forEach(option => {
            const item = createElement('li');
             const button = createElement(
                'button',
                extend(getAttributesFromSelector(this.config.selectors.buttons.loop), {
                    type: 'button',
                    class: this.config.classNames.control,
                    'data-plyr-loop-action': option,
                }),
                i18n.get(option, this.config)
            );
             if (['start', 'end'].includes(option)) {
                const badge = controls.createBadge.call(this, '00:00');
                button.appendChild(badge);
            }
             item.appendChild(button);
            list.appendChild(item);
        });
    }, */
      // Get current selected caption language
      // TODO: rework this to user the getter in the API?
      // Set a list of available captions languages
      setCaptionsMenu: function setCaptionsMenu() {
        var _this6 = this;

        // Menu required
        if (!is$1.element(this.elements.settings.panels.captions)) {
          return;
        } // TODO: Captions or language? Currently it's mixed

        var type = "captions";
        var list = this.elements.settings.panels.captions.querySelector(
          '[role="menu"]'
        );
        var tracks = captions.getTracks.call(this);
        var toggle = Boolean(tracks.length); // Toggle the pane and tab

        controls.toggleMenuButton.call(this, type, toggle); // Empty the menu

        emptyElement(list); // Check if we need to toggle the parent

        controls.checkMenu.call(this); // If there's no captions, bail

        if (!toggle) {
          return;
        } // Generate options data

        var options = tracks.map(function (track, value) {
          return {
            value: value,
            checked: _this6.captions.toggled && _this6.currentTrack === value,
            title: captions.getLabel.call(_this6, track),
            badge:
              track.language &&
              controls.createBadge.call(_this6, track.language.toUpperCase()),
            list: list,
            type: "language",
          };
        }); // Add the "Disabled" option to turn off captions

        options.unshift({
          value: -1,
          checked: !this.captions.toggled,
          title: i18n.get("disabled", this.config),
          list: list,
          type: "language",
        }); // Generate options

        options.forEach(controls.createMenuItem.bind(this));
        controls.updateSetting.call(this, type, list);
      },
      // Set a list of available captions languages
      setSpeedMenu: function setSpeedMenu(options) {
        var _this7 = this;

        // Menu required
        if (!is$1.element(this.elements.settings.panels.speed)) {
          return;
        }

        var type = "speed";
        var list = this.elements.settings.panels.speed.querySelector(
          '[role="menu"]'
        ); // Set the speed options

        if (is$1.array(options)) {
          this.options.speed = options;
        } else if (this.isHTML5 || this.isVimeo) {
          this.options.speed = [0.5, 0.75, 1, 1.25, 1.5, 1.75, 2];
        } // Set options if passed and filter based on config

        this.options.speed = this.options.speed.filter(function (speed) {
          return _this7.config.speed.options.includes(speed);
        }); // Toggle the pane and tab

        var toggle =
          !is$1.empty(this.options.speed) && this.options.speed.length > 1;
        controls.toggleMenuButton.call(this, type, toggle); // Empty the menu

        emptyElement(list); // Check if we need to toggle the parent

        controls.checkMenu.call(this); // If we're hiding, nothing more to do

        if (!toggle) {
          return;
        } // Create items

        this.options.speed.forEach(function (speed) {
          controls.createMenuItem.call(_this7, {
            value: speed,
            list: list,
            type: type,
            title: controls.getLabel.call(_this7, "speed", speed),
          });
        });
        controls.updateSetting.call(this, type, list);
      },
      // Check if we need to hide/show the settings menu
      checkMenu: function checkMenu() {
        var buttons = this.elements.settings.buttons;
        var visible =
          !is$1.empty(buttons) &&
          Object.values(buttons).some(function (button) {
            return !button.hidden;
          });
        toggleHidden(this.elements.settings.menu, !visible);
      },
      // Focus the first menu item in a given (or visible) menu
      focusFirstMenuItem: function focusFirstMenuItem(pane) {
        var tabFocus =
          arguments.length > 1 && arguments[1] !== undefined
            ? arguments[1]
            : false;

        if (this.elements.settings.popup.hidden) {
          return;
        }

        var target = pane;

        if (!is$1.element(target)) {
          target = Object.values(this.elements.settings.panels).find(function (
            pane
          ) {
            return !pane.hidden;
          });
        }

        var firstItem = target.querySelector('[role^="menuitem"]');
        setFocus.call(this, firstItem, tabFocus);
      },
      // Show/hide menu
      toggleMenu: function toggleMenu(input) {
        var popup = this.elements.settings.popup;
        var button = this.elements.buttons.settings; // Menu and button are required

        if (!is$1.element(popup) || !is$1.element(button)) {
          return;
        } // True toggle by default

        var hidden = popup.hidden;
        var show = hidden;

        if (is$1.boolean(input)) {
          show = input;
        } else if (is$1.keyboardEvent(input) && input.which === 27) {
          show = false;
        } else if (is$1.event(input)) {
          var isMenuItem = popup.contains(input.target); // If the click was inside the menu or if the click
          // wasn't the button or menu item and we're trying to
          // show the menu (a doc click shouldn't show the menu)

          if (isMenuItem || (!isMenuItem && input.target !== button && show)) {
            return;
          }
        } // Set button attributes

        button.setAttribute("aria-expanded", show); // Show the actual popup

        toggleHidden(popup, !show); // Add class hook

        toggleClass(
          this.elements.container,
          this.config.classNames.menu.open,
          show
        ); // Focus the first item if key interaction

        if (show && is$1.keyboardEvent(input)) {
          controls.focusFirstMenuItem.call(this, null, true);
        } else if (!show && !hidden) {
          // If closing, re-focus the button
          setFocus.call(this, button, is$1.keyboardEvent(input));
        }
      },
      // Get the natural size of a menu panel
      getMenuSize: function getMenuSize(tab) {
        var clone = tab.cloneNode(true);
        clone.style.position = "absolute";
        clone.style.opacity = 0;
        clone.removeAttribute("hidden"); // Append to parent so we get the "real" size

        tab.parentNode.appendChild(clone); // Get the sizes before we remove

        var width = clone.scrollWidth;
        var height = clone.scrollHeight; // Remove from the DOM

        removeElement(clone);
        return {
          width: width,
          height: height,
        };
      },
      // Show a panel in the menu
      showMenuPanel: function showMenuPanel() {
        var _this8 = this;

        var type =
          arguments.length > 0 && arguments[0] !== undefined
            ? arguments[0]
            : "";
        var tabFocus =
          arguments.length > 1 && arguments[1] !== undefined
            ? arguments[1]
            : false;
        var target = document.getElementById(
          "plyr-settings-".concat(this.id, "-").concat(type)
        ); // Nothing to show, bail

        if (!is$1.element(target)) {
          return;
        } // Hide all other panels

        var container = target.parentNode;
        var current = Array.from(container.children).find(function (node) {
          return !node.hidden;
        }); // If we can do fancy animations, we'll animate the height/width

        if (support.transitions && !support.reducedMotion) {
          // Set the current width as a base
          container.style.width = "".concat(current.scrollWidth, "px");
          container.style.height = "".concat(current.scrollHeight, "px"); // Get potential sizes

          var size = controls.getMenuSize.call(this, target); // Restore auto height/width

          var restore = function restore(event) {
            // We're only bothered about height and width on the container
            if (
              event.target !== container ||
              !["width", "height"].includes(event.propertyName)
            ) {
              return;
            } // Revert back to auto

            container.style.width = "";
            container.style.height = ""; // Only listen once

            off.call(_this8, container, transitionEndEvent, restore);
          }; // Listen for the transition finishing and restore auto height/width

          on.call(this, container, transitionEndEvent, restore); // Set dimensions to target

          container.style.width = "".concat(size.width, "px");
          container.style.height = "".concat(size.height, "px");
        } // Set attributes on current tab

        toggleHidden(current, true); // Set attributes on target

        toggleHidden(target, false); // Focus the first item

        controls.focusFirstMenuItem.call(this, target, tabFocus);
      },
      // Set the download link
      setDownloadLink: function setDownloadLink() {
        var button = this.elements.buttons.download; // Bail if no button

        if (!is$1.element(button)) {
          return;
        } // Set download link

        button.setAttribute("href", this.download);
      },
      // Build the default HTML
      // TODO: Set order based on order in the config.controls array?
      create: function create(data) {
        var _this9 = this;

        // Create the container
        var container = createElement(
          "div",
          getAttributesFromSelector(this.config.selectors.controls.wrapper)
        ); // Restart button

        if (this.config.controls.includes("restart")) {
          container.appendChild(controls.createButton.call(this, "restart"));
        } // Rewind button

        if (this.config.controls.includes("rewind")) {
          container.appendChild(controls.createButton.call(this, "rewind"));
        } // Play/Pause button

        if (this.config.controls.includes("play")) {
          container.appendChild(controls.createButton.call(this, "play"));
        } // Fast forward button

        if (this.config.controls.includes("fast-forward")) {
          container.appendChild(
            controls.createButton.call(this, "fast-forward")
          );
        } // Progress

        if (this.config.controls.includes("progress")) {
          var progress = createElement(
            "div",
            getAttributesFromSelector(this.config.selectors.progress)
          ); // Seek range slider

          progress.appendChild(
            controls.createRange.call(this, "seek", {
              id: "plyr-seek-".concat(data.id),
            })
          ); // Buffer progress

          progress.appendChild(controls.createProgress.call(this, "buffer")); // TODO: Add loop display indicator
          // Seek tooltip

          if (this.config.tooltips.seek) {
            var tooltip = createElement(
              "span",
              {
                class: this.config.classNames.tooltip,
              },
              "00:00"
            );
            progress.appendChild(tooltip);
            this.elements.display.seekTooltip = tooltip;
          }

          this.elements.progress = progress;
          container.appendChild(this.elements.progress);
        } // Media current time display

        if (this.config.controls.includes("current-time")) {
          container.appendChild(controls.createTime.call(this, "currentTime"));
        } // Media duration display

        if (this.config.controls.includes("duration")) {
          container.appendChild(controls.createTime.call(this, "duration"));
        } // Volume controls

        if (
          this.config.controls.includes("mute") ||
          this.config.controls.includes("volume")
        ) {
          var volume = createElement("div", {
            class: "plyr__volume",
          }); // Toggle mute button

          if (this.config.controls.includes("mute")) {
            volume.appendChild(controls.createButton.call(this, "mute"));
          } // Volume range control

          if (this.config.controls.includes("volume")) {
            // Set the attributes
            var attributes = {
              max: 1,
              step: 0.05,
              value: this.config.volume,
            }; // Create the volume range slider

            volume.appendChild(
              controls.createRange.call(
                this,
                "volume",
                extend(attributes, {
                  id: "plyr-volume-".concat(data.id),
                })
              )
            );
            this.elements.volume = volume;
          }

          container.appendChild(volume);
        } // Toggle captions button

        if (this.config.controls.includes("captions")) {
          container.appendChild(controls.createButton.call(this, "captions"));
        } // Settings button / menu

        if (
          this.config.controls.includes("settings") &&
          !is$1.empty(this.config.settings)
        ) {
          var control = createElement("div", {
            class: "plyr__menu",
            hidden: "",
          });
          control.appendChild(
            controls.createButton.call(this, "settings", {
              "aria-haspopup": true,
              "aria-controls": "plyr-settings-".concat(data.id),
              "aria-expanded": false,
            })
          );
          var popup = createElement("div", {
            class: "plyr__menu__container",
            id: "plyr-settings-".concat(data.id),
            hidden: "",
          });
          var inner = createElement("div");
          var home = createElement("div", {
            id: "plyr-settings-".concat(data.id, "-home"),
          }); // Create the menu

          var menu = createElement("div", {
            role: "menu",
          });
          home.appendChild(menu);
          inner.appendChild(home);
          this.elements.settings.panels.home = home; // Build the menu items

          this.config.settings.forEach(function (type) {
            // TODO: bundle this with the createMenuItem helper and bindings
            var menuItem = createElement(
              "button",
              extend(
                getAttributesFromSelector(
                  _this9.config.selectors.buttons.settings
                ),
                {
                  type: "button",
                  class: ""
                    .concat(_this9.config.classNames.control, " ")
                    .concat(_this9.config.classNames.control, "--forward"),
                  role: "menuitem",
                  "aria-haspopup": true,
                  hidden: "",
                }
              )
            ); // Bind menu shortcuts for keyboard users

            controls.bindMenuItemShortcuts.call(_this9, menuItem, type); // Show menu on click

            on(menuItem, "click", function () {
              controls.showMenuPanel.call(_this9, type, false);
            });
            var flex = createElement(
              "span",
              null,
              i18n.get(type, _this9.config)
            );
            var value = createElement("span", {
              class: _this9.config.classNames.menu.value,
            }); // Speed contains HTML entities

            value.innerHTML = data[type];
            flex.appendChild(value);
            menuItem.appendChild(flex);
            menu.appendChild(menuItem); // Build the panes

            var pane = createElement("div", {
              id: "plyr-settings-".concat(data.id, "-").concat(type),
              hidden: "",
            }); // Back button

            var backButton = createElement("button", {
              type: "button",
              class: ""
                .concat(_this9.config.classNames.control, " ")
                .concat(_this9.config.classNames.control, "--back"),
            }); // Visible label

            backButton.appendChild(
              createElement(
                "span",
                {
                  "aria-hidden": true,
                },
                i18n.get(type, _this9.config)
              )
            ); // Screen reader label

            backButton.appendChild(
              createElement(
                "span",
                {
                  class: _this9.config.classNames.hidden,
                },
                i18n.get("menuBack", _this9.config)
              )
            ); // Go back via keyboard

            on(
              pane,
              "keydown",
              function (event) {
                // We only care about <-
                if (event.which !== 37) {
                  return;
                } // Prevent seek

                event.preventDefault();
                event.stopPropagation(); // Show the respective menu

                controls.showMenuPanel.call(_this9, "home", true);
              },
              false
            ); // Go back via button click

            on(backButton, "click", function () {
              controls.showMenuPanel.call(_this9, "home", false);
            }); // Add to pane

            pane.appendChild(backButton); // Menu

            pane.appendChild(
              createElement("div", {
                role: "menu",
              })
            );
            inner.appendChild(pane);
            _this9.elements.settings.buttons[type] = menuItem;
            _this9.elements.settings.panels[type] = pane;
          });
          popup.appendChild(inner);
          control.appendChild(popup);
          container.appendChild(control);
          this.elements.settings.popup = popup;
          this.elements.settings.menu = control;
        } // Picture in picture button

        if (this.config.controls.includes("pip") && support.pip) {
          container.appendChild(controls.createButton.call(this, "pip"));
        } // Airplay button

        if (this.config.controls.includes("airplay") && support.airplay) {
          container.appendChild(controls.createButton.call(this, "airplay"));
        } // Download button

        if (this.config.controls.includes("download")) {
          var _attributes = {
            element: "a",
            href: this.download,
            target: "_blank",
          };
          var download = this.config.urls.download;

          if (!is$1.url(download) && this.isEmbed) {
            extend(_attributes, {
              icon: "logo-".concat(this.provider),
              label: this.provider,
            });
          }

          container.appendChild(
            controls.createButton.call(this, "download", _attributes)
          );
        } // Toggle fullscreen button

        if (this.config.controls.includes("fullscreen")) {
          container.appendChild(controls.createButton.call(this, "fullscreen"));
        } // Larger overlaid play button

        if (this.config.controls.includes("play-large")) {
          this.elements.container.appendChild(
            controls.createButton.call(this, "play-large")
          );
        }

        this.elements.controls = container; // Set available quality levels

        if (this.isHTML5) {
          controls.setQualityMenu.call(
            this,
            html5.getQualityOptions.call(this)
          );
        }

        controls.setSpeedMenu.call(this);
        return container;
      },
      // Insert controls
      inject: function inject() {
        var _this10 = this;

        // Sprite
        if (this.config.loadSprite) {
          var icon = controls.getIconUrl.call(this); // Only load external sprite using AJAX

          if (icon.cors) {
            loadSprite(icon.url, "sprite-plyr");
          }
        } // Create a unique ID

        this.id = Math.floor(Math.random() * 10000); // Null by default

        var container = null;
        this.elements.controls = null; // Set template properties

        var props = {
          id: this.id,
          seektime: this.config.seekTime,
          title: this.config.title,
        };
        var update = true; // If function, run it and use output

        if (is$1.function(this.config.controls)) {
          this.config.controls = this.config.controls.call(this.props);
        } // Convert falsy controls to empty array (primarily for empty strings)

        if (!this.config.controls) {
          this.config.controls = [];
        }

        if (
          is$1.element(this.config.controls) ||
          is$1.string(this.config.controls)
        ) {
          // HTMLElement or Non-empty string passed as the option
          container = this.config.controls;
        } else {
          // Create controls
          container = controls.create.call(this, {
            id: this.id,
            seektime: this.config.seekTime,
            speed: this.speed,
            quality: this.quality,
            captions: captions.getLabel.call(this), // TODO: Looping
            // loop: 'None',
          });
          update = false;
        } // Replace props with their value

        var replace = function replace(input) {
          var result = input;
          Object.entries(props).forEach(function (_ref2) {
            var _ref3 = _slicedToArray(_ref2, 2),
              key = _ref3[0],
              value = _ref3[1];

            result = replaceAll(result, "{".concat(key, "}"), value);
          });
          return result;
        }; // Update markup

        if (update) {
          if (is$1.string(this.config.controls)) {
            container = replace(container);
          } else if (is$1.element(container)) {
            container.innerHTML = replace(container.innerHTML);
          }
        } // Controls container

        var target; // Inject to custom location

        if (is$1.string(this.config.selectors.controls.container)) {
          target = document.querySelector(
            this.config.selectors.controls.container
          );
        } // Inject into the container by default

        if (!is$1.element(target)) {
          target = this.elements.container;
        } // Inject controls HTML (needs to be before captions, hence "afterbegin")

        var insertMethod = is$1.element(container)
          ? "insertAdjacentElement"
          : "insertAdjacentHTML";
        target[insertMethod]("afterbegin", container); // Find the elements if need be

        if (!is$1.element(this.elements.controls)) {
          controls.findElements.call(this);
        } // Add pressed property to buttons

        if (!is$1.empty(this.elements.buttons)) {
          var addProperty = function addProperty(button) {
            var className = _this10.config.classNames.controlPressed;
            Object.defineProperty(button, "pressed", {
              enumerable: true,
              get: function get() {
                return hasClass(button, className);
              },
              set: function set() {
                var pressed =
                  arguments.length > 0 && arguments[0] !== undefined
                    ? arguments[0]
                    : false;
                toggleClass(button, className, pressed);
              },
            });
          }; // Toggle classname when pressed property is set

          Object.values(this.elements.buttons)
            .filter(Boolean)
            .forEach(function (button) {
              if (is$1.array(button) || is$1.nodeList(button)) {
                Array.from(button).filter(Boolean).forEach(addProperty);
              } else {
                addProperty(button);
              }
            });
        } // Edge sometimes doesn't finish the paint so force a redraw

        if (window.navigator.userAgent.includes("Edge")) {
          repaint(target);
        } // Setup tooltips

        if (this.config.tooltips.controls) {
          var _this$config = this.config,
            classNames = _this$config.classNames,
            selectors = _this$config.selectors;
          var selector = ""
            .concat(selectors.controls.wrapper, " ")
            .concat(selectors.labels, " .")
            .concat(classNames.hidden);
          var labels = getElements.call(this, selector);
          Array.from(labels).forEach(function (label) {
            toggleClass(label, _this10.config.classNames.hidden, false);
            toggleClass(label, _this10.config.classNames.tooltip, true);
          });
        }
      },
    };

    /**
     * Parse a string to a URL object
     * @param {string} input - the URL to be parsed
     * @param {boolean} safe - failsafe parsing
     */

    function parseUrl(input) {
      var safe =
        arguments.length > 1 && arguments[1] !== undefined
          ? arguments[1]
          : true;
      var url = input;

      if (safe) {
        var parser = document.createElement("a");
        parser.href = url;
        url = parser.href;
      }

      try {
        return new URL(url);
      } catch (e) {
        return null;
      }
    } // Convert object to URLSearchParams

    function buildUrlParams(input) {
      var params = new URLSearchParams();

      if (is$1.object(input)) {
        Object.entries(input).forEach(function (_ref) {
          var _ref2 = _slicedToArray(_ref, 2),
            key = _ref2[0],
            value = _ref2[1];

          params.set(key, value);
        });
      }

      return params;
    }

    var captions = {
      // Setup captions
      setup: function setup() {
        // Requires UI support
        if (!this.supported.ui) {
          return;
        } // Only Vimeo and HTML5 video supported at this point

        if (
          !this.isVideo ||
          this.isYouTube ||
          (this.isHTML5 && !support.textTracks)
        ) {
          // Clear menu and hide
          if (
            is$1.array(this.config.controls) &&
            this.config.controls.includes("settings") &&
            this.config.settings.includes("captions")
          ) {
            controls.setCaptionsMenu.call(this);
          }

          return;
        } // Inject the container

        if (!is$1.element(this.elements.captions)) {
          this.elements.captions = createElement(
            "div",
            getAttributesFromSelector(this.config.selectors.captions)
          );
          insertAfter(this.elements.captions, this.elements.wrapper);
        } // Fix IE captions if CORS is used
        // Fetch captions and inject as blobs instead (data URIs not supported!)

        if (browser.isIE && window.URL) {
          var elements = this.media.querySelectorAll("track");
          Array.from(elements).forEach(function (track) {
            var src = track.getAttribute("src");
            var url = parseUrl(src);

            if (
              url !== null &&
              url.hostname !== window.location.href.hostname &&
              ["http:", "https:"].includes(url.protocol)
            ) {
              fetch(src, "blob")
                .then(function (blob) {
                  track.setAttribute("src", window.URL.createObjectURL(blob));
                })
                .catch(function () {
                  removeElement(track);
                });
            }
          });
        } // Get and set initial data
        // The "preferred" options are not realized unless / until the wanted language has a match
        // * languages: Array of user's browser languages.
        // * language:  The language preferred by user settings or config
        // * active:    The state preferred by user settings or config
        // * toggled:   The real captions state

        var browserLanguages = navigator.languages || [
          navigator.language || navigator.userLanguage || "en",
        ];
        var languages = dedupe(
          browserLanguages.map(function (language) {
            return language.split("-")[0];
          })
        );
        var language = (
          this.storage.get("language") ||
          this.config.captions.language ||
          "auto"
        ).toLowerCase(); // Use first browser language when language is 'auto'

        if (language === "auto") {
          var _languages = _slicedToArray(languages, 1);

          language = _languages[0];
        }

        var active = this.storage.get("captions");

        if (!is$1.boolean(active)) {
          active = this.config.captions.active;
        }

        Object.assign(this.captions, {
          toggled: false,
          active: active,
          language: language,
          languages: languages,
        }); // Watch changes to textTracks and update captions menu

        if (this.isHTML5) {
          var trackEvents = this.config.captions.update
            ? "addtrack removetrack"
            : "removetrack";
          on.call(
            this,
            this.media.textTracks,
            trackEvents,
            captions.update.bind(this)
          );
        } // Update available languages in list next tick (the event must not be triggered before the listeners)

        setTimeout(captions.update.bind(this), 0);
      },
      // Update available language options in settings based on tracks
      update: function update() {
        var _this = this;

        var tracks = captions.getTracks.call(this, true); // Get the wanted language

        var _this$captions = this.captions,
          active = _this$captions.active,
          language = _this$captions.language,
          meta = _this$captions.meta,
          currentTrackNode = _this$captions.currentTrackNode;
        var languageExists = Boolean(
          tracks.find(function (track) {
            return track.language === language;
          })
        ); // Handle tracks (add event listener and "pseudo"-default)

        if (this.isHTML5 && this.isVideo) {
          tracks
            .filter(function (track) {
              return !meta.get(track);
            })
            .forEach(function (track) {
              _this.debug.log("Track added", track); // Attempt to store if the original dom element was "default"

              meta.set(track, {
                default: track.mode === "showing",
              }); // Turn off native caption rendering to avoid double captions

              track.mode = "hidden"; // Add event listener for cue changes

              on.call(_this, track, "cuechange", function () {
                return captions.updateCues.call(_this);
              });
            });
        } // Update language first time it matches, or if the previous matching track was removed

        if (
          (languageExists && this.language !== language) ||
          !tracks.includes(currentTrackNode)
        ) {
          captions.setLanguage.call(this, language);
          captions.toggle.call(this, active && languageExists);
        } // Enable or disable captions based on track length

        toggleClass(
          this.elements.container,
          this.config.classNames.captions.enabled,
          !is$1.empty(tracks)
        ); // Update available languages in list

        if (
          (this.config.controls || []).includes("settings") &&
          this.config.settings.includes("captions")
        ) {
          controls.setCaptionsMenu.call(this);
        }
      },
      // Toggle captions display
      // Used internally for the toggleCaptions method, with the passive option forced to false
      toggle: function toggle(input) {
        var passive =
          arguments.length > 1 && arguments[1] !== undefined
            ? arguments[1]
            : true;

        // If there's no full support
        if (!this.supported.ui) {
          return;
        }

        var toggled = this.captions.toggled; // Current state

        var activeClass = this.config.classNames.captions.active; // Get the next state
        // If the method is called without parameter, toggle based on current value

        var active = is$1.nullOrUndefined(input) ? !toggled : input; // Update state and trigger event

        if (active !== toggled) {
          // When passive, don't override user preferences
          if (!passive) {
            this.captions.active = active;
            this.storage.set({
              captions: active,
            });
          } // Force language if the call isn't passive and there is no matching language to toggle to

          if (!this.language && active && !passive) {
            var tracks = captions.getTracks.call(this);
            var track = captions.findTrack.call(
              this,
              [this.captions.language].concat(
                _toConsumableArray(this.captions.languages)
              ),
              true
            ); // Override user preferences to avoid switching languages if a matching track is added

            this.captions.language = track.language; // Set caption, but don't store in localStorage as user preference

            captions.set.call(this, tracks.indexOf(track));
            return;
          } // Toggle button if it's enabled

          if (this.elements.buttons.captions) {
            this.elements.buttons.captions.pressed = active;
          } // Add class hook

          toggleClass(this.elements.container, activeClass, active);
          this.captions.toggled = active; // Update settings menu

          controls.updateSetting.call(this, "captions"); // Trigger event (not used internally)

          triggerEvent.call(
            this,
            this.media,
            active ? "captionsenabled" : "captionsdisabled"
          );
        }
      },
      // Set captions by track index
      // Used internally for the currentTrack setter with the passive option forced to false
      set: function set(index) {
        var passive =
          arguments.length > 1 && arguments[1] !== undefined
            ? arguments[1]
            : true;
        var tracks = captions.getTracks.call(this); // Disable captions if setting to -1

        if (index === -1) {
          captions.toggle.call(this, false, passive);
          return;
        }

        if (!is$1.number(index)) {
          this.debug.warn("Invalid caption argument", index);
          return;
        }

        if (!(index in tracks)) {
          this.debug.warn("Track not found", index);
          return;
        }

        if (this.captions.currentTrack !== index) {
          this.captions.currentTrack = index;
          var track = tracks[index];

          var _ref = track || {},
            language = _ref.language; // Store reference to node for invalidation on remove

          this.captions.currentTrackNode = track; // Update settings menu

          controls.updateSetting.call(this, "captions"); // When passive, don't override user preferences

          if (!passive) {
            this.captions.language = language;
            this.storage.set({
              language: language,
            });
          } // Handle Vimeo captions

          if (this.isVimeo) {
            this.embed.enableTextTrack(language);
          } // Trigger event

          triggerEvent.call(this, this.media, "languagechange");
        } // Show captions

        captions.toggle.call(this, true, passive);

        if (this.isHTML5 && this.isVideo) {
          // If we change the active track while a cue is already displayed we need to update it
          captions.updateCues.call(this);
        }
      },
      // Set captions by language
      // Used internally for the language setter with the passive option forced to false
      setLanguage: function setLanguage(input) {
        var passive =
          arguments.length > 1 && arguments[1] !== undefined
            ? arguments[1]
            : true;

        if (!is$1.string(input)) {
          this.debug.warn("Invalid language argument", input);
          return;
        } // Normalize

        var language = input.toLowerCase();
        this.captions.language = language; // Set currentTrack

        var tracks = captions.getTracks.call(this);
        var track = captions.findTrack.call(this, [language]);
        captions.set.call(this, tracks.indexOf(track), passive);
      },
      // Get current valid caption tracks
      // If update is false it will also ignore tracks without metadata
      // This is used to "freeze" the language options when captions.update is false
      getTracks: function getTracks() {
        var _this2 = this;

        var update =
          arguments.length > 0 && arguments[0] !== undefined
            ? arguments[0]
            : false;
        // Handle media or textTracks missing or null
        var tracks = Array.from((this.media || {}).textTracks || []); // For HTML5, use cache instead of current tracks when it exists (if captions.update is false)
        // Filter out removed tracks and tracks that aren't captions/subtitles (for example metadata)

        return tracks
          .filter(function (track) {
            return !_this2.isHTML5 || update || _this2.captions.meta.has(track);
          })
          .filter(function (track) {
            return ["captions", "subtitles"].includes(track.kind);
          });
      },
      // Match tracks based on languages and get the first
      findTrack: function findTrack(languages) {
        var _this3 = this;

        var force =
          arguments.length > 1 && arguments[1] !== undefined
            ? arguments[1]
            : false;
        var tracks = captions.getTracks.call(this);

        var sortIsDefault = function sortIsDefault(track) {
          return Number((_this3.captions.meta.get(track) || {}).default);
        };

        var sorted = Array.from(tracks).sort(function (a, b) {
          return sortIsDefault(b) - sortIsDefault(a);
        });
        var track;
        languages.every(function (language) {
          track = sorted.find(function (track) {
            return track.language === language;
          });
          return !track; // Break iteration if there is a match
        }); // If no match is found but is required, get first

        return track || (force ? sorted[0] : undefined);
      },
      // Get the current track
      getCurrentTrack: function getCurrentTrack() {
        return captions.getTracks.call(this)[this.currentTrack];
      },
      // Get UI label for track
      getLabel: function getLabel(track) {
        var currentTrack = track;

        if (
          !is$1.track(currentTrack) &&
          support.textTracks &&
          this.captions.toggled
        ) {
          currentTrack = captions.getCurrentTrack.call(this);
        }

        if (is$1.track(currentTrack)) {
          if (!is$1.empty(currentTrack.label)) {
            return currentTrack.label;
          }

          if (!is$1.empty(currentTrack.language)) {
            return track.language.toUpperCase();
          }

          return i18n.get("enabled", this.config);
        }

        return i18n.get("disabled", this.config);
      },
      // Update captions using current track's active cues
      // Also optional array argument in case there isn't any track (ex: vimeo)
      updateCues: function updateCues(input) {
        // Requires UI
        if (!this.supported.ui) {
          return;
        }

        if (!is$1.element(this.elements.captions)) {
          this.debug.warn("No captions element to render to");
          return;
        } // Only accept array or empty input

        if (!is$1.nullOrUndefined(input) && !Array.isArray(input)) {
          this.debug.warn("updateCues: Invalid input", input);
          return;
        }

        var cues = input; // Get cues from track

        if (!cues) {
          var track = captions.getCurrentTrack.call(this);
          cues = Array.from((track || {}).activeCues || [])
            .map(function (cue) {
              return cue.getCueAsHTML();
            })
            .map(getHTML);
        } // Set new caption text

        var content = cues
          .map(function (cueText) {
            return cueText.trim();
          })
          .join("\n");
        var changed = content !== this.elements.captions.innerHTML;

        if (changed) {
          // Empty the container and create a new child element
          emptyElement(this.elements.captions);
          var caption = createElement(
            "span",
            getAttributesFromSelector(this.config.selectors.caption)
          );
          caption.innerHTML = content;
          this.elements.captions.appendChild(caption); // Trigger event

          triggerEvent.call(this, this.media, "cuechange");
        }
      },
    };

    // ==========================================================================
    // Plyr default config
    // ==========================================================================
    var defaults = {
      // Disable
      enabled: true,
      // Custom media title
      title: "",
      // Logging to console
      debug: false,
      // Auto play (if supported)
      autoplay: false,
      // Only allow one media playing at once (vimeo only)
      autopause: true,
      // Allow inline playback on iOS (this effects YouTube/Vimeo - HTML5 requires the attribute present)
      // TODO: Remove iosNative fullscreen option in favour of this (logic needs work)
      playsinline: true,
      // Default time to skip when rewind/fast forward
      seekTime: 10,
      // Default volume
      volume: 1,
      muted: false,
      // Pass a custom duration
      duration: null,
      // Display the media duration on load in the current time position
      // If you have opted to display both duration and currentTime, this is ignored
      displayDuration: true,
      // Invert the current time to be a countdown
      invertTime: true,
      // Clicking the currentTime inverts it's value to show time left rather than elapsed
      toggleInvert: true,
      // Aspect ratio (for embeds)
      ratio: "16:9",
      // Click video container to play/pause
      clickToPlay: true,
      // Auto hide the controls
      hideControls: true,
      // Reset to start when playback ended
      resetOnEnd: false,
      // Disable the standard context menu
      disableContextMenu: true,
      // Sprite (for icons)
      loadSprite: true,
      iconPrefix: "plyr",
      iconUrl: "https://cdn.plyr.io/3.4.6/plyr.svg",
      // Blank video (used to prevent errors on source change)
      blankVideo: "https://cdn.plyr.io/static/blank.mp4",
      // Quality default
      quality: {
        default: 576,
        options: [4320, 2880, 2160, 1440, 1080, 720, 576, 480, 360, 240],
      },
      // Set loops
      loop: {
        active: false, // start: null,
        // end: null,
      },
      // Speed default and options to display
      speed: {
        selected: 1,
        options: [0.5, 0.75, 1, 1.25, 1.5, 1.75, 2],
      },
      // Keyboard shortcut settings
      keyboard: {
        focused: true,
        global: false,
      },
      // Display tooltips
      tooltips: {
        controls: false,
        seek: true,
      },
      // Captions settings
      captions: {
        active: false,
        language: "auto",
        // Listen to new tracks added after Plyr is initialized.
        // This is needed for streaming captions, but may result in unselectable options
        update: false,
      },
      // Fullscreen settings
      fullscreen: {
        enabled: true,
        // Allow fullscreen?
        fallback: true,
        // Fallback for vintage browsers
        iosNative: false, // Use the native fullscreen in iOS (disables custom controls)
      },
      // Local storage
      storage: {
        enabled: true,
        key: "plyr",
      },
      // Default controls
      controls: [
        "play-large", // 'restart',
        // 'rewind',
        "play", // 'fast-forward',
        "progress",
        "current-time",
        "mute",
        "volume",
        "captions",
        "settings",
        "pip",
        "airplay", // 'download',
        "fullscreen",
      ],
      settings: ["captions", "quality", "speed"],
      // Localisation
      i18n: {
        restart: "Restart",
        rewind: "Rewind {seektime}s",
        play: "Play",
        pause: "Pause",
        fastForward: "Forward {seektime}s",
        seek: "Seek",
        seekLabel: "{currentTime} of {duration}",
        played: "Played",
        buffered: "Buffered",
        currentTime: "Current time",
        duration: "Duration",
        volume: "Volume",
        mute: "Mute",
        unmute: "Unmute",
        enableCaptions: "Enable captions",
        disableCaptions: "Disable captions",
        download: "Download",
        enterFullscreen: "Enter fullscreen",
        exitFullscreen: "Exit fullscreen",
        frameTitle: "Player for {title}",
        captions: "Captions",
        settings: "Settings",
        menuBack: "Go back to previous menu",
        speed: "Speed",
        normal: "Normal",
        quality: "Quality",
        loop: "Loop",
        start: "Start",
        end: "End",
        all: "All",
        reset: "Reset",
        disabled: "Disabled",
        enabled: "Enabled",
        advertisement: "Ad",
        qualityBadge: {
          2160: "4K",
          1440: "HD",
          1080: "HD",
          720: "HD",
          576: "SD",
          480: "SD",
        },
      },
      // URLs
      urls: {
        download: null,
        vimeo: {
          sdk: "https://player.vimeo.com/api/player.js",
          iframe: "https://player.vimeo.com/video/{0}?{1}",
          api: "https://vimeo.com/api/v2/video/{0}.json",
        },
        youtube: {
          sdk: "https://www.youtube.com/iframe_api",
          api:
            "https://www.googleapis.com/youtube/v3/videos?id={0}&key={1}&fields=items(snippet(title))&part=snippet",
        },
        googleIMA: {
          sdk: "https://imasdk.googleapis.com/js/sdkloader/ima3.js",
        },
      },
      // Custom control listeners
      listeners: {
        seek: null,
        play: null,
        pause: null,
        restart: null,
        rewind: null,
        fastForward: null,
        mute: null,
        volume: null,
        captions: null,
        download: null,
        fullscreen: null,
        pip: null,
        airplay: null,
        speed: null,
        quality: null,
        loop: null,
        language: null,
      },
      // Events to watch and bubble
      events: [
        // Events to watch on HTML5 media elements and bubble
        // https://developer.mozilla.org/en/docs/Web/Guide/Events/Media_events
        "ended",
        "progress",
        "stalled",
        "playing",
        "waiting",
        "canplay",
        "canplaythrough",
        "loadstart",
        "loadeddata",
        "loadedmetadata",
        "timeupdate",
        "volumechange",
        "play",
        "pause",
        "error",
        "seeking",
        "seeked",
        "emptied",
        "ratechange",
        "cuechange", // Custom events
        "download",
        "enterfullscreen",
        "exitfullscreen",
        "captionsenabled",
        "captionsdisabled",
        "languagechange",
        "controlshidden",
        "controlsshown",
        "ready", // YouTube
        "statechange", // Quality
        "qualitychange", // Ads
        "adsloaded",
        "adscontentpause",
        "adscontentresume",
        "adstarted",
        "adsmidpoint",
        "adscomplete",
        "adsallcomplete",
        "adsimpression",
        "adsclick",
      ],
      // Selectors
      // Change these to match your template if using custom HTML
      selectors: {
        editable: "input, textarea, select, [contenteditable]",
        container: ".plyr",
        controls: {
          container: null,
          wrapper: ".plyr__controls",
        },
        labels: "[data-plyr]",
        buttons: {
          play: '[data-plyr="play"]',
          pause: '[data-plyr="pause"]',
          restart: '[data-plyr="restart"]',
          rewind: '[data-plyr="rewind"]',
          fastForward: '[data-plyr="fast-forward"]',
          mute: '[data-plyr="mute"]',
          captions: '[data-plyr="captions"]',
          download: '[data-plyr="download"]',
          fullscreen: '[data-plyr="fullscreen"]',
          pip: '[data-plyr="pip"]',
          airplay: '[data-plyr="airplay"]',
          settings: '[data-plyr="settings"]',
          loop: '[data-plyr="loop"]',
        },
        inputs: {
          seek: '[data-plyr="seek"]',
          volume: '[data-plyr="volume"]',
          speed: '[data-plyr="speed"]',
          language: '[data-plyr="language"]',
          quality: '[data-plyr="quality"]',
        },
        display: {
          currentTime: ".plyr__time--current",
          duration: ".plyr__time--duration",
          buffer: ".plyr__progress__buffer",
          loop: ".plyr__progress__loop",
          // Used later
          volume: ".plyr__volume--display",
        },
        progress: ".plyr__progress",
        captions: ".plyr__captions",
        caption: ".plyr__caption",
        menu: {
          quality: ".js-plyr__menu__list--quality",
        },
      },
      // Class hooks added to the player in different states
      classNames: {
        type: "plyr--{0}",
        provider: "plyr--{0}",
        video: "plyr__video-wrapper",
        embed: "plyr__video-embed",
        embedContainer: "plyr__video-embed__container",
        poster: "plyr__poster",
        posterEnabled: "plyr__poster-enabled",
        ads: "plyr__ads",
        control: "plyr__control",
        controlPressed: "plyr__control--pressed",
        playing: "plyr--playing",
        paused: "plyr--paused",
        stopped: "plyr--stopped",
        loading: "plyr--loading",
        hover: "plyr--hover",
        tooltip: "plyr__tooltip",
        cues: "plyr__cues",
        hidden: "plyr__sr-only",
        hideControls: "plyr--hide-controls",
        isIos: "plyr--is-ios",
        isTouch: "plyr--is-touch",
        uiSupported: "plyr--full-ui",
        noTransition: "plyr--no-transition",
        display: {
          time: "plyr__time",
        },
        menu: {
          value: "plyr__menu__value",
          badge: "plyr__badge",
          open: "plyr--menu-open",
        },
        captions: {
          enabled: "plyr--captions-enabled",
          active: "plyr--captions-active",
        },
        fullscreen: {
          enabled: "plyr--fullscreen-enabled",
          fallback: "plyr--fullscreen-fallback",
        },
        pip: {
          supported: "plyr--pip-supported",
          active: "plyr--pip-active",
        },
        airplay: {
          supported: "plyr--airplay-supported",
          active: "plyr--airplay-active",
        },
        tabFocus: "plyr__tab-focus",
      },
      // Embed attributes
      attributes: {
        embed: {
          provider: "data-plyr-provider",
          id: "data-plyr-embed-id",
        },
      },
      // API keys
      keys: {
        google: null,
      },
      // Advertisements plugin
      // Register for an account here: http://vi.ai/publisher-video-monetization/?aid=plyrio
      ads: {
        enabled: false,
        publisherId: "",
      },
    };

    // ==========================================================================
    // Plyr states
    // ==========================================================================
    var pip = {
      active: "picture-in-picture",
      inactive: "inline",
    };

    // ==========================================================================
    // Plyr supported types and providers
    // ==========================================================================
    var providers = {
      html5: "html5",
      youtube: "youtube",
      vimeo: "vimeo",
    };
    var types = {
      audio: "audio",
      video: "video",
    };
    /**
     * Get provider by URL
     * @param {String} url
     */

    function getProviderByUrl(url) {
      // YouTube
      if (/^(https?:\/\/)?(www\.)?(youtube\.com|youtu\.?be)\/.+$/.test(url)) {
        return providers.youtube;
      } // Vimeo

      if (/^https?:\/\/player.vimeo.com\/video\/\d{0,9}(?=\b|\/)/.test(url)) {
        return providers.vimeo;
      }

      return null;
    }

    // ==========================================================================
    // Console wrapper
    // ==========================================================================
    var noop = function noop() {};

    var Console =
      /*#__PURE__*/
      (function () {
        function Console() {
          var enabled =
            arguments.length > 0 && arguments[0] !== undefined
              ? arguments[0]
              : false;

          _classCallCheck(this, Console);

          this.enabled = window.console && enabled;

          if (this.enabled) {
            this.log("Debugging enabled");
          }
        }

        _createClass(Console, [
          {
            key: "log",
            get: function get() {
              // eslint-disable-next-line no-console
              return this.enabled
                ? Function.prototype.bind.call(console.log, console)
                : noop;
            },
          },
          {
            key: "warn",
            get: function get() {
              // eslint-disable-next-line no-console
              return this.enabled
                ? Function.prototype.bind.call(console.warn, console)
                : noop;
            },
          },
          {
            key: "error",
            get: function get() {
              // eslint-disable-next-line no-console
              return this.enabled
                ? Function.prototype.bind.call(console.error, console)
                : noop;
            },
          },
        ]);

        return Console;
      })();

    function onChange() {
      if (!this.enabled) {
        return;
      } // Update toggle button

      var button = this.player.elements.buttons.fullscreen;

      if (is$1.element(button)) {
        button.pressed = this.active;
      } // Trigger an event

      triggerEvent.call(
        this.player,
        this.target,
        this.active ? "enterfullscreen" : "exitfullscreen",
        true
      ); // Trap focus in container

      if (!browser.isIos) {
        trapFocus.call(this.player, this.target, this.active);
      }
    }

    function toggleFallback() {
      var _this = this;

      var toggle =
        arguments.length > 0 && arguments[0] !== undefined
          ? arguments[0]
          : false;

      // Store or restore scroll position
      if (toggle) {
        this.scrollPosition = {
          x: window.scrollX || 0,
          y: window.scrollY || 0,
        };
      } else {
        window.scrollTo(this.scrollPosition.x, this.scrollPosition.y);
      } // Toggle scroll

      document.body.style.overflow = toggle ? "hidden" : ""; // Toggle class hook

      toggleClass(
        this.target,
        this.player.config.classNames.fullscreen.fallback,
        toggle
      ); // Force full viewport on iPhone X+

      if (browser.isIos) {
        var viewport = document.head.querySelector('meta[name="viewport"]');
        var property = "viewport-fit=cover"; // Inject the viewport meta if required

        if (!viewport) {
          viewport = document.createElement("meta");
          viewport.setAttribute("name", "viewport");
        } // Check if the property already exists

        var hasProperty =
          is$1.string(viewport.content) && viewport.content.includes(property);

        if (toggle) {
          this.cleanupViewport = !hasProperty;

          if (!hasProperty) {
            viewport.content += ",".concat(property);
          }
        } else if (this.cleanupViewport) {
          viewport.content = viewport.content
            .split(",")
            .filter(function (part) {
              return part.trim() !== property;
            })
            .join(",");
        } // Force a repaint as sometimes Safari doesn't want to fill the screen

        setTimeout(function () {
          return repaint(_this.target);
        }, 100);
      } // Toggle button and fire events

      onChange.call(this);
    }

    var Fullscreen =
      /*#__PURE__*/
      (function () {
        function Fullscreen(player) {
          var _this2 = this;

          _classCallCheck(this, Fullscreen);

          // Keep reference to parent
          this.player = player; // Get prefix

          this.prefix = Fullscreen.prefix;
          this.property = Fullscreen.property; // Scroll position

          this.scrollPosition = {
            x: 0,
            y: 0,
          }; // Register event listeners
          // Handle event (incase user presses escape etc)

          on.call(
            this.player,
            document,
            this.prefix === "ms"
              ? "MSFullscreenChange"
              : "".concat(this.prefix, "fullscreenchange"),
            function () {
              // TODO: Filter for target??
              onChange.call(_this2);
            }
          ); // Fullscreen toggle on double click

          on.call(
            this.player,
            this.player.elements.container,
            "dblclick",
            function (event) {
              // Ignore double click in controls
              if (
                is$1.element(_this2.player.elements.controls) &&
                _this2.player.elements.controls.contains(event.target)
              ) {
                return;
              }

              _this2.toggle();
            }
          ); // Update the UI

          this.update();
        } // Determine if native supported

        _createClass(
          Fullscreen,
          [
            {
              key: "update",
              // Update UI
              value: function update() {
                if (this.enabled) {
                  this.player.debug.log(
                    "".concat(
                      Fullscreen.native ? "Native" : "Fallback",
                      " fullscreen enabled"
                    )
                  );
                } else {
                  this.player.debug.log(
                    "Fullscreen not supported and fallback disabled"
                  );
                } // Add styling hook to show button

                toggleClass(
                  this.player.elements.container,
                  this.player.config.classNames.fullscreen.enabled,
                  this.enabled
                );
              }, // Make an element fullscreen
            },
            {
              key: "enter",
              value: function enter() {
                if (!this.enabled) {
                  return;
                } // iOS native fullscreen doesn't need the request step

                if (browser.isIos && this.player.config.fullscreen.iosNative) {
                  this.target.webkitEnterFullscreen();
                } else if (!Fullscreen.native) {
                  toggleFallback.call(this, true);
                } else if (!this.prefix) {
                  this.target.requestFullscreen();
                } else if (!is$1.empty(this.prefix)) {
                  this.target[
                    "".concat(this.prefix, "Request").concat(this.property)
                  ]();
                }
              }, // Bail from fullscreen
            },
            {
              key: "exit",
              value: function exit() {
                if (!this.enabled) {
                  return;
                } // iOS native fullscreen

                if (browser.isIos && this.player.config.fullscreen.iosNative) {
                  this.target.webkitExitFullscreen();
                  this.player.play();
                } else if (!Fullscreen.native) {
                  toggleFallback.call(this, false);
                } else if (!this.prefix) {
                  (document.cancelFullScreen || document.exitFullscreen).call(
                    document
                  );
                } else if (!is$1.empty(this.prefix)) {
                  var action = this.prefix === "moz" ? "Cancel" : "Exit";
                  document[
                    "".concat(this.prefix).concat(action).concat(this.property)
                  ]();
                }
              }, // Toggle state
            },
            {
              key: "toggle",
              value: function toggle() {
                if (!this.active) {
                  this.enter();
                } else {
                  this.exit();
                }
              },
            },
            {
              key: "enabled",
              // Determine if fullscreen is enabled
              get: function get() {
                return (
                  (Fullscreen.native ||
                    this.player.config.fullscreen.fallback) &&
                  this.player.config.fullscreen.enabled &&
                  this.player.supported.ui &&
                  this.player.isVideo
                );
              }, // Get active state
            },
            {
              key: "active",
              get: function get() {
                if (!this.enabled) {
                  return false;
                } // Fallback using classname

                if (!Fullscreen.native) {
                  return hasClass(
                    this.target,
                    this.player.config.classNames.fullscreen.fallback
                  );
                }

                var element = !this.prefix
                  ? document.fullscreenElement
                  : document[
                      "".concat(this.prefix).concat(this.property, "Element")
                    ];
                return element === this.target;
              }, // Get target element
            },
            {
              key: "target",
              get: function get() {
                return browser.isIos && this.player.config.fullscreen.iosNative
                  ? this.player.media
                  : this.player.elements.container;
              },
            },
          ],
          [
            {
              key: "native",
              get: function get() {
                return !!(
                  document.fullscreenEnabled ||
                  document.webkitFullscreenEnabled ||
                  document.mozFullScreenEnabled ||
                  document.msFullscreenEnabled
                );
              }, // Get the prefix for handlers
            },
            {
              key: "prefix",
              get: function get() {
                // No prefix
                if (is$1.function(document.exitFullscreen)) {
                  return "";
                } // Check for fullscreen support by vendor prefix

                var value = "";
                var prefixes = ["webkit", "moz", "ms"];
                prefixes.some(function (pre) {
                  if (
                    is$1.function(document["".concat(pre, "ExitFullscreen")]) ||
                    is$1.function(document["".concat(pre, "CancelFullScreen")])
                  ) {
                    value = pre;
                    return true;
                  }

                  return false;
                });
                return value;
              },
            },
            {
              key: "property",
              get: function get() {
                return this.prefix === "moz" ? "FullScreen" : "Fullscreen";
              },
            },
          ]
        );

        return Fullscreen;
      })();

    // 20.2.2.28 Math.sign(x)
    var _mathSign =
      Math.sign ||
      function sign(x) {
        // eslint-disable-next-line no-self-compare
        return (x = +x) == 0 || x != x ? x : x < 0 ? -1 : 1;
      };

    // 20.2.2.28 Math.sign(x)

    _export(_export.S, "Math", { sign: _mathSign });

    // ==========================================================================
    // Load image avoiding xhr/fetch CORS issues
    // Server status can't be obtained this way unfortunately, so this uses "naturalWidth" to determine if the image has loaded
    // By default it checks if it is at least 1px, but you can add a second argument to change this
    // ==========================================================================
    function loadImage(src) {
      var minWidth =
        arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 1;
      return new Promise(function (resolve, reject) {
        var image = new Image();

        var handler = function handler() {
          delete image.onload;
          delete image.onerror;
          (image.naturalWidth >= minWidth ? resolve : reject)(image);
        };

        Object.assign(image, {
          onload: handler,
          onerror: handler,
          src: src,
        });
      });
    }

    var ui = {
      addStyleHook: function addStyleHook() {
        toggleClass(
          this.elements.container,
          this.config.selectors.container.replace(".", ""),
          true
        );
        toggleClass(
          this.elements.container,
          this.config.classNames.uiSupported,
          this.supported.ui
        );
      },
      // Toggle native HTML5 media controls
      toggleNativeControls: function toggleNativeControls() {
        var toggle =
          arguments.length > 0 && arguments[0] !== undefined
            ? arguments[0]
            : false;

        if (toggle && this.isHTML5) {
          this.media.setAttribute("controls", "");
        } else {
          this.media.removeAttribute("controls");
        }
      },
      // Setup the UI
      build: function build() {
        var _this = this;

        // Re-attach media element listeners
        // TODO: Use event bubbling?
        this.listeners.media(); // Don't setup interface if no support

        if (!this.supported.ui) {
          this.debug.warn(
            "Basic support only for "
              .concat(this.provider, " ")
              .concat(this.type)
          ); // Restore native controls

          ui.toggleNativeControls.call(this, true); // Bail

          return;
        } // Inject custom controls if not present

        if (!is$1.element(this.elements.controls)) {
          // Inject custom controls
          controls.inject.call(this); // Re-attach control listeners

          this.listeners.controls();
        } // Remove native controls

        ui.toggleNativeControls.call(this); // Setup captions for HTML5

        if (this.isHTML5) {
          captions.setup.call(this);
        } // Reset volume

        this.volume = null; // Reset mute state

        this.muted = null; // Reset speed

        this.speed = null; // Reset loop state

        this.loop = null; // Reset quality setting

        this.quality = null; // Reset volume display

        controls.updateVolume.call(this); // Reset time display

        controls.timeUpdate.call(this); // Update the UI

        ui.checkPlaying.call(this); // Check for picture-in-picture support

        toggleClass(
          this.elements.container,
          this.config.classNames.pip.supported,
          support.pip && this.isHTML5 && this.isVideo
        ); // Check for airplay support

        toggleClass(
          this.elements.container,
          this.config.classNames.airplay.supported,
          support.airplay && this.isHTML5
        ); // Add iOS class

        toggleClass(
          this.elements.container,
          this.config.classNames.isIos,
          browser.isIos
        ); // Add touch class

        toggleClass(
          this.elements.container,
          this.config.classNames.isTouch,
          this.touch
        ); // Ready for API calls

        this.ready = true; // Ready event at end of execution stack

        setTimeout(function () {
          triggerEvent.call(_this, _this.media, "ready");
        }, 0); // Set the title

        ui.setTitle.call(this); // Assure the poster image is set, if the property was added before the element was created

        if (this.poster) {
          ui.setPoster.call(this, this.poster, false).catch(function () {});
        } // Manually set the duration if user has overridden it.
        // The event listeners for it doesn't get called if preload is disabled (#701)

        if (this.config.duration) {
          controls.durationUpdate.call(this);
        }
      },
      // Setup aria attribute for play and iframe title
      setTitle: function setTitle() {
        // Find the current text
        var label = i18n.get("play", this.config); // If there's a media title set, use that for the label

        if (is$1.string(this.config.title) && !is$1.empty(this.config.title)) {
          label += ", ".concat(this.config.title);
        } // If there's a play button, set label

        Array.from(this.elements.buttons.play || []).forEach(function (button) {
          button.setAttribute("aria-label", label);
        }); // Set iframe title
        // https://github.com/sampotts/plyr/issues/124

        if (this.isEmbed) {
          var iframe = getElement.call(this, "iframe");

          if (!is$1.element(iframe)) {
            return;
          } // Default to media type

          var title = !is$1.empty(this.config.title)
            ? this.config.title
            : "video";
          var format = i18n.get("frameTitle", this.config);
          iframe.setAttribute("title", format.replace("{title}", title));
        }
      },
      // Toggle poster
      togglePoster: function togglePoster(enable) {
        toggleClass(
          this.elements.container,
          this.config.classNames.posterEnabled,
          enable
        );
      },
      // Set the poster image (async)
      // Used internally for the poster setter, with the passive option forced to false
      setPoster: function setPoster(poster) {
        var _this2 = this;

        var passive =
          arguments.length > 1 && arguments[1] !== undefined
            ? arguments[1]
            : true;

        // Don't override if call is passive
        if (passive && this.poster) {
          return Promise.reject(new Error("Poster already set"));
        } // Set property synchronously to respect the call order

        this.media.setAttribute("poster", poster); // Wait until ui is ready

        return ready
          .call(this) // Load image
          .then(function () {
            return loadImage(poster);
          })
          .catch(function (err) {
            // Hide poster on error unless it's been set by another call
            if (poster === _this2.poster) {
              ui.togglePoster.call(_this2, false);
            } // Rethrow

            throw err;
          })
          .then(function () {
            // Prevent race conditions
            if (poster !== _this2.poster) {
              throw new Error("setPoster cancelled by later call to setPoster");
            }
          })
          .then(function () {
            Object.assign(_this2.elements.poster.style, {
              backgroundImage: "url('".concat(poster, "')"),
              // Reset backgroundSize as well (since it can be set to "cover" for padded thumbnails for youtube)
              backgroundSize: "",
            });
            ui.togglePoster.call(_this2, true);
            return poster;
          });
      },
      // Check playing state
      checkPlaying: function checkPlaying(event) {
        var _this3 = this;

        // Class hooks
        toggleClass(
          this.elements.container,
          this.config.classNames.playing,
          this.playing
        );
        toggleClass(
          this.elements.container,
          this.config.classNames.paused,
          this.paused
        );
        toggleClass(
          this.elements.container,
          this.config.classNames.stopped,
          this.stopped
        ); // Set state

        Array.from(this.elements.buttons.play || []).forEach(function (target) {
          target.pressed = _this3.playing;
        }); // Only update controls on non timeupdate events

        if (is$1.event(event) && event.type === "timeupdate") {
          return;
        } // Toggle controls

        ui.toggleControls.call(this);
      },
      // Check if media is loading
      checkLoading: function checkLoading(event) {
        var _this4 = this;

        this.loading = ["stalled", "waiting"].includes(event.type); // Clear timer

        clearTimeout(this.timers.loading); // Timer to prevent flicker when seeking

        this.timers.loading = setTimeout(
          function () {
            // Update progress bar loading class state
            toggleClass(
              _this4.elements.container,
              _this4.config.classNames.loading,
              _this4.loading
            ); // Update controls visibility

            ui.toggleControls.call(_this4);
          },
          this.loading ? 250 : 0
        );
      },
      // Toggle controls based on state and `force` argument
      toggleControls: function toggleControls(force) {
        var controls$$1 = this.elements.controls;

        if (controls$$1 && this.config.hideControls) {
          // Don't hide controls if a touch-device user recently seeked. (Must be limited to touch devices, or it occasionally prevents desktop controls from hiding.)
          var recentTouchSeek =
            this.touch && this.lastSeekTime + 2000 > Date.now(); // Show controls if force, loading, paused, button interaction, or recent seek, otherwise hide

          this.toggleControls(
            Boolean(
              force ||
                this.loading ||
                this.paused ||
                controls$$1.pressed ||
                controls$$1.hover ||
                recentTouchSeek
            )
          );
        }
      },
    };

    var Listeners =
      /*#__PURE__*/
      (function () {
        function Listeners(player) {
          _classCallCheck(this, Listeners);

          this.player = player;
          this.lastKey = null;
          this.focusTimer = null;
          this.lastKeyDown = null;
          this.handleKey = this.handleKey.bind(this);
          this.toggleMenu = this.toggleMenu.bind(this);
          this.setTabFocus = this.setTabFocus.bind(this);
          this.firstTouch = this.firstTouch.bind(this);
        } // Handle key presses

        _createClass(Listeners, [
          {
            key: "handleKey",
            value: function handleKey(event) {
              var player = this.player;
              var elements = player.elements;
              var code = event.keyCode ? event.keyCode : event.which;
              var pressed = event.type === "keydown";
              var repeat = pressed && code === this.lastKey; // Bail if a modifier key is set

              if (
                event.altKey ||
                event.ctrlKey ||
                event.metaKey ||
                event.shiftKey
              ) {
                return;
              } // If the event is bubbled from the media element
              // Firefox doesn't get the keycode for whatever reason

              if (!is$1.number(code)) {
                return;
              } // Seek by the number keys

              var seekByKey = function seekByKey() {
                // Divide the max duration into 10th's and times by the number value
                player.currentTime = (player.duration / 10) * (code - 48);
              }; // Handle the key on keydown
              // Reset on keyup

              if (pressed) {
                // Check focused element
                // and if the focused element is not editable (e.g. text input)
                // and any that accept key input http://webaim.org/techniques/keyboard/
                var focused = document.activeElement;

                if (is$1.element(focused)) {
                  var editable = player.config.selectors.editable;
                  var seek = elements.inputs.seek;

                  if (focused !== seek && matches(focused, editable)) {
                    return;
                  }

                  if (
                    event.which === 32 &&
                    matches(focused, 'button, [role^="menuitem"]')
                  ) {
                    return;
                  }
                } // Which keycodes should we prevent default

                var preventDefault = [
                  32,
                  37,
                  38,
                  39,
                  40,
                  48,
                  49,
                  50,
                  51,
                  52,
                  53,
                  54,
                  56,
                  57,
                  67,
                  70,
                  73,
                  75,
                  76,
                  77,
                  79,
                ]; // If the code is found prevent default (e.g. prevent scrolling for arrows)

                if (preventDefault.includes(code)) {
                  event.preventDefault();
                  event.stopPropagation();
                }

                switch (code) {
                  case 48:
                  case 49:
                  case 50:
                  case 51:
                  case 52:
                  case 53:
                  case 54:
                  case 55:
                  case 56:
                  case 57:
                    // 0-9
                    if (!repeat) {
                      seekByKey();
                    }

                    break;

                  case 32:
                  case 75:
                    // Space and K key
                    if (!repeat) {
                      player.togglePlay();
                    }

                    break;

                  case 38:
                    // Arrow up
                    player.increaseVolume(0.1);
                    break;

                  case 40:
                    // Arrow down
                    player.decreaseVolume(0.1);
                    break;

                  case 77:
                    // M key
                    if (!repeat) {
                      player.muted = !player.muted;
                    }

                    break;

                  case 39:
                    // Arrow forward
                    player.forward();
                    break;

                  case 37:
                    // Arrow back
                    player.rewind();
                    break;

                  case 70:
                    // F key
                    player.fullscreen.toggle();
                    break;

                  case 67:
                    // C key
                    if (!repeat) {
                      player.toggleCaptions();
                    }

                    break;

                  case 76:
                    // L key
                    player.loop = !player.loop;
                    break;

                  /* case 73:
            this.setLoop('start');
            break;
            case 76:
            this.setLoop();
            break;
            case 79:
            this.setLoop('end');
            break; */

                  default:
                    break;
                } // Escape is handle natively when in full screen
                // So we only need to worry about non native

                if (
                  !player.fullscreen.enabled &&
                  player.fullscreen.active &&
                  code === 27
                ) {
                  player.fullscreen.toggle();
                } // Store last code for next cycle

                this.lastKey = code;
              } else {
                this.lastKey = null;
              }
            }, // Toggle menu
          },
          {
            key: "toggleMenu",
            value: function toggleMenu(event) {
              controls.toggleMenu.call(this.player, event);
            }, // Device is touch enabled
          },
          {
            key: "firstTouch",
            value: function firstTouch() {
              var player = this.player;
              var elements = player.elements;
              player.touch = true; // Add touch class

              toggleClass(
                elements.container,
                player.config.classNames.isTouch,
                true
              );
            },
          },
          {
            key: "setTabFocus",
            value: function setTabFocus(event) {
              var player = this.player;
              var elements = player.elements;
              clearTimeout(this.focusTimer); // Ignore any key other than tab

              if (event.type === "keydown" && event.which !== 9) {
                return;
              } // Store reference to event timeStamp

              if (event.type === "keydown") {
                this.lastKeyDown = event.timeStamp;
              } // Remove current classes

              var removeCurrent = function removeCurrent() {
                var className = player.config.classNames.tabFocus;
                var current = getElements.call(player, ".".concat(className));
                toggleClass(current, className, false);
              }; // Determine if a key was pressed to trigger this event

              var wasKeyDown = event.timeStamp - this.lastKeyDown <= 20; // Ignore focus events if a key was pressed prior

              if (event.type === "focus" && !wasKeyDown) {
                return;
              } // Remove all current

              removeCurrent(); // Delay the adding of classname until the focus has changed
              // This event fires before the focusin event

              this.focusTimer = setTimeout(function () {
                var focused = document.activeElement; // Ignore if current focus element isn't inside the player

                if (!elements.container.contains(focused)) {
                  return;
                }

                toggleClass(
                  document.activeElement,
                  player.config.classNames.tabFocus,
                  true
                );
              }, 10);
            }, // Global window & document listeners
          },
          {
            key: "global",
            value: function global() {
              var toggle =
                arguments.length > 0 && arguments[0] !== undefined
                  ? arguments[0]
                  : true;
              var player = this.player; // Keyboard shortcuts

              if (player.config.keyboard.global) {
                toggleListener.call(
                  player,
                  window,
                  "keydown keyup",
                  this.handleKey,
                  toggle,
                  false
                );
              } // Click anywhere closes menu

              toggleListener.call(
                player,
                document.body,
                "click",
                this.toggleMenu,
                toggle
              ); // Detect touch by events

              once.call(player, document.body, "touchstart", this.firstTouch); // Tab focus detection

              toggleListener.call(
                player,
                document.body,
                "keydown focus blur",
                this.setTabFocus,
                toggle,
                false,
                true
              );
            }, // Container listeners
          },
          {
            key: "container",
            value: function container() {
              var player = this.player;
              var elements = player.elements; // Keyboard shortcuts

              if (
                !player.config.keyboard.global &&
                player.config.keyboard.focused
              ) {
                on.call(
                  player,
                  elements.container,
                  "keydown keyup",
                  this.handleKey,
                  false
                );
              } // Toggle controls on mouse events and entering fullscreen

              on.call(
                player,
                elements.container,
                "mousemove mouseleave touchstart touchmove enterfullscreen exitfullscreen",
                function (event) {
                  var controls$$1 = elements.controls; // Remove button states for fullscreen

                  if (controls$$1 && event.type === "enterfullscreen") {
                    controls$$1.pressed = false;
                    controls$$1.hover = false;
                  } // Show, then hide after a timeout unless another control event occurs

                  var show = ["touchstart", "touchmove", "mousemove"].includes(
                    event.type
                  );
                  var delay = 0;

                  if (show) {
                    ui.toggleControls.call(player, true); // Use longer timeout for touch devices

                    delay = player.touch ? 3000 : 2000;
                  } // Clear timer

                  clearTimeout(player.timers.controls); // Set new timer to prevent flicker when seeking

                  player.timers.controls = setTimeout(function () {
                    return ui.toggleControls.call(player, false);
                  }, delay);
                }
              );
            }, // Listen for media events
          },
          {
            key: "media",
            value: function media() {
              var player = this.player;
              var elements = player.elements; // Time change on media

              on.call(
                player,
                player.media,
                "timeupdate seeking seeked",
                function (event) {
                  return controls.timeUpdate.call(player, event);
                }
              ); // Display duration

              on.call(
                player,
                player.media,
                "durationchange loadeddata loadedmetadata",
                function (event) {
                  return controls.durationUpdate.call(player, event);
                }
              ); // Check for audio tracks on load
              // We can't use `loadedmetadata` as it doesn't seem to have audio tracks at that point

              on.call(player, player.media, "canplay", function () {
                toggleHidden(elements.volume, !player.hasAudio);
                toggleHidden(elements.buttons.mute, !player.hasAudio);
              }); // Handle the media finishing

              on.call(player, player.media, "ended", function () {
                // Show poster on end
                if (
                  player.isHTML5 &&
                  player.isVideo &&
                  player.config.resetOnEnd
                ) {
                  // Restart
                  player.restart();
                }
              }); // Check for buffer progress

              on.call(
                player,
                player.media,
                "progress playing seeking seeked",
                function (event) {
                  return controls.updateProgress.call(player, event);
                }
              ); // Handle volume changes

              on.call(player, player.media, "volumechange", function (event) {
                return controls.updateVolume.call(player, event);
              }); // Handle play/pause

              on.call(
                player,
                player.media,
                "playing play pause ended emptied timeupdate",
                function (event) {
                  return ui.checkPlaying.call(player, event);
                }
              ); // Loading state

              on.call(
                player,
                player.media,
                "waiting canplay seeked playing",
                function (event) {
                  return ui.checkLoading.call(player, event);
                }
              ); // If autoplay, then load advertisement if required
              // TODO: Show some sort of loading state while the ad manager loads else there's a delay before ad shows

              on.call(player, player.media, "playing", function () {
                if (!player.ads) {
                  return;
                } // If ads are enabled, wait for them first

                if (player.ads.enabled && !player.ads.initialized) {
                  // Wait for manager response
                  player.ads.managerPromise
                    .then(function () {
                      return player.ads.play();
                    })
                    .catch(function () {
                      return player.play();
                    });
                }
              }); // Click video

              if (
                player.supported.ui &&
                player.config.clickToPlay &&
                !player.isAudio
              ) {
                // Re-fetch the wrapper
                var wrapper = getElement.call(
                  player,
                  ".".concat(player.config.classNames.video)
                ); // Bail if there's no wrapper (this should never happen)

                if (!is$1.element(wrapper)) {
                  return;
                } // On click play, pause or restart

                on.call(player, elements.container, "click", function (event) {
                  var targets = [elements.container, wrapper]; // Ignore if click if not container or in video wrapper

                  if (
                    !targets.includes(event.target) &&
                    !wrapper.contains(event.target)
                  ) {
                    return;
                  } // Touch devices will just show controls (if hidden)

                  if (player.touch && player.config.hideControls) {
                    return;
                  }

                  if (player.ended) {
                    player.restart();
                    player.play();
                  } else {
                    player.togglePlay();
                  }
                });
              } // Disable right click

              if (player.supported.ui && player.config.disableContextMenu) {
                on.call(
                  player,
                  elements.wrapper,
                  "contextmenu",
                  function (event) {
                    event.preventDefault();
                  },
                  false
                );
              } // Volume change

              on.call(player, player.media, "volumechange", function () {
                // Save to storage
                player.storage.set({
                  volume: player.volume,
                  muted: player.muted,
                });
              }); // Speed change

              on.call(player, player.media, "ratechange", function () {
                // Update UI
                controls.updateSetting.call(player, "speed"); // Save to storage

                player.storage.set({
                  speed: player.speed,
                });
              }); // Quality change

              on.call(player, player.media, "qualitychange", function (event) {
                // Update UI
                controls.updateSetting.call(
                  player,
                  "quality",
                  null,
                  event.detail.quality
                );
              }); // Update download link when ready and if quality changes

              on.call(player, player.media, "ready qualitychange", function () {
                controls.setDownloadLink.call(player);
              }); // Proxy events to container
              // Bubble up key events for Edge

              var proxyEvents = player.config.events
                .concat(["keyup", "keydown"])
                .join(" ");
              on.call(player, player.media, proxyEvents, function (event) {
                var _event$detail = event.detail,
                  detail = _event$detail === void 0 ? {} : _event$detail; // Get error details from media

                if (event.type === "error") {
                  detail = player.media.error;
                }

                triggerEvent.call(
                  player,
                  elements.container,
                  event.type,
                  true,
                  detail
                );
              });
            }, // Run default and custom handlers
          },
          {
            key: "proxy",
            value: function proxy(event, defaultHandler, customHandlerKey) {
              var player = this.player;
              var customHandler = player.config.listeners[customHandlerKey];
              var hasCustomHandler = is$1.function(customHandler);
              var returned = true; // Execute custom handler

              if (hasCustomHandler) {
                returned = customHandler.call(player, event);
              } // Only call default handler if not prevented in custom handler

              if (returned && is$1.function(defaultHandler)) {
                defaultHandler.call(player, event);
              }
            }, // Trigger custom and default handlers
          },
          {
            key: "bind",
            value: function bind(
              element,
              type,
              defaultHandler,
              customHandlerKey
            ) {
              var _this = this;

              var passive =
                arguments.length > 4 && arguments[4] !== undefined
                  ? arguments[4]
                  : true;
              var player = this.player;
              var customHandler = player.config.listeners[customHandlerKey];
              var hasCustomHandler = is$1.function(customHandler);
              on.call(
                player,
                element,
                type,
                function (event) {
                  return _this.proxy(event, defaultHandler, customHandlerKey);
                },
                passive && !hasCustomHandler
              );
            }, // Listen for control events
          },
          {
            key: "controls",
            value: function controls$$1() {
              var _this2 = this;

              var player = this.player;
              var elements = player.elements; // IE doesn't support input event, so we fallback to change

              var inputEvent = browser.isIE ? "change" : "input"; // Play/pause toggle

              if (elements.buttons.play) {
                Array.from(elements.buttons.play).forEach(function (button) {
                  _this2.bind(button, "click", player.togglePlay, "play");
                });
              } // Pause

              this.bind(
                elements.buttons.restart,
                "click",
                player.restart,
                "restart"
              ); // Rewind

              this.bind(
                elements.buttons.rewind,
                "click",
                player.rewind,
                "rewind"
              ); // Rewind

              this.bind(
                elements.buttons.fastForward,
                "click",
                player.forward,
                "fastForward"
              ); // Mute toggle

              this.bind(
                elements.buttons.mute,
                "click",
                function () {
                  player.muted = !player.muted;
                },
                "mute"
              ); // Captions toggle

              this.bind(elements.buttons.captions, "click", function () {
                return player.toggleCaptions();
              }); // Download

              this.bind(
                elements.buttons.download,
                "click",
                function () {
                  triggerEvent.call(player, player.media, "download");
                },
                "download"
              ); // Fullscreen toggle

              this.bind(
                elements.buttons.fullscreen,
                "click",
                function () {
                  player.fullscreen.toggle();
                },
                "fullscreen"
              ); // Picture-in-Picture

              this.bind(
                elements.buttons.pip,
                "click",
                function () {
                  player.pip = "toggle";
                },
                "pip"
              ); // Airplay

              this.bind(
                elements.buttons.airplay,
                "click",
                player.airplay,
                "airplay"
              ); // Settings menu - click toggle

              this.bind(elements.buttons.settings, "click", function (event) {
                // Prevent the document click listener closing the menu
                event.stopPropagation();

                controls.toggleMenu.call(player, event);
              }); // Settings menu - keyboard toggle
              // We have to bind to keyup otherwise Firefox triggers a click when a keydown event handler shifts focus
              // https://bugzilla.mozilla.org/show_bug.cgi?id=1220143

              this.bind(
                elements.buttons.settings,
                "keyup",
                function (event) {
                  var code = event.which; // We only care about space and return

                  if (![13, 32].includes(code)) {
                    return;
                  } // Because return triggers a click anyway, all we need to do is set focus

                  if (code === 13) {
                    controls.focusFirstMenuItem.call(player, null, true);

                    return;
                  } // Prevent scroll

                  event.preventDefault(); // Prevent playing video (Firefox)

                  event.stopPropagation(); // Toggle menu

                  controls.toggleMenu.call(player, event);
                },
                null,
                false // Can't be passive as we're preventing default
              ); // Escape closes menu

              this.bind(elements.settings.menu, "keydown", function (event) {
                if (event.which === 27) {
                  controls.toggleMenu.call(player, event);
                }
              }); // Set range input alternative "value", which matches the tooltip time (#954)

              this.bind(elements.inputs.seek, "mousedown mousemove", function (
                event
              ) {
                var rect = elements.progress.getBoundingClientRect();
                var percent = (100 / rect.width) * (event.pageX - rect.left);
                event.currentTarget.setAttribute("seek-value", percent);
              }); // Pause while seeking

              this.bind(
                elements.inputs.seek,
                "mousedown mouseup keydown keyup touchstart touchend",
                function (event) {
                  var seek = event.currentTarget;
                  var code = event.keyCode ? event.keyCode : event.which;
                  var attribute = "play-on-seeked";

                  if (is$1.keyboardEvent(event) && code !== 39 && code !== 37) {
                    return;
                  } // Record seek time so we can prevent hiding controls for a few seconds after seek

                  player.lastSeekTime = Date.now(); // Was playing before?

                  var play = seek.hasAttribute(attribute); // Done seeking

                  var done = ["mouseup", "touchend", "keyup"].includes(
                    event.type
                  ); // If we're done seeking and it was playing, resume playback

                  if (play && done) {
                    seek.removeAttribute(attribute);
                    player.play();
                  } else if (!done && player.playing) {
                    seek.setAttribute(attribute, "");
                    player.pause();
                  }
                }
              ); // Fix range inputs on iOS
              // Super weird iOS bug where after you interact with an <input type="range">,
              // it takes over further interactions on the page. This is a hack

              if (browser.isIos) {
                var inputs = getElements.call(player, 'input[type="range"]');
                Array.from(inputs).forEach(function (input) {
                  return _this2.bind(input, inputEvent, function (event) {
                    return repaint(event.target);
                  });
                });
              } // Seek

              this.bind(
                elements.inputs.seek,
                inputEvent,
                function (event) {
                  var seek = event.currentTarget; // If it exists, use seek-value instead of "value" for consistency with tooltip time (#954)

                  var seekTo = seek.getAttribute("seek-value");

                  if (is$1.empty(seekTo)) {
                    seekTo = seek.value;
                  }

                  seek.removeAttribute("seek-value");
                  player.currentTime = (seekTo / seek.max) * player.duration;
                },
                "seek"
              ); // Seek tooltip

              this.bind(
                elements.progress,
                "mouseenter mouseleave mousemove",
                function (event) {
                  return controls.updateSeekTooltip.call(player, event);
                }
              ); // Polyfill for lower fill in <input type="range"> for webkit

              if (browser.isWebkit) {
                Array.from(
                  getElements.call(player, 'input[type="range"]')
                ).forEach(function (element) {
                  _this2.bind(element, "input", function (event) {
                    return controls.updateRangeFill.call(player, event.target);
                  });
                });
              } // Current time invert
              // Only if one time element is used for both currentTime and duration

              if (
                player.config.toggleInvert &&
                !is$1.element(elements.display.duration)
              ) {
                this.bind(elements.display.currentTime, "click", function () {
                  // Do nothing if we're at the start
                  if (player.currentTime === 0) {
                    return;
                  }

                  player.config.invertTime = !player.config.invertTime;

                  controls.timeUpdate.call(player);
                });
              } // Volume

              this.bind(
                elements.inputs.volume,
                inputEvent,
                function (event) {
                  player.volume = event.target.value;
                },
                "volume"
              ); // Update controls.hover state (used for ui.toggleControls to avoid hiding when interacting)

              this.bind(elements.controls, "mouseenter mouseleave", function (
                event
              ) {
                elements.controls.hover =
                  !player.touch && event.type === "mouseenter";
              }); // Update controls.pressed state (used for ui.toggleControls to avoid hiding when interacting)

              this.bind(
                elements.controls,
                "mousedown mouseup touchstart touchend touchcancel",
                function (event) {
                  elements.controls.pressed = [
                    "mousedown",
                    "touchstart",
                  ].includes(event.type);
                }
              ); // Show controls when they receive focus (e.g., when using keyboard tab key)

              this.bind(elements.controls, "focusin", function () {
                var config = player.config,
                  elements = player.elements,
                  timers = player.timers; // Skip transition to prevent focus from scrolling the parent element

                toggleClass(
                  elements.controls,
                  config.classNames.noTransition,
                  true
                ); // Toggle

                ui.toggleControls.call(player, true); // Restore transition

                setTimeout(function () {
                  toggleClass(
                    elements.controls,
                    config.classNames.noTransition,
                    false
                  );
                }, 0); // Delay a little more for mouse users

                var delay = _this2.touch ? 3000 : 4000; // Clear timer

                clearTimeout(timers.controls); // Hide again after delay

                timers.controls = setTimeout(function () {
                  return ui.toggleControls.call(player, false);
                }, delay);
              }); // Mouse wheel for volume

              this.bind(
                elements.inputs.volume,
                "wheel",
                function (event) {
                  // Detect "natural" scroll - suppored on OS X Safari only
                  // Other browsers on OS X will be inverted until support improves
                  var inverted = event.webkitDirectionInvertedFromDevice; // Get delta from event. Invert if `inverted` is true

                  var _map = [event.deltaX, -event.deltaY].map(function (
                      value
                    ) {
                      return inverted ? -value : value;
                    }),
                    _map2 = _slicedToArray(_map, 2),
                    x = _map2[0],
                    y = _map2[1]; // Using the biggest delta, normalize to 1 or -1 (or 0 if no delta)

                  var direction = Math.sign(Math.abs(x) > Math.abs(y) ? x : y); // Change the volume by 2%

                  player.increaseVolume(direction / 50); // Don't break page scrolling at max and min

                  var volume = player.media.volume;

                  if (
                    (direction === 1 && volume < 1) ||
                    (direction === -1 && volume > 0)
                  ) {
                    event.preventDefault();
                  }
                },
                "volume",
                false
              );
            },
          },
        ]);

        return Listeners;
      })();

    var dP$3 = _objectDp.f;
    var FProto = Function.prototype;
    var nameRE = /^\s*function ([^ (]*)/;
    var NAME$1 = "name";

    // 19.2.4.2 name
    NAME$1 in FProto ||
      (_descriptors &&
        dP$3(FProto, NAME$1, {
          configurable: true,
          get: function () {
            try {
              return ("" + this).match(nameRE)[1];
            } catch (e) {
              return "";
            }
          },
        }));

    // @@match logic
    _fixReWks("match", 1, function (defined, MATCH, $match) {
      // 21.1.3.11 String.prototype.match(regexp)
      return [
        function match(regexp) {
          var O = defined(this);
          var fn = regexp == undefined ? undefined : regexp[MATCH];
          return fn !== undefined
            ? fn.call(regexp, O)
            : new RegExp(regexp)[MATCH](String(O));
        },
        $match,
      ];
    });

    var loadjs_umd = createCommonjsModule(function (module, exports) {
      (function (root, factory) {
        if (typeof undefined === "function" && undefined.amd) {
          undefined([], factory);
        } else {
          module.exports = factory();
        }
      })(commonjsGlobal, function () {
        /**
         * Global dependencies.
         * @global {Object} document - DOM
         */

        var devnull = function () {},
          bundleIdCache = {},
          bundleResultCache = {},
          bundleCallbackQueue = {};

        /**
         * Subscribe to bundle load event.
         * @param {string[]} bundleIds - Bundle ids
         * @param {Function} callbackFn - The callback function
         */
        function subscribe(bundleIds, callbackFn) {
          // listify
          bundleIds = bundleIds.push ? bundleIds : [bundleIds];

          var depsNotFound = [],
            i = bundleIds.length,
            numWaiting = i,
            fn,
            bundleId,
            r,
            q;

          // define callback function
          fn = function (bundleId, pathsNotFound) {
            if (pathsNotFound.length) depsNotFound.push(bundleId);

            numWaiting--;
            if (!numWaiting) callbackFn(depsNotFound);
          };

          // register callback
          while (i--) {
            bundleId = bundleIds[i];

            // execute callback if in result cache
            r = bundleResultCache[bundleId];
            if (r) {
              fn(bundleId, r);
              continue;
            }

            // add to callback queue
            q = bundleCallbackQueue[bundleId] =
              bundleCallbackQueue[bundleId] || [];
            q.push(fn);
          }
        }

        /**
         * Publish bundle load event.
         * @param {string} bundleId - Bundle id
         * @param {string[]} pathsNotFound - List of files not found
         */
        function publish(bundleId, pathsNotFound) {
          // exit if id isn't defined
          if (!bundleId) return;

          var q = bundleCallbackQueue[bundleId];

          // cache result
          bundleResultCache[bundleId] = pathsNotFound;

          // exit if queue is empty
          if (!q) return;

          // empty callback queue
          while (q.length) {
            q[0](bundleId, pathsNotFound);
            q.splice(0, 1);
          }
        }

        /**
         * Execute callbacks.
         * @param {Object or Function} args - The callback args
         * @param {string[]} depsNotFound - List of dependencies not found
         */
        function executeCallbacks(args, depsNotFound) {
          // accept function as argument
          if (args.call) args = { success: args };

          // success and error callbacks
          if (depsNotFound.length) (args.error || devnull)(depsNotFound);
          else (args.success || devnull)(args);
        }

        /**
         * Load individual file.
         * @param {string} path - The file path
         * @param {Function} callbackFn - The callback function
         */
        function loadFile(path, callbackFn, args, numTries) {
          var doc = document,
            async = args.async,
            maxTries = (args.numRetries || 0) + 1,
            beforeCallbackFn = args.before || devnull,
            pathStripped = path.replace(/^(css|img)!/, ""),
            isCss,
            e;

          numTries = numTries || 0;

          if (/(^css!|\.css$)/.test(path)) {
            isCss = true;

            // css
            e = doc.createElement("link");
            e.rel = "stylesheet";
            e.href = pathStripped; //.replace(/^css!/, '');  // remove "css!" prefix
          } else if (/(^img!|\.(png|gif|jpg|svg)$)/.test(path)) {
            // image
            e = doc.createElement("img");
            e.src = pathStripped;
          } else {
            // javascript
            e = doc.createElement("script");
            e.src = path;
            e.async = async === undefined ? true : async;
          }

          e.onload = e.onerror = e.onbeforeload = function (ev) {
            var result = ev.type[0];

            // Note: The following code isolates IE using `hideFocus` and treats empty
            // stylesheets as failures to get around lack of onerror support
            if (isCss && "hideFocus" in e) {
              try {
                if (!e.sheet.cssText.length) result = "e";
              } catch (x) {
                // sheets objects created from load errors don't allow access to
                // `cssText`
                result = "e";
              }
            }

            // handle retries in case of load failure
            if (result == "e") {
              // increment counter
              numTries += 1;

              // exit function and try again
              if (numTries < maxTries) {
                return loadFile(path, callbackFn, args, numTries);
              }
            }

            // execute callback
            callbackFn(path, result, ev.defaultPrevented);
          };

          // add to document (unless callback returns `false`)
          if (beforeCallbackFn(path, e) !== false) doc.head.appendChild(e);
        }

        /**
         * Load multiple files.
         * @param {string[]} paths - The file paths
         * @param {Function} callbackFn - The callback function
         */
        function loadFiles(paths, callbackFn, args) {
          // listify paths
          paths = paths.push ? paths : [paths];

          var numWaiting = paths.length,
            x = numWaiting,
            pathsNotFound = [],
            fn,
            i;

          // define callback function
          fn = function (path, result, defaultPrevented) {
            // handle error
            if (result == "e") pathsNotFound.push(path);

            // handle beforeload event. If defaultPrevented then that means the load
            // will be blocked (ex. Ghostery/ABP on Safari)
            if (result == "b") {
              if (defaultPrevented) pathsNotFound.push(path);
              else return;
            }

            numWaiting--;
            if (!numWaiting) callbackFn(pathsNotFound);
          };

          // load scripts
          for (i = 0; i < x; i++) loadFile(paths[i], fn, args);
        }

        /**
         * Initiate script load and register bundle.
         * @param {(string|string[])} paths - The file paths
         * @param {(string|Function)} [arg1] - The bundleId or success callback
         * @param {Function} [arg2] - The success or error callback
         * @param {Function} [arg3] - The error callback
         */
        function loadjs(paths, arg1, arg2) {
          var bundleId, args;

          // bundleId (if string)
          if (arg1 && arg1.trim) bundleId = arg1;

          // args (default is {})
          args = (bundleId ? arg2 : arg1) || {};

          // throw error if bundle is already defined
          if (bundleId) {
            if (bundleId in bundleIdCache) {
              throw "LoadJS";
            } else {
              bundleIdCache[bundleId] = true;
            }
          }

          // load scripts
          loadFiles(
            paths,
            function (pathsNotFound) {
              // execute callbacks
              executeCallbacks(args, pathsNotFound);

              // publish bundle load event
              publish(bundleId, pathsNotFound);
            },
            args
          );
        }

        /**
         * Execute callbacks when dependencies have been satisfied.
         * @param {(string|string[])} deps - List of bundle ids
         * @param {Object} args - success/error arguments
         */
        loadjs.ready = function ready(deps, args) {
          // subscribe to bundle load event
          subscribe(deps, function (depsNotFound) {
            // execute callbacks
            executeCallbacks(args, depsNotFound);
          });

          return loadjs;
        };

        /**
         * Manually satisfy bundle dependencies.
         * @param {string} bundleId - The bundle id
         */
        loadjs.done = function done(bundleId) {
          publish(bundleId, []);
        };

        /**
         * Reset loadjs dependencies statuses
         */
        loadjs.reset = function reset() {
          bundleIdCache = {};
          bundleResultCache = {};
          bundleCallbackQueue = {};
        };

        /**
         * Determine if bundle has already been defined
         * @param String} bundleId - The bundle id
         */
        loadjs.isDefined = function isDefined(bundleId) {
          return bundleId in bundleIdCache;
        };

        // export
        return loadjs;
      });
    });

    function loadScript(url) {
      return new Promise(function (resolve, reject) {
        loadjs_umd(url, {
          success: resolve,
          error: reject,
        });
      });
    }

    function parseId(url) {
      if (is$1.empty(url)) {
        return null;
      }

      if (is$1.number(Number(url))) {
        return url;
      }

      var regex = /^.*(vimeo.com\/|video\/)(\d+).*/;
      return url.match(regex) ? RegExp.$2 : url;
    } // Get aspect ratio for dimensions

    function getAspectRatio(width, height) {
      var getRatio = function getRatio(w, h) {
        return h === 0 ? w : getRatio(h, w % h);
      };

      var ratio = getRatio(width, height);
      return "".concat(width / ratio, ":").concat(height / ratio);
    } // Set playback state and trigger change (only on actual change)

    function assurePlaybackState(play) {
      if (play && !this.embed.hasPlayed) {
        this.embed.hasPlayed = true;
      }

      if (this.media.paused === play) {
        this.media.paused = !play;
        triggerEvent.call(this, this.media, play ? "play" : "pause");
      }
    }

    var vimeo = {
      setup: function setup() {
        var _this = this;

        // Add embed class for responsive
        toggleClass(this.elements.wrapper, this.config.classNames.embed, true); // Set intial ratio

        vimeo.setAspectRatio.call(this); // Load the API if not already

        if (!is$1.object(window.Vimeo)) {
          loadScript(this.config.urls.vimeo.sdk)
            .then(function () {
              vimeo.ready.call(_this);
            })
            .catch(function (error) {
              _this.debug.warn("Vimeo API failed to load", error);
            });
        } else {
          vimeo.ready.call(this);
        }
      },
      // Set aspect ratio
      // For Vimeo we have an extra 300% height <div> to hide the standard controls and UI
      setAspectRatio: function setAspectRatio(input) {
        var _split = (is$1.string(input) ? input : this.config.ratio).split(
            ":"
          ),
          _split2 = _slicedToArray(_split, 2),
          x = _split2[0],
          y = _split2[1];

        var padding = (100 / x) * y;
        this.elements.wrapper.style.paddingBottom = "".concat(padding, "%");

        if (this.supported.ui) {
          var height = 240;
          var offset = (height - padding) / (height / 50);
          this.media.style.transform = "translateY(-".concat(offset, "%)");
        }
      },
      // API Ready
      ready: function ready$$1() {
        var _this2 = this;

        var player = this; // Get Vimeo params for the iframe

        var options = {
          loop: player.config.loop.active,
          autoplay: player.autoplay,
          // muted: player.muted,
          byline: false,
          portrait: false,
          title: false,
          speed: true,
          transparent: 0,
          gesture: "media",
          playsinline: !this.config.fullscreen.iosNative,
        };
        var params = buildUrlParams(options); // Get the source URL or ID

        var source = player.media.getAttribute("src"); // Get from <div> if needed

        if (is$1.empty(source)) {
          source = player.media.getAttribute(player.config.attributes.embed.id);
        }

        var id = parseId(source); // Build an iframe

        var iframe = createElement("iframe");
        var src = format(player.config.urls.vimeo.iframe, id, params);
        iframe.setAttribute("src", src);
        iframe.setAttribute("allowfullscreen", "");
        iframe.setAttribute("allowtransparency", "");
        iframe.setAttribute("allow", "autoplay"); // Get poster, if already set

        var poster = player.poster; // Inject the package

        var wrapper = createElement("div", {
          poster: poster,
          class: player.config.classNames.embedContainer,
        });
        wrapper.appendChild(iframe);
        player.media = replaceElement(wrapper, player.media); // Get poster image

        fetch(format(player.config.urls.vimeo.api, id), "json").then(function (
          response
        ) {
          if (is$1.empty(response)) {
            return;
          } // Get the URL for thumbnail

          var url = new URL(response[0].thumbnail_large); // Get original image

          url.pathname = "".concat(url.pathname.split("_")[0], ".jpg"); // Set and show poster

          ui.setPoster.call(player, url.href).catch(function () {});
        }); // Setup instance
        // https://github.com/vimeo/player.js

        player.embed = new window.Vimeo.Player(iframe, {
          autopause: player.config.autopause,
          muted: player.muted,
        });
        player.media.paused = true;
        player.media.currentTime = 0; // Disable native text track rendering

        if (player.supported.ui) {
          player.embed.disableTextTrack();
        } // Create a faux HTML5 API using the Vimeo API

        player.media.play = function () {
          assurePlaybackState.call(player, true);
          return player.embed.play();
        };

        player.media.pause = function () {
          assurePlaybackState.call(player, false);
          return player.embed.pause();
        };

        player.media.stop = function () {
          player.pause();
          player.currentTime = 0;
        }; // Seeking

        var currentTime = player.media.currentTime;
        Object.defineProperty(player.media, "currentTime", {
          get: function get() {
            return currentTime;
          },
          set: function set(time) {
            // Vimeo will automatically play on seek if the video hasn't been played before
            // Get current paused state and volume etc
            var embed = player.embed,
              media = player.media,
              paused = player.paused,
              volume = player.volume;
            var restorePause = paused && !embed.hasPlayed; // Set seeking state and trigger event

            media.seeking = true;
            triggerEvent.call(player, media, "seeking"); // If paused, mute until seek is complete

            Promise.resolve(restorePause && embed.setVolume(0)) // Seek
              .then(function () {
                return embed.setCurrentTime(time);
              }) // Restore paused
              .then(function () {
                return restorePause && embed.pause();
              }) // Restore volume
              .then(function () {
                return restorePause && embed.setVolume(volume);
              })
              .catch(function () {
                // Do nothing
              });
          },
        }); // Playback speed

        var speed = player.config.speed.selected;
        Object.defineProperty(player.media, "playbackRate", {
          get: function get() {
            return speed;
          },
          set: function set(input) {
            player.embed
              .setPlaybackRate(input)
              .then(function () {
                speed = input;
                triggerEvent.call(player, player.media, "ratechange");
              })
              .catch(function (error) {
                // Hide menu item (and menu if empty)
                if (error.name === "Error") {
                  controls.setSpeedMenu.call(player, []);
                }
              });
          },
        }); // Volume

        var volume = player.config.volume;
        Object.defineProperty(player.media, "volume", {
          get: function get() {
            return volume;
          },
          set: function set(input) {
            player.embed.setVolume(input).then(function () {
              volume = input;
              triggerEvent.call(player, player.media, "volumechange");
            });
          },
        }); // Muted

        var muted = player.config.muted;
        Object.defineProperty(player.media, "muted", {
          get: function get() {
            return muted;
          },
          set: function set(input) {
            var toggle = is$1.boolean(input) ? input : false;
            player.embed
              .setVolume(toggle ? 0 : player.config.volume)
              .then(function () {
                muted = toggle;
                triggerEvent.call(player, player.media, "volumechange");
              });
          },
        }); // Loop

        var loop = player.config.loop;
        Object.defineProperty(player.media, "loop", {
          get: function get() {
            return loop;
          },
          set: function set(input) {
            var toggle = is$1.boolean(input)
              ? input
              : player.config.loop.active;
            player.embed.setLoop(toggle).then(function () {
              loop = toggle;
            });
          },
        }); // Source

        var currentSrc;
        player.embed
          .getVideoUrl()
          .then(function (value) {
            currentSrc = value;
            controls.setDownloadLink.call(player);
          })
          .catch(function (error) {
            _this2.debug.warn(error);
          });
        Object.defineProperty(player.media, "currentSrc", {
          get: function get() {
            return currentSrc;
          },
        }); // Ended

        Object.defineProperty(player.media, "ended", {
          get: function get() {
            return player.currentTime === player.duration;
          },
        }); // Set aspect ratio based on video size

        Promise.all([
          player.embed.getVideoWidth(),
          player.embed.getVideoHeight(),
        ]).then(function (dimensions) {
          var ratio = getAspectRatio(dimensions[0], dimensions[1]);
          vimeo.setAspectRatio.call(_this2, ratio);
        }); // Set autopause

        player.embed
          .setAutopause(player.config.autopause)
          .then(function (state) {
            player.config.autopause = state;
          }); // Get title

        player.embed.getVideoTitle().then(function (title) {
          player.config.title = title;
          ui.setTitle.call(_this2);
        }); // Get current time

        player.embed.getCurrentTime().then(function (value) {
          currentTime = value;
          triggerEvent.call(player, player.media, "timeupdate");
        }); // Get duration

        player.embed.getDuration().then(function (value) {
          player.media.duration = value;
          triggerEvent.call(player, player.media, "durationchange");
        }); // Get captions

        player.embed.getTextTracks().then(function (tracks) {
          player.media.textTracks = tracks;
          captions.setup.call(player);
        });
        player.embed.on("cuechange", function (_ref) {
          var _ref$cues = _ref.cues,
            cues = _ref$cues === void 0 ? [] : _ref$cues;
          var strippedCues = cues.map(function (cue) {
            return stripHTML(cue.text);
          });
          captions.updateCues.call(player, strippedCues);
        });
        player.embed.on("loaded", function () {
          // Assure state and events are updated on autoplay
          player.embed.getPaused().then(function (paused) {
            assurePlaybackState.call(player, !paused);

            if (!paused) {
              triggerEvent.call(player, player.media, "playing");
            }
          });

          if (is$1.element(player.embed.element) && player.supported.ui) {
            var frame = player.embed.element; // Fix keyboard focus issues
            // https://github.com/sampotts/plyr/issues/317

            frame.setAttribute("tabindex", -1);
          }
        });
        player.embed.on("play", function () {
          assurePlaybackState.call(player, true);
          triggerEvent.call(player, player.media, "playing");
        });
        player.embed.on("pause", function () {
          assurePlaybackState.call(player, false);
        });
        player.embed.on("timeupdate", function (data) {
          player.media.seeking = false;
          currentTime = data.seconds;
          triggerEvent.call(player, player.media, "timeupdate");
        });
        player.embed.on("progress", function (data) {
          player.media.buffered = data.percent;
          triggerEvent.call(player, player.media, "progress"); // Check all loaded

          if (parseInt(data.percent, 10) === 1) {
            triggerEvent.call(player, player.media, "canplaythrough");
          } // Get duration as if we do it before load, it gives an incorrect value
          // https://github.com/sampotts/plyr/issues/891

          player.embed.getDuration().then(function (value) {
            if (value !== player.media.duration) {
              player.media.duration = value;
              triggerEvent.call(player, player.media, "durationchange");
            }
          });
        });
        player.embed.on("seeked", function () {
          player.media.seeking = false;
          triggerEvent.call(player, player.media, "seeked");
        });
        player.embed.on("ended", function () {
          player.media.paused = true;
          triggerEvent.call(player, player.media, "ended");
        });
        player.embed.on("error", function (detail) {
          player.media.error = detail;
          triggerEvent.call(player, player.media, "error");
        }); // Rebuild UI

        setTimeout(function () {
          return ui.build.call(player);
        }, 0);
      },
    };

    function parseId$1(url) {
      if (is$1.empty(url)) {
        return null;
      }

      var regex = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|&v=)([^#&?]*).*/;
      return url.match(regex) ? RegExp.$2 : url;
    } // Set playback state and trigger change (only on actual change)

    function assurePlaybackState$1(play) {
      if (play && !this.embed.hasPlayed) {
        this.embed.hasPlayed = true;
      }

      if (this.media.paused === play) {
        this.media.paused = !play;
        triggerEvent.call(this, this.media, play ? "play" : "pause");
      }
    }

    var youtube = {
      setup: function setup() {
        var _this = this;

        // Add embed class for responsive
        toggleClass(this.elements.wrapper, this.config.classNames.embed, true); // Set aspect ratio

        youtube.setAspectRatio.call(this); // Setup API

        if (is$1.object(window.YT) && is$1.function(window.YT.Player)) {
          youtube.ready.call(this);
        } else {
          // Load the API
          loadScript(this.config.urls.youtube.sdk).catch(function (error) {
            _this.debug.warn("YouTube API failed to load", error);
          }); // Setup callback for the API
          // YouTube has it's own system of course...

          window.onYouTubeReadyCallbacks = window.onYouTubeReadyCallbacks || []; // Add to queue

          window.onYouTubeReadyCallbacks.push(function () {
            youtube.ready.call(_this);
          }); // Set callback to process queue

          window.onYouTubeIframeAPIReady = function () {
            window.onYouTubeReadyCallbacks.forEach(function (callback) {
              callback();
            });
          };
        }
      },
      // Get the media title
      getTitle: function getTitle(videoId) {
        var _this2 = this;

        // Try via undocumented API method first
        // This method disappears now and then though...
        // https://github.com/sampotts/plyr/issues/709
        if (is$1.function(this.embed.getVideoData)) {
          var _this$embed$getVideoD = this.embed.getVideoData(),
            title = _this$embed$getVideoD.title;

          if (is$1.empty(title)) {
            this.config.title = title;
            ui.setTitle.call(this);
            return;
          }
        } // Or via Google API

        var key = this.config.keys.google;

        if (is$1.string(key) && !is$1.empty(key)) {
          var url = format(this.config.urls.youtube.api, videoId, key);
          fetch(url)
            .then(function (result) {
              if (is$1.object(result)) {
                _this2.config.title = result.items[0].snippet.title;
                ui.setTitle.call(_this2);
              }
            })
            .catch(function () {});
        }
      },
      // Set aspect ratio
      setAspectRatio: function setAspectRatio() {
        var ratio = this.config.ratio.split(":");
        this.elements.wrapper.style.paddingBottom = "".concat(
          (100 / ratio[0]) * ratio[1],
          "%"
        );
      },
      // API ready
      ready: function ready$$1() {
        var player = this; // Ignore already setup (race condition)

        var currentId = player.media.getAttribute("id");

        if (!is$1.empty(currentId) && currentId.startsWith("youtube-")) {
          return;
        } // Get the source URL or ID

        var source = player.media.getAttribute("src"); // Get from <div> if needed

        if (is$1.empty(source)) {
          source = player.media.getAttribute(this.config.attributes.embed.id);
        } // Replace the <iframe> with a <div> due to YouTube API issues

        var videoId = parseId$1(source);
        var id = generateId(player.provider); // Get poster, if already set

        var poster = player.poster; // Replace media element

        var container = createElement("div", {
          id: id,
          poster: poster,
        });
        player.media = replaceElement(container, player.media); // Id to poster wrapper

        var posterSrc = function posterSrc(format$$1) {
          return "https://img.youtube.com/vi/"
            .concat(videoId, "/")
            .concat(format$$1, "default.jpg");
        }; // Check thumbnail images in order of quality, but reject fallback thumbnails (120px wide)

        loadImage(posterSrc("maxres"), 121) // Higest quality and unpadded
          .catch(function () {
            return loadImage(posterSrc("sd"), 121);
          }) // 480p padded 4:3
          .catch(function () {
            return loadImage(posterSrc("hq"));
          }) // 360p padded 4:3. Always exists
          .then(function (image) {
            return ui.setPoster.call(player, image.src);
          })
          .then(function (posterSrc) {
            // If the image is padded, use background-size "cover" instead (like youtube does too with their posters)
            if (!posterSrc.includes("maxres")) {
              player.elements.poster.style.backgroundSize = "cover";
            }
          })
          .catch(function () {}); // Setup instance
        // https://developers.google.com/youtube/iframe_api_reference

        player.embed = new window.YT.Player(id, {
          videoId: videoId,
          playerVars: {
            autoplay: player.config.autoplay ? 1 : 0,
            // Autoplay
            hl: player.config.hl,
            // iframe interface language
            controls: player.supported.ui ? 0 : 1,
            // Only show controls if not fully supported
            rel: 0,
            // No related vids
            showinfo: 0,
            // Hide info
            iv_load_policy: 3,
            // Hide annotations
            modestbranding: 1,
            // Hide logos as much as possible (they still show one in the corner when paused)
            disablekb: 1,
            // Disable keyboard as we handle it
            playsinline: 1,
            // Allow iOS inline playback
            // Tracking for stats
            // origin: window ? `${window.location.protocol}//${window.location.host}` : null,
            widget_referrer: window ? window.location.href : null,
            // Captions are flaky on YouTube
            cc_load_policy: player.captions.active ? 1 : 0,
            cc_lang_pref: player.config.captions.language,
          },
          events: {
            onError: function onError(event) {
              // YouTube may fire onError twice, so only handle it once
              if (!player.media.error) {
                var code = event.data; // Messages copied from https://developers.google.com/youtube/iframe_api_reference#onError

                var message =
                  {
                    2: "The request contains an invalid parameter value. For example, this error occurs if you specify a video ID that does not have 11 characters, or if the video ID contains invalid characters, such as exclamation points or asterisks.",
                    5: "The requested content cannot be played in an HTML5 player or another error related to the HTML5 player has occurred.",
                    100: "The video requested was not found. This error occurs when a video has been removed (for any reason) or has been marked as private.",
                    101: "The owner of the requested video does not allow it to be played in embedded players.",
                    150: "The owner of the requested video does not allow it to be played in embedded players.",
                  }[code] || "An unknown error occured";
                player.media.error = {
                  code: code,
                  message: message,
                };
                triggerEvent.call(player, player.media, "error");
              }
            },
            onPlaybackRateChange: function onPlaybackRateChange(event) {
              // Get the instance
              var instance = event.target; // Get current speed

              player.media.playbackRate = instance.getPlaybackRate();
              triggerEvent.call(player, player.media, "ratechange");
            },
            onReady: function onReady(event) {
              // Bail if onReady has already been called. See issue #1108
              if (is$1.function(player.media.play)) {
                return;
              } // Get the instance

              var instance = event.target; // Get the title

              youtube.getTitle.call(player, videoId); // Create a faux HTML5 API using the YouTube API

              player.media.play = function () {
                assurePlaybackState$1.call(player, true);
                instance.playVideo();
              };

              player.media.pause = function () {
                assurePlaybackState$1.call(player, false);
                instance.pauseVideo();
              };

              player.media.stop = function () {
                instance.stopVideo();
              };

              player.media.duration = instance.getDuration();
              player.media.paused = true; // Seeking

              player.media.currentTime = 0;
              Object.defineProperty(player.media, "currentTime", {
                get: function get() {
                  return Number(instance.getCurrentTime());
                },
                set: function set(time) {
                  // If paused and never played, mute audio preventively (YouTube starts playing on seek if the video hasn't been played yet).
                  if (player.paused && !player.embed.hasPlayed) {
                    player.embed.mute();
                  } // Set seeking state and trigger event

                  player.media.seeking = true;
                  triggerEvent.call(player, player.media, "seeking"); // Seek after events sent

                  instance.seekTo(time);
                },
              }); // Playback speed

              Object.defineProperty(player.media, "playbackRate", {
                get: function get() {
                  return instance.getPlaybackRate();
                },
                set: function set(input) {
                  instance.setPlaybackRate(input);
                },
              }); // Volume

              var volume = player.config.volume;
              Object.defineProperty(player.media, "volume", {
                get: function get() {
                  return volume;
                },
                set: function set(input) {
                  volume = input;
                  instance.setVolume(volume * 100);
                  triggerEvent.call(player, player.media, "volumechange");
                },
              }); // Muted

              var muted = player.config.muted;
              Object.defineProperty(player.media, "muted", {
                get: function get() {
                  return muted;
                },
                set: function set(input) {
                  var toggle = is$1.boolean(input) ? input : muted;
                  muted = toggle;
                  instance[toggle ? "mute" : "unMute"]();
                  triggerEvent.call(player, player.media, "volumechange");
                },
              }); // Source

              Object.defineProperty(player.media, "currentSrc", {
                get: function get() {
                  return instance.getVideoUrl();
                },
              }); // Ended

              Object.defineProperty(player.media, "ended", {
                get: function get() {
                  return player.currentTime === player.duration;
                },
              }); // Get available speeds

              player.options.speed = instance.getAvailablePlaybackRates(); // Set the tabindex to avoid focus entering iframe

              if (player.supported.ui) {
                player.media.setAttribute("tabindex", -1);
              }

              triggerEvent.call(player, player.media, "timeupdate");
              triggerEvent.call(player, player.media, "durationchange"); // Reset timer

              clearInterval(player.timers.buffering); // Setup buffering

              player.timers.buffering = setInterval(function () {
                // Get loaded % from YouTube
                player.media.buffered = instance.getVideoLoadedFraction(); // Trigger progress only when we actually buffer something

                if (
                  player.media.lastBuffered === null ||
                  player.media.lastBuffered < player.media.buffered
                ) {
                  triggerEvent.call(player, player.media, "progress");
                } // Set last buffer point

                player.media.lastBuffered = player.media.buffered; // Bail if we're at 100%

                if (player.media.buffered === 1) {
                  clearInterval(player.timers.buffering); // Trigger event

                  triggerEvent.call(player, player.media, "canplaythrough");
                }
              }, 200); // Rebuild UI

              setTimeout(function () {
                return ui.build.call(player);
              }, 50);
            },
            onStateChange: function onStateChange(event) {
              // Get the instance
              var instance = event.target; // Reset timer

              clearInterval(player.timers.playing);
              var seeked = player.media.seeking && [1, 2].includes(event.data);

              if (seeked) {
                // Unset seeking and fire seeked event
                player.media.seeking = false;
                triggerEvent.call(player, player.media, "seeked");
              } // Handle events
              // -1   Unstarted
              // 0    Ended
              // 1    Playing
              // 2    Paused
              // 3    Buffering
              // 5    Video cued

              switch (event.data) {
                case -1:
                  // Update scrubber
                  triggerEvent.call(player, player.media, "timeupdate"); // Get loaded % from YouTube

                  player.media.buffered = instance.getVideoLoadedFraction();
                  triggerEvent.call(player, player.media, "progress");
                  break;

                case 0:
                  assurePlaybackState$1.call(player, false); // YouTube doesn't support loop for a single video, so mimick it.

                  if (player.media.loop) {
                    // YouTube needs a call to `stopVideo` before playing again
                    instance.stopVideo();
                    instance.playVideo();
                  } else {
                    triggerEvent.call(player, player.media, "ended");
                  }

                  break;

                case 1:
                  // Restore paused state (YouTube starts playing on seek if the video hasn't been played yet)
                  if (player.media.paused && !player.embed.hasPlayed) {
                    player.media.pause();
                  } else {
                    assurePlaybackState$1.call(player, true);
                    triggerEvent.call(player, player.media, "playing"); // Poll to get playback progress

                    player.timers.playing = setInterval(function () {
                      triggerEvent.call(player, player.media, "timeupdate");
                    }, 50); // Check duration again due to YouTube bug
                    // https://github.com/sampotts/plyr/issues/374
                    // https://code.google.com/p/gdata-issues/issues/detail?id=8690

                    if (player.media.duration !== instance.getDuration()) {
                      player.media.duration = instance.getDuration();
                      triggerEvent.call(player, player.media, "durationchange");
                    }
                  }

                  break;

                case 2:
                  // Restore audio (YouTube starts playing on seek if the video hasn't been played yet)
                  if (!player.muted) {
                    player.embed.unMute();
                  }

                  assurePlaybackState$1.call(player, false);
                  break;

                default:
                  break;
              }

              triggerEvent.call(
                player,
                player.elements.container,
                "statechange",
                false,
                {
                  code: event.data,
                }
              );
            },
          },
        });
      },
    };

    var media = {
      // Setup media
      setup: function setup() {
        // If there's no media, bail
        if (!this.media) {
          this.debug.warn("No media element found!");
          return;
        } // Add type class

        toggleClass(
          this.elements.container,
          this.config.classNames.type.replace("{0}", this.type),
          true
        ); // Add provider class

        toggleClass(
          this.elements.container,
          this.config.classNames.provider.replace("{0}", this.provider),
          true
        ); // Add video class for embeds
        // This will require changes if audio embeds are added

        if (this.isEmbed) {
          toggleClass(
            this.elements.container,
            this.config.classNames.type.replace("{0}", "video"),
            true
          );
        } // Inject the player wrapper

        if (this.isVideo) {
          // Create the wrapper div
          this.elements.wrapper = createElement("div", {
            class: this.config.classNames.video,
          }); // Wrap the video in a container

          wrap(this.media, this.elements.wrapper); // Faux poster container

          this.elements.poster = createElement("div", {
            class: this.config.classNames.poster,
          });
          this.elements.wrapper.appendChild(this.elements.poster);
        }

        if (this.isHTML5) {
          html5.extend.call(this);
        } else if (this.isYouTube) {
          youtube.setup.call(this);
        } else if (this.isVimeo) {
          vimeo.setup.call(this);
        }
      },
    };

    var Ads =
      /*#__PURE__*/
      (function () {
        /**
         * Ads constructor.
         * @param {object} player
         * @return {Ads}
         */
        function Ads(player) {
          var _this = this;

          _classCallCheck(this, Ads);

          this.player = player;
          this.publisherId = player.config.ads.publisherId;
          this.playing = false;
          this.initialized = false;
          this.elements = {
            container: null,
            displayContainer: null,
          };
          this.manager = null;
          this.loader = null;
          this.cuePoints = null;
          this.events = {};
          this.safetyTimer = null;
          this.countdownTimer = null; // Setup a promise to resolve when the IMA manager is ready

          this.managerPromise = new Promise(function (resolve, reject) {
            // The ad is loaded and ready
            _this.on("loaded", resolve); // Ads failed

            _this.on("error", reject);
          });
          this.load();
        }

        _createClass(Ads, [
          {
            key: "load",

            /**
             * Load the IMA SDK
             */
            value: function load() {
              var _this2 = this;

              if (this.enabled) {
                // Check if the Google IMA3 SDK is loaded or load it ourselves
                if (
                  !is$1.object(window.google) ||
                  !is$1.object(window.google.ima)
                ) {
                  loadScript(this.player.config.urls.googleIMA.sdk)
                    .then(function () {
                      _this2.ready();
                    })
                    .catch(function () {
                      // Script failed to load or is blocked
                      _this2.trigger(
                        "error",
                        new Error("Google IMA SDK failed to load")
                      );
                    });
                } else {
                  this.ready();
                }
              }
            },
            /**
             * Get the ads instance ready
             */
          },
          {
            key: "ready",
            value: function ready$$1() {
              var _this3 = this;

              // Start ticking our safety timer. If the whole advertisement
              // thing doesn't resolve within our set time; we bail
              this.startSafetyTimer(12000, "ready()"); // Clear the safety timer

              this.managerPromise.then(function () {
                _this3.clearSafetyTimer("onAdsManagerLoaded()");
              }); // Set listeners on the Plyr instance

              this.listeners(); // Setup the IMA SDK

              this.setupIMA();
            }, // Build the default tag URL
          },
          {
            key: "setupIMA",

            /**
             * In order for the SDK to display ads for our video, we need to tell it where to put them,
             * so here we define our ad container. This div is set up to render on top of the video player.
             * Using the code below, we tell the SDK to render ads within that div. We also provide a
             * handle to the content video player - the SDK will poll the current time of our player to
             * properly place mid-rolls. After we create the ad display container, we initialize it. On
             * mobile devices, this initialization is done as the result of a user action.
             */
            value: function setupIMA() {
              // Create the container for our advertisements
              this.elements.container = createElement("div", {
                class: this.player.config.classNames.ads,
              });
              this.player.elements.container.appendChild(
                this.elements.container
              ); // So we can run VPAID2

              google.ima.settings.setVpaidMode(
                google.ima.ImaSdkSettings.VpaidMode.ENABLED
              ); // Set language

              google.ima.settings.setLocale(this.player.config.ads.language); // We assume the adContainer is the video container of the plyr element
              // that will house the ads

              this.elements.displayContainer = new google.ima.AdDisplayContainer(
                this.elements.container
              ); // Request video ads to be pre-loaded

              this.requestAds();
            },
            /**
             * Request advertisements
             */
          },
          {
            key: "requestAds",
            value: function requestAds() {
              var _this4 = this;

              var container = this.player.elements.container;

              try {
                // Create ads loader
                this.loader = new google.ima.AdsLoader(
                  this.elements.displayContainer
                ); // Listen and respond to ads loaded and error events

                this.loader.addEventListener(
                  google.ima.AdsManagerLoadedEvent.Type.ADS_MANAGER_LOADED,
                  function (event) {
                    return _this4.onAdsManagerLoaded(event);
                  },
                  false
                );
                this.loader.addEventListener(
                  google.ima.AdErrorEvent.Type.AD_ERROR,
                  function (error) {
                    return _this4.onAdError(error);
                  },
                  false
                ); // Request video ads

                var request = new google.ima.AdsRequest();
                request.adTagUrl = this.tagUrl; // Specify the linear and nonlinear slot sizes. This helps the SDK
                // to select the correct creative if multiple are returned

                request.linearAdSlotWidth = container.offsetWidth;
                request.linearAdSlotHeight = container.offsetHeight;
                request.nonLinearAdSlotWidth = container.offsetWidth;
                request.nonLinearAdSlotHeight = container.offsetHeight; // We only overlay ads as we only support video.

                request.forceNonLinearFullSlot = false; // Mute based on current state

                request.setAdWillPlayMuted(!this.player.muted);
                this.loader.requestAds(request);
              } catch (e) {
                this.onAdError(e);
              }
            },
            /**
             * Update the ad countdown
             * @param {boolean} start
             */
          },
          {
            key: "pollCountdown",
            value: function pollCountdown() {
              var _this5 = this;

              var start =
                arguments.length > 0 && arguments[0] !== undefined
                  ? arguments[0]
                  : false;

              if (!start) {
                clearInterval(this.countdownTimer);
                this.elements.container.removeAttribute("data-badge-text");
                return;
              }

              var update = function update() {
                var time = formatTime(
                  Math.max(_this5.manager.getRemainingTime(), 0)
                );
                var label = ""
                  .concat(
                    i18n.get("advertisement", _this5.player.config),
                    " - "
                  )
                  .concat(time);

                _this5.elements.container.setAttribute(
                  "data-badge-text",
                  label
                );
              };

              this.countdownTimer = setInterval(update, 100);
            },
            /**
             * This method is called whenever the ads are ready inside the AdDisplayContainer
             * @param {Event} adsManagerLoadedEvent
             */
          },
          {
            key: "onAdsManagerLoaded",
            value: function onAdsManagerLoaded(event) {
              var _this6 = this;

              // Load could occur after a source change (race condition)
              if (!this.enabled) {
                return;
              } // Get the ads manager

              var settings = new google.ima.AdsRenderingSettings(); // Tell the SDK to save and restore content video state on our behalf

              settings.restoreCustomPlaybackStateOnAdBreakComplete = true;
              settings.enablePreloading = true; // The SDK is polling currentTime on the contentPlayback. And needs a duration
              // so it can determine when to start the mid- and post-roll

              this.manager = event.getAdsManager(this.player, settings); // Get the cue points for any mid-rolls by filtering out the pre- and post-roll

              this.cuePoints = this.manager.getCuePoints(); // Add advertisement cue's within the time line if available

              if (!is$1.empty(this.cuePoints)) {
                this.cuePoints.forEach(function (cuePoint) {
                  if (
                    cuePoint !== 0 &&
                    cuePoint !== -1 &&
                    cuePoint < _this6.player.duration
                  ) {
                    var seekElement = _this6.player.elements.progress;

                    if (is$1.element(seekElement)) {
                      var cuePercentage =
                        (100 / _this6.player.duration) * cuePoint;
                      var cue = createElement("span", {
                        class: _this6.player.config.classNames.cues,
                      });
                      cue.style.left = "".concat(cuePercentage.toString(), "%");
                      seekElement.appendChild(cue);
                    }
                  }
                });
              } // Set volume to match player

              this.manager.setVolume(this.player.volume); // Add listeners to the required events
              // Advertisement error events

              this.manager.addEventListener(
                google.ima.AdErrorEvent.Type.AD_ERROR,
                function (error) {
                  return _this6.onAdError(error);
                }
              ); // Advertisement regular events

              Object.keys(google.ima.AdEvent.Type).forEach(function (type) {
                _this6.manager.addEventListener(
                  google.ima.AdEvent.Type[type],
                  function (event) {
                    return _this6.onAdEvent(event);
                  }
                );
              }); // Resolve our adsManager

              this.trigger("loaded");
            },
            /**
             * This is where all the event handling takes place. Retrieve the ad from the event. Some
             * events (e.g. ALL_ADS_COMPLETED) don't have the ad object associated
             * https://developers.google.com/interactive-media-ads/docs/sdks/html5/v3/apis#ima.AdEvent.Type
             * @param {Event} event
             */
          },
          {
            key: "onAdEvent",
            value: function onAdEvent(event) {
              var _this7 = this;

              var container = this.player.elements.container; // Retrieve the ad from the event. Some events (e.g. ALL_ADS_COMPLETED)
              // don't have ad object associated

              var ad = event.getAd(); // Proxy event

              var dispatchEvent = function dispatchEvent(type) {
                var event = "ads".concat(type.replace(/_/g, "").toLowerCase());
                triggerEvent.call(_this7.player, _this7.player.media, event);
              };

              switch (event.type) {
                case google.ima.AdEvent.Type.LOADED:
                  // This is the first event sent for an ad - it is possible to determine whether the
                  // ad is a video ad or an overlay
                  this.trigger("loaded"); // Bubble event

                  dispatchEvent(event.type); // Start countdown

                  this.pollCountdown(true);

                  if (!ad.isLinear()) {
                    // Position AdDisplayContainer correctly for overlay
                    ad.width = container.offsetWidth;
                    ad.height = container.offsetHeight;
                  } // console.info('Ad type: ' + event.getAd().getAdPodInfo().getPodIndex());
                  // console.info('Ad time: ' + event.getAd().getAdPodInfo().getTimeOffset());

                  break;

                case google.ima.AdEvent.Type.ALL_ADS_COMPLETED:
                  // All ads for the current videos are done. We can now request new advertisements
                  // in case the video is re-played
                  // Fire event
                  dispatchEvent(event.type); // TODO: Example for what happens when a next video in a playlist would be loaded.
                  // So here we load a new video when all ads are done.
                  // Then we load new ads within a new adsManager. When the video
                  // Is started - after - the ads are loaded, then we get ads.
                  // You can also easily test cancelling and reloading by running
                  // player.ads.cancel() and player.ads.play from the console I guess.
                  // this.player.source = {
                  //     type: 'video',
                  //     title: 'View From A Blue Moon',
                  //     sources: [{
                  //         src:
                  // 'https://cdn.plyr.io/static/demo/View_From_A_Blue_Moon_Trailer-HD.mp4', type:
                  // 'video/mp4', }], poster:
                  // 'https://cdn.plyr.io/static/demo/View_From_A_Blue_Moon_Trailer-HD.jpg', tracks:
                  // [ { kind: 'captions', label: 'English', srclang: 'en', src:
                  // 'https://cdn.plyr.io/static/demo/View_From_A_Blue_Moon_Trailer-HD.en.vtt',
                  // default: true, }, { kind: 'captions', label: 'French', srclang: 'fr', src:
                  // 'https://cdn.plyr.io/static/demo/View_From_A_Blue_Moon_Trailer-HD.fr.vtt', }, ],
                  // };
                  // TODO: So there is still this thing where a video should only be allowed to start
                  // playing when the IMA SDK is ready or has failed

                  this.loadAds();
                  break;

                case google.ima.AdEvent.Type.CONTENT_PAUSE_REQUESTED:
                  // This event indicates the ad has started - the video player can adjust the UI,
                  // for example display a pause button and remaining time. Fired when content should
                  // be paused. This usually happens right before an ad is about to cover the content
                  dispatchEvent(event.type);
                  this.pauseContent();
                  break;

                case google.ima.AdEvent.Type.CONTENT_RESUME_REQUESTED:
                  // This event indicates the ad has finished - the video player can perform
                  // appropriate UI actions, such as removing the timer for remaining time detection.
                  // Fired when content should be resumed. This usually happens when an ad finishes
                  // or collapses
                  dispatchEvent(event.type);
                  this.pollCountdown();
                  this.resumeContent();
                  break;

                case google.ima.AdEvent.Type.STARTED:
                case google.ima.AdEvent.Type.MIDPOINT:
                case google.ima.AdEvent.Type.COMPLETE:
                case google.ima.AdEvent.Type.IMPRESSION:
                case google.ima.AdEvent.Type.CLICK:
                  dispatchEvent(event.type);
                  break;

                default:
                  break;
              }
            },
            /**
             * Any ad error handling comes through here
             * @param {Event} event
             */
          },
          {
            key: "onAdError",
            value: function onAdError(event) {
              this.cancel();
              this.player.debug.warn("Ads error", event);
            },
            /**
             * Setup hooks for Plyr and window events. This ensures
             * the mid- and post-roll launch at the correct time. And
             * resize the advertisement when the player resizes
             */
          },
          {
            key: "listeners",
            value: function listeners() {
              var _this8 = this;

              var container = this.player.elements.container;
              var time; // Add listeners to the required events

              this.player.on("ended", function () {
                _this8.loader.contentComplete();
              });
              this.player.on("seeking", function () {
                time = _this8.player.currentTime;
                return time;
              });
              this.player.on("seeked", function () {
                var seekedTime = _this8.player.currentTime;

                if (is$1.empty(_this8.cuePoints)) {
                  return;
                }

                _this8.cuePoints.forEach(function (cuePoint, index) {
                  if (time < cuePoint && cuePoint < seekedTime) {
                    _this8.manager.discardAdBreak();

                    _this8.cuePoints.splice(index, 1);
                  }
                });
              }); // Listen to the resizing of the window. And resize ad accordingly
              // TODO: eventually implement ResizeObserver

              window.addEventListener("resize", function () {
                if (_this8.manager) {
                  _this8.manager.resize(
                    container.offsetWidth,
                    container.offsetHeight,
                    google.ima.ViewMode.NORMAL
                  );
                }
              });
            },
            /**
             * Initialize the adsManager and start playing advertisements
             */
          },
          {
            key: "play",
            value: function play() {
              var _this9 = this;

              var container = this.player.elements.container;

              if (!this.managerPromise) {
                this.resumeContent();
              } // Play the requested advertisement whenever the adsManager is ready

              this.managerPromise
                .then(function () {
                  // Initialize the container. Must be done via a user action on mobile devices
                  _this9.elements.displayContainer.initialize();

                  try {
                    if (!_this9.initialized) {
                      // Initialize the ads manager. Ad rules playlist will start at this time
                      _this9.manager.init(
                        container.offsetWidth,
                        container.offsetHeight,
                        google.ima.ViewMode.NORMAL
                      ); // Call play to start showing the ad. Single video and overlay ads will
                      // start at this time; the call will be ignored for ad rules

                      _this9.manager.start();
                    }

                    _this9.initialized = true;
                  } catch (adError) {
                    // An error may be thrown if there was a problem with the
                    // VAST response
                    _this9.onAdError(adError);
                  }
                })
                .catch(function () {});
            },
            /**
             * Resume our video
             */
          },
          {
            key: "resumeContent",
            value: function resumeContent() {
              // Hide the advertisement container
              this.elements.container.style.zIndex = ""; // Ad is stopped

              this.playing = false; // Play our video

              if (this.player.currentTime < this.player.duration) {
                this.player.play();
              }
            },
            /**
             * Pause our video
             */
          },
          {
            key: "pauseContent",
            value: function pauseContent() {
              // Show the advertisement container
              this.elements.container.style.zIndex = 3; // Ad is playing.

              this.playing = true; // Pause our video.

              this.player.pause();
            },
            /**
             * Destroy the adsManager so we can grab new ads after this. If we don't then we're not
             * allowed to call new ads based on google policies, as they interpret this as an accidental
             * video requests. https://developers.google.com/interactive-
             * media-ads/docs/sdks/android/faq#8
             */
          },
          {
            key: "cancel",
            value: function cancel() {
              // Pause our video
              if (this.initialized) {
                this.resumeContent();
              } // Tell our instance that we're done for now

              this.trigger("error"); // Re-create our adsManager

              this.loadAds();
            },
            /**
             * Re-create our adsManager
             */
          },
          {
            key: "loadAds",
            value: function loadAds() {
              var _this10 = this;

              // Tell our adsManager to go bye bye
              this.managerPromise
                .then(function () {
                  // Destroy our adsManager
                  if (_this10.manager) {
                    _this10.manager.destroy();
                  } // Re-set our adsManager promises

                  _this10.managerPromise = new Promise(function (resolve) {
                    _this10.on("loaded", resolve);

                    _this10.player.debug.log(_this10.manager);
                  }); // Now request some new advertisements

                  _this10.requestAds();
                })
                .catch(function () {});
            },
            /**
             * Handles callbacks after an ad event was invoked
             * @param {string} event - Event type
             */
          },
          {
            key: "trigger",
            value: function trigger(event) {
              var _this11 = this;

              for (
                var _len = arguments.length,
                  args = new Array(_len > 1 ? _len - 1 : 0),
                  _key = 1;
                _key < _len;
                _key++
              ) {
                args[_key - 1] = arguments[_key];
              }

              var handlers = this.events[event];

              if (is$1.array(handlers)) {
                handlers.forEach(function (handler) {
                  if (is$1.function(handler)) {
                    handler.apply(_this11, args);
                  }
                });
              }
            },
            /**
             * Add event listeners
             * @param {string} event - Event type
             * @param {function} callback - Callback for when event occurs
             * @return {Ads}
             */
          },
          {
            key: "on",
            value: function on$$1(event, callback) {
              if (!is$1.array(this.events[event])) {
                this.events[event] = [];
              }

              this.events[event].push(callback);
              return this;
            },
            /**
             * Setup a safety timer for when the ad network doesn't respond for whatever reason.
             * The advertisement has 12 seconds to get its things together. We stop this timer when the
             * advertisement is playing, or when a user action is required to start, then we clear the
             * timer on ad ready
             * @param {number} time
             * @param {string} from
             */
          },
          {
            key: "startSafetyTimer",
            value: function startSafetyTimer(time, from) {
              var _this12 = this;

              this.player.debug.log("Safety timer invoked from: ".concat(from));
              this.safetyTimer = setTimeout(function () {
                _this12.cancel();

                _this12.clearSafetyTimer("startSafetyTimer()");
              }, time);
            },
            /**
             * Clear our safety timer(s)
             * @param {string} from
             */
          },
          {
            key: "clearSafetyTimer",
            value: function clearSafetyTimer(from) {
              if (!is$1.nullOrUndefined(this.safetyTimer)) {
                this.player.debug.log(
                  "Safety timer cleared from: ".concat(from)
                );
                clearTimeout(this.safetyTimer);
                this.safetyTimer = null;
              }
            },
          },
          {
            key: "enabled",
            get: function get() {
              return (
                this.player.isHTML5 &&
                this.player.isVideo &&
                this.player.config.ads.enabled &&
                !is$1.empty(this.publisherId)
              );
            },
          },
          {
            key: "tagUrl",
            get: function get() {
              var params = {
                AV_PUBLISHERID: "58c25bb0073ef448b1087ad6",
                AV_CHANNELID: "5a0458dc28a06145e4519d21",
                AV_URL: window.location.hostname,
                cb: Date.now(),
                AV_WIDTH: 640,
                AV_HEIGHT: 480,
                AV_CDIM2: this.publisherId,
              };
              var base = "https://go.aniview.com/api/adserver6/vast/";
              return "".concat(base, "?").concat(buildUrlParams(params));
            },
          },
        ]);

        return Ads;
      })();

    var source = {
      // Add elements to HTML5 media (source, tracks, etc)
      insertElements: function insertElements(type, attributes) {
        var _this = this;

        if (is$1.string(attributes)) {
          insertElement(type, this.media, {
            src: attributes,
          });
        } else if (is$1.array(attributes)) {
          attributes.forEach(function (attribute) {
            insertElement(type, _this.media, attribute);
          });
        }
      },
      // Update source
      // Sources are not checked for support so be careful
      change: function change(input) {
        var _this2 = this;

        if (!getDeep(input, "sources.length")) {
          this.debug.warn("Invalid source format");
          return;
        } // Cancel current network requests

        html5.cancelRequests.call(this); // Destroy instance and re-setup

        this.destroy.call(
          this,
          function () {
            // Reset quality options
            _this2.options.quality = []; // Remove elements

            removeElement(_this2.media);
            _this2.media = null; // Reset class name

            if (is$1.element(_this2.elements.container)) {
              _this2.elements.container.removeAttribute("class");
            } // Set the type and provider

            var sources = input.sources,
              type = input.type;

            var _sources = _slicedToArray(sources, 1),
              _sources$ = _sources[0],
              _sources$$provider = _sources$.provider,
              provider =
                _sources$$provider === void 0
                  ? providers.html5
                  : _sources$$provider,
              src = _sources$.src;

            var tagName = provider === "html5" ? type : "div";
            var attributes =
              provider === "html5"
                ? {}
                : {
                    src: src,
                  };
            Object.assign(_this2, {
              provider: provider,
              type: type,
              // Check for support
              supported: support.check(
                type,
                provider,
                _this2.config.playsinline
              ),
              // Create new element
              media: createElement(tagName, attributes),
            }); // Inject the new element

            _this2.elements.container.appendChild(_this2.media); // Autoplay the new source?

            if (is$1.boolean(input.autoplay)) {
              _this2.config.autoplay = input.autoplay;
            } // Set attributes for audio and video

            if (_this2.isHTML5) {
              if (_this2.config.crossorigin) {
                _this2.media.setAttribute("crossorigin", "");
              }

              if (_this2.config.autoplay) {
                _this2.media.setAttribute("autoplay", "");
              }

              if (!is$1.empty(input.poster)) {
                _this2.poster = input.poster;
              }

              if (_this2.config.loop.active) {
                _this2.media.setAttribute("loop", "");
              }

              if (_this2.config.muted) {
                _this2.media.setAttribute("muted", "");
              }

              if (_this2.config.playsinline) {
                _this2.media.setAttribute("playsinline", "");
              }
            } // Restore class hook

            ui.addStyleHook.call(_this2); // Set new sources for html5

            if (_this2.isHTML5) {
              source.insertElements.call(_this2, "source", sources);
            } // Set video title

            _this2.config.title = input.title; // Set up from scratch

            media.setup.call(_this2); // HTML5 stuff

            if (_this2.isHTML5) {
              // Setup captions
              if ("tracks" in input) {
                source.insertElements.call(_this2, "track", input.tracks);
              } // Load HTML5 sources

              _this2.media.load();
            } // If HTML5 or embed but not fully supported, setupInterface and call ready now

            if (_this2.isHTML5 || (_this2.isEmbed && !_this2.supported.ui)) {
              // Setup interface
              ui.build.call(_this2);
            } // Update the fullscreen support

            _this2.fullscreen.update();
          },
          true
        );
      },
    };

    // TODO: Use a WeakMap for private globals
    // const globals = new WeakMap();
    // Plyr instance

    var Plyr =
      /*#__PURE__*/
      (function () {
        function Plyr(target, options) {
          var _this = this;

          _classCallCheck(this, Plyr);

          this.timers = {}; // State

          this.ready = false;
          this.loading = false;
          this.failed = false; // Touch device

          this.touch = support.touch; // Set the media element

          this.media = target; // String selector passed

          if (is$1.string(this.media)) {
            this.media = document.querySelectorAll(this.media);
          } // jQuery, NodeList or Array passed, use first element

          if (
            (window.jQuery && this.media instanceof jQuery) ||
            is$1.nodeList(this.media) ||
            is$1.array(this.media)
          ) {
            // eslint-disable-next-line
            this.media = this.media[0];
          } // Set config

          this.config = extend(
            {},
            defaults,
            Plyr.defaults,
            options || {},
            (function () {
              try {
                return JSON.parse(_this.media.getAttribute("data-plyr-config"));
              } catch (e) {
                return {};
              }
            })()
          ); // Elements cache

          this.elements = {
            container: null,
            captions: null,
            buttons: {},
            display: {},
            progress: {},
            inputs: {},
            settings: {
              popup: null,
              menu: null,
              panels: {},
              buttons: {},
            },
          }; // Captions

          this.captions = {
            active: null,
            currentTrack: -1,
            meta: new WeakMap(),
          }; // Fullscreen

          this.fullscreen = {
            active: false,
          }; // Options

          this.options = {
            speed: [],
            quality: [],
          }; // Debugging
          // TODO: move to globals

          this.debug = new Console(this.config.debug); // Log config options and support

          this.debug.log("Config", this.config);
          this.debug.log("Support", support); // We need an element to setup

          if (is$1.nullOrUndefined(this.media) || !is$1.element(this.media)) {
            this.debug.error("Setup failed: no suitable element passed");
            return;
          } // Bail if the element is initialized

          if (this.media.plyr) {
            this.debug.warn("Target already setup");
            return;
          } // Bail if not enabled

          if (!this.config.enabled) {
            this.debug.error("Setup failed: disabled by config");
            return;
          } // Bail if disabled or no basic support
          // You may want to disable certain UAs etc

          if (!support.check().api) {
            this.debug.error("Setup failed: no support");
            return;
          } // Cache original element state for .destroy()

          var clone = this.media.cloneNode(true);
          clone.autoplay = false;
          this.elements.original = clone; // Set media type based on tag or data attribute
          // Supported: video, audio, vimeo, youtube

          var type = this.media.tagName.toLowerCase(); // Embed properties

          var iframe = null;
          var url = null; // Different setup based on type

          switch (type) {
            case "div":
              // Find the frame
              iframe = this.media.querySelector("iframe"); // <iframe> type

              if (is$1.element(iframe)) {
                // Detect provider
                url = parseUrl(iframe.getAttribute("src"));
                this.provider = getProviderByUrl(url.toString()); // Rework elements

                this.elements.container = this.media;
                this.media = iframe; // Reset classname

                this.elements.container.className = ""; // Get attributes from URL and set config

                if (url.search.length) {
                  var truthy = ["1", "true"];

                  if (truthy.includes(url.searchParams.get("autoplay"))) {
                    this.config.autoplay = true;
                  }

                  if (truthy.includes(url.searchParams.get("loop"))) {
                    this.config.loop.active = true;
                  } // TODO: replace fullscreen.iosNative with this playsinline config option
                  // YouTube requires the playsinline in the URL

                  if (this.isYouTube) {
                    this.config.playsinline = truthy.includes(
                      url.searchParams.get("playsinline")
                    );
                    this.config.hl = url.searchParams.get("hl"); // TODO: Should this be setting language?
                  } else {
                    this.config.playsinline = true;
                  }
                }
              } else {
                // <div> with attributes
                this.provider = this.media.getAttribute(
                  this.config.attributes.embed.provider
                ); // Remove attribute

                this.media.removeAttribute(
                  this.config.attributes.embed.provider
                );
              } // Unsupported or missing provider

              if (
                is$1.empty(this.provider) ||
                !Object.keys(providers).includes(this.provider)
              ) {
                this.debug.error("Setup failed: Invalid provider");
                return;
              } // Audio will come later for external providers

              this.type = types.video;
              break;

            case "video":
            case "audio":
              this.type = type;
              this.provider = providers.html5; // Get config from attributes

              if (this.media.hasAttribute("crossorigin")) {
                this.config.crossorigin = true;
              }

              if (this.media.hasAttribute("autoplay")) {
                this.config.autoplay = true;
              }

              if (
                this.media.hasAttribute("playsinline") ||
                this.media.hasAttribute("webkit-playsinline")
              ) {
                this.config.playsinline = true;
              }

              if (this.media.hasAttribute("muted")) {
                this.config.muted = true;
              }

              if (this.media.hasAttribute("loop")) {
                this.config.loop.active = true;
              }

              break;

            default:
              this.debug.error("Setup failed: unsupported type");
              return;
          } // Check for support again but with type

          this.supported = support.check(
            this.type,
            this.provider,
            this.config.playsinline
          ); // If no support for even API, bail

          if (!this.supported.api) {
            this.debug.error("Setup failed: no support");
            return;
          }

          this.eventListeners = []; // Create listeners

          this.listeners = new Listeners(this); // Setup local storage for user settings

          this.storage = new Storage(this); // Store reference

          this.media.plyr = this; // Wrap media

          if (!is$1.element(this.elements.container)) {
            this.elements.container = createElement("div");
            wrap(this.media, this.elements.container);
          } // Add style hook

          ui.addStyleHook.call(this); // Setup media

          media.setup.call(this); // Listen for events if debugging

          if (this.config.debug) {
            on.call(
              this,
              this.elements.container,
              this.config.events.join(" "),
              function (event) {
                _this.debug.log("event: ".concat(event.type));
              }
            );
          } // Setup interface
          // If embed but not fully supported, build interface now to avoid flash of controls

          if (this.isHTML5 || (this.isEmbed && !this.supported.ui)) {
            ui.build.call(this);
          } // Container listeners

          this.listeners.container(); // Global listeners

          this.listeners.global(); // Setup fullscreen

          this.fullscreen = new Fullscreen(this); // Setup ads if provided

          if (this.config.ads.enabled) {
            this.ads = new Ads(this);
          } // Autoplay if required

          if (this.config.autoplay) {
            this.play();
          } // Seek time will be recorded (in listeners.js) so we can prevent hiding controls for a few seconds after seek

          this.lastSeekTime = 0;
        } // ---------------------------------------
        // API
        // ---------------------------------------

        /**
         * Types and provider helpers
         */

        _createClass(
          Plyr,
          [
            {
              key: "play",

              /**
               * Play the media, or play the advertisement (if they are not blocked)
               */
              value: function play() {
                if (!is$1.function(this.media.play)) {
                  return null;
                } // Return the promise (for HTML5)

                return this.media.play();
              },
              /**
               * Pause the media
               */
            },
            {
              key: "pause",
              value: function pause() {
                if (!this.playing || !is$1.function(this.media.pause)) {
                  return;
                }

                this.media.pause();
              },
              /**
               * Get playing state
               */
            },
            {
              key: "togglePlay",

              /**
               * Toggle playback based on current status
               * @param {boolean} input
               */
              value: function togglePlay(input) {
                // Toggle based on current state if nothing passed
                var toggle = is$1.boolean(input) ? input : !this.playing;

                if (toggle) {
                  this.play();
                } else {
                  this.pause();
                }
              },
              /**
               * Stop playback
               */
            },
            {
              key: "stop",
              value: function stop() {
                if (this.isHTML5) {
                  this.pause();
                  this.restart();
                } else if (is$1.function(this.media.stop)) {
                  this.media.stop();
                }
              },
              /**
               * Restart playback
               */
            },
            {
              key: "restart",
              value: function restart() {
                this.currentTime = 0;
              },
              /**
               * Rewind
               * @param {number} seekTime - how far to rewind in seconds. Defaults to the config.seekTime
               */
            },
            {
              key: "rewind",
              value: function rewind(seekTime) {
                this.currentTime =
                  this.currentTime -
                  (is$1.number(seekTime) ? seekTime : this.config.seekTime);
              },
              /**
               * Fast forward
               * @param {number} seekTime - how far to fast forward in seconds. Defaults to the config.seekTime
               */
            },
            {
              key: "forward",
              value: function forward(seekTime) {
                this.currentTime =
                  this.currentTime +
                  (is$1.number(seekTime) ? seekTime : this.config.seekTime);
              },
              /**
               * Seek to a time
               * @param {number} input - where to seek to in seconds. Defaults to 0 (the start)
               */
            },
            {
              key: "increaseVolume",

              /**
               * Increase volume
               * @param {boolean} step - How much to decrease by (between 0 and 1)
               */
              value: function increaseVolume(step) {
                var volume = this.media.muted ? 0 : this.volume;
                this.volume = volume + (is$1.number(step) ? step : 0);
              },
              /**
               * Decrease volume
               * @param {boolean} step - How much to decrease by (between 0 and 1)
               */
            },
            {
              key: "decreaseVolume",
              value: function decreaseVolume(step) {
                this.increaseVolume(-step);
              },
              /**
               * Set muted state
               * @param {boolean} mute
               */
            },
            {
              key: "toggleCaptions",

              /**
               * Toggle captions
               * @param {boolean} input - Whether to enable captions
               */
              value: function toggleCaptions(input) {
                captions.toggle.call(this, input, false);
              },
              /**
               * Set the caption track by index
               * @param {number} - Caption index
               */
            },
            {
              key: "airplay",

              /**
               * Trigger the airplay dialog
               * TODO: update player with state, support, enabled
               */
              value: function airplay() {
                // Show dialog if supported
                if (support.airplay) {
                  this.media.webkitShowPlaybackTargetPicker();
                }
              },
              /**
               * Toggle the player controls
               * @param {boolean} [toggle] - Whether to show the controls
               */
            },
            {
              key: "toggleControls",
              value: function toggleControls(toggle) {
                // Don't toggle if missing UI support or if it's audio
                if (this.supported.ui && !this.isAudio) {
                  // Get state before change
                  var isHidden = hasClass(
                    this.elements.container,
                    this.config.classNames.hideControls
                  ); // Negate the argument if not undefined since adding the class to hides the controls

                  var force =
                    typeof toggle === "undefined" ? undefined : !toggle; // Apply and get updated state

                  var hiding = toggleClass(
                    this.elements.container,
                    this.config.classNames.hideControls,
                    force
                  ); // Close menu

                  if (
                    hiding &&
                    this.config.controls.includes("settings") &&
                    !is$1.empty(this.config.settings)
                  ) {
                    controls.toggleMenu.call(this, false);
                  } // Trigger event on change

                  if (hiding !== isHidden) {
                    var eventName = hiding ? "controlshidden" : "controlsshown";
                    triggerEvent.call(this, this.media, eventName);
                  }

                  return !hiding;
                }

                return false;
              },
              /**
               * Add event listeners
               * @param {string} event - Event type
               * @param {function} callback - Callback for when event occurs
               */
            },
            {
              key: "on",
              value: function on$$1(event, callback) {
                on.call(this, this.elements.container, event, callback);
              },
              /**
               * Add event listeners once
               * @param {string} event - Event type
               * @param {function} callback - Callback for when event occurs
               */
            },
            {
              key: "once",
              value: function once$$1(event, callback) {
                once.call(this, this.elements.container, event, callback);
              },
              /**
               * Remove event listeners
               * @param {string} event - Event type
               * @param {function} callback - Callback for when event occurs
               */
            },
            {
              key: "off",
              value: function off$$1(event, callback) {
                off(this.elements.container, event, callback);
              },
              /**
               * Destroy an instance
               * Event listeners are removed when elements are removed
               * http://stackoverflow.com/questions/12528049/if-a-dom-element-is-removed-are-its-listeners-also-removed-from-memory
               * @param {function} callback - Callback for when destroy is complete
               * @param {boolean} soft - Whether it's a soft destroy (for source changes etc)
               */
            },
            {
              key: "destroy",
              value: function destroy(callback) {
                var _this2 = this;

                var soft =
                  arguments.length > 1 && arguments[1] !== undefined
                    ? arguments[1]
                    : false;

                if (!this.ready) {
                  return;
                }

                var done = function done() {
                  // Reset overflow (incase destroyed while in fullscreen)
                  document.body.style.overflow = ""; // GC for embed

                  _this2.embed = null; // If it's a soft destroy, make minimal changes

                  if (soft) {
                    if (Object.keys(_this2.elements).length) {
                      // Remove elements
                      removeElement(_this2.elements.buttons.play);
                      removeElement(_this2.elements.captions);
                      removeElement(_this2.elements.controls);
                      removeElement(_this2.elements.wrapper); // Clear for GC

                      _this2.elements.buttons.play = null;
                      _this2.elements.captions = null;
                      _this2.elements.controls = null;
                      _this2.elements.wrapper = null;
                    } // Callback

                    if (is$1.function(callback)) {
                      callback();
                    }
                  } else {
                    // Unbind listeners
                    unbindListeners.call(_this2); // Replace the container with the original element provided

                    replaceElement(
                      _this2.elements.original,
                      _this2.elements.container
                    ); // Event

                    triggerEvent.call(
                      _this2,
                      _this2.elements.original,
                      "destroyed",
                      true
                    ); // Callback

                    if (is$1.function(callback)) {
                      callback.call(_this2.elements.original);
                    } // Reset state

                    _this2.ready = false; // Clear for garbage collection

                    setTimeout(function () {
                      _this2.elements = null;
                      _this2.media = null;
                    }, 200);
                  }
                }; // Stop playback

                this.stop(); // Provider specific stuff

                if (this.isHTML5) {
                  // Clear timeout
                  clearTimeout(this.timers.loading); // Restore native video controls

                  ui.toggleNativeControls.call(this, true); // Clean up

                  done();
                } else if (this.isYouTube) {
                  // Clear timers
                  clearInterval(this.timers.buffering);
                  clearInterval(this.timers.playing); // Destroy YouTube API

                  if (
                    this.embed !== null &&
                    is$1.function(this.embed.destroy)
                  ) {
                    this.embed.destroy();
                  } // Clean up

                  done();
                } else if (this.isVimeo) {
                  // Destroy Vimeo API
                  // then clean up (wait, to prevent postmessage errors)
                  if (this.embed !== null) {
                    this.embed.unload().then(done);
                  } // Vimeo does not always return

                  setTimeout(done, 200);
                }
              },
              /**
               * Check for support for a mime type (HTML5 only)
               * @param {string} type - Mime type
               */
            },
            {
              key: "supports",
              value: function supports(type) {
                return support.mime.call(this, type);
              },
              /**
               * Check for support
               * @param {string} type - Player type (audio/video)
               * @param {string} provider - Provider (html5/youtube/vimeo)
               * @param {bool} inline - Where player has `playsinline` sttribute
               */
            },
            {
              key: "isHTML5",
              get: function get() {
                return Boolean(this.provider === providers.html5);
              },
            },
            {
              key: "isEmbed",
              get: function get() {
                return Boolean(this.isYouTube || this.isVimeo);
              },
            },
            {
              key: "isYouTube",
              get: function get() {
                return Boolean(this.provider === providers.youtube);
              },
            },
            {
              key: "isVimeo",
              get: function get() {
                return Boolean(this.provider === providers.vimeo);
              },
            },
            {
              key: "isVideo",
              get: function get() {
                return Boolean(this.type === types.video);
              },
            },
            {
              key: "isAudio",
              get: function get() {
                return Boolean(this.type === types.audio);
              },
            },
            {
              key: "playing",
              get: function get() {
                return Boolean(this.ready && !this.paused && !this.ended);
              },
              /**
               * Get paused state
               */
            },
            {
              key: "paused",
              get: function get() {
                return Boolean(this.media.paused);
              },
              /**
               * Get stopped state
               */
            },
            {
              key: "stopped",
              get: function get() {
                return Boolean(this.paused && this.currentTime === 0);
              },
              /**
               * Get ended state
               */
            },
            {
              key: "ended",
              get: function get() {
                return Boolean(this.media.ended);
              },
            },
            {
              key: "currentTime",
              set: function set(input) {
                // Bail if media duration isn't available yet
                if (!this.duration) {
                  return;
                } // Validate input

                var inputIsValid = is$1.number(input) && input > 0; // Set

                this.media.currentTime = inputIsValid
                  ? Math.min(input, this.duration)
                  : 0; // Logging

                this.debug.log(
                  "Seeking to ".concat(this.currentTime, " seconds")
                );
              },
              /**
               * Get current time
               */
              get: function get() {
                return Number(this.media.currentTime);
              },
              /**
               * Get buffered
               */
            },
            {
              key: "buffered",
              get: function get() {
                var buffered = this.media.buffered; // YouTube / Vimeo return a float between 0-1

                if (is$1.number(buffered)) {
                  return buffered;
                } // HTML5
                // TODO: Handle buffered chunks of the media
                // (i.e. seek to another section buffers only that section)

                if (buffered && buffered.length && this.duration > 0) {
                  return buffered.end(0) / this.duration;
                }

                return 0;
              },
              /**
               * Get seeking status
               */
            },
            {
              key: "seeking",
              get: function get() {
                return Boolean(this.media.seeking);
              },
              /**
               * Get the duration of the current media
               */
            },
            {
              key: "duration",
              get: function get() {
                // Faux duration set via config
                var fauxDuration = parseFloat(this.config.duration); // Media duration can be NaN or Infinity before the media has loaded

                var realDuration = (this.media || {}).duration;
                var duration =
                  !is$1.number(realDuration) || realDuration === Infinity
                    ? 0
                    : realDuration; // If config duration is funky, use regular duration

                return fauxDuration || duration;
              },
              /**
               * Set the player volume
               * @param {number} value - must be between 0 and 1. Defaults to the value from local storage and config.volume if not set in storage
               */
            },
            {
              key: "volume",
              set: function set(value) {
                var volume = value;
                var max = 1;
                var min = 0;

                if (is$1.string(volume)) {
                  volume = Number(volume);
                } // Load volume from storage if no value specified

                if (!is$1.number(volume)) {
                  volume = this.storage.get("volume");
                } // Use config if all else fails

                if (!is$1.number(volume)) {
                  volume = this.config.volume;
                } // Maximum is volumeMax

                if (volume > max) {
                  volume = max;
                } // Minimum is volumeMin

                if (volume < min) {
                  volume = min;
                } // Update config

                this.config.volume = volume; // Set the player volume

                this.media.volume = volume; // If muted, and we're increasing volume manually, reset muted state

                if (!is$1.empty(value) && this.muted && volume > 0) {
                  this.muted = false;
                }
              },
              /**
               * Get the current player volume
               */
              get: function get() {
                return Number(this.media.volume);
              },
            },
            {
              key: "muted",
              set: function set(mute) {
                var toggle = mute; // Load muted state from storage

                if (!is$1.boolean(toggle)) {
                  toggle = this.storage.get("muted");
                } // Use config if all else fails

                if (!is$1.boolean(toggle)) {
                  toggle = this.config.muted;
                } // Update config

                this.config.muted = toggle; // Set mute on the player

                this.media.muted = toggle;
              },
              /**
               * Get current muted state
               */
              get: function get() {
                return Boolean(this.media.muted);
              },
              /**
               * Check if the media has audio
               */
            },
            {
              key: "hasAudio",
              get: function get() {
                // Assume yes for all non HTML5 (as we can't tell...)
                if (!this.isHTML5) {
                  return true;
                }

                if (this.isAudio) {
                  return true;
                } // Get audio tracks

                return (
                  Boolean(this.media.mozHasAudio) ||
                  Boolean(this.media.webkitAudioDecodedByteCount) ||
                  Boolean(
                    this.media.audioTracks && this.media.audioTracks.length
                  )
                );
              },
              /**
               * Set playback speed
               * @param {number} speed - the speed of playback (0.5-2.0)
               */
            },
            {
              key: "speed",
              set: function set(input) {
                var speed = null;

                if (is$1.number(input)) {
                  speed = input;
                }

                if (!is$1.number(speed)) {
                  speed = this.storage.get("speed");
                }

                if (!is$1.number(speed)) {
                  speed = this.config.speed.selected;
                } // Set min/max

                if (speed < 0.1) {
                  speed = 0.1;
                }

                if (speed > 2.0) {
                  speed = 2.0;
                }

                if (!this.config.speed.options.includes(speed)) {
                  this.debug.warn("Unsupported speed (".concat(speed, ")"));
                  return;
                } // Update config

                this.config.speed.selected = speed; // Set media speed

                this.media.playbackRate = speed;
              },
              /**
               * Get current playback speed
               */
              get: function get() {
                return Number(this.media.playbackRate);
              },
              /**
               * Set playback quality
               * Currently HTML5 & YouTube only
               * @param {number} input - Quality level
               */
            },
            {
              key: "quality",
              set: function set(input) {
                var config = this.config.quality;
                var options = this.options.quality;

                if (!options.length) {
                  return;
                }

                var quality = [
                  !is$1.empty(input) && Number(input),
                  this.storage.get("quality"),
                  config.selected,
                  config.default,
                ].find(is$1.number);
                var updateStorage = true;

                if (!options.includes(quality)) {
                  var value = closest(options, quality);
                  this.debug.warn(
                    "Unsupported quality option: "
                      .concat(quality, ", using ")
                      .concat(value, " instead")
                  );
                  quality = value; // Don't update storage if quality is not supported

                  updateStorage = false;
                } // Update config

                config.selected = quality; // Set quality

                this.media.quality = quality; // Save to storage

                if (updateStorage) {
                  this.storage.set({
                    quality: quality,
                  });
                }
              },
              /**
               * Get current quality level
               */
              get: function get() {
                return this.media.quality;
              },
              /**
               * Toggle loop
               * TODO: Finish fancy new logic. Set the indicator on load as user may pass loop as config
               * @param {boolean} input - Whether to loop or not
               */
            },
            {
              key: "loop",
              set: function set(input) {
                var toggle = is$1.boolean(input)
                  ? input
                  : this.config.loop.active;
                this.config.loop.active = toggle;
                this.media.loop = toggle; // Set default to be a true toggle

                /* const type = ['start', 'end', 'all', 'none', 'toggle'].includes(input) ? input : 'toggle';
         switch (type) {
            case 'start':
                if (this.config.loop.end && this.config.loop.end <= this.currentTime) {
                    this.config.loop.end = null;
                }
                this.config.loop.start = this.currentTime;
                // this.config.loop.indicator.start = this.elements.display.played.value;
                break;
             case 'end':
                if (this.config.loop.start >= this.currentTime) {
                    return this;
                }
                this.config.loop.end = this.currentTime;
                // this.config.loop.indicator.end = this.elements.display.played.value;
                break;
             case 'all':
                this.config.loop.start = 0;
                this.config.loop.end = this.duration - 2;
                this.config.loop.indicator.start = 0;
                this.config.loop.indicator.end = 100;
                break;
             case 'toggle':
                if (this.config.loop.active) {
                    this.config.loop.start = 0;
                    this.config.loop.end = null;
                } else {
                    this.config.loop.start = 0;
                    this.config.loop.end = this.duration - 2;
                }
                break;
             default:
                this.config.loop.start = 0;
                this.config.loop.end = null;
                break;
        } */
              },
              /**
               * Get current loop state
               */
              get: function get() {
                return Boolean(this.media.loop);
              },
              /**
               * Set new media source
               * @param {object} input - The new source object (see docs)
               */
            },
            {
              key: "source",
              set: function set(input) {
                source.change.call(this, input);
              },
              /**
               * Get current source
               */
              get: function get() {
                return this.media.currentSrc;
              },
              /**
               * Get a download URL (either source or custom)
               */
            },
            {
              key: "download",
              get: function get() {
                var download = this.config.urls.download;
                return is$1.url(download) ? download : this.source;
              },
              /**
               * Set the poster image for a video
               * @param {input} - the URL for the new poster image
               */
            },
            {
              key: "poster",
              set: function set(input) {
                if (!this.isVideo) {
                  this.debug.warn("Poster can only be set for video");
                  return;
                }

                ui.setPoster.call(this, input, false).catch(function () {});
              },
              /**
               * Get the current poster image
               */
              get: function get() {
                if (!this.isVideo) {
                  return null;
                }

                return this.media.getAttribute("poster");
              },
              /**
               * Set the autoplay state
               * @param {boolean} input - Whether to autoplay or not
               */
            },
            {
              key: "autoplay",
              set: function set(input) {
                var toggle = is$1.boolean(input) ? input : this.config.autoplay;
                this.config.autoplay = toggle;
              },
              /**
               * Get the current autoplay state
               */
              get: function get() {
                return Boolean(this.config.autoplay);
              },
            },
            {
              key: "currentTrack",
              set: function set(input) {
                captions.set.call(this, input, false);
              },
              /**
               * Get the current caption track index (-1 if disabled)
               */
              get: function get() {
                var _this$captions = this.captions,
                  toggled = _this$captions.toggled,
                  currentTrack = _this$captions.currentTrack;
                return toggled ? currentTrack : -1;
              },
              /**
               * Set the wanted language for captions
               * Since tracks can be added later it won't update the actual caption track until there is a matching track
               * @param {string} - Two character ISO language code (e.g. EN, FR, PT, etc)
               */
            },
            {
              key: "language",
              set: function set(input) {
                captions.setLanguage.call(this, input, false);
              },
              /**
               * Get the current track's language
               */
              get: function get() {
                return (captions.getCurrentTrack.call(this) || {}).language;
              },
              /**
               * Toggle picture-in-picture playback on WebKit/MacOS
               * TODO: update player with state, support, enabled
               * TODO: detect outside changes
               */
            },
            {
              key: "pip",
              set: function set(input) {
                // Bail if no support
                if (!support.pip) {
                  return;
                } // Toggle based on current state if not passed

                var toggle = is$1.boolean(input) ? input : !this.pip; // Toggle based on current state
                // Safari

                if (is$1.function(this.media.webkitSetPresentationMode)) {
                  this.media.webkitSetPresentationMode(
                    toggle ? pip.active : pip.inactive
                  );
                } // Chrome

                if (is$1.function(this.media.requestPictureInPicture)) {
                  if (!this.pip && toggle) {
                    this.media.requestPictureInPicture();
                  } else if (this.pip && !toggle) {
                    document.exitPictureInPicture();
                  }
                }
              },
              /**
               * Get the current picture-in-picture state
               */
              get: function get() {
                if (!support.pip) {
                  return null;
                } // Safari

                if (!is$1.empty(this.media.webkitPresentationMode)) {
                  return this.media.webkitPresentationMode === pip.active;
                } // Chrome

                return this.media === document.pictureInPictureElement;
              },
            },
          ],
          [
            {
              key: "supported",
              value: function supported(type, provider, inline) {
                return support.check(type, provider, inline);
              },
              /**
               * Load an SVG sprite into the page
               * @param {string} url - URL for the SVG sprite
               * @param {string} [id] - Unique ID
               */
            },
            {
              key: "loadSprite",
              value: function loadSprite$$1(url, id) {
                return loadSprite(url, id);
              },
              /**
               * Setup multiple instances
               * @param {*} selector
               * @param {object} options
               */
            },
            {
              key: "setup",
              value: function setup(selector) {
                var options =
                  arguments.length > 1 && arguments[1] !== undefined
                    ? arguments[1]
                    : {};
                var targets = null;

                if (is$1.string(selector)) {
                  targets = Array.from(document.querySelectorAll(selector));
                } else if (is$1.nodeList(selector)) {
                  targets = Array.from(selector);
                } else if (is$1.array(selector)) {
                  targets = selector.filter(is$1.element);
                }

                if (is$1.empty(targets)) {
                  return null;
                }

                return targets.map(function (t) {
                  return new Plyr(t, options);
                });
              },
            },
          ]
        );

        return Plyr;
      })();

    Plyr.defaults = cloneDeep(defaults);

    // ==========================================================================

    return Plyr;
  });

//# sourceMappingURL=plyr.polyfilled.js.map
/*!
 * Select2 4.0.6-rc.1
 * https://select2.github.io
 *
 * Released under the MIT license
 * https://github.com/select2/select2/blob/master/LICENSE.md
 */
(function (factory) {
  if (typeof define === "function" && define.amd) {
    // AMD. Register as an anonymous module.
    define(["jquery"], factory);
  } else if (typeof module === "object" && module.exports) {
    // Node/CommonJS
    module.exports = function (root, jQuery) {
      if (jQuery === undefined) {
        // require('jQuery') returns a factory that requires window to
        // build a jQuery instance, we normalize how we use modules
        // that require this pattern but the window provided is a noop
        // if it's defined (how jquery works)
        if (typeof window !== "undefined") {
          jQuery = require("jquery");
        } else {
          jQuery = require("jquery")(root);
        }
      }
      factory(jQuery);
      return jQuery;
    };
  } else {
    // Browser globals
    factory(jQuery);
  }
})(function (jQuery) {
  // This is needed so we can catch the AMD loader configuration and use it
  // The inner file should be wrapped (by `banner.start.js`) in a function that
  // returns the AMD loader references.
  var S2 = (function () {
    // Restore the Select2 AMD loader so it can be used
    // Needed mostly in the language files, where the loader is not inserted
    if (jQuery && jQuery.fn && jQuery.fn.select2 && jQuery.fn.select2.amd) {
      var S2 = jQuery.fn.select2.amd;
    }
    var S2;
    (function () {
      if (!S2 || !S2.requirejs) {
        if (!S2) {
          S2 = {};
        } else {
          require = S2;
        }
        /**
         * @license almond 0.3.3 Copyright jQuery Foundation and other contributors.
         * Released under MIT license, http://github.com/requirejs/almond/LICENSE
         */
        //Going sloppy to avoid 'use strict' string cost, but strict practices should
        //be followed.
        /*global setTimeout: false */

        var requirejs, require, define;
        (function (undef) {
          var main,
            req,
            makeMap,
            handlers,
            defined = {},
            waiting = {},
            config = {},
            defining = {},
            hasOwn = Object.prototype.hasOwnProperty,
            aps = [].slice,
            jsSuffixRegExp = /\.js$/;

          function hasProp(obj, prop) {
            return hasOwn.call(obj, prop);
          }

          /**
           * Given a relative module name, like ./something, normalize it to
           * a real name that can be mapped to a path.
           * @param {String} name the relative name
           * @param {String} baseName a real name that the name arg is relative
           * to.
           * @returns {String} normalized name
           */
          function normalize(name, baseName) {
            var nameParts,
              nameSegment,
              mapValue,
              foundMap,
              lastIndex,
              foundI,
              foundStarMap,
              starI,
              i,
              j,
              part,
              normalizedBaseParts,
              baseParts = baseName && baseName.split("/"),
              map = config.map,
              starMap = (map && map["*"]) || {};

            //Adjust any relative paths.
            if (name) {
              name = name.split("/");
              lastIndex = name.length - 1;

              // If wanting node ID compatibility, strip .js from end
              // of IDs. Have to do this here, and not in nameToUrl
              // because node allows either .js or non .js to map
              // to same file.
              if (config.nodeIdCompat && jsSuffixRegExp.test(name[lastIndex])) {
                name[lastIndex] = name[lastIndex].replace(jsSuffixRegExp, "");
              }

              // Starts with a '.' so need the baseName
              if (name[0].charAt(0) === "." && baseParts) {
                //Convert baseName to array, and lop off the last part,
                //so that . matches that 'directory' and not name of the baseName's
                //module. For instance, baseName of 'one/two/three', maps to
                //'one/two/three.js', but we want the directory, 'one/two' for
                //this normalization.
                normalizedBaseParts = baseParts.slice(0, baseParts.length - 1);
                name = normalizedBaseParts.concat(name);
              }

              //start trimDots
              for (i = 0; i < name.length; i++) {
                part = name[i];
                if (part === ".") {
                  name.splice(i, 1);
                  i -= 1;
                } else if (part === "..") {
                  // If at the start, or previous value is still ..,
                  // keep them so that when converted to a path it may
                  // still work when converted to a path, even though
                  // as an ID it is less than ideal. In larger point
                  // releases, may be better to just kick out an error.
                  if (
                    i === 0 ||
                    (i === 1 && name[2] === "..") ||
                    name[i - 1] === ".."
                  ) {
                    continue;
                  } else if (i > 0) {
                    name.splice(i - 1, 2);
                    i -= 2;
                  }
                }
              }
              //end trimDots

              name = name.join("/");
            }

            //Apply map config if available.
            if ((baseParts || starMap) && map) {
              nameParts = name.split("/");

              for (i = nameParts.length; i > 0; i -= 1) {
                nameSegment = nameParts.slice(0, i).join("/");

                if (baseParts) {
                  //Find the longest baseName segment match in the config.
                  //So, do joins on the biggest to smallest lengths of baseParts.
                  for (j = baseParts.length; j > 0; j -= 1) {
                    mapValue = map[baseParts.slice(0, j).join("/")];

                    //baseName segment has  config, find if it has one for
                    //this name.
                    if (mapValue) {
                      mapValue = mapValue[nameSegment];
                      if (mapValue) {
                        //Match, update name to the new value.
                        foundMap = mapValue;
                        foundI = i;
                        break;
                      }
                    }
                  }
                }

                if (foundMap) {
                  break;
                }

                //Check for a star map match, but just hold on to it,
                //if there is a shorter segment match later in a matching
                //config, then favor over this star map.
                if (!foundStarMap && starMap && starMap[nameSegment]) {
                  foundStarMap = starMap[nameSegment];
                  starI = i;
                }
              }

              if (!foundMap && foundStarMap) {
                foundMap = foundStarMap;
                foundI = starI;
              }

              if (foundMap) {
                nameParts.splice(0, foundI, foundMap);
                name = nameParts.join("/");
              }
            }

            return name;
          }

          function makeRequire(relName, forceSync) {
            return function () {
              //A version of a require function that passes a moduleName
              //value for items that may need to
              //look up paths relative to the moduleName
              var args = aps.call(arguments, 0);

              //If first arg is not require('string'), and there is only
              //one arg, it is the array form without a callback. Insert
              //a null so that the following concat is correct.
              if (typeof args[0] !== "string" && args.length === 1) {
                args.push(null);
              }
              return req.apply(undef, args.concat([relName, forceSync]));
            };
          }

          function makeNormalize(relName) {
            return function (name) {
              return normalize(name, relName);
            };
          }

          function makeLoad(depName) {
            return function (value) {
              defined[depName] = value;
            };
          }

          function callDep(name) {
            if (hasProp(waiting, name)) {
              var args = waiting[name];
              delete waiting[name];
              defining[name] = true;
              main.apply(undef, args);
            }

            if (!hasProp(defined, name) && !hasProp(defining, name)) {
              throw new Error("No " + name);
            }
            return defined[name];
          }

          //Turns a plugin!resource to [plugin, resource]
          //with the plugin being undefined if the name
          //did not have a plugin prefix.
          function splitPrefix(name) {
            var prefix,
              index = name ? name.indexOf("!") : -1;
            if (index > -1) {
              prefix = name.substring(0, index);
              name = name.substring(index + 1, name.length);
            }
            return [prefix, name];
          }

          //Creates a parts array for a relName where first part is plugin ID,
          //second part is resource ID. Assumes relName has already been normalized.
          function makeRelParts(relName) {
            return relName ? splitPrefix(relName) : [];
          }

          /**
           * Makes a name map, normalizing the name, and using a plugin
           * for normalization if necessary. Grabs a ref to plugin
           * too, as an optimization.
           */
          makeMap = function (name, relParts) {
            var plugin,
              parts = splitPrefix(name),
              prefix = parts[0],
              relResourceName = relParts[1];

            name = parts[1];

            if (prefix) {
              prefix = normalize(prefix, relResourceName);
              plugin = callDep(prefix);
            }

            //Normalize according
            if (prefix) {
              if (plugin && plugin.normalize) {
                name = plugin.normalize(name, makeNormalize(relResourceName));
              } else {
                name = normalize(name, relResourceName);
              }
            } else {
              name = normalize(name, relResourceName);
              parts = splitPrefix(name);
              prefix = parts[0];
              name = parts[1];
              if (prefix) {
                plugin = callDep(prefix);
              }
            }

            //Using ridiculous property names for space reasons
            return {
              f: prefix ? prefix + "!" + name : name, //fullName
              n: name,
              pr: prefix,
              p: plugin,
            };
          };

          function makeConfig(name) {
            return function () {
              return (config && config.config && config.config[name]) || {};
            };
          }

          handlers = {
            require: function (name) {
              return makeRequire(name);
            },
            exports: function (name) {
              var e = defined[name];
              if (typeof e !== "undefined") {
                return e;
              } else {
                return (defined[name] = {});
              }
            },
            module: function (name) {
              return {
                id: name,
                uri: "",
                exports: defined[name],
                config: makeConfig(name),
              };
            },
          };

          main = function (name, deps, callback, relName) {
            var cjsModule,
              depName,
              ret,
              map,
              i,
              relParts,
              args = [],
              callbackType = typeof callback,
              usingExports;

            //Use name if no relName
            relName = relName || name;
            relParts = makeRelParts(relName);

            //Call the callback to define the module, if necessary.
            if (callbackType === "undefined" || callbackType === "function") {
              //Pull out the defined dependencies and pass the ordered
              //values to the callback.
              //Default to [require, exports, module] if no deps
              deps =
                !deps.length && callback.length
                  ? ["require", "exports", "module"]
                  : deps;
              for (i = 0; i < deps.length; i += 1) {
                map = makeMap(deps[i], relParts);
                depName = map.f;

                //Fast path CommonJS standard dependencies.
                if (depName === "require") {
                  args[i] = handlers.require(name);
                } else if (depName === "exports") {
                  //CommonJS module spec 1.1
                  args[i] = handlers.exports(name);
                  usingExports = true;
                } else if (depName === "module") {
                  //CommonJS module spec 1.1
                  cjsModule = args[i] = handlers.module(name);
                } else if (
                  hasProp(defined, depName) ||
                  hasProp(waiting, depName) ||
                  hasProp(defining, depName)
                ) {
                  args[i] = callDep(depName);
                } else if (map.p) {
                  map.p.load(
                    map.n,
                    makeRequire(relName, true),
                    makeLoad(depName),
                    {}
                  );
                  args[i] = defined[depName];
                } else {
                  throw new Error(name + " missing " + depName);
                }
              }

              ret = callback ? callback.apply(defined[name], args) : undefined;

              if (name) {
                //If setting exports via "module" is in play,
                //favor that over return value and exports. After that,
                //favor a non-undefined return value over exports use.
                if (
                  cjsModule &&
                  cjsModule.exports !== undef &&
                  cjsModule.exports !== defined[name]
                ) {
                  defined[name] = cjsModule.exports;
                } else if (ret !== undef || !usingExports) {
                  //Use the return value from the function.
                  defined[name] = ret;
                }
              }
            } else if (name) {
              //May just be an object definition for the module. Only
              //worry about defining if have a module name.
              defined[name] = callback;
            }
          };

          requirejs = require = req = function (
            deps,
            callback,
            relName,
            forceSync,
            alt
          ) {
            if (typeof deps === "string") {
              if (handlers[deps]) {
                //callback in this case is really relName
                return handlers[deps](callback);
              }
              //Just return the module wanted. In this scenario, the
              //deps arg is the module name, and second arg (if passed)
              //is just the relName.
              //Normalize module name, if it contains . or ..
              return callDep(makeMap(deps, makeRelParts(callback)).f);
            } else if (!deps.splice) {
              //deps is a config object, not an array.
              config = deps;
              if (config.deps) {
                req(config.deps, config.callback);
              }
              if (!callback) {
                return;
              }

              if (callback.splice) {
                //callback is an array, which means it is a dependency list.
                //Adjust args if there are dependencies
                deps = callback;
                callback = relName;
                relName = null;
              } else {
                deps = undef;
              }
            }

            //Support require(['a'])
            callback = callback || function () {};

            //If relName is a function, it is an errback handler,
            //so remove it.
            if (typeof relName === "function") {
              relName = forceSync;
              forceSync = alt;
            }

            //Simulate async callback;
            if (forceSync) {
              main(undef, deps, callback, relName);
            } else {
              //Using a non-zero value because of concern for what old browsers
              //do, and latest browsers "upgrade" to 4 if lower value is used:
              //http://www.whatwg.org/specs/web-apps/current-work/multipage/timers.html#dom-windowtimers-settimeout:
              //If want a value immediately, use require('id') instead -- something
              //that works in almond on the global level, but not guaranteed and
              //unlikely to work in other AMD implementations.
              setTimeout(function () {
                main(undef, deps, callback, relName);
              }, 4);
            }

            return req;
          };

          /**
           * Just drops the config on the floor, but returns req in case
           * the config return value is used.
           */
          req.config = function (cfg) {
            return req(cfg);
          };

          /**
           * Expose module registry for debugging and tooling
           */
          requirejs._defined = defined;

          define = function (name, deps, callback) {
            if (typeof name !== "string") {
              throw new Error(
                "See almond README: incorrect module build, no module name"
              );
            }

            //This module may not have dependencies
            if (!deps.splice) {
              //deps is not an array, so probably means
              //an object literal or factory function for
              //the value. Adjust args.
              callback = deps;
              deps = [];
            }

            if (!hasProp(defined, name) && !hasProp(waiting, name)) {
              waiting[name] = [name, deps, callback];
            }
          };

          define.amd = {
            jQuery: true,
          };
        })();

        S2.requirejs = requirejs;
        S2.require = require;
        S2.define = define;
      }
    })();
    S2.define("almond", function () {});

    /* global jQuery:false, $:false */
    S2.define("jquery", [], function () {
      var _$ = jQuery || $;

      if (_$ == null && console && console.error) {
        console.error(
          "Select2: An instance of jQuery or a jQuery-compatible library was not " +
            "found. Make sure that you are including jQuery before Select2 on your " +
            "web page."
        );
      }

      return _$;
    });

    S2.define("select2/utils", ["jquery"], function ($) {
      var Utils = {};

      Utils.Extend = function (ChildClass, SuperClass) {
        var __hasProp = {}.hasOwnProperty;

        function BaseConstructor() {
          this.constructor = ChildClass;
        }

        for (var key in SuperClass) {
          if (__hasProp.call(SuperClass, key)) {
            ChildClass[key] = SuperClass[key];
          }
        }

        BaseConstructor.prototype = SuperClass.prototype;
        ChildClass.prototype = new BaseConstructor();
        ChildClass.__super__ = SuperClass.prototype;

        return ChildClass;
      };

      function getMethods(theClass) {
        var proto = theClass.prototype;

        var methods = [];

        for (var methodName in proto) {
          var m = proto[methodName];

          if (typeof m !== "function") {
            continue;
          }

          if (methodName === "constructor") {
            continue;
          }

          methods.push(methodName);
        }

        return methods;
      }

      Utils.Decorate = function (SuperClass, DecoratorClass) {
        var decoratedMethods = getMethods(DecoratorClass);
        var superMethods = getMethods(SuperClass);

        function DecoratedClass() {
          var unshift = Array.prototype.unshift;

          var argCount = DecoratorClass.prototype.constructor.length;

          var calledConstructor = SuperClass.prototype.constructor;

          if (argCount > 0) {
            unshift.call(arguments, SuperClass.prototype.constructor);

            calledConstructor = DecoratorClass.prototype.constructor;
          }

          calledConstructor.apply(this, arguments);
        }

        DecoratorClass.displayName = SuperClass.displayName;

        function ctr() {
          this.constructor = DecoratedClass;
        }

        DecoratedClass.prototype = new ctr();

        for (var m = 0; m < superMethods.length; m++) {
          var superMethod = superMethods[m];

          DecoratedClass.prototype[superMethod] =
            SuperClass.prototype[superMethod];
        }

        var calledMethod = function (methodName) {
          // Stub out the original method if it's not decorating an actual method
          var originalMethod = function () {};

          if (methodName in DecoratedClass.prototype) {
            originalMethod = DecoratedClass.prototype[methodName];
          }

          var decoratedMethod = DecoratorClass.prototype[methodName];

          return function () {
            var unshift = Array.prototype.unshift;

            unshift.call(arguments, originalMethod);

            return decoratedMethod.apply(this, arguments);
          };
        };

        for (var d = 0; d < decoratedMethods.length; d++) {
          var decoratedMethod = decoratedMethods[d];

          DecoratedClass.prototype[decoratedMethod] = calledMethod(
            decoratedMethod
          );
        }

        return DecoratedClass;
      };

      var Observable = function () {
        this.listeners = {};
      };

      Observable.prototype.on = function (event, callback) {
        this.listeners = this.listeners || {};

        if (event in this.listeners) {
          this.listeners[event].push(callback);
        } else {
          this.listeners[event] = [callback];
        }
      };

      Observable.prototype.trigger = function (event) {
        var slice = Array.prototype.slice;
        var params = slice.call(arguments, 1);

        this.listeners = this.listeners || {};

        // Params should always come in as an array
        if (params == null) {
          params = [];
        }

        // If there are no arguments to the event, use a temporary object
        if (params.length === 0) {
          params.push({});
        }

        // Set the `_type` of the first object to the event
        params[0]._type = event;

        if (event in this.listeners) {
          this.invoke(this.listeners[event], slice.call(arguments, 1));
        }

        if ("*" in this.listeners) {
          this.invoke(this.listeners["*"], arguments);
        }
      };

      Observable.prototype.invoke = function (listeners, params) {
        for (var i = 0, len = listeners.length; i < len; i++) {
          listeners[i].apply(this, params);
        }
      };

      Utils.Observable = Observable;

      Utils.generateChars = function (length) {
        var chars = "";

        for (var i = 0; i < length; i++) {
          var randomChar = Math.floor(Math.random() * 36);
          chars += randomChar.toString(36);
        }

        return chars;
      };

      Utils.bind = function (func, context) {
        return function () {
          func.apply(context, arguments);
        };
      };

      Utils._convertData = function (data) {
        for (var originalKey in data) {
          var keys = originalKey.split("-");

          var dataLevel = data;

          if (keys.length === 1) {
            continue;
          }

          for (var k = 0; k < keys.length; k++) {
            var key = keys[k];

            // Lowercase the first letter
            // By default, dash-separated becomes camelCase
            key = key.substring(0, 1).toLowerCase() + key.substring(1);

            if (!(key in dataLevel)) {
              dataLevel[key] = {};
            }

            if (k == keys.length - 1) {
              dataLevel[key] = data[originalKey];
            }

            dataLevel = dataLevel[key];
          }

          delete data[originalKey];
        }

        return data;
      };

      Utils.hasScroll = function (index, el) {
        // Adapted from the function created by @ShadowScripter
        // and adapted by @BillBarry on the Stack Exchange Code Review website.
        // The original code can be found at
        // http://codereview.stackexchange.com/q/13338
        // and was designed to be used with the Sizzle selector engine.

        var $el = $(el);
        var overflowX = el.style.overflowX;
        var overflowY = el.style.overflowY;

        //Check both x and y declarations
        if (
          overflowX === overflowY &&
          (overflowY === "hidden" || overflowY === "visible")
        ) {
          return false;
        }

        if (overflowX === "scroll" || overflowY === "scroll") {
          return true;
        }

        return (
          $el.innerHeight() < el.scrollHeight ||
          $el.innerWidth() < el.scrollWidth
        );
      };

      Utils.escapeMarkup = function (markup) {
        var replaceMap = {
          "\\": "&#92;",
          "&": "&amp;",
          "<": "&lt;",
          ">": "&gt;",
          '"': "&quot;",
          "'": "&#39;",
          "/": "&#47;",
        };

        // Do not try to escape the markup if it's not a string
        if (typeof markup !== "string") {
          return markup;
        }

        return String(markup).replace(/[&<>"'\/\\]/g, function (match) {
          return replaceMap[match];
        });
      };

      // Append an array of jQuery nodes to a given element.
      Utils.appendMany = function ($element, $nodes) {
        // jQuery 1.7.x does not support $.fn.append() with an array
        // Fall back to a jQuery object collection using $.fn.add()
        if ($.fn.jquery.substr(0, 3) === "1.7") {
          var $jqNodes = $();

          $.map($nodes, function (node) {
            $jqNodes = $jqNodes.add(node);
          });

          $nodes = $jqNodes;
        }

        $element.append($nodes);
      };

      // Cache objects in Utils.__cache instead of $.data (see #4346)
      Utils.__cache = {};

      var id = 0;
      Utils.GetUniqueElementId = function (element) {
        // Get a unique element Id. If element has no id,
        // creates a new unique number, stores it in the id
        // attribute and returns the new id.
        // If an id already exists, it simply returns it.

        var select2Id = element.getAttribute("data-select2-id");
        if (select2Id == null) {
          // If element has id, use it.
          if (element.id) {
            select2Id = element.id;
            element.setAttribute("data-select2-id", select2Id);
          } else {
            element.setAttribute("data-select2-id", ++id);
            select2Id = id.toString();
          }
        }
        return select2Id;
      };

      Utils.StoreData = function (element, name, value) {
        // Stores an item in the cache for a specified element.
        // name is the cache key.
        var id = Utils.GetUniqueElementId(element);
        if (!Utils.__cache[id]) {
          Utils.__cache[id] = {};
        }

        Utils.__cache[id][name] = value;
      };

      Utils.GetData = function (element, name) {
        // Retrieves a value from the cache by its key (name)
        // name is optional. If no name specified, return
        // all cache items for the specified element.
        // and for a specified element.
        var id = Utils.GetUniqueElementId(element);
        if (name) {
          if (Utils.__cache[id]) {
            return Utils.__cache[id][name] != null
              ? Utils.__cache[id][name]
              : $(element).data(name); // Fallback to HTML5 data attribs.
          }
          return $(element).data(name); // Fallback to HTML5 data attribs.
        } else {
          return Utils.__cache[id];
        }
      };

      Utils.RemoveData = function (element) {
        // Removes all cached items for a specified element.
        var id = Utils.GetUniqueElementId(element);
        if (Utils.__cache[id] != null) {
          delete Utils.__cache[id];
        }
      };

      return Utils;
    });

    S2.define("select2/results", ["jquery", "./utils"], function ($, Utils) {
      function Results($element, options, dataAdapter) {
        this.$element = $element;
        this.data = dataAdapter;
        this.options = options;

        Results.__super__.constructor.call(this);
      }

      Utils.Extend(Results, Utils.Observable);

      Results.prototype.render = function () {
        var $results = $(
          '<ul class="select2-results__options" role="tree"></ul>'
        );

        if (this.options.get("multiple")) {
          $results.attr("aria-multiselectable", "true");
        }

        this.$results = $results;

        return $results;
      };

      Results.prototype.clear = function () {
        this.$results.empty();
      };

      Results.prototype.displayMessage = function (params) {
        var escapeMarkup = this.options.get("escapeMarkup");

        this.clear();
        this.hideLoading();

        var $message = $(
          '<li role="treeitem" aria-live="assertive"' +
            ' class="select2-results__option"></li>'
        );

        var message = this.options.get("translations").get(params.message);

        $message.append(escapeMarkup(message(params.args)));

        $message[0].className += " select2-results__message";

        this.$results.append($message);
      };

      Results.prototype.hideMessages = function () {
        this.$results.find(".select2-results__message").remove();
      };

      Results.prototype.append = function (data) {
        this.hideLoading();

        var $options = [];

        if (data.results == null || data.results.length === 0) {
          if (this.$results.children().length === 0) {
            this.trigger("results:message", {
              message: "noResults",
            });
          }

          return;
        }

        data.results = this.sort(data.results);

        for (var d = 0; d < data.results.length; d++) {
          var item = data.results[d];

          var $option = this.option(item);

          $options.push($option);
        }

        this.$results.append($options);
      };

      Results.prototype.position = function ($results, $dropdown) {
        var $resultsContainer = $dropdown.find(".select2-results");
        $resultsContainer.append($results);
      };

      Results.prototype.sort = function (data) {
        var sorter = this.options.get("sorter");

        return sorter(data);
      };

      Results.prototype.highlightFirstItem = function () {
        var $options = this.$results.find(
          ".select2-results__option[aria-selected]"
        );

        var $selected = $options.filter("[aria-selected=true]");

        // Check if there are any selected options
        if ($selected.length > 0) {
          // If there are selected options, highlight the first
          $selected.first().trigger("mouseenter");
        } else {
          // If there are no selected options, highlight the first option
          // in the dropdown
          $options.first().trigger("mouseenter");
        }

        this.ensureHighlightVisible();
      };

      Results.prototype.setClasses = function () {
        var self = this;

        this.data.current(function (selected) {
          var selectedIds = $.map(selected, function (s) {
            return s.id.toString();
          });

          var $options = self.$results.find(
            ".select2-results__option[aria-selected]"
          );

          $options.each(function () {
            var $option = $(this);

            var item = Utils.GetData(this, "data");

            // id needs to be converted to a string when comparing
            var id = "" + item.id;

            if (
              (item.element != null && item.element.selected) ||
              (item.element == null && $.inArray(id, selectedIds) > -1)
            ) {
              $option.attr("aria-selected", "true");
            } else {
              $option.attr("aria-selected", "false");
            }
          });
        });
      };

      Results.prototype.showLoading = function (params) {
        this.hideLoading();

        var loadingMore = this.options.get("translations").get("searching");

        var loading = {
          disabled: true,
          loading: true,
          text: loadingMore(params),
        };
        var $loading = this.option(loading);
        $loading.className += " loading-results";

        this.$results.prepend($loading);
      };

      Results.prototype.hideLoading = function () {
        this.$results.find(".loading-results").remove();
      };

      Results.prototype.option = function (data) {
        var option = document.createElement("li");
        option.className = "select2-results__option";

        var attrs = {
          role: "treeitem",
          "aria-selected": "false",
        };

        if (data.disabled) {
          delete attrs["aria-selected"];
          attrs["aria-disabled"] = "true";
        }

        if (data.id == null) {
          delete attrs["aria-selected"];
        }

        if (data._resultId != null) {
          option.id = data._resultId;
        }

        if (data.title) {
          option.title = data.title;
        }

        if (data.children) {
          attrs.role = "group";
          attrs["aria-label"] = data.text;
          delete attrs["aria-selected"];
        }

        for (var attr in attrs) {
          var val = attrs[attr];

          option.setAttribute(attr, val);
        }

        if (data.children) {
          var $option = $(option);

          var label = document.createElement("strong");
          label.className = "select2-results__group";

          var $label = $(label);
          this.template(data, label);

          var $children = [];

          for (var c = 0; c < data.children.length; c++) {
            var child = data.children[c];

            var $child = this.option(child);

            $children.push($child);
          }

          var $childrenContainer = $("<ul></ul>", {
            class: "select2-results__options select2-results__options--nested",
          });

          $childrenContainer.append($children);

          $option.append(label);
          $option.append($childrenContainer);
        } else {
          this.template(data, option);
        }

        Utils.StoreData(option, "data", data);

        return option;
      };

      Results.prototype.bind = function (container, $container) {
        var self = this;

        var id = container.id + "-results";

        this.$results.attr("id", id);

        container.on("results:all", function (params) {
          self.clear();
          self.append(params.data);

          if (container.isOpen()) {
            self.setClasses();
            self.highlightFirstItem();
          }
        });

        container.on("results:append", function (params) {
          self.append(params.data);

          if (container.isOpen()) {
            self.setClasses();
          }
        });

        container.on("query", function (params) {
          self.hideMessages();
          self.showLoading(params);
        });

        container.on("select", function () {
          if (!container.isOpen()) {
            return;
          }

          self.setClasses();
          self.highlightFirstItem();
        });

        container.on("unselect", function () {
          if (!container.isOpen()) {
            return;
          }

          self.setClasses();
          self.highlightFirstItem();
        });

        container.on("open", function () {
          // When the dropdown is open, aria-expended="true"
          self.$results.attr("aria-expanded", "true");
          self.$results.attr("aria-hidden", "false");

          self.setClasses();
          self.ensureHighlightVisible();
        });

        container.on("close", function () {
          // When the dropdown is closed, aria-expended="false"
          self.$results.attr("aria-expanded", "false");
          self.$results.attr("aria-hidden", "true");
          self.$results.removeAttr("aria-activedescendant");
        });

        container.on("results:toggle", function () {
          var $highlighted = self.getHighlightedResults();

          if ($highlighted.length === 0) {
            return;
          }

          $highlighted.trigger("mouseup");
        });

        container.on("results:select", function () {
          var $highlighted = self.getHighlightedResults();

          if ($highlighted.length === 0) {
            return;
          }

          var data = Utils.GetData($highlighted[0], "data");

          if ($highlighted.attr("aria-selected") == "true") {
            self.trigger("close", {});
          } else {
            self.trigger("select", {
              data: data,
            });
          }
        });

        container.on("results:previous", function () {
          var $highlighted = self.getHighlightedResults();

          var $options = self.$results.find("[aria-selected]");

          var currentIndex = $options.index($highlighted);

          // If we are already at te top, don't move further
          // If no options, currentIndex will be -1
          if (currentIndex <= 0) {
            return;
          }

          var nextIndex = currentIndex - 1;

          // If none are highlighted, highlight the first
          if ($highlighted.length === 0) {
            nextIndex = 0;
          }

          var $next = $options.eq(nextIndex);

          $next.trigger("mouseenter");

          var currentOffset = self.$results.offset().top;
          var nextTop = $next.offset().top;
          var nextOffset =
            self.$results.scrollTop() + (nextTop - currentOffset);

          if (nextIndex === 0) {
            self.$results.scrollTop(0);
          } else if (nextTop - currentOffset < 0) {
            self.$results.scrollTop(nextOffset);
          }
        });

        container.on("results:next", function () {
          var $highlighted = self.getHighlightedResults();

          var $options = self.$results.find("[aria-selected]");

          var currentIndex = $options.index($highlighted);

          var nextIndex = currentIndex + 1;

          // If we are at the last option, stay there
          if (nextIndex >= $options.length) {
            return;
          }

          var $next = $options.eq(nextIndex);

          $next.trigger("mouseenter");

          var currentOffset =
            self.$results.offset().top + self.$results.outerHeight(false);
          var nextBottom = $next.offset().top + $next.outerHeight(false);
          var nextOffset =
            self.$results.scrollTop() + nextBottom - currentOffset;

          if (nextIndex === 0) {
            self.$results.scrollTop(0);
          } else if (nextBottom > currentOffset) {
            self.$results.scrollTop(nextOffset);
          }
        });

        container.on("results:focus", function (params) {
          params.element.addClass("select2-results__option--highlighted");
        });

        container.on("results:message", function (params) {
          self.displayMessage(params);
        });

        if ($.fn.mousewheel) {
          this.$results.on("mousewheel", function (e) {
            var top = self.$results.scrollTop();

            var bottom = self.$results.get(0).scrollHeight - top + e.deltaY;

            var isAtTop = e.deltaY > 0 && top - e.deltaY <= 0;
            var isAtBottom = e.deltaY < 0 && bottom <= self.$results.height();

            if (isAtTop) {
              self.$results.scrollTop(0);

              e.preventDefault();
              e.stopPropagation();
            } else if (isAtBottom) {
              self.$results.scrollTop(
                self.$results.get(0).scrollHeight - self.$results.height()
              );

              e.preventDefault();
              e.stopPropagation();
            }
          });
        }

        this.$results.on(
          "mouseup",
          ".select2-results__option[aria-selected]",
          function (evt) {
            var $this = $(this);

            var data = Utils.GetData(this, "data");

            if ($this.attr("aria-selected") === "true") {
              if (self.options.get("multiple")) {
                self.trigger("unselect", {
                  originalEvent: evt,
                  data: data,
                });
              } else {
                self.trigger("close", {});
              }

              return;
            }

            self.trigger("select", {
              originalEvent: evt,
              data: data,
            });
          }
        );

        this.$results.on(
          "mouseenter",
          ".select2-results__option[aria-selected]",
          function (evt) {
            var data = Utils.GetData(this, "data");

            self
              .getHighlightedResults()
              .removeClass("select2-results__option--highlighted");

            self.trigger("results:focus", {
              data: data,
              element: $(this),
            });
          }
        );
      };

      Results.prototype.getHighlightedResults = function () {
        var $highlighted = this.$results.find(
          ".select2-results__option--highlighted"
        );

        return $highlighted;
      };

      Results.prototype.destroy = function () {
        this.$results.remove();
      };

      Results.prototype.ensureHighlightVisible = function () {
        var $highlighted = this.getHighlightedResults();

        if ($highlighted.length === 0) {
          return;
        }

        var $options = this.$results.find("[aria-selected]");

        var currentIndex = $options.index($highlighted);

        var currentOffset = this.$results.offset().top;
        var nextTop = $highlighted.offset().top;
        var nextOffset = this.$results.scrollTop() + (nextTop - currentOffset);

        var offsetDelta = nextTop - currentOffset;
        nextOffset -= $highlighted.outerHeight(false) * 2;

        if (currentIndex <= 2) {
          this.$results.scrollTop(0);
        } else if (
          offsetDelta > this.$results.outerHeight() ||
          offsetDelta < 0
        ) {
          this.$results.scrollTop(nextOffset);
        }
      };

      Results.prototype.template = function (result, container) {
        var template = this.options.get("templateResult");
        var escapeMarkup = this.options.get("escapeMarkup");

        var content = template(result, container);

        if (content == null) {
          container.style.display = "none";
        } else if (typeof content === "string") {
          container.innerHTML = escapeMarkup(content);
        } else {
          $(container).append(content);
        }
      };

      return Results;
    });

    S2.define("select2/keys", [], function () {
      var KEYS = {
        BACKSPACE: 8,
        TAB: 9,
        ENTER: 13,
        SHIFT: 16,
        CTRL: 17,
        ALT: 18,
        ESC: 27,
        SPACE: 32,
        PAGE_UP: 33,
        PAGE_DOWN: 34,
        END: 35,
        HOME: 36,
        LEFT: 37,
        UP: 38,
        RIGHT: 39,
        DOWN: 40,
        DELETE: 46,
      };

      return KEYS;
    });

    S2.define(
      "select2/selection/base",
      ["jquery", "../utils", "../keys"],
      function ($, Utils, KEYS) {
        function BaseSelection($element, options) {
          this.$element = $element;
          this.options = options;

          BaseSelection.__super__.constructor.call(this);
        }

        Utils.Extend(BaseSelection, Utils.Observable);

        BaseSelection.prototype.render = function () {
          var $selection = $(
            '<span class="select2-selection" role="combobox" ' +
              ' aria-haspopup="true" aria-expanded="false">' +
              "</span>"
          );

          this._tabindex = 0;

          if (Utils.GetData(this.$element[0], "old-tabindex") != null) {
            this._tabindex = Utils.GetData(this.$element[0], "old-tabindex");
          } else if (this.$element.attr("tabindex") != null) {
            this._tabindex = this.$element.attr("tabindex");
          }

          $selection.attr("title", this.$element.attr("title"));
          $selection.attr("tabindex", this._tabindex);

          this.$selection = $selection;

          return $selection;
        };

        BaseSelection.prototype.bind = function (container, $container) {
          var self = this;

          var id = container.id + "-container";
          var resultsId = container.id + "-results";

          this.container = container;

          this.$selection.on("focus", function (evt) {
            self.trigger("focus", evt);
          });

          this.$selection.on("blur", function (evt) {
            self._handleBlur(evt);
          });

          this.$selection.on("keydown", function (evt) {
            self.trigger("keypress", evt);

            if (evt.which === KEYS.SPACE) {
              evt.preventDefault();
            }
          });

          container.on("results:focus", function (params) {
            self.$selection.attr(
              "aria-activedescendant",
              params.data._resultId
            );
          });

          container.on("selection:update", function (params) {
            self.update(params.data);
          });

          container.on("open", function () {
            // When the dropdown is open, aria-expanded="true"
            self.$selection.attr("aria-expanded", "true");
            self.$selection.attr("aria-owns", resultsId);

            self._attachCloseHandler(container);
          });

          container.on("close", function () {
            // When the dropdown is closed, aria-expanded="false"
            self.$selection.attr("aria-expanded", "false");
            self.$selection.removeAttr("aria-activedescendant");
            self.$selection.removeAttr("aria-owns");

            self.$selection.focus();
            window.setTimeout(function () {
              self.$selection.focus();
            }, 0);

            self._detachCloseHandler(container);
          });

          container.on("enable", function () {
            self.$selection.attr("tabindex", self._tabindex);
          });

          container.on("disable", function () {
            self.$selection.attr("tabindex", "-1");
          });
        };

        BaseSelection.prototype._handleBlur = function (evt) {
          var self = this;

          // This needs to be delayed as the active element is the body when the tab
          // key is pressed, possibly along with others.
          window.setTimeout(function () {
            // Don't trigger `blur` if the focus is still in the selection
            if (
              document.activeElement == self.$selection[0] ||
              $.contains(self.$selection[0], document.activeElement)
            ) {
              return;
            }

            self.trigger("blur", evt);
          }, 1);
        };

        BaseSelection.prototype._attachCloseHandler = function (container) {
          var self = this;

          $(document.body).on("mousedown.select2." + container.id, function (
            e
          ) {
            var $target = $(e.target);

            var $select = $target.closest(".select2");

            var $all = $(".select2.select2-container--open");

            $all.each(function () {
              var $this = $(this);

              if (this == $select[0]) {
                return;
              }

              var $element = Utils.GetData(this, "element");

              $element.select2("close");
            });
          });
        };

        BaseSelection.prototype._detachCloseHandler = function (container) {
          $(document.body).off("mousedown.select2." + container.id);
        };

        BaseSelection.prototype.position = function ($selection, $container) {
          var $selectionContainer = $container.find(".selection");
          $selectionContainer.append($selection);
        };

        BaseSelection.prototype.destroy = function () {
          this._detachCloseHandler(this.container);
        };

        BaseSelection.prototype.update = function (data) {
          throw new Error(
            "The `update` method must be defined in child classes."
          );
        };

        return BaseSelection;
      }
    );

    S2.define(
      "select2/selection/single",
      ["jquery", "./base", "../utils", "../keys"],
      function ($, BaseSelection, Utils, KEYS) {
        function SingleSelection() {
          SingleSelection.__super__.constructor.apply(this, arguments);
        }

        Utils.Extend(SingleSelection, BaseSelection);

        SingleSelection.prototype.render = function () {
          var $selection = SingleSelection.__super__.render.call(this);

          $selection.addClass("select2-selection--single");

          $selection.html(
            '<span class="select2-selection__rendered"></span>' +
              '<span class="select2-selection__arrow" role="presentation">' +
              '<b role="presentation"></b>' +
              "</span>"
          );

          return $selection;
        };

        SingleSelection.prototype.bind = function (container, $container) {
          var self = this;

          SingleSelection.__super__.bind.apply(this, arguments);

          var id = container.id + "-container";

          this.$selection
            .find(".select2-selection__rendered")
            .attr("id", id)
            .attr("role", "textbox")
            .attr("aria-readonly", "true");
          this.$selection.attr("aria-labelledby", id);

          this.$selection.on("mousedown", function (evt) {
            // Only respond to left clicks
            if (evt.which !== 1) {
              return;
            }

            self.trigger("toggle", {
              originalEvent: evt,
            });
          });

          this.$selection.on("focus", function (evt) {
            // User focuses on the container
          });

          this.$selection.on("blur", function (evt) {
            // User exits the container
          });

          container.on("focus", function (evt) {
            if (!container.isOpen()) {
              self.$selection.focus();
            }
          });
        };

        SingleSelection.prototype.clear = function () {
          var $rendered = this.$selection.find(".select2-selection__rendered");
          $rendered.empty();
          $rendered.removeAttr("title"); // clear tooltip on empty
        };

        SingleSelection.prototype.display = function (data, container) {
          var template = this.options.get("templateSelection");
          var escapeMarkup = this.options.get("escapeMarkup");

          return escapeMarkup(template(data, container));
        };

        SingleSelection.prototype.selectionContainer = function () {
          return $("<span></span>");
        };

        SingleSelection.prototype.update = function (data) {
          if (data.length === 0) {
            this.clear();
            return;
          }

          var selection = data[0];

          var $rendered = this.$selection.find(".select2-selection__rendered");
          var formatted = this.display(selection, $rendered);

          $rendered.empty().append(formatted);
          $rendered.attr("title", selection.title || selection.text);
        };

        return SingleSelection;
      }
    );

    S2.define(
      "select2/selection/multiple",
      ["jquery", "./base", "../utils"],
      function ($, BaseSelection, Utils) {
        function MultipleSelection($element, options) {
          MultipleSelection.__super__.constructor.apply(this, arguments);
        }

        Utils.Extend(MultipleSelection, BaseSelection);

        MultipleSelection.prototype.render = function () {
          var $selection = MultipleSelection.__super__.render.call(this);

          $selection.addClass("select2-selection--multiple");

          $selection.html('<ul class="select2-selection__rendered"></ul>');

          return $selection;
        };

        MultipleSelection.prototype.bind = function (container, $container) {
          var self = this;

          MultipleSelection.__super__.bind.apply(this, arguments);

          this.$selection.on("click", function (evt) {
            self.trigger("toggle", {
              originalEvent: evt,
            });
          });

          this.$selection.on(
            "click",
            ".select2-selection__choice__remove",
            function (evt) {
              // Ignore the event if it is disabled
              if (self.options.get("disabled")) {
                return;
              }

              var $remove = $(this);
              var $selection = $remove.parent();

              var data = Utils.GetData($selection[0], "data");

              self.trigger("unselect", {
                originalEvent: evt,
                data: data,
              });
            }
          );
        };

        MultipleSelection.prototype.clear = function () {
          var $rendered = this.$selection.find(".select2-selection__rendered");
          $rendered.empty();
          $rendered.removeAttr("title");
        };

        MultipleSelection.prototype.display = function (data, container) {
          var template = this.options.get("templateSelection");
          var escapeMarkup = this.options.get("escapeMarkup");

          return escapeMarkup(template(data, container));
        };

        MultipleSelection.prototype.selectionContainer = function () {
          var $container = $(
            '<li class="select2-selection__choice">' +
              '<span class="select2-selection__choice__remove" role="presentation">' +
              "&times;" +
              "</span>" +
              "</li>"
          );

          return $container;
        };

        MultipleSelection.prototype.update = function (data) {
          this.clear();

          if (data.length === 0) {
            return;
          }

          var $selections = [];

          for (var d = 0; d < data.length; d++) {
            var selection = data[d];

            var $selection = this.selectionContainer();
            var formatted = this.display(selection, $selection);

            $selection.append(formatted);
            $selection.attr("title", selection.title || selection.text);

            Utils.StoreData($selection[0], "data", selection);

            $selections.push($selection);
          }

          var $rendered = this.$selection.find(".select2-selection__rendered");

          Utils.appendMany($rendered, $selections);
        };

        return MultipleSelection;
      }
    );

    S2.define("select2/selection/placeholder", ["../utils"], function (Utils) {
      function Placeholder(decorated, $element, options) {
        this.placeholder = this.normalizePlaceholder(
          options.get("placeholder")
        );

        decorated.call(this, $element, options);
      }

      Placeholder.prototype.normalizePlaceholder = function (_, placeholder) {
        if (typeof placeholder === "string") {
          placeholder = {
            id: "",
            text: placeholder,
          };
        }

        return placeholder;
      };

      Placeholder.prototype.createPlaceholder = function (
        decorated,
        placeholder
      ) {
        var $placeholder = this.selectionContainer();

        $placeholder.html(this.display(placeholder));
        $placeholder
          .addClass("select2-selection__placeholder")
          .removeClass("select2-selection__choice");

        return $placeholder;
      };

      Placeholder.prototype.update = function (decorated, data) {
        var singlePlaceholder =
          data.length == 1 && data[0].id != this.placeholder.id;
        var multipleSelections = data.length > 1;

        if (multipleSelections || singlePlaceholder) {
          return decorated.call(this, data);
        }

        this.clear();

        var $placeholder = this.createPlaceholder(this.placeholder);

        this.$selection
          .find(".select2-selection__rendered")
          .append($placeholder);
      };

      return Placeholder;
    });

    S2.define(
      "select2/selection/allowClear",
      ["jquery", "../keys", "../utils"],
      function ($, KEYS, Utils) {
        function AllowClear() {}

        AllowClear.prototype.bind = function (
          decorated,
          container,
          $container
        ) {
          var self = this;

          decorated.call(this, container, $container);

          if (this.placeholder == null) {
            if (this.options.get("debug") && window.console && console.error) {
              console.error(
                "Select2: The `allowClear` option should be used in combination " +
                  "with the `placeholder` option."
              );
            }
          }

          this.$selection.on(
            "mousedown",
            ".select2-selection__clear",
            function (evt) {
              self._handleClear(evt);
            }
          );

          container.on("keypress", function (evt) {
            self._handleKeyboardClear(evt, container);
          });
        };

        AllowClear.prototype._handleClear = function (_, evt) {
          // Ignore the event if it is disabled
          if (this.options.get("disabled")) {
            return;
          }

          var $clear = this.$selection.find(".select2-selection__clear");

          // Ignore the event if nothing has been selected
          if ($clear.length === 0) {
            return;
          }

          evt.stopPropagation();

          var data = Utils.GetData($clear[0], "data");

          var previousVal = this.$element.val();
          this.$element.val(this.placeholder.id);

          var unselectData = {
            data: data,
          };
          this.trigger("clear", unselectData);
          if (unselectData.prevented) {
            this.$element.val(previousVal);
            return;
          }

          for (var d = 0; d < data.length; d++) {
            unselectData = {
              data: data[d],
            };

            // Trigger the `unselect` event, so people can prevent it from being
            // cleared.
            this.trigger("unselect", unselectData);

            // If the event was prevented, don't clear it out.
            if (unselectData.prevented) {
              this.$element.val(previousVal);
              return;
            }
          }

          this.$element.trigger("change");

          this.trigger("toggle", {});
        };

        AllowClear.prototype._handleKeyboardClear = function (
          _,
          evt,
          container
        ) {
          if (container.isOpen()) {
            return;
          }

          if (evt.which == KEYS.DELETE || evt.which == KEYS.BACKSPACE) {
            this._handleClear(evt);
          }
        };

        AllowClear.prototype.update = function (decorated, data) {
          decorated.call(this, data);

          if (
            this.$selection.find(".select2-selection__placeholder").length >
              0 ||
            data.length === 0
          ) {
            return;
          }

          var $remove = $(
            '<span class="select2-selection__clear">' + "&times;" + "</span>"
          );
          Utils.StoreData($remove[0], "data", data);

          this.$selection.find(".select2-selection__rendered").prepend($remove);
        };

        return AllowClear;
      }
    );

    S2.define(
      "select2/selection/search",
      ["jquery", "../utils", "../keys"],
      function ($, Utils, KEYS) {
        function Search(decorated, $element, options) {
          decorated.call(this, $element, options);
        }

        Search.prototype.render = function (decorated) {
          var $search = $(
            '<li class="select2-search select2-search--inline">' +
              '<input class="select2-search__field" type="search" tabindex="-1"' +
              ' autocomplete="off" autocorrect="off" autocapitalize="none"' +
              ' spellcheck="false" role="textbox" aria-autocomplete="list" />' +
              "</li>"
          );

          this.$searchContainer = $search;
          this.$search = $search.find("input");

          var $rendered = decorated.call(this);

          this._transferTabIndex();

          return $rendered;
        };

        Search.prototype.bind = function (decorated, container, $container) {
          var self = this;

          decorated.call(this, container, $container);

          container.on("open", function () {
            self.$search.trigger("focus");
          });

          container.on("close", function () {
            self.$search.val("");
            self.$search.removeAttr("aria-activedescendant");
            self.$search.trigger("focus");
          });

          container.on("enable", function () {
            self.$search.prop("disabled", false);

            self._transferTabIndex();
          });

          container.on("disable", function () {
            self.$search.prop("disabled", true);
          });

          container.on("focus", function (evt) {
            self.$search.trigger("focus");
          });

          container.on("results:focus", function (params) {
            self.$search.attr("aria-activedescendant", params.id);
          });

          this.$selection.on("focusin", ".select2-search--inline", function (
            evt
          ) {
            self.trigger("focus", evt);
          });

          this.$selection.on("focusout", ".select2-search--inline", function (
            evt
          ) {
            self._handleBlur(evt);
          });

          this.$selection.on("keydown", ".select2-search--inline", function (
            evt
          ) {
            evt.stopPropagation();

            self.trigger("keypress", evt);

            self._keyUpPrevented = evt.isDefaultPrevented();

            var key = evt.which;

            if (key === KEYS.BACKSPACE && self.$search.val() === "") {
              var $previousChoice = self.$searchContainer.prev(
                ".select2-selection__choice"
              );

              if ($previousChoice.length > 0) {
                var item = Utils.GetData($previousChoice[0], "data");

                self.searchRemoveChoice(item);

                evt.preventDefault();
              }
            }
          });

          // Try to detect the IE version should the `documentMode` property that
          // is stored on the document. This is only implemented in IE and is
          // slightly cleaner than doing a user agent check.
          // This property is not available in Edge, but Edge also doesn't have
          // this bug.
          var msie = document.documentMode;
          var disableInputEvents = msie && msie <= 11;

          // Workaround for browsers which do not support the `input` event
          // This will prevent double-triggering of events for browsers which support
          // both the `keyup` and `input` events.
          this.$selection.on(
            "input.searchcheck",
            ".select2-search--inline",
            function (evt) {
              // IE will trigger the `input` event when a placeholder is used on a
              // search box. To get around this issue, we are forced to ignore all
              // `input` events in IE and keep using `keyup`.
              if (disableInputEvents) {
                self.$selection.off("input.search input.searchcheck");
                return;
              }

              // Unbind the duplicated `keyup` event
              self.$selection.off("keyup.search");
            }
          );

          this.$selection.on(
            "keyup.search input.search",
            ".select2-search--inline",
            function (evt) {
              // IE will trigger the `input` event when a placeholder is used on a
              // search box. To get around this issue, we are forced to ignore all
              // `input` events in IE and keep using `keyup`.
              if (disableInputEvents && evt.type === "input") {
                self.$selection.off("input.search input.searchcheck");
                return;
              }

              var key = evt.which;

              // We can freely ignore events from modifier keys
              if (key == KEYS.SHIFT || key == KEYS.CTRL || key == KEYS.ALT) {
                return;
              }

              // Tabbing will be handled during the `keydown` phase
              if (key == KEYS.TAB) {
                return;
              }

              self.handleSearch(evt);
            }
          );
        };

        /**
         * This method will transfer the tabindex attribute from the rendered
         * selection to the search box. This allows for the search box to be used as
         * the primary focus instead of the selection container.
         *
         * @private
         */
        Search.prototype._transferTabIndex = function (decorated) {
          this.$search.attr("tabindex", this.$selection.attr("tabindex"));
          this.$selection.attr("tabindex", "-1");
        };

        Search.prototype.createPlaceholder = function (decorated, placeholder) {
          this.$search.attr("placeholder", placeholder.text);
        };

        Search.prototype.update = function (decorated, data) {
          var searchHadFocus = this.$search[0] == document.activeElement;

          this.$search.attr("placeholder", "");

          decorated.call(this, data);

          this.$selection
            .find(".select2-selection__rendered")
            .append(this.$searchContainer);

          this.resizeSearch();
          if (searchHadFocus) {
            var isTagInput = this.$element.find("[data-select2-tag]").length;
            if (isTagInput) {
              // fix IE11 bug where tag input lost focus
              this.$element.focus();
            } else {
              this.$search.focus();
            }
          }
        };

        Search.prototype.handleSearch = function () {
          this.resizeSearch();

          if (!this._keyUpPrevented) {
            var input = this.$search.val();

            this.trigger("query", {
              term: input,
            });
          }

          this._keyUpPrevented = false;
        };

        Search.prototype.searchRemoveChoice = function (decorated, item) {
          this.trigger("unselect", {
            data: item,
          });

          this.$search.val(item.text);
          this.handleSearch();
        };

        Search.prototype.resizeSearch = function () {
          this.$search.css("width", "25px");

          var width = "";

          if (this.$search.attr("placeholder") !== "") {
            width = this.$selection
              .find(".select2-selection__rendered")
              .innerWidth();
          } else {
            var minimumWidth = this.$search.val().length + 1;

            width = minimumWidth * 0.75 + "em";
          }

          this.$search.css("width", width);
        };

        return Search;
      }
    );

    S2.define("select2/selection/eventRelay", ["jquery"], function ($) {
      function EventRelay() {}

      EventRelay.prototype.bind = function (decorated, container, $container) {
        var self = this;
        var relayEvents = [
          "open",
          "opening",
          "close",
          "closing",
          "select",
          "selecting",
          "unselect",
          "unselecting",
          "clear",
          "clearing",
        ];

        var preventableEvents = [
          "opening",
          "closing",
          "selecting",
          "unselecting",
          "clearing",
        ];

        decorated.call(this, container, $container);

        container.on("*", function (name, params) {
          // Ignore events that should not be relayed
          if ($.inArray(name, relayEvents) === -1) {
            return;
          }

          // The parameters should always be an object
          params = params || {};

          // Generate the jQuery event for the Select2 event
          var evt = $.Event("select2:" + name, {
            params: params,
          });

          self.$element.trigger(evt);

          // Only handle preventable events if it was one
          if ($.inArray(name, preventableEvents) === -1) {
            return;
          }

          params.prevented = evt.isDefaultPrevented();
        });
      };

      return EventRelay;
    });

    S2.define("select2/translation", ["jquery", "require"], function (
      $,
      require
    ) {
      function Translation(dict) {
        this.dict = dict || {};
      }

      Translation.prototype.all = function () {
        return this.dict;
      };

      Translation.prototype.get = function (key) {
        return this.dict[key];
      };

      Translation.prototype.extend = function (translation) {
        this.dict = $.extend({}, translation.all(), this.dict);
      };

      // Static functions

      Translation._cache = {};

      Translation.loadPath = function (path) {
        if (!(path in Translation._cache)) {
          var translations = require(path);

          Translation._cache[path] = translations;
        }

        return new Translation(Translation._cache[path]);
      };

      return Translation;
    });

    S2.define("select2/diacritics", [], function () {
      var diacritics = {
        "\u24B6": "A",
        "\uFF21": "A",
        "\u00C0": "A",
        "\u00C1": "A",
        "\u00C2": "A",
        "\u1EA6": "A",
        "\u1EA4": "A",
        "\u1EAA": "A",
        "\u1EA8": "A",
        "\u00C3": "A",
        "\u0100": "A",
        "\u0102": "A",
        "\u1EB0": "A",
        "\u1EAE": "A",
        "\u1EB4": "A",
        "\u1EB2": "A",
        "\u0226": "A",
        "\u01E0": "A",
        "\u00C4": "A",
        "\u01DE": "A",
        "\u1EA2": "A",
        "\u00C5": "A",
        "\u01FA": "A",
        "\u01CD": "A",
        "\u0200": "A",
        "\u0202": "A",
        "\u1EA0": "A",
        "\u1EAC": "A",
        "\u1EB6": "A",
        "\u1E00": "A",
        "\u0104": "A",
        "\u023A": "A",
        "\u2C6F": "A",
        "\uA732": "AA",
        "\u00C6": "AE",
        "\u01FC": "AE",
        "\u01E2": "AE",
        "\uA734": "AO",
        "\uA736": "AU",
        "\uA738": "AV",
        "\uA73A": "AV",
        "\uA73C": "AY",
        "\u24B7": "B",
        "\uFF22": "B",
        "\u1E02": "B",
        "\u1E04": "B",
        "\u1E06": "B",
        "\u0243": "B",
        "\u0182": "B",
        "\u0181": "B",
        "\u24B8": "C",
        "\uFF23": "C",
        "\u0106": "C",
        "\u0108": "C",
        "\u010A": "C",
        "\u010C": "C",
        "\u00C7": "C",
        "\u1E08": "C",
        "\u0187": "C",
        "\u023B": "C",
        "\uA73E": "C",
        "\u24B9": "D",
        "\uFF24": "D",
        "\u1E0A": "D",
        "\u010E": "D",
        "\u1E0C": "D",
        "\u1E10": "D",
        "\u1E12": "D",
        "\u1E0E": "D",
        "\u0110": "D",
        "\u018B": "D",
        "\u018A": "D",
        "\u0189": "D",
        "\uA779": "D",
        "\u01F1": "DZ",
        "\u01C4": "DZ",
        "\u01F2": "Dz",
        "\u01C5": "Dz",
        "\u24BA": "E",
        "\uFF25": "E",
        "\u00C8": "E",
        "\u00C9": "E",
        "\u00CA": "E",
        "\u1EC0": "E",
        "\u1EBE": "E",
        "\u1EC4": "E",
        "\u1EC2": "E",
        "\u1EBC": "E",
        "\u0112": "E",
        "\u1E14": "E",
        "\u1E16": "E",
        "\u0114": "E",
        "\u0116": "E",
        "\u00CB": "E",
        "\u1EBA": "E",
        "\u011A": "E",
        "\u0204": "E",
        "\u0206": "E",
        "\u1EB8": "E",
        "\u1EC6": "E",
        "\u0228": "E",
        "\u1E1C": "E",
        "\u0118": "E",
        "\u1E18": "E",
        "\u1E1A": "E",
        "\u0190": "E",
        "\u018E": "E",
        "\u24BB": "F",
        "\uFF26": "F",
        "\u1E1E": "F",
        "\u0191": "F",
        "\uA77B": "F",
        "\u24BC": "G",
        "\uFF27": "G",
        "\u01F4": "G",
        "\u011C": "G",
        "\u1E20": "G",
        "\u011E": "G",
        "\u0120": "G",
        "\u01E6": "G",
        "\u0122": "G",
        "\u01E4": "G",
        "\u0193": "G",
        "\uA7A0": "G",
        "\uA77D": "G",
        "\uA77E": "G",
        "\u24BD": "H",
        "\uFF28": "H",
        "\u0124": "H",
        "\u1E22": "H",
        "\u1E26": "H",
        "\u021E": "H",
        "\u1E24": "H",
        "\u1E28": "H",
        "\u1E2A": "H",
        "\u0126": "H",
        "\u2C67": "H",
        "\u2C75": "H",
        "\uA78D": "H",
        "\u24BE": "I",
        "\uFF29": "I",
        "\u00CC": "I",
        "\u00CD": "I",
        "\u00CE": "I",
        "\u0128": "I",
        "\u012A": "I",
        "\u012C": "I",
        "\u0130": "I",
        "\u00CF": "I",
        "\u1E2E": "I",
        "\u1EC8": "I",
        "\u01CF": "I",
        "\u0208": "I",
        "\u020A": "I",
        "\u1ECA": "I",
        "\u012E": "I",
        "\u1E2C": "I",
        "\u0197": "I",
        "\u24BF": "J",
        "\uFF2A": "J",
        "\u0134": "J",
        "\u0248": "J",
        "\u24C0": "K",
        "\uFF2B": "K",
        "\u1E30": "K",
        "\u01E8": "K",
        "\u1E32": "K",
        "\u0136": "K",
        "\u1E34": "K",
        "\u0198": "K",
        "\u2C69": "K",
        "\uA740": "K",
        "\uA742": "K",
        "\uA744": "K",
        "\uA7A2": "K",
        "\u24C1": "L",
        "\uFF2C": "L",
        "\u013F": "L",
        "\u0139": "L",
        "\u013D": "L",
        "\u1E36": "L",
        "\u1E38": "L",
        "\u013B": "L",
        "\u1E3C": "L",
        "\u1E3A": "L",
        "\u0141": "L",
        "\u023D": "L",
        "\u2C62": "L",
        "\u2C60": "L",
        "\uA748": "L",
        "\uA746": "L",
        "\uA780": "L",
        "\u01C7": "LJ",
        "\u01C8": "Lj",
        "\u24C2": "M",
        "\uFF2D": "M",
        "\u1E3E": "M",
        "\u1E40": "M",
        "\u1E42": "M",
        "\u2C6E": "M",
        "\u019C": "M",
        "\u24C3": "N",
        "\uFF2E": "N",
        "\u01F8": "N",
        "\u0143": "N",
        "\u00D1": "N",
        "\u1E44": "N",
        "\u0147": "N",
        "\u1E46": "N",
        "\u0145": "N",
        "\u1E4A": "N",
        "\u1E48": "N",
        "\u0220": "N",
        "\u019D": "N",
        "\uA790": "N",
        "\uA7A4": "N",
        "\u01CA": "NJ",
        "\u01CB": "Nj",
        "\u24C4": "O",
        "\uFF2F": "O",
        "\u00D2": "O",
        "\u00D3": "O",
        "\u00D4": "O",
        "\u1ED2": "O",
        "\u1ED0": "O",
        "\u1ED6": "O",
        "\u1ED4": "O",
        "\u00D5": "O",
        "\u1E4C": "O",
        "\u022C": "O",
        "\u1E4E": "O",
        "\u014C": "O",
        "\u1E50": "O",
        "\u1E52": "O",
        "\u014E": "O",
        "\u022E": "O",
        "\u0230": "O",
        "\u00D6": "O",
        "\u022A": "O",
        "\u1ECE": "O",
        "\u0150": "O",
        "\u01D1": "O",
        "\u020C": "O",
        "\u020E": "O",
        "\u01A0": "O",
        "\u1EDC": "O",
        "\u1EDA": "O",
        "\u1EE0": "O",
        "\u1EDE": "O",
        "\u1EE2": "O",
        "\u1ECC": "O",
        "\u1ED8": "O",
        "\u01EA": "O",
        "\u01EC": "O",
        "\u00D8": "O",
        "\u01FE": "O",
        "\u0186": "O",
        "\u019F": "O",
        "\uA74A": "O",
        "\uA74C": "O",
        "\u01A2": "OI",
        "\uA74E": "OO",
        "\u0222": "OU",
        "\u24C5": "P",
        "\uFF30": "P",
        "\u1E54": "P",
        "\u1E56": "P",
        "\u01A4": "P",
        "\u2C63": "P",
        "\uA750": "P",
        "\uA752": "P",
        "\uA754": "P",
        "\u24C6": "Q",
        "\uFF31": "Q",
        "\uA756": "Q",
        "\uA758": "Q",
        "\u024A": "Q",
        "\u24C7": "R",
        "\uFF32": "R",
        "\u0154": "R",
        "\u1E58": "R",
        "\u0158": "R",
        "\u0210": "R",
        "\u0212": "R",
        "\u1E5A": "R",
        "\u1E5C": "R",
        "\u0156": "R",
        "\u1E5E": "R",
        "\u024C": "R",
        "\u2C64": "R",
        "\uA75A": "R",
        "\uA7A6": "R",
        "\uA782": "R",
        "\u24C8": "S",
        "\uFF33": "S",
        "\u1E9E": "S",
        "\u015A": "S",
        "\u1E64": "S",
        "\u015C": "S",
        "\u1E60": "S",
        "\u0160": "S",
        "\u1E66": "S",
        "\u1E62": "S",
        "\u1E68": "S",
        "\u0218": "S",
        "\u015E": "S",
        "\u2C7E": "S",
        "\uA7A8": "S",
        "\uA784": "S",
        "\u24C9": "T",
        "\uFF34": "T",
        "\u1E6A": "T",
        "\u0164": "T",
        "\u1E6C": "T",
        "\u021A": "T",
        "\u0162": "T",
        "\u1E70": "T",
        "\u1E6E": "T",
        "\u0166": "T",
        "\u01AC": "T",
        "\u01AE": "T",
        "\u023E": "T",
        "\uA786": "T",
        "\uA728": "TZ",
        "\u24CA": "U",
        "\uFF35": "U",
        "\u00D9": "U",
        "\u00DA": "U",
        "\u00DB": "U",
        "\u0168": "U",
        "\u1E78": "U",
        "\u016A": "U",
        "\u1E7A": "U",
        "\u016C": "U",
        "\u00DC": "U",
        "\u01DB": "U",
        "\u01D7": "U",
        "\u01D5": "U",
        "\u01D9": "U",
        "\u1EE6": "U",
        "\u016E": "U",
        "\u0170": "U",
        "\u01D3": "U",
        "\u0214": "U",
        "\u0216": "U",
        "\u01AF": "U",
        "\u1EEA": "U",
        "\u1EE8": "U",
        "\u1EEE": "U",
        "\u1EEC": "U",
        "\u1EF0": "U",
        "\u1EE4": "U",
        "\u1E72": "U",
        "\u0172": "U",
        "\u1E76": "U",
        "\u1E74": "U",
        "\u0244": "U",
        "\u24CB": "V",
        "\uFF36": "V",
        "\u1E7C": "V",
        "\u1E7E": "V",
        "\u01B2": "V",
        "\uA75E": "V",
        "\u0245": "V",
        "\uA760": "VY",
        "\u24CC": "W",
        "\uFF37": "W",
        "\u1E80": "W",
        "\u1E82": "W",
        "\u0174": "W",
        "\u1E86": "W",
        "\u1E84": "W",
        "\u1E88": "W",
        "\u2C72": "W",
        "\u24CD": "X",
        "\uFF38": "X",
        "\u1E8A": "X",
        "\u1E8C": "X",
        "\u24CE": "Y",
        "\uFF39": "Y",
        "\u1EF2": "Y",
        "\u00DD": "Y",
        "\u0176": "Y",
        "\u1EF8": "Y",
        "\u0232": "Y",
        "\u1E8E": "Y",
        "\u0178": "Y",
        "\u1EF6": "Y",
        "\u1EF4": "Y",
        "\u01B3": "Y",
        "\u024E": "Y",
        "\u1EFE": "Y",
        "\u24CF": "Z",
        "\uFF3A": "Z",
        "\u0179": "Z",
        "\u1E90": "Z",
        "\u017B": "Z",
        "\u017D": "Z",
        "\u1E92": "Z",
        "\u1E94": "Z",
        "\u01B5": "Z",
        "\u0224": "Z",
        "\u2C7F": "Z",
        "\u2C6B": "Z",
        "\uA762": "Z",
        "\u24D0": "a",
        "\uFF41": "a",
        "\u1E9A": "a",
        "\u00E0": "a",
        "\u00E1": "a",
        "\u00E2": "a",
        "\u1EA7": "a",
        "\u1EA5": "a",
        "\u1EAB": "a",
        "\u1EA9": "a",
        "\u00E3": "a",
        "\u0101": "a",
        "\u0103": "a",
        "\u1EB1": "a",
        "\u1EAF": "a",
        "\u1EB5": "a",
        "\u1EB3": "a",
        "\u0227": "a",
        "\u01E1": "a",
        "\u00E4": "a",
        "\u01DF": "a",
        "\u1EA3": "a",
        "\u00E5": "a",
        "\u01FB": "a",
        "\u01CE": "a",
        "\u0201": "a",
        "\u0203": "a",
        "\u1EA1": "a",
        "\u1EAD": "a",
        "\u1EB7": "a",
        "\u1E01": "a",
        "\u0105": "a",
        "\u2C65": "a",
        "\u0250": "a",
        "\uA733": "aa",
        "\u00E6": "ae",
        "\u01FD": "ae",
        "\u01E3": "ae",
        "\uA735": "ao",
        "\uA737": "au",
        "\uA739": "av",
        "\uA73B": "av",
        "\uA73D": "ay",
        "\u24D1": "b",
        "\uFF42": "b",
        "\u1E03": "b",
        "\u1E05": "b",
        "\u1E07": "b",
        "\u0180": "b",
        "\u0183": "b",
        "\u0253": "b",
        "\u24D2": "c",
        "\uFF43": "c",
        "\u0107": "c",
        "\u0109": "c",
        "\u010B": "c",
        "\u010D": "c",
        "\u00E7": "c",
        "\u1E09": "c",
        "\u0188": "c",
        "\u023C": "c",
        "\uA73F": "c",
        "\u2184": "c",
        "\u24D3": "d",
        "\uFF44": "d",
        "\u1E0B": "d",
        "\u010F": "d",
        "\u1E0D": "d",
        "\u1E11": "d",
        "\u1E13": "d",
        "\u1E0F": "d",
        "\u0111": "d",
        "\u018C": "d",
        "\u0256": "d",
        "\u0257": "d",
        "\uA77A": "d",
        "\u01F3": "dz",
        "\u01C6": "dz",
        "\u24D4": "e",
        "\uFF45": "e",
        "\u00E8": "e",
        "\u00E9": "e",
        "\u00EA": "e",
        "\u1EC1": "e",
        "\u1EBF": "e",
        "\u1EC5": "e",
        "\u1EC3": "e",
        "\u1EBD": "e",
        "\u0113": "e",
        "\u1E15": "e",
        "\u1E17": "e",
        "\u0115": "e",
        "\u0117": "e",
        "\u00EB": "e",
        "\u1EBB": "e",
        "\u011B": "e",
        "\u0205": "e",
        "\u0207": "e",
        "\u1EB9": "e",
        "\u1EC7": "e",
        "\u0229": "e",
        "\u1E1D": "e",
        "\u0119": "e",
        "\u1E19": "e",
        "\u1E1B": "e",
        "\u0247": "e",
        "\u025B": "e",
        "\u01DD": "e",
        "\u24D5": "f",
        "\uFF46": "f",
        "\u1E1F": "f",
        "\u0192": "f",
        "\uA77C": "f",
        "\u24D6": "g",
        "\uFF47": "g",
        "\u01F5": "g",
        "\u011D": "g",
        "\u1E21": "g",
        "\u011F": "g",
        "\u0121": "g",
        "\u01E7": "g",
        "\u0123": "g",
        "\u01E5": "g",
        "\u0260": "g",
        "\uA7A1": "g",
        "\u1D79": "g",
        "\uA77F": "g",
        "\u24D7": "h",
        "\uFF48": "h",
        "\u0125": "h",
        "\u1E23": "h",
        "\u1E27": "h",
        "\u021F": "h",
        "\u1E25": "h",
        "\u1E29": "h",
        "\u1E2B": "h",
        "\u1E96": "h",
        "\u0127": "h",
        "\u2C68": "h",
        "\u2C76": "h",
        "\u0265": "h",
        "\u0195": "hv",
        "\u24D8": "i",
        "\uFF49": "i",
        "\u00EC": "i",
        "\u00ED": "i",
        "\u00EE": "i",
        "\u0129": "i",
        "\u012B": "i",
        "\u012D": "i",
        "\u00EF": "i",
        "\u1E2F": "i",
        "\u1EC9": "i",
        "\u01D0": "i",
        "\u0209": "i",
        "\u020B": "i",
        "\u1ECB": "i",
        "\u012F": "i",
        "\u1E2D": "i",
        "\u0268": "i",
        "\u0131": "i",
        "\u24D9": "j",
        "\uFF4A": "j",
        "\u0135": "j",
        "\u01F0": "j",
        "\u0249": "j",
        "\u24DA": "k",
        "\uFF4B": "k",
        "\u1E31": "k",
        "\u01E9": "k",
        "\u1E33": "k",
        "\u0137": "k",
        "\u1E35": "k",
        "\u0199": "k",
        "\u2C6A": "k",
        "\uA741": "k",
        "\uA743": "k",
        "\uA745": "k",
        "\uA7A3": "k",
        "\u24DB": "l",
        "\uFF4C": "l",
        "\u0140": "l",
        "\u013A": "l",
        "\u013E": "l",
        "\u1E37": "l",
        "\u1E39": "l",
        "\u013C": "l",
        "\u1E3D": "l",
        "\u1E3B": "l",
        "\u017F": "l",
        "\u0142": "l",
        "\u019A": "l",
        "\u026B": "l",
        "\u2C61": "l",
        "\uA749": "l",
        "\uA781": "l",
        "\uA747": "l",
        "\u01C9": "lj",
        "\u24DC": "m",
        "\uFF4D": "m",
        "\u1E3F": "m",
        "\u1E41": "m",
        "\u1E43": "m",
        "\u0271": "m",
        "\u026F": "m",
        "\u24DD": "n",
        "\uFF4E": "n",
        "\u01F9": "n",
        "\u0144": "n",
        "\u00F1": "n",
        "\u1E45": "n",
        "\u0148": "n",
        "\u1E47": "n",
        "\u0146": "n",
        "\u1E4B": "n",
        "\u1E49": "n",
        "\u019E": "n",
        "\u0272": "n",
        "\u0149": "n",
        "\uA791": "n",
        "\uA7A5": "n",
        "\u01CC": "nj",
        "\u24DE": "o",
        "\uFF4F": "o",
        "\u00F2": "o",
        "\u00F3": "o",
        "\u00F4": "o",
        "\u1ED3": "o",
        "\u1ED1": "o",
        "\u1ED7": "o",
        "\u1ED5": "o",
        "\u00F5": "o",
        "\u1E4D": "o",
        "\u022D": "o",
        "\u1E4F": "o",
        "\u014D": "o",
        "\u1E51": "o",
        "\u1E53": "o",
        "\u014F": "o",
        "\u022F": "o",
        "\u0231": "o",
        "\u00F6": "o",
        "\u022B": "o",
        "\u1ECF": "o",
        "\u0151": "o",
        "\u01D2": "o",
        "\u020D": "o",
        "\u020F": "o",
        "\u01A1": "o",
        "\u1EDD": "o",
        "\u1EDB": "o",
        "\u1EE1": "o",
        "\u1EDF": "o",
        "\u1EE3": "o",
        "\u1ECD": "o",
        "\u1ED9": "o",
        "\u01EB": "o",
        "\u01ED": "o",
        "\u00F8": "o",
        "\u01FF": "o",
        "\u0254": "o",
        "\uA74B": "o",
        "\uA74D": "o",
        "\u0275": "o",
        "\u01A3": "oi",
        "\u0223": "ou",
        "\uA74F": "oo",
        "\u24DF": "p",
        "\uFF50": "p",
        "\u1E55": "p",
        "\u1E57": "p",
        "\u01A5": "p",
        "\u1D7D": "p",
        "\uA751": "p",
        "\uA753": "p",
        "\uA755": "p",
        "\u24E0": "q",
        "\uFF51": "q",
        "\u024B": "q",
        "\uA757": "q",
        "\uA759": "q",
        "\u24E1": "r",
        "\uFF52": "r",
        "\u0155": "r",
        "\u1E59": "r",
        "\u0159": "r",
        "\u0211": "r",
        "\u0213": "r",
        "\u1E5B": "r",
        "\u1E5D": "r",
        "\u0157": "r",
        "\u1E5F": "r",
        "\u024D": "r",
        "\u027D": "r",
        "\uA75B": "r",
        "\uA7A7": "r",
        "\uA783": "r",
        "\u24E2": "s",
        "\uFF53": "s",
        "\u00DF": "s",
        "\u015B": "s",
        "\u1E65": "s",
        "\u015D": "s",
        "\u1E61": "s",
        "\u0161": "s",
        "\u1E67": "s",
        "\u1E63": "s",
        "\u1E69": "s",
        "\u0219": "s",
        "\u015F": "s",
        "\u023F": "s",
        "\uA7A9": "s",
        "\uA785": "s",
        "\u1E9B": "s",
        "\u24E3": "t",
        "\uFF54": "t",
        "\u1E6B": "t",
        "\u1E97": "t",
        "\u0165": "t",
        "\u1E6D": "t",
        "\u021B": "t",
        "\u0163": "t",
        "\u1E71": "t",
        "\u1E6F": "t",
        "\u0167": "t",
        "\u01AD": "t",
        "\u0288": "t",
        "\u2C66": "t",
        "\uA787": "t",
        "\uA729": "tz",
        "\u24E4": "u",
        "\uFF55": "u",
        "\u00F9": "u",
        "\u00FA": "u",
        "\u00FB": "u",
        "\u0169": "u",
        "\u1E79": "u",
        "\u016B": "u",
        "\u1E7B": "u",
        "\u016D": "u",
        "\u00FC": "u",
        "\u01DC": "u",
        "\u01D8": "u",
        "\u01D6": "u",
        "\u01DA": "u",
        "\u1EE7": "u",
        "\u016F": "u",
        "\u0171": "u",
        "\u01D4": "u",
        "\u0215": "u",
        "\u0217": "u",
        "\u01B0": "u",
        "\u1EEB": "u",
        "\u1EE9": "u",
        "\u1EEF": "u",
        "\u1EED": "u",
        "\u1EF1": "u",
        "\u1EE5": "u",
        "\u1E73": "u",
        "\u0173": "u",
        "\u1E77": "u",
        "\u1E75": "u",
        "\u0289": "u",
        "\u24E5": "v",
        "\uFF56": "v",
        "\u1E7D": "v",
        "\u1E7F": "v",
        "\u028B": "v",
        "\uA75F": "v",
        "\u028C": "v",
        "\uA761": "vy",
        "\u24E6": "w",
        "\uFF57": "w",
        "\u1E81": "w",
        "\u1E83": "w",
        "\u0175": "w",
        "\u1E87": "w",
        "\u1E85": "w",
        "\u1E98": "w",
        "\u1E89": "w",
        "\u2C73": "w",
        "\u24E7": "x",
        "\uFF58": "x",
        "\u1E8B": "x",
        "\u1E8D": "x",
        "\u24E8": "y",
        "\uFF59": "y",
        "\u1EF3": "y",
        "\u00FD": "y",
        "\u0177": "y",
        "\u1EF9": "y",
        "\u0233": "y",
        "\u1E8F": "y",
        "\u00FF": "y",
        "\u1EF7": "y",
        "\u1E99": "y",
        "\u1EF5": "y",
        "\u01B4": "y",
        "\u024F": "y",
        "\u1EFF": "y",
        "\u24E9": "z",
        "\uFF5A": "z",
        "\u017A": "z",
        "\u1E91": "z",
        "\u017C": "z",
        "\u017E": "z",
        "\u1E93": "z",
        "\u1E95": "z",
        "\u01B6": "z",
        "\u0225": "z",
        "\u0240": "z",
        "\u2C6C": "z",
        "\uA763": "z",
        "\u0386": "\u0391",
        "\u0388": "\u0395",
        "\u0389": "\u0397",
        "\u038A": "\u0399",
        "\u03AA": "\u0399",
        "\u038C": "\u039F",
        "\u038E": "\u03A5",
        "\u03AB": "\u03A5",
        "\u038F": "\u03A9",
        "\u03AC": "\u03B1",
        "\u03AD": "\u03B5",
        "\u03AE": "\u03B7",
        "\u03AF": "\u03B9",
        "\u03CA": "\u03B9",
        "\u0390": "\u03B9",
        "\u03CC": "\u03BF",
        "\u03CD": "\u03C5",
        "\u03CB": "\u03C5",
        "\u03B0": "\u03C5",
        "\u03C9": "\u03C9",
        "\u03C2": "\u03C3",
      };

      return diacritics;
    });

    S2.define("select2/data/base", ["../utils"], function (Utils) {
      function BaseAdapter($element, options) {
        BaseAdapter.__super__.constructor.call(this);
      }

      Utils.Extend(BaseAdapter, Utils.Observable);

      BaseAdapter.prototype.current = function (callback) {
        throw new Error(
          "The `current` method must be defined in child classes."
        );
      };

      BaseAdapter.prototype.query = function (params, callback) {
        throw new Error("The `query` method must be defined in child classes.");
      };

      BaseAdapter.prototype.bind = function (container, $container) {
        // Can be implemented in subclasses
      };

      BaseAdapter.prototype.destroy = function () {
        // Can be implemented in subclasses
      };

      BaseAdapter.prototype.generateResultId = function (container, data) {
        var id = container.id + "-result-";

        id += Utils.generateChars(4);

        if (data.id != null) {
          id += "-" + data.id.toString();
        } else {
          id += "-" + Utils.generateChars(4);
        }
        return id;
      };

      return BaseAdapter;
    });

    S2.define(
      "select2/data/select",
      ["./base", "../utils", "jquery"],
      function (BaseAdapter, Utils, $) {
        function SelectAdapter($element, options) {
          this.$element = $element;
          this.options = options;

          SelectAdapter.__super__.constructor.call(this);
        }

        Utils.Extend(SelectAdapter, BaseAdapter);

        SelectAdapter.prototype.current = function (callback) {
          var data = [];
          var self = this;

          this.$element.find(":selected").each(function () {
            var $option = $(this);

            var option = self.item($option);

            data.push(option);
          });

          callback(data);
        };

        SelectAdapter.prototype.select = function (data) {
          var self = this;

          data.selected = true;

          // If data.element is a DOM node, use it instead
          if ($(data.element).is("option")) {
            data.element.selected = true;

            this.$element.trigger("change");

            return;
          }

          if (this.$element.prop("multiple")) {
            this.current(function (currentData) {
              var val = [];

              data = [data];
              data.push.apply(data, currentData);

              for (var d = 0; d < data.length; d++) {
                var id = data[d].id;

                if ($.inArray(id, val) === -1) {
                  val.push(id);
                }
              }

              self.$element.val(val);
              self.$element.trigger("change");
            });
          } else {
            var val = data.id;

            this.$element.val(val);
            this.$element.trigger("change");
          }
        };

        SelectAdapter.prototype.unselect = function (data) {
          var self = this;

          if (!this.$element.prop("multiple")) {
            return;
          }

          data.selected = false;

          if ($(data.element).is("option")) {
            data.element.selected = false;

            this.$element.trigger("change");

            return;
          }

          this.current(function (currentData) {
            var val = [];

            for (var d = 0; d < currentData.length; d++) {
              var id = currentData[d].id;

              if (id !== data.id && $.inArray(id, val) === -1) {
                val.push(id);
              }
            }

            self.$element.val(val);

            self.$element.trigger("change");
          });
        };

        SelectAdapter.prototype.bind = function (container, $container) {
          var self = this;

          this.container = container;

          container.on("select", function (params) {
            self.select(params.data);
          });

          container.on("unselect", function (params) {
            self.unselect(params.data);
          });
        };

        SelectAdapter.prototype.destroy = function () {
          // Remove anything added to child elements
          this.$element.find("*").each(function () {
            // Remove any custom data set by Select2
            Utils.RemoveData(this);
          });
        };

        SelectAdapter.prototype.query = function (params, callback) {
          var data = [];
          var self = this;

          var $options = this.$element.children();

          $options.each(function () {
            var $option = $(this);

            if (!$option.is("option") && !$option.is("optgroup")) {
              return;
            }

            var option = self.item($option);

            var matches = self.matches(params, option);

            if (matches !== null) {
              data.push(matches);
            }
          });

          callback({
            results: data,
          });
        };

        SelectAdapter.prototype.addOptions = function ($options) {
          Utils.appendMany(this.$element, $options);
        };

        SelectAdapter.prototype.option = function (data) {
          var option;

          if (data.children) {
            option = document.createElement("optgroup");
            option.label = data.text;
          } else {
            option = document.createElement("option");

            if (option.textContent !== undefined) {
              option.textContent = data.text;
            } else {
              option.innerText = data.text;
            }
          }

          if (data.id !== undefined) {
            option.value = data.id;
          }

          if (data.disabled) {
            option.disabled = true;
          }

          if (data.selected) {
            option.selected = true;
          }

          if (data.title) {
            option.title = data.title;
          }

          var $option = $(option);

          var normalizedData = this._normalizeItem(data);
          normalizedData.element = option;

          // Override the option's data with the combined data
          Utils.StoreData(option, "data", normalizedData);

          return $option;
        };

        SelectAdapter.prototype.item = function ($option) {
          var data = {};

          data = Utils.GetData($option[0], "data");

          if (data != null) {
            return data;
          }

          if ($option.is("option")) {
            data = {
              id: $option.val(),
              text: $option.text(),
              disabled: $option.prop("disabled"),
              selected: $option.prop("selected"),
              title: $option.prop("title"),
            };
          } else if ($option.is("optgroup")) {
            data = {
              text: $option.prop("label"),
              children: [],
              title: $option.prop("title"),
            };

            var $children = $option.children("option");
            var children = [];

            for (var c = 0; c < $children.length; c++) {
              var $child = $($children[c]);

              var child = this.item($child);

              children.push(child);
            }

            data.children = children;
          }

          data = this._normalizeItem(data);
          data.element = $option[0];

          Utils.StoreData($option[0], "data", data);

          return data;
        };

        SelectAdapter.prototype._normalizeItem = function (item) {
          if (item !== Object(item)) {
            item = {
              id: item,
              text: item,
            };
          }

          item = $.extend(
            {},
            {
              text: "",
            },
            item
          );

          var defaults = {
            selected: false,
            disabled: false,
          };

          if (item.id != null) {
            item.id = item.id.toString();
          }

          if (item.text != null) {
            item.text = item.text.toString();
          }

          if (item._resultId == null && item.id && this.container != null) {
            item._resultId = this.generateResultId(this.container, item);
          }

          return $.extend({}, defaults, item);
        };

        SelectAdapter.prototype.matches = function (params, data) {
          var matcher = this.options.get("matcher");

          return matcher(params, data);
        };

        return SelectAdapter;
      }
    );

    S2.define(
      "select2/data/array",
      ["./select", "../utils", "jquery"],
      function (SelectAdapter, Utils, $) {
        function ArrayAdapter($element, options) {
          var data = options.get("data") || [];

          ArrayAdapter.__super__.constructor.call(this, $element, options);

          this.addOptions(this.convertToOptions(data));
        }

        Utils.Extend(ArrayAdapter, SelectAdapter);

        ArrayAdapter.prototype.select = function (data) {
          var $option = this.$element.find("option").filter(function (i, elm) {
            return elm.value == data.id.toString();
          });

          if ($option.length === 0) {
            $option = this.option(data);

            this.addOptions($option);
          }

          ArrayAdapter.__super__.select.call(this, data);
        };

        ArrayAdapter.prototype.convertToOptions = function (data) {
          var self = this;

          var $existing = this.$element.find("option");
          var existingIds = $existing
            .map(function () {
              return self.item($(this)).id;
            })
            .get();

          var $options = [];

          // Filter out all items except for the one passed in the argument
          function onlyItem(item) {
            return function () {
              return $(this).val() == item.id;
            };
          }

          for (var d = 0; d < data.length; d++) {
            var item = this._normalizeItem(data[d]);

            // Skip items which were pre-loaded, only merge the data
            if ($.inArray(item.id, existingIds) >= 0) {
              var $existingOption = $existing.filter(onlyItem(item));

              var existingData = this.item($existingOption);
              var newData = $.extend(true, {}, item, existingData);

              var $newOption = this.option(newData);

              $existingOption.replaceWith($newOption);

              continue;
            }

            var $option = this.option(item);

            if (item.children) {
              var $children = this.convertToOptions(item.children);

              Utils.appendMany($option, $children);
            }

            $options.push($option);
          }

          return $options;
        };

        return ArrayAdapter;
      }
    );

    S2.define("select2/data/ajax", ["./array", "../utils", "jquery"], function (
      ArrayAdapter,
      Utils,
      $
    ) {
      function AjaxAdapter($element, options) {
        this.ajaxOptions = this._applyDefaults(options.get("ajax"));

        if (this.ajaxOptions.processResults != null) {
          this.processResults = this.ajaxOptions.processResults;
        }

        AjaxAdapter.__super__.constructor.call(this, $element, options);
      }

      Utils.Extend(AjaxAdapter, ArrayAdapter);

      AjaxAdapter.prototype._applyDefaults = function (options) {
        var defaults = {
          data: function (params) {
            return $.extend({}, params, {
              q: params.term,
            });
          },
          transport: function (params, success, failure) {
            var $request = $.ajax(params);

            $request.then(success);
            $request.fail(failure);

            return $request;
          },
        };

        return $.extend({}, defaults, options, true);
      };

      AjaxAdapter.prototype.processResults = function (results) {
        return results;
      };

      AjaxAdapter.prototype.query = function (params, callback) {
        var matches = [];
        var self = this;

        if (this._request != null) {
          // JSONP requests cannot always be aborted
          if ($.isFunction(this._request.abort)) {
            this._request.abort();
          }

          this._request = null;
        }

        var options = $.extend(
          {
            type: "GET",
          },
          this.ajaxOptions
        );

        if (typeof options.url === "function") {
          options.url = options.url.call(this.$element, params);
        }

        if (typeof options.data === "function") {
          options.data = options.data.call(this.$element, params);
        }

        function request() {
          var $request = options.transport(
            options,
            function (data) {
              var results = self.processResults(data, params);

              if (
                self.options.get("debug") &&
                window.console &&
                console.error
              ) {
                // Check to make sure that the response included a `results` key.
                if (
                  !results ||
                  !results.results ||
                  !$.isArray(results.results)
                ) {
                  console.error(
                    "Select2: The AJAX results did not return an array in the " +
                      "`results` key of the response."
                  );
                }
              }

              callback(results);
            },
            function () {
              // Attempt to detect if a request was aborted
              // Only works if the transport exposes a status property
              if (
                "status" in $request &&
                ($request.status === 0 || $request.status === "0")
              ) {
                return;
              }

              self.trigger("results:message", {
                message: "errorLoading",
              });
            }
          );

          self._request = $request;
        }

        if (this.ajaxOptions.delay && params.term != null) {
          if (this._queryTimeout) {
            window.clearTimeout(this._queryTimeout);
          }

          this._queryTimeout = window.setTimeout(
            request,
            this.ajaxOptions.delay
          );
        } else {
          request();
        }
      };

      return AjaxAdapter;
    });

    S2.define("select2/data/tags", ["jquery"], function ($) {
      function Tags(decorated, $element, options) {
        var tags = options.get("tags");

        var createTag = options.get("createTag");

        if (createTag !== undefined) {
          this.createTag = createTag;
        }

        var insertTag = options.get("insertTag");

        if (insertTag !== undefined) {
          this.insertTag = insertTag;
        }

        decorated.call(this, $element, options);

        if ($.isArray(tags)) {
          for (var t = 0; t < tags.length; t++) {
            var tag = tags[t];
            var item = this._normalizeItem(tag);

            var $option = this.option(item);

            this.$element.append($option);
          }
        }
      }

      Tags.prototype.query = function (decorated, params, callback) {
        var self = this;

        this._removeOldTags();

        if (params.term == null || params.page != null) {
          decorated.call(this, params, callback);
          return;
        }

        function wrapper(obj, child) {
          var data = obj.results;

          for (var i = 0; i < data.length; i++) {
            var option = data[i];

            var checkChildren =
              option.children != null &&
              !wrapper(
                {
                  results: option.children,
                },
                true
              );

            var optionText = (option.text || "").toUpperCase();
            var paramsTerm = (params.term || "").toUpperCase();

            var checkText = optionText === paramsTerm;

            if (checkText || checkChildren) {
              if (child) {
                return false;
              }

              obj.data = data;
              callback(obj);

              return;
            }
          }

          if (child) {
            return true;
          }

          var tag = self.createTag(params);

          if (tag != null) {
            var $option = self.option(tag);
            $option.attr("data-select2-tag", true);

            self.addOptions([$option]);

            self.insertTag(data, tag);
          }

          obj.results = data;

          callback(obj);
        }

        decorated.call(this, params, wrapper);
      };

      Tags.prototype.createTag = function (decorated, params) {
        var term = $.trim(params.term);

        if (term === "") {
          return null;
        }

        return {
          id: term,
          text: term,
        };
      };

      Tags.prototype.insertTag = function (_, data, tag) {
        data.unshift(tag);
      };

      Tags.prototype._removeOldTags = function (_) {
        var tag = this._lastTag;

        var $options = this.$element.find("option[data-select2-tag]");

        $options.each(function () {
          if (this.selected) {
            return;
          }

          $(this).remove();
        });
      };

      return Tags;
    });

    S2.define("select2/data/tokenizer", ["jquery"], function ($) {
      function Tokenizer(decorated, $element, options) {
        var tokenizer = options.get("tokenizer");

        if (tokenizer !== undefined) {
          this.tokenizer = tokenizer;
        }

        decorated.call(this, $element, options);
      }

      Tokenizer.prototype.bind = function (decorated, container, $container) {
        decorated.call(this, container, $container);

        this.$search =
          container.dropdown.$search ||
          container.selection.$search ||
          $container.find(".select2-search__field");
      };

      Tokenizer.prototype.query = function (decorated, params, callback) {
        var self = this;

        function createAndSelect(data) {
          // Normalize the data object so we can use it for checks
          var item = self._normalizeItem(data);

          // Check if the data object already exists as a tag
          // Select it if it doesn't
          var $existingOptions = self.$element
            .find("option")
            .filter(function () {
              return $(this).val() === item.id;
            });

          // If an existing option wasn't found for it, create the option
          if (!$existingOptions.length) {
            var $option = self.option(item);
            $option.attr("data-select2-tag", true);

            self._removeOldTags();
            self.addOptions([$option]);
          }

          // Select the item, now that we know there is an option for it
          select(item);
        }

        function select(data) {
          self.trigger("select", {
            data: data,
          });
        }

        params.term = params.term || "";

        var tokenData = this.tokenizer(params, this.options, createAndSelect);

        if (tokenData.term !== params.term) {
          // Replace the search term if we have the search box
          if (this.$search.length) {
            this.$search.val(tokenData.term);
            this.$search.focus();
          }

          params.term = tokenData.term;
        }

        decorated.call(this, params, callback);
      };

      Tokenizer.prototype.tokenizer = function (_, params, options, callback) {
        var separators = options.get("tokenSeparators") || [];
        var term = params.term;
        var i = 0;

        var createTag =
          this.createTag ||
          function (params) {
            return {
              id: params.term,
              text: params.term,
            };
          };

        while (i < term.length) {
          var termChar = term[i];

          if ($.inArray(termChar, separators) === -1) {
            i++;

            continue;
          }

          var part = term.substr(0, i);
          var partParams = $.extend({}, params, {
            term: part,
          });

          var data = createTag(partParams);

          if (data == null) {
            i++;
            continue;
          }

          callback(data);

          // Reset the term to not include the tokenized portion
          term = term.substr(i + 1) || "";
          i = 0;
        }

        return {
          term: term,
        };
      };

      return Tokenizer;
    });

    S2.define("select2/data/minimumInputLength", [], function () {
      function MinimumInputLength(decorated, $e, options) {
        this.minimumInputLength = options.get("minimumInputLength");

        decorated.call(this, $e, options);
      }

      MinimumInputLength.prototype.query = function (
        decorated,
        params,
        callback
      ) {
        params.term = params.term || "";

        if (params.term.length < this.minimumInputLength) {
          this.trigger("results:message", {
            message: "inputTooShort",
            args: {
              minimum: this.minimumInputLength,
              input: params.term,
              params: params,
            },
          });

          return;
        }

        decorated.call(this, params, callback);
      };

      return MinimumInputLength;
    });

    S2.define("select2/data/maximumInputLength", [], function () {
      function MaximumInputLength(decorated, $e, options) {
        this.maximumInputLength = options.get("maximumInputLength");

        decorated.call(this, $e, options);
      }

      MaximumInputLength.prototype.query = function (
        decorated,
        params,
        callback
      ) {
        params.term = params.term || "";

        if (
          this.maximumInputLength > 0 &&
          params.term.length > this.maximumInputLength
        ) {
          this.trigger("results:message", {
            message: "inputTooLong",
            args: {
              maximum: this.maximumInputLength,
              input: params.term,
              params: params,
            },
          });

          return;
        }

        decorated.call(this, params, callback);
      };

      return MaximumInputLength;
    });

    S2.define("select2/data/maximumSelectionLength", [], function () {
      function MaximumSelectionLength(decorated, $e, options) {
        this.maximumSelectionLength = options.get("maximumSelectionLength");

        decorated.call(this, $e, options);
      }

      MaximumSelectionLength.prototype.query = function (
        decorated,
        params,
        callback
      ) {
        var self = this;

        this.current(function (currentData) {
          var count = currentData != null ? currentData.length : 0;
          if (
            self.maximumSelectionLength > 0 &&
            count >= self.maximumSelectionLength
          ) {
            self.trigger("results:message", {
              message: "maximumSelected",
              args: {
                maximum: self.maximumSelectionLength,
              },
            });
            return;
          }
          decorated.call(self, params, callback);
        });
      };

      return MaximumSelectionLength;
    });

    S2.define("select2/dropdown", ["jquery", "./utils"], function ($, Utils) {
      function Dropdown($element, options) {
        this.$element = $element;
        this.options = options;

        Dropdown.__super__.constructor.call(this);
      }

      Utils.Extend(Dropdown, Utils.Observable);

      Dropdown.prototype.render = function () {
        var $dropdown = $(
          '<span class="select2-dropdown">' +
            '<span class="select2-results"></span>' +
            "</span>"
        );

        $dropdown.attr("dir", this.options.get("dir"));

        this.$dropdown = $dropdown;

        return $dropdown;
      };

      Dropdown.prototype.bind = function () {
        // Should be implemented in subclasses
      };

      Dropdown.prototype.position = function ($dropdown, $container) {
        // Should be implmented in subclasses
      };

      Dropdown.prototype.destroy = function () {
        // Remove the dropdown from the DOM
        this.$dropdown.remove();
      };

      return Dropdown;
    });

    S2.define("select2/dropdown/search", ["jquery", "../utils"], function (
      $,
      Utils
    ) {
      function Search() {}

      Search.prototype.render = function (decorated) {
        var $rendered = decorated.call(this);

        var $search = $(
          '<span class="select2-search select2-search--dropdown">' +
            '<input class="select2-search__field" type="search" tabindex="-1"' +
            ' autocomplete="off" autocorrect="off" autocapitalize="none"' +
            ' spellcheck="false" role="textbox" />' +
            "</span>"
        );

        this.$searchContainer = $search;
        this.$search = $search.find("input");

        $rendered.prepend($search);

        return $rendered;
      };

      Search.prototype.bind = function (decorated, container, $container) {
        var self = this;

        decorated.call(this, container, $container);

        this.$search.on("keydown", function (evt) {
          self.trigger("keypress", evt);

          self._keyUpPrevented = evt.isDefaultPrevented();
        });

        // Workaround for browsers which do not support the `input` event
        // This will prevent double-triggering of events for browsers which support
        // both the `keyup` and `input` events.
        this.$search.on("input", function (evt) {
          // Unbind the duplicated `keyup` event
          $(this).off("keyup");
        });

        this.$search.on("keyup input", function (evt) {
          self.handleSearch(evt);
        });

        container.on("open", function () {
          self.$search.attr("tabindex", 0);

          self.$search.focus();

          window.setTimeout(function () {
            self.$search.focus();
          }, 0);
        });

        container.on("close", function () {
          self.$search.attr("tabindex", -1);

          self.$search.val("");
          self.$search.blur();
        });

        container.on("focus", function () {
          if (!container.isOpen()) {
            self.$search.focus();
          }
        });

        container.on("results:all", function (params) {
          if (params.query.term == null || params.query.term === "") {
            var showSearch = self.showSearch(params);

            if (showSearch) {
              self.$searchContainer.removeClass("select2-search--hide");
            } else {
              self.$searchContainer.addClass("select2-search--hide");
            }
          }
        });
      };

      Search.prototype.handleSearch = function (evt) {
        if (!this._keyUpPrevented) {
          var input = this.$search.val();

          this.trigger("query", {
            term: input,
          });
        }

        this._keyUpPrevented = false;
      };

      Search.prototype.showSearch = function (_, params) {
        return true;
      };

      return Search;
    });

    S2.define("select2/dropdown/hidePlaceholder", [], function () {
      function HidePlaceholder(decorated, $element, options, dataAdapter) {
        this.placeholder = this.normalizePlaceholder(
          options.get("placeholder")
        );

        decorated.call(this, $element, options, dataAdapter);
      }

      HidePlaceholder.prototype.append = function (decorated, data) {
        data.results = this.removePlaceholder(data.results);

        decorated.call(this, data);
      };

      HidePlaceholder.prototype.normalizePlaceholder = function (
        _,
        placeholder
      ) {
        if (typeof placeholder === "string") {
          placeholder = {
            id: "",
            text: placeholder,
          };
        }

        return placeholder;
      };

      HidePlaceholder.prototype.removePlaceholder = function (_, data) {
        var modifiedData = data.slice(0);

        for (var d = data.length - 1; d >= 0; d--) {
          var item = data[d];

          if (this.placeholder.id === item.id) {
            modifiedData.splice(d, 1);
          }
        }

        return modifiedData;
      };

      return HidePlaceholder;
    });

    S2.define("select2/dropdown/infiniteScroll", ["jquery"], function ($) {
      function InfiniteScroll(decorated, $element, options, dataAdapter) {
        this.lastParams = {};

        decorated.call(this, $element, options, dataAdapter);

        this.$loadingMore = this.createLoadingMore();
        this.loading = false;
      }

      InfiniteScroll.prototype.append = function (decorated, data) {
        this.$loadingMore.remove();
        this.loading = false;

        decorated.call(this, data);

        if (this.showLoadingMore(data)) {
          this.$results.append(this.$loadingMore);
        }
      };

      InfiniteScroll.prototype.bind = function (
        decorated,
        container,
        $container
      ) {
        var self = this;

        decorated.call(this, container, $container);

        container.on("query", function (params) {
          self.lastParams = params;
          self.loading = true;
        });

        container.on("query:append", function (params) {
          self.lastParams = params;
          self.loading = true;
        });

        this.$results.on("scroll", function () {
          var isLoadMoreVisible = $.contains(
            document.documentElement,
            self.$loadingMore[0]
          );

          if (self.loading || !isLoadMoreVisible) {
            return;
          }

          var currentOffset =
            self.$results.offset().top + self.$results.outerHeight(false);
          var loadingMoreOffset =
            self.$loadingMore.offset().top +
            self.$loadingMore.outerHeight(false);

          if (currentOffset + 50 >= loadingMoreOffset) {
            self.loadMore();
          }
        });
      };

      InfiniteScroll.prototype.loadMore = function () {
        this.loading = true;

        var params = $.extend({}, { page: 1 }, this.lastParams);

        params.page++;

        this.trigger("query:append", params);
      };

      InfiniteScroll.prototype.showLoadingMore = function (_, data) {
        return data.pagination && data.pagination.more;
      };

      InfiniteScroll.prototype.createLoadingMore = function () {
        var $option = $(
          "<li " +
            'class="select2-results__option select2-results__option--load-more"' +
            'role="treeitem" aria-disabled="true"></li>'
        );

        var message = this.options.get("translations").get("loadingMore");

        $option.html(message(this.lastParams));

        return $option;
      };

      return InfiniteScroll;
    });

    S2.define("select2/dropdown/attachBody", ["jquery", "../utils"], function (
      $,
      Utils
    ) {
      function AttachBody(decorated, $element, options) {
        this.$dropdownParent =
          options.get("dropdownParent") || $(document.body);

        decorated.call(this, $element, options);
      }

      AttachBody.prototype.bind = function (decorated, container, $container) {
        var self = this;

        var setupResultsEvents = false;

        decorated.call(this, container, $container);

        container.on("open", function () {
          self._showDropdown();
          self._attachPositioningHandler(container);

          if (!setupResultsEvents) {
            setupResultsEvents = true;

            container.on("results:all", function () {
              self._positionDropdown();
              self._resizeDropdown();
            });

            container.on("results:append", function () {
              self._positionDropdown();
              self._resizeDropdown();
            });
          }
        });

        container.on("close", function () {
          self._hideDropdown();
          self._detachPositioningHandler(container);
        });

        this.$dropdownContainer.on("mousedown", function (evt) {
          evt.stopPropagation();
        });
      };

      AttachBody.prototype.destroy = function (decorated) {
        decorated.call(this);

        this.$dropdownContainer.remove();
      };

      AttachBody.prototype.position = function (
        decorated,
        $dropdown,
        $container
      ) {
        // Clone all of the container classes
        $dropdown.attr("class", $container.attr("class"));

        $dropdown.removeClass("select2");
        $dropdown.addClass("select2-container--open");

        $dropdown.css({
          position: "absolute",
          top: -999999,
        });

        this.$container = $container;
      };

      AttachBody.prototype.render = function (decorated) {
        var $container = $("<span></span>");

        var $dropdown = decorated.call(this);
        $container.append($dropdown);

        this.$dropdownContainer = $container;

        return $container;
      };

      AttachBody.prototype._hideDropdown = function (decorated) {
        this.$dropdownContainer.detach();
      };

      AttachBody.prototype._attachPositioningHandler = function (
        decorated,
        container
      ) {
        var self = this;

        var scrollEvent = "scroll.select2." + container.id;
        var resizeEvent = "resize.select2." + container.id;
        var orientationEvent = "orientationchange.select2." + container.id;

        var $watchers = this.$container.parents().filter(Utils.hasScroll);
        $watchers.each(function () {
          Utils.StoreData(this, "select2-scroll-position", {
            x: $(this).scrollLeft(),
            y: $(this).scrollTop(),
          });
        });

        $watchers.on(scrollEvent, function (ev) {
          var position = Utils.GetData(this, "select2-scroll-position");
          $(this).scrollTop(position.y);
        });

        $(window).on(
          scrollEvent + " " + resizeEvent + " " + orientationEvent,
          function (e) {
            self._positionDropdown();
            self._resizeDropdown();
          }
        );
      };

      AttachBody.prototype._detachPositioningHandler = function (
        decorated,
        container
      ) {
        var scrollEvent = "scroll.select2." + container.id;
        var resizeEvent = "resize.select2." + container.id;
        var orientationEvent = "orientationchange.select2." + container.id;

        var $watchers = this.$container.parents().filter(Utils.hasScroll);
        $watchers.off(scrollEvent);

        $(window).off(scrollEvent + " " + resizeEvent + " " + orientationEvent);
      };

      AttachBody.prototype._positionDropdown = function () {
        var $window = $(window);

        var isCurrentlyAbove = this.$dropdown.hasClass(
          "select2-dropdown--above"
        );
        var isCurrentlyBelow = this.$dropdown.hasClass(
          "select2-dropdown--below"
        );

        var newDirection = null;

        var offset = this.$container.offset();

        offset.bottom = offset.top + this.$container.outerHeight(false);

        var container = {
          height: this.$container.outerHeight(false),
        };

        container.top = offset.top;
        container.bottom = offset.top + container.height;

        var dropdown = {
          height: this.$dropdown.outerHeight(false),
        };

        var viewport = {
          top: $window.scrollTop(),
          bottom: $window.scrollTop() + $window.height(),
        };

        var enoughRoomAbove = viewport.top < offset.top - dropdown.height;
        var enoughRoomBelow = viewport.bottom > offset.bottom + dropdown.height;

        var css = {
          left: offset.left,
          top: container.bottom,
        };

        // Determine what the parent element is to use for calciulating the offset
        var $offsetParent = this.$dropdownParent;

        // For statically positoned elements, we need to get the element
        // that is determining the offset
        if ($offsetParent.css("position") === "static") {
          $offsetParent = $offsetParent.offsetParent();
        }

        var parentOffset = $offsetParent.offset();

        css.top -= parentOffset.top;
        css.left -= parentOffset.left;

        if (!isCurrentlyAbove && !isCurrentlyBelow) {
          newDirection = "below";
        }

        if (!enoughRoomBelow && enoughRoomAbove && !isCurrentlyAbove) {
          newDirection = "above";
        } else if (!enoughRoomAbove && enoughRoomBelow && isCurrentlyAbove) {
          newDirection = "below";
        }

        if (
          newDirection == "above" ||
          (isCurrentlyAbove && newDirection !== "below")
        ) {
          css.top = container.top - parentOffset.top - dropdown.height;
        }

        if (newDirection != null) {
          this.$dropdown
            .removeClass("select2-dropdown--below select2-dropdown--above")
            .addClass("select2-dropdown--" + newDirection);
          this.$container
            .removeClass("select2-container--below select2-container--above")
            .addClass("select2-container--" + newDirection);
        }

        this.$dropdownContainer.css(css);
      };

      AttachBody.prototype._resizeDropdown = function () {
        var css = {
          width: this.$container.outerWidth(false) + "px",
        };

        if (this.options.get("dropdownAutoWidth")) {
          css.minWidth = css.width;
          css.position = "relative";
          css.width = "auto";
        }

        this.$dropdown.css(css);
      };

      AttachBody.prototype._showDropdown = function (decorated) {
        this.$dropdownContainer.appendTo(this.$dropdownParent);

        this._positionDropdown();
        this._resizeDropdown();
      };

      return AttachBody;
    });

    S2.define("select2/dropdown/minimumResultsForSearch", [], function () {
      function countResults(data) {
        var count = 0;

        for (var d = 0; d < data.length; d++) {
          var item = data[d];

          if (item.children) {
            count += countResults(item.children);
          } else {
            count++;
          }
        }

        return count;
      }

      function MinimumResultsForSearch(
        decorated,
        $element,
        options,
        dataAdapter
      ) {
        this.minimumResultsForSearch = options.get("minimumResultsForSearch");

        if (this.minimumResultsForSearch < 0) {
          this.minimumResultsForSearch = Infinity;
        }

        decorated.call(this, $element, options, dataAdapter);
      }

      MinimumResultsForSearch.prototype.showSearch = function (
        decorated,
        params
      ) {
        if (countResults(params.data.results) < this.minimumResultsForSearch) {
          return false;
        }

        return decorated.call(this, params);
      };

      return MinimumResultsForSearch;
    });

    S2.define("select2/dropdown/selectOnClose", ["../utils"], function (Utils) {
      function SelectOnClose() {}

      SelectOnClose.prototype.bind = function (
        decorated,
        container,
        $container
      ) {
        var self = this;

        decorated.call(this, container, $container);

        container.on("close", function (params) {
          self._handleSelectOnClose(params);
        });
      };

      SelectOnClose.prototype._handleSelectOnClose = function (_, params) {
        if (params && params.originalSelect2Event != null) {
          var event = params.originalSelect2Event;

          // Don't select an item if the close event was triggered from a select or
          // unselect event
          if (event._type === "select" || event._type === "unselect") {
            return;
          }
        }

        var $highlightedResults = this.getHighlightedResults();

        // Only select highlighted results
        if ($highlightedResults.length < 1) {
          return;
        }

        var data = Utils.GetData($highlightedResults[0], "data");

        // Don't re-select already selected resulte
        if (
          (data.element != null && data.element.selected) ||
          (data.element == null && data.selected)
        ) {
          return;
        }

        this.trigger("select", {
          data: data,
        });
      };

      return SelectOnClose;
    });

    S2.define("select2/dropdown/closeOnSelect", [], function () {
      function CloseOnSelect() {}

      CloseOnSelect.prototype.bind = function (
        decorated,
        container,
        $container
      ) {
        var self = this;

        decorated.call(this, container, $container);

        container.on("select", function (evt) {
          self._selectTriggered(evt);
        });

        container.on("unselect", function (evt) {
          self._selectTriggered(evt);
        });
      };

      CloseOnSelect.prototype._selectTriggered = function (_, evt) {
        var originalEvent = evt.originalEvent;

        // Don't close if the control key is being held
        if (originalEvent && originalEvent.ctrlKey) {
          return;
        }

        this.trigger("close", {
          originalEvent: originalEvent,
          originalSelect2Event: evt,
        });
      };

      return CloseOnSelect;
    });

    S2.define("select2/i18n/en", [], function () {
      // English
      return {
        errorLoading: function () {
          return "The results could not be loaded.";
        },
        inputTooLong: function (args) {
          var overChars = args.input.length - args.maximum;

          var message = "Please delete " + overChars + " character";

          if (overChars != 1) {
            message += "s";
          }

          return message;
        },
        inputTooShort: function (args) {
          var remainingChars = args.minimum - args.input.length;

          var message =
            "Please enter " + remainingChars + " or more characters";

          return message;
        },
        loadingMore: function () {
          return "Loading more results…";
        },
        maximumSelected: function (args) {
          var message = "You can only select " + args.maximum + " item";

          if (args.maximum != 1) {
            message += "s";
          }

          return message;
        },
        noResults: function () {
          return "No results found";
        },
        searching: function () {
          return "Searching…";
        },
      };
    });

    S2.define(
      "select2/defaults",
      [
        "jquery",
        "require",

        "./results",

        "./selection/single",
        "./selection/multiple",
        "./selection/placeholder",
        "./selection/allowClear",
        "./selection/search",
        "./selection/eventRelay",

        "./utils",
        "./translation",
        "./diacritics",

        "./data/select",
        "./data/array",
        "./data/ajax",
        "./data/tags",
        "./data/tokenizer",
        "./data/minimumInputLength",
        "./data/maximumInputLength",
        "./data/maximumSelectionLength",

        "./dropdown",
        "./dropdown/search",
        "./dropdown/hidePlaceholder",
        "./dropdown/infiniteScroll",
        "./dropdown/attachBody",
        "./dropdown/minimumResultsForSearch",
        "./dropdown/selectOnClose",
        "./dropdown/closeOnSelect",

        "./i18n/en",
      ],
      function (
        $,
        require,

        ResultsList,

        SingleSelection,
        MultipleSelection,
        Placeholder,
        AllowClear,
        SelectionSearch,
        EventRelay,

        Utils,
        Translation,
        DIACRITICS,

        SelectData,
        ArrayData,
        AjaxData,
        Tags,
        Tokenizer,
        MinimumInputLength,
        MaximumInputLength,
        MaximumSelectionLength,

        Dropdown,
        DropdownSearch,
        HidePlaceholder,
        InfiniteScroll,
        AttachBody,
        MinimumResultsForSearch,
        SelectOnClose,
        CloseOnSelect,

        EnglishTranslation
      ) {
        function Defaults() {
          this.reset();
        }

        Defaults.prototype.apply = function (options) {
          options = $.extend(true, {}, this.defaults, options);

          if (options.dataAdapter == null) {
            if (options.ajax != null) {
              options.dataAdapter = AjaxData;
            } else if (options.data != null) {
              options.dataAdapter = ArrayData;
            } else {
              options.dataAdapter = SelectData;
            }

            if (options.minimumInputLength > 0) {
              options.dataAdapter = Utils.Decorate(
                options.dataAdapter,
                MinimumInputLength
              );
            }

            if (options.maximumInputLength > 0) {
              options.dataAdapter = Utils.Decorate(
                options.dataAdapter,
                MaximumInputLength
              );
            }

            if (options.maximumSelectionLength > 0) {
              options.dataAdapter = Utils.Decorate(
                options.dataAdapter,
                MaximumSelectionLength
              );
            }

            if (options.tags) {
              options.dataAdapter = Utils.Decorate(options.dataAdapter, Tags);
            }

            if (options.tokenSeparators != null || options.tokenizer != null) {
              options.dataAdapter = Utils.Decorate(
                options.dataAdapter,
                Tokenizer
              );
            }

            if (options.query != null) {
              var Query = require(options.amdBase + "compat/query");

              options.dataAdapter = Utils.Decorate(options.dataAdapter, Query);
            }

            if (options.initSelection != null) {
              var InitSelection = require(options.amdBase +
                "compat/initSelection");

              options.dataAdapter = Utils.Decorate(
                options.dataAdapter,
                InitSelection
              );
            }
          }

          if (options.resultsAdapter == null) {
            options.resultsAdapter = ResultsList;

            if (options.ajax != null) {
              options.resultsAdapter = Utils.Decorate(
                options.resultsAdapter,
                InfiniteScroll
              );
            }

            if (options.placeholder != null) {
              options.resultsAdapter = Utils.Decorate(
                options.resultsAdapter,
                HidePlaceholder
              );
            }

            if (options.selectOnClose) {
              options.resultsAdapter = Utils.Decorate(
                options.resultsAdapter,
                SelectOnClose
              );
            }
          }

          if (options.dropdownAdapter == null) {
            if (options.multiple) {
              options.dropdownAdapter = Dropdown;
            } else {
              var SearchableDropdown = Utils.Decorate(Dropdown, DropdownSearch);

              options.dropdownAdapter = SearchableDropdown;
            }

            if (options.minimumResultsForSearch !== 0) {
              options.dropdownAdapter = Utils.Decorate(
                options.dropdownAdapter,
                MinimumResultsForSearch
              );
            }

            if (options.closeOnSelect) {
              options.dropdownAdapter = Utils.Decorate(
                options.dropdownAdapter,
                CloseOnSelect
              );
            }

            if (
              options.dropdownCssClass != null ||
              options.dropdownCss != null ||
              options.adaptDropdownCssClass != null
            ) {
              var DropdownCSS = require(options.amdBase + "compat/dropdownCss");

              options.dropdownAdapter = Utils.Decorate(
                options.dropdownAdapter,
                DropdownCSS
              );
            }

            options.dropdownAdapter = Utils.Decorate(
              options.dropdownAdapter,
              AttachBody
            );
          }

          if (options.selectionAdapter == null) {
            if (options.multiple) {
              options.selectionAdapter = MultipleSelection;
            } else {
              options.selectionAdapter = SingleSelection;
            }

            // Add the placeholder mixin if a placeholder was specified
            if (options.placeholder != null) {
              options.selectionAdapter = Utils.Decorate(
                options.selectionAdapter,
                Placeholder
              );
            }

            if (options.allowClear) {
              options.selectionAdapter = Utils.Decorate(
                options.selectionAdapter,
                AllowClear
              );
            }

            if (options.multiple) {
              options.selectionAdapter = Utils.Decorate(
                options.selectionAdapter,
                SelectionSearch
              );
            }

            if (
              options.containerCssClass != null ||
              options.containerCss != null ||
              options.adaptContainerCssClass != null
            ) {
              var ContainerCSS = require(options.amdBase +
                "compat/containerCss");

              options.selectionAdapter = Utils.Decorate(
                options.selectionAdapter,
                ContainerCSS
              );
            }

            options.selectionAdapter = Utils.Decorate(
              options.selectionAdapter,
              EventRelay
            );
          }

          if (typeof options.language === "string") {
            // Check if the language is specified with a region
            if (options.language.indexOf("-") > 0) {
              // Extract the region information if it is included
              var languageParts = options.language.split("-");
              var baseLanguage = languageParts[0];

              options.language = [options.language, baseLanguage];
            } else {
              options.language = [options.language];
            }
          }

          if ($.isArray(options.language)) {
            var languages = new Translation();
            options.language.push("en");

            var languageNames = options.language;

            for (var l = 0; l < languageNames.length; l++) {
              var name = languageNames[l];
              var language = {};

              try {
                // Try to load it with the original name
                language = Translation.loadPath(name);
              } catch (e) {
                try {
                  // If we couldn't load it, check if it wasn't the full path
                  name = this.defaults.amdLanguageBase + name;
                  language = Translation.loadPath(name);
                } catch (ex) {
                  // The translation could not be loaded at all. Sometimes this is
                  // because of a configuration problem, other times this can be
                  // because of how Select2 helps load all possible translation files.
                  if (options.debug && window.console && console.warn) {
                    console.warn(
                      'Select2: The language file for "' +
                        name +
                        '" could not be ' +
                        "automatically loaded. A fallback will be used instead."
                    );
                  }

                  continue;
                }
              }

              languages.extend(language);
            }

            options.translations = languages;
          } else {
            var baseTranslation = Translation.loadPath(
              this.defaults.amdLanguageBase + "en"
            );
            var customTranslation = new Translation(options.language);

            customTranslation.extend(baseTranslation);

            options.translations = customTranslation;
          }

          return options;
        };

        Defaults.prototype.reset = function () {
          function stripDiacritics(text) {
            // Used 'uni range + named function' from http://jsperf.com/diacritics/18
            function match(a) {
              return DIACRITICS[a] || a;
            }

            return text.replace(/[^\u0000-\u007E]/g, match);
          }

          function matcher(params, data) {
            // Always return the object if there is nothing to compare
            if ($.trim(params.term) === "") {
              return data;
            }

            // Do a recursive check for options with children
            if (data.children && data.children.length > 0) {
              // Clone the data object if there are children
              // This is required as we modify the object to remove any non-matches
              var match = $.extend(true, {}, data);

              // Check each child of the option
              for (var c = data.children.length - 1; c >= 0; c--) {
                var child = data.children[c];

                var matches = matcher(params, child);

                // If there wasn't a match, remove the object in the array
                if (matches == null) {
                  match.children.splice(c, 1);
                }
              }

              // If any children matched, return the new object
              if (match.children.length > 0) {
                return match;
              }

              // If there were no matching children, check just the plain object
              return matcher(params, match);
            }

            var original = stripDiacritics(data.text).toUpperCase();
            var term = stripDiacritics(params.term).toUpperCase();

            // Check if the text contains the term
            if (original.indexOf(term) > -1) {
              return data;
            }

            // If it doesn't contain the term, don't return anything
            return null;
          }

          this.defaults = {
            amdBase: "./",
            amdLanguageBase: "./i18n/",
            closeOnSelect: true,
            debug: false,
            dropdownAutoWidth: false,
            escapeMarkup: Utils.escapeMarkup,
            language: EnglishTranslation,
            matcher: matcher,
            minimumInputLength: 0,
            maximumInputLength: 0,
            maximumSelectionLength: 0,
            minimumResultsForSearch: 0,
            selectOnClose: false,
            sorter: function (data) {
              return data;
            },
            templateResult: function (result) {
              return result.text;
            },
            templateSelection: function (selection) {
              return selection.text;
            },
            theme: "default",
            width: "resolve",
          };
        };

        Defaults.prototype.set = function (key, value) {
          var camelKey = $.camelCase(key);

          var data = {};
          data[camelKey] = value;

          var convertedData = Utils._convertData(data);

          $.extend(true, this.defaults, convertedData);
        };

        var defaults = new Defaults();

        return defaults;
      }
    );

    S2.define(
      "select2/options",
      ["require", "jquery", "./defaults", "./utils"],
      function (require, $, Defaults, Utils) {
        function Options(options, $element) {
          this.options = options;

          if ($element != null) {
            this.fromElement($element);
          }

          this.options = Defaults.apply(this.options);

          if ($element && $element.is("input")) {
            var InputCompat = require(this.get("amdBase") + "compat/inputData");

            this.options.dataAdapter = Utils.Decorate(
              this.options.dataAdapter,
              InputCompat
            );
          }
        }

        Options.prototype.fromElement = function ($e) {
          var excludedData = ["select2"];

          if (this.options.multiple == null) {
            this.options.multiple = $e.prop("multiple");
          }

          if (this.options.disabled == null) {
            this.options.disabled = $e.prop("disabled");
          }

          if (this.options.language == null) {
            if ($e.prop("lang")) {
              this.options.language = $e.prop("lang").toLowerCase();
            } else if ($e.closest("[lang]").prop("lang")) {
              this.options.language = $e.closest("[lang]").prop("lang");
            }
          }

          if (this.options.dir == null) {
            if ($e.prop("dir")) {
              this.options.dir = $e.prop("dir");
            } else if ($e.closest("[dir]").prop("dir")) {
              this.options.dir = $e.closest("[dir]").prop("dir");
            } else {
              this.options.dir = "ltr";
            }
          }

          $e.prop("disabled", this.options.disabled);
          $e.prop("multiple", this.options.multiple);

          if (Utils.GetData($e[0], "select2Tags")) {
            if (this.options.debug && window.console && console.warn) {
              console.warn(
                "Select2: The `data-select2-tags` attribute has been changed to " +
                  'use the `data-data` and `data-tags="true"` attributes and will be ' +
                  "removed in future versions of Select2."
              );
            }

            Utils.StoreData($e[0], "data", Utils.GetData($e[0], "select2Tags"));
            Utils.StoreData($e[0], "tags", true);
          }

          if (Utils.GetData($e[0], "ajaxUrl")) {
            if (this.options.debug && window.console && console.warn) {
              console.warn(
                "Select2: The `data-ajax-url` attribute has been changed to " +
                  "`data-ajax--url` and support for the old attribute will be removed" +
                  " in future versions of Select2."
              );
            }

            $e.attr("ajax--url", Utils.GetData($e[0], "ajaxUrl"));
            Utils.StoreData($e[0], "ajax-Url", Utils.GetData($e[0], "ajaxUrl"));
          }

          var dataset = {};

          // Prefer the element's `dataset` attribute if it exists
          // jQuery 1.x does not correctly handle data attributes with multiple dashes
          if (
            $.fn.jquery &&
            $.fn.jquery.substr(0, 2) == "1." &&
            $e[0].dataset
          ) {
            dataset = $.extend(true, {}, $e[0].dataset, Utils.GetData($e[0]));
          } else {
            dataset = Utils.GetData($e[0]);
          }

          var data = $.extend(true, {}, dataset);

          data = Utils._convertData(data);

          for (var key in data) {
            if ($.inArray(key, excludedData) > -1) {
              continue;
            }

            if ($.isPlainObject(this.options[key])) {
              $.extend(this.options[key], data[key]);
            } else {
              this.options[key] = data[key];
            }
          }

          return this;
        };

        Options.prototype.get = function (key) {
          return this.options[key];
        };

        Options.prototype.set = function (key, val) {
          this.options[key] = val;
        };

        return Options;
      }
    );

    S2.define(
      "select2/core",
      ["jquery", "./options", "./utils", "./keys"],
      function ($, Options, Utils, KEYS) {
        var Select2 = function ($element, options) {
          if (Utils.GetData($element[0], "select2") != null) {
            Utils.GetData($element[0], "select2").destroy();
          }

          this.$element = $element;

          this.id = this._generateId($element);

          options = options || {};

          this.options = new Options(options, $element);

          Select2.__super__.constructor.call(this);

          // Set up the tabindex

          var tabindex = $element.attr("tabindex") || 0;
          Utils.StoreData($element[0], "old-tabindex", tabindex);
          $element.attr("tabindex", "-1");

          // Set up containers and adapters

          var DataAdapter = this.options.get("dataAdapter");
          this.dataAdapter = new DataAdapter($element, this.options);

          var $container = this.render();

          this._placeContainer($container);

          var SelectionAdapter = this.options.get("selectionAdapter");
          this.selection = new SelectionAdapter($element, this.options);
          this.$selection = this.selection.render();

          this.selection.position(this.$selection, $container);

          var DropdownAdapter = this.options.get("dropdownAdapter");
          this.dropdown = new DropdownAdapter($element, this.options);
          this.$dropdown = this.dropdown.render();

          this.dropdown.position(this.$dropdown, $container);

          var ResultsAdapter = this.options.get("resultsAdapter");
          this.results = new ResultsAdapter(
            $element,
            this.options,
            this.dataAdapter
          );
          this.$results = this.results.render();

          this.results.position(this.$results, this.$dropdown);

          // Bind events

          var self = this;

          // Bind the container to all of the adapters
          this._bindAdapters();

          // Register any DOM event handlers
          this._registerDomEvents();

          // Register any internal event handlers
          this._registerDataEvents();
          this._registerSelectionEvents();
          this._registerDropdownEvents();
          this._registerResultsEvents();
          this._registerEvents();

          // Set the initial state
          this.dataAdapter.current(function (initialData) {
            self.trigger("selection:update", {
              data: initialData,
            });
          });

          // Hide the original select
          $element.addClass("select2-hidden-accessible");
          $element.attr("aria-hidden", "true");

          // Synchronize any monitored attributes
          this._syncAttributes();

          Utils.StoreData($element[0], "select2", this);

          // Ensure backwards compatibility with $element.data('select2').
          $element.data("select2", this);
        };

        Utils.Extend(Select2, Utils.Observable);

        Select2.prototype._generateId = function ($element) {
          var id = "";

          if ($element.attr("id") != null) {
            id = $element.attr("id");
          } else if ($element.attr("name") != null) {
            id = $element.attr("name") + "-" + Utils.generateChars(2);
          } else {
            id = Utils.generateChars(4);
          }

          id = id.replace(/(:|\.|\[|\]|,)/g, "");
          id = "select2-" + id;

          return id;
        };

        Select2.prototype._placeContainer = function ($container) {
          $container.insertAfter(this.$element);

          var width = this._resolveWidth(
            this.$element,
            this.options.get("width")
          );

          if (width != null) {
            $container.css("width", width);
          }
        };

        Select2.prototype._resolveWidth = function ($element, method) {
          var WIDTH = /^width:(([-+]?([0-9]*\.)?[0-9]+)(px|em|ex|%|in|cm|mm|pt|pc))/i;

          if (method == "resolve") {
            var styleWidth = this._resolveWidth($element, "style");

            if (styleWidth != null) {
              return styleWidth;
            }

            return this._resolveWidth($element, "element");
          }

          if (method == "element") {
            var elementWidth = $element.outerWidth(false);

            if (elementWidth <= 0) {
              return "auto";
            }

            return elementWidth + "px";
          }

          if (method == "style") {
            var style = $element.attr("style");

            if (typeof style !== "string") {
              return null;
            }

            var attrs = style.split(";");

            for (var i = 0, l = attrs.length; i < l; i = i + 1) {
              var attr = attrs[i].replace(/\s/g, "");
              var matches = attr.match(WIDTH);

              if (matches !== null && matches.length >= 1) {
                return matches[1];
              }
            }

            return null;
          }

          return method;
        };

        Select2.prototype._bindAdapters = function () {
          this.dataAdapter.bind(this, this.$container);
          this.selection.bind(this, this.$container);

          this.dropdown.bind(this, this.$container);
          this.results.bind(this, this.$container);
        };

        Select2.prototype._registerDomEvents = function () {
          var self = this;

          this.$element.on("change.select2", function () {
            self.dataAdapter.current(function (data) {
              self.trigger("selection:update", {
                data: data,
              });
            });
          });

          this.$element.on("focus.select2", function (evt) {
            self.trigger("focus", evt);
          });

          this._syncA = Utils.bind(this._syncAttributes, this);
          this._syncS = Utils.bind(this._syncSubtree, this);

          if (this.$element[0].attachEvent) {
            this.$element[0].attachEvent("onpropertychange", this._syncA);
          }

          var observer =
            window.MutationObserver ||
            window.WebKitMutationObserver ||
            window.MozMutationObserver;
          if (observer != null) {
            this._observer = new observer(function (mutations) {
              $.each(mutations, self._syncA);
              $.each(mutations, self._syncS);
            });
            this._observer.observe(this.$element[0], {
              attributes: true,
              childList: true,
              subtree: false,
            });
          } else if (this.$element[0].addEventListener) {
            this.$element[0].addEventListener(
              "DOMAttrModified",
              self._syncA,
              false
            );
            this.$element[0].addEventListener(
              "DOMNodeInserted",
              self._syncS,
              false
            );
            this.$element[0].addEventListener(
              "DOMNodeRemoved",
              self._syncS,
              false
            );
          }
        };

        Select2.prototype._registerDataEvents = function () {
          var self = this;

          this.dataAdapter.on("*", function (name, params) {
            self.trigger(name, params);
          });
        };

        Select2.prototype._registerSelectionEvents = function () {
          var self = this;
          var nonRelayEvents = ["toggle", "focus"];

          this.selection.on("toggle", function () {
            self.toggleDropdown();
          });

          this.selection.on("focus", function (params) {
            self.focus(params);
          });

          this.selection.on("*", function (name, params) {
            if ($.inArray(name, nonRelayEvents) !== -1) {
              return;
            }

            self.trigger(name, params);
          });
        };

        Select2.prototype._registerDropdownEvents = function () {
          var self = this;

          this.dropdown.on("*", function (name, params) {
            self.trigger(name, params);
          });
        };

        Select2.prototype._registerResultsEvents = function () {
          var self = this;

          this.results.on("*", function (name, params) {
            self.trigger(name, params);
          });
        };

        Select2.prototype._registerEvents = function () {
          var self = this;

          this.on("open", function () {
            self.$container.addClass("select2-container--open");
          });

          this.on("close", function () {
            self.$container.removeClass("select2-container--open");
          });

          this.on("enable", function () {
            self.$container.removeClass("select2-container--disabled");
          });

          this.on("disable", function () {
            self.$container.addClass("select2-container--disabled");
          });

          this.on("blur", function () {
            self.$container.removeClass("select2-container--focus");
          });

          this.on("query", function (params) {
            if (!self.isOpen()) {
              self.trigger("open", {});
            }

            this.dataAdapter.query(params, function (data) {
              self.trigger("results:all", {
                data: data,
                query: params,
              });
            });
          });

          this.on("query:append", function (params) {
            this.dataAdapter.query(params, function (data) {
              self.trigger("results:append", {
                data: data,
                query: params,
              });
            });
          });

          this.on("keypress", function (evt) {
            var key = evt.which;

            if (self.isOpen()) {
              if (key === KEYS.ENTER) {
                self.trigger("results:select");

                evt.preventDefault();
              } else if (key === KEYS.SPACE && evt.ctrlKey) {
                self.trigger("results:toggle");

                evt.preventDefault();
              } else if (key === KEYS.UP) {
                self.trigger("results:previous");

                evt.preventDefault();
              } else if (key === KEYS.DOWN) {
                self.trigger("results:next");

                evt.preventDefault();
              } else if (key === KEYS.ESC || key === KEYS.TAB) {
                self.close();

                evt.preventDefault();
              }
            } else {
              if (
                key === KEYS.ENTER ||
                key === KEYS.SPACE ||
                ((key === KEYS.DOWN || key === KEYS.UP) && evt.altKey)
              ) {
                self.open();

                evt.preventDefault();
              }
              if (key === KEYS.DOWN) {
                if (
                  undefined !=
                  this.$element.find("option:selected").next().val()
                ) {
                  this.$element.val(
                    this.$element.find("option:selected").next().val()
                  );
                  this.$element.trigger("change");
                }
                evt.preventDefault();
              }
              if (key === KEYS.UP) {
                if (
                  undefined !=
                  this.$element.find("option:selected").prev().val()
                ) {
                  this.$element.val(
                    this.$element.find("option:selected").prev().val()
                  );
                  this.$element.trigger("change");
                }
                evt.preventDefault();
              }
            }
          });
        };

        Select2.prototype._syncAttributes = function () {
          this.options.set("disabled", this.$element.prop("disabled"));

          if (this.options.get("disabled")) {
            if (this.isOpen()) {
              this.close();
            }

            this.trigger("disable", {});
          } else {
            this.trigger("enable", {});
          }
        };

        Select2.prototype._syncSubtree = function (evt, mutations) {
          var changed = false;
          var self = this;

          // Ignore any mutation events raised for elements that aren't options or
          // optgroups. This handles the case when the select element is destroyed
          if (
            evt &&
            evt.target &&
            evt.target.nodeName !== "OPTION" &&
            evt.target.nodeName !== "OPTGROUP"
          ) {
            return;
          }

          if (!mutations) {
            // If mutation events aren't supported, then we can only assume that the
            // change affected the selections
            changed = true;
          } else if (mutations.addedNodes && mutations.addedNodes.length > 0) {
            for (var n = 0; n < mutations.addedNodes.length; n++) {
              var node = mutations.addedNodes[n];

              if (node.selected) {
                changed = true;
              }
            }
          } else if (
            mutations.removedNodes &&
            mutations.removedNodes.length > 0
          ) {
            changed = true;
          }

          // Only re-pull the data if we think there is a change
          if (changed) {
            this.dataAdapter.current(function (currentData) {
              self.trigger("selection:update", {
                data: currentData,
              });
            });
          }
        };

        /**
         * Override the trigger method to automatically trigger pre-events when
         * there are events that can be prevented.
         */
        Select2.prototype.trigger = function (name, args) {
          var actualTrigger = Select2.__super__.trigger;
          var preTriggerMap = {
            open: "opening",
            close: "closing",
            select: "selecting",
            unselect: "unselecting",
            clear: "clearing",
          };

          if (args === undefined) {
            args = {};
          }

          if (name in preTriggerMap) {
            var preTriggerName = preTriggerMap[name];
            var preTriggerArgs = {
              prevented: false,
              name: name,
              args: args,
            };

            actualTrigger.call(this, preTriggerName, preTriggerArgs);

            if (preTriggerArgs.prevented) {
              args.prevented = true;

              return;
            }
          }

          actualTrigger.call(this, name, args);
        };

        Select2.prototype.toggleDropdown = function () {
          if (this.options.get("disabled")) {
            return;
          }

          if (this.isOpen()) {
            this.close();
          } else {
            this.open();
          }
        };

        Select2.prototype.open = function () {
          if (this.isOpen()) {
            return;
          }

          this.trigger("query", {});
        };

        Select2.prototype.close = function () {
          if (!this.isOpen()) {
            return;
          }

          this.trigger("close", {});
        };

        Select2.prototype.isOpen = function () {
          return this.$container.hasClass("select2-container--open");
        };

        Select2.prototype.hasFocus = function () {
          return this.$container.hasClass("select2-container--focus");
        };

        Select2.prototype.focus = function (data) {
          // No need to re-trigger focus events if we are already focused
          if (this.hasFocus()) {
            return;
          }

          this.$container.addClass("select2-container--focus");
          this.trigger("focus", {});
        };

        Select2.prototype.enable = function (args) {
          if (this.options.get("debug") && window.console && console.warn) {
            console.warn(
              'Select2: The `select2("enable")` method has been deprecated and will' +
                ' be removed in later Select2 versions. Use $element.prop("disabled")' +
                " instead."
            );
          }

          if (args == null || args.length === 0) {
            args = [true];
          }

          var disabled = !args[0];

          this.$element.prop("disabled", disabled);
        };

        Select2.prototype.data = function () {
          if (
            this.options.get("debug") &&
            arguments.length > 0 &&
            window.console &&
            console.warn
          ) {
            console.warn(
              'Select2: Data can no longer be set using `select2("data")`. You ' +
                "should consider setting the value instead using `$element.val()`."
            );
          }

          var data = [];

          this.dataAdapter.current(function (currentData) {
            data = currentData;
          });

          return data;
        };

        Select2.prototype.val = function (args) {
          if (this.options.get("debug") && window.console && console.warn) {
            console.warn(
              'Select2: The `select2("val")` method has been deprecated and will be' +
                " removed in later Select2 versions. Use $element.val() instead."
            );
          }

          if (args == null || args.length === 0) {
            return this.$element.val();
          }

          var newVal = args[0];

          if ($.isArray(newVal)) {
            newVal = $.map(newVal, function (obj) {
              return obj.toString();
            });
          }

          this.$element.val(newVal).trigger("change");
        };

        Select2.prototype.destroy = function () {
          this.$container.remove();

          if (this.$element[0].detachEvent) {
            this.$element[0].detachEvent("onpropertychange", this._syncA);
          }

          if (this._observer != null) {
            this._observer.disconnect();
            this._observer = null;
          } else if (this.$element[0].removeEventListener) {
            this.$element[0].removeEventListener(
              "DOMAttrModified",
              this._syncA,
              false
            );
            this.$element[0].removeEventListener(
              "DOMNodeInserted",
              this._syncS,
              false
            );
            this.$element[0].removeEventListener(
              "DOMNodeRemoved",
              this._syncS,
              false
            );
          }

          this._syncA = null;
          this._syncS = null;

          this.$element.off(".select2");
          this.$element.attr(
            "tabindex",
            Utils.GetData(this.$element[0], "old-tabindex")
          );

          this.$element.removeClass("select2-hidden-accessible");
          this.$element.attr("aria-hidden", "false");
          Utils.RemoveData(this.$element[0]);
          this.$element.removeData("select2");

          this.dataAdapter.destroy();
          this.selection.destroy();
          this.dropdown.destroy();
          this.results.destroy();

          this.dataAdapter = null;
          this.selection = null;
          this.dropdown = null;
          this.results = null;
        };

        Select2.prototype.render = function () {
          var $container = $(
            '<span class="select2 select2-container">' +
              '<span class="selection"></span>' +
              '<span class="dropdown-wrapper" aria-hidden="true"></span>' +
              "</span>"
          );

          $container.attr("dir", this.options.get("dir"));

          this.$container = $container;

          this.$container.addClass(
            "select2-container--" + this.options.get("theme")
          );

          Utils.StoreData($container[0], "element", this.$element);

          return $container;
        };

        return Select2;
      }
    );

    S2.define("select2/compat/utils", ["jquery"], function ($) {
      function syncCssClasses($dest, $src, adapter) {
        var classes,
          replacements = [],
          adapted;

        classes = $.trim($dest.attr("class"));

        if (classes) {
          classes = "" + classes; // for IE which returns object

          $(classes.split(/\s+/)).each(function () {
            // Save all Select2 classes
            if (this.indexOf("select2-") === 0) {
              replacements.push(this);
            }
          });
        }

        classes = $.trim($src.attr("class"));

        if (classes) {
          classes = "" + classes; // for IE which returns object

          $(classes.split(/\s+/)).each(function () {
            // Only adapt non-Select2 classes
            if (this.indexOf("select2-") !== 0) {
              adapted = adapter(this);

              if (adapted != null) {
                replacements.push(adapted);
              }
            }
          });
        }

        $dest.attr("class", replacements.join(" "));
      }

      return {
        syncCssClasses: syncCssClasses,
      };
    });

    S2.define("select2/compat/containerCss", ["jquery", "./utils"], function (
      $,
      CompatUtils
    ) {
      // No-op CSS adapter that discards all classes by default
      function _containerAdapter(clazz) {
        return null;
      }

      function ContainerCSS() {}

      ContainerCSS.prototype.render = function (decorated) {
        var $container = decorated.call(this);

        var containerCssClass = this.options.get("containerCssClass") || "";

        if ($.isFunction(containerCssClass)) {
          containerCssClass = containerCssClass(this.$element);
        }

        var containerCssAdapter = this.options.get("adaptContainerCssClass");
        containerCssAdapter = containerCssAdapter || _containerAdapter;

        if (containerCssClass.indexOf(":all:") !== -1) {
          containerCssClass = containerCssClass.replace(":all:", "");

          var _cssAdapter = containerCssAdapter;

          containerCssAdapter = function (clazz) {
            var adapted = _cssAdapter(clazz);

            if (adapted != null) {
              // Append the old one along with the adapted one
              return adapted + " " + clazz;
            }

            return clazz;
          };
        }

        var containerCss = this.options.get("containerCss") || {};

        if ($.isFunction(containerCss)) {
          containerCss = containerCss(this.$element);
        }

        CompatUtils.syncCssClasses(
          $container,
          this.$element,
          containerCssAdapter
        );

        $container.css(containerCss);
        $container.addClass(containerCssClass);

        return $container;
      };

      return ContainerCSS;
    });

    S2.define("select2/compat/dropdownCss", ["jquery", "./utils"], function (
      $,
      CompatUtils
    ) {
      // No-op CSS adapter that discards all classes by default
      function _dropdownAdapter(clazz) {
        return null;
      }

      function DropdownCSS() {}

      DropdownCSS.prototype.render = function (decorated) {
        var $dropdown = decorated.call(this);

        var dropdownCssClass = this.options.get("dropdownCssClass") || "";

        if ($.isFunction(dropdownCssClass)) {
          dropdownCssClass = dropdownCssClass(this.$element);
        }

        var dropdownCssAdapter = this.options.get("adaptDropdownCssClass");
        dropdownCssAdapter = dropdownCssAdapter || _dropdownAdapter;

        if (dropdownCssClass.indexOf(":all:") !== -1) {
          dropdownCssClass = dropdownCssClass.replace(":all:", "");

          var _cssAdapter = dropdownCssAdapter;

          dropdownCssAdapter = function (clazz) {
            var adapted = _cssAdapter(clazz);

            if (adapted != null) {
              // Append the old one along with the adapted one
              return adapted + " " + clazz;
            }

            return clazz;
          };
        }

        var dropdownCss = this.options.get("dropdownCss") || {};

        if ($.isFunction(dropdownCss)) {
          dropdownCss = dropdownCss(this.$element);
        }

        CompatUtils.syncCssClasses(
          $dropdown,
          this.$element,
          dropdownCssAdapter
        );

        $dropdown.css(dropdownCss);
        $dropdown.addClass(dropdownCssClass);

        return $dropdown;
      };

      return DropdownCSS;
    });

    S2.define("select2/compat/initSelection", ["jquery"], function ($) {
      function InitSelection(decorated, $element, options) {
        if (options.get("debug") && window.console && console.warn) {
          console.warn(
            "Select2: The `initSelection` option has been deprecated in favor" +
              " of a custom data adapter that overrides the `current` method. " +
              "This method is now called multiple times instead of a single " +
              "time when the instance is initialized. Support will be removed " +
              "for the `initSelection` option in future versions of Select2"
          );
        }

        this.initSelection = options.get("initSelection");
        this._isInitialized = false;

        decorated.call(this, $element, options);
      }

      InitSelection.prototype.current = function (decorated, callback) {
        var self = this;

        if (this._isInitialized) {
          decorated.call(this, callback);

          return;
        }

        this.initSelection.call(null, this.$element, function (data) {
          self._isInitialized = true;

          if (!$.isArray(data)) {
            data = [data];
          }

          callback(data);
        });
      };

      return InitSelection;
    });

    S2.define("select2/compat/inputData", ["jquery", "../utils"], function (
      $,
      Utils
    ) {
      function InputData(decorated, $element, options) {
        this._currentData = [];
        this._valueSeparator = options.get("valueSeparator") || ",";

        if ($element.prop("type") === "hidden") {
          if (options.get("debug") && console && console.warn) {
            console.warn(
              "Select2: Using a hidden input with Select2 is no longer " +
                "supported and may stop working in the future. It is recommended " +
                "to use a `<select>` element instead."
            );
          }
        }

        decorated.call(this, $element, options);
      }

      InputData.prototype.current = function (_, callback) {
        function getSelected(data, selectedIds) {
          var selected = [];

          if (data.selected || $.inArray(data.id, selectedIds) !== -1) {
            data.selected = true;
            selected.push(data);
          } else {
            data.selected = false;
          }

          if (data.children) {
            selected.push.apply(
              selected,
              getSelected(data.children, selectedIds)
            );
          }

          return selected;
        }

        var selected = [];

        for (var d = 0; d < this._currentData.length; d++) {
          var data = this._currentData[d];

          selected.push.apply(
            selected,
            getSelected(data, this.$element.val().split(this._valueSeparator))
          );
        }

        callback(selected);
      };

      InputData.prototype.select = function (_, data) {
        if (!this.options.get("multiple")) {
          this.current(function (allData) {
            $.map(allData, function (data) {
              data.selected = false;
            });
          });

          this.$element.val(data.id);
          this.$element.trigger("change");
        } else {
          var value = this.$element.val();
          value += this._valueSeparator + data.id;

          this.$element.val(value);
          this.$element.trigger("change");
        }
      };

      InputData.prototype.unselect = function (_, data) {
        var self = this;

        data.selected = false;

        this.current(function (allData) {
          var values = [];

          for (var d = 0; d < allData.length; d++) {
            var item = allData[d];

            if (data.id == item.id) {
              continue;
            }

            values.push(item.id);
          }

          self.$element.val(values.join(self._valueSeparator));
          self.$element.trigger("change");
        });
      };

      InputData.prototype.query = function (_, params, callback) {
        var results = [];

        for (var d = 0; d < this._currentData.length; d++) {
          var data = this._currentData[d];

          var matches = this.matches(params, data);

          if (matches !== null) {
            results.push(matches);
          }
        }

        callback({
          results: results,
        });
      };

      InputData.prototype.addOptions = function (_, $options) {
        var options = $.map($options, function ($option) {
          return Utils.GetData($option[0], "data");
        });

        this._currentData.push.apply(this._currentData, options);
      };

      return InputData;
    });

    S2.define("select2/compat/matcher", ["jquery"], function ($) {
      function oldMatcher(matcher) {
        function wrappedMatcher(params, data) {
          var match = $.extend(true, {}, data);

          if (params.term == null || $.trim(params.term) === "") {
            return match;
          }

          if (data.children) {
            for (var c = data.children.length - 1; c >= 0; c--) {
              var child = data.children[c];

              // Check if the child object matches
              // The old matcher returned a boolean true or false
              var doesMatch = matcher(params.term, child.text, child);

              // If the child didn't match, pop it off
              if (!doesMatch) {
                match.children.splice(c, 1);
              }
            }

            if (match.children.length > 0) {
              return match;
            }
          }

          if (matcher(params.term, data.text, data)) {
            return match;
          }

          return null;
        }

        return wrappedMatcher;
      }

      return oldMatcher;
    });

    S2.define("select2/compat/query", [], function () {
      function Query(decorated, $element, options) {
        if (options.get("debug") && window.console && console.warn) {
          console.warn(
            "Select2: The `query` option has been deprecated in favor of a " +
              "custom data adapter that overrides the `query` method. Support " +
              "will be removed for the `query` option in future versions of " +
              "Select2."
          );
        }

        decorated.call(this, $element, options);
      }

      Query.prototype.query = function (_, params, callback) {
        params.callback = callback;

        var query = this.options.get("query");

        query.call(null, params);
      };

      return Query;
    });

    S2.define("select2/dropdown/attachContainer", [], function () {
      function AttachContainer(decorated, $element, options) {
        decorated.call(this, $element, options);
      }

      AttachContainer.prototype.position = function (
        decorated,
        $dropdown,
        $container
      ) {
        var $dropdownContainer = $container.find(".dropdown-wrapper");
        $dropdownContainer.append($dropdown);

        $dropdown.addClass("select2-dropdown--below");
        $container.addClass("select2-container--below");
      };

      return AttachContainer;
    });

    S2.define("select2/dropdown/stopPropagation", [], function () {
      function StopPropagation() {}

      StopPropagation.prototype.bind = function (
        decorated,
        container,
        $container
      ) {
        decorated.call(this, container, $container);

        var stoppedEvents = [
          "blur",
          "change",
          "click",
          "dblclick",
          "focus",
          "focusin",
          "focusout",
          "input",
          "keydown",
          "keyup",
          "keypress",
          "mousedown",
          "mouseenter",
          "mouseleave",
          "mousemove",
          "mouseover",
          "mouseup",
          "search",
          "touchend",
          "touchstart",
        ];

        this.$dropdown.on(stoppedEvents.join(" "), function (evt) {
          evt.stopPropagation();
        });
      };

      return StopPropagation;
    });

    S2.define("select2/selection/stopPropagation", [], function () {
      function StopPropagation() {}

      StopPropagation.prototype.bind = function (
        decorated,
        container,
        $container
      ) {
        decorated.call(this, container, $container);

        var stoppedEvents = [
          "blur",
          "change",
          "click",
          "dblclick",
          "focus",
          "focusin",
          "focusout",
          "input",
          "keydown",
          "keyup",
          "keypress",
          "mousedown",
          "mouseenter",
          "mouseleave",
          "mousemove",
          "mouseover",
          "mouseup",
          "search",
          "touchend",
          "touchstart",
        ];

        this.$selection.on(stoppedEvents.join(" "), function (evt) {
          evt.stopPropagation();
        });
      };

      return StopPropagation;
    });

    /*!
     * jQuery Mousewheel 3.1.13
     *
     * Copyright jQuery Foundation and other contributors
     * Released under the MIT license
     * http://jquery.org/license
     */

    (function (factory) {
      if (typeof S2.define === "function" && S2.define.amd) {
        // AMD. Register as an anonymous module.
        S2.define("jquery-mousewheel", ["jquery"], factory);
      } else if (typeof exports === "object") {
        // Node/CommonJS style for Browserify
        module.exports = factory;
      } else {
        // Browser globals
        factory(jQuery);
      }
    })(function ($) {
      var toFix = [
          "wheel",
          "mousewheel",
          "DOMMouseScroll",
          "MozMousePixelScroll",
        ],
        toBind =
          "onwheel" in document || document.documentMode >= 9
            ? ["wheel"]
            : ["mousewheel", "DomMouseScroll", "MozMousePixelScroll"],
        slice = Array.prototype.slice,
        nullLowestDeltaTimeout,
        lowestDelta;

      if ($.event.fixHooks) {
        for (var i = toFix.length; i; ) {
          $.event.fixHooks[toFix[--i]] = $.event.mouseHooks;
        }
      }

      var special = ($.event.special.mousewheel = {
        version: "3.1.12",

        setup: function () {
          if (this.addEventListener) {
            for (var i = toBind.length; i; ) {
              this.addEventListener(toBind[--i], handler, false);
            }
          } else {
            this.onmousewheel = handler;
          }
          // Store the line height and page height for this particular element
          $.data(this, "mousewheel-line-height", special.getLineHeight(this));
          $.data(this, "mousewheel-page-height", special.getPageHeight(this));
        },

        teardown: function () {
          if (this.removeEventListener) {
            for (var i = toBind.length; i; ) {
              this.removeEventListener(toBind[--i], handler, false);
            }
          } else {
            this.onmousewheel = null;
          }
          // Clean up the data we added to the element
          $.removeData(this, "mousewheel-line-height");
          $.removeData(this, "mousewheel-page-height");
        },

        getLineHeight: function (elem) {
          var $elem = $(elem),
            $parent = $elem[
              "offsetParent" in $.fn ? "offsetParent" : "parent"
            ]();
          if (!$parent.length) {
            $parent = $("body");
          }
          return (
            parseInt($parent.css("fontSize"), 10) ||
            parseInt($elem.css("fontSize"), 10) ||
            16
          );
        },

        getPageHeight: function (elem) {
          return $(elem).height();
        },

        settings: {
          adjustOldDeltas: true, // see shouldAdjustOldDeltas() below
          normalizeOffset: true, // calls getBoundingClientRect for each event
        },
      });

      $.fn.extend({
        mousewheel: function (fn) {
          return fn ? this.bind("mousewheel", fn) : this.trigger("mousewheel");
        },

        unmousewheel: function (fn) {
          return this.unbind("mousewheel", fn);
        },
      });

      function handler(event) {
        var orgEvent = event || window.event,
          args = slice.call(arguments, 1),
          delta = 0,
          deltaX = 0,
          deltaY = 0,
          absDelta = 0,
          offsetX = 0,
          offsetY = 0;
        event = $.event.fix(orgEvent);
        event.type = "mousewheel";

        // Old school scrollwheel delta
        if ("detail" in orgEvent) {
          deltaY = orgEvent.detail * -1;
        }
        if ("wheelDelta" in orgEvent) {
          deltaY = orgEvent.wheelDelta;
        }
        if ("wheelDeltaY" in orgEvent) {
          deltaY = orgEvent.wheelDeltaY;
        }
        if ("wheelDeltaX" in orgEvent) {
          deltaX = orgEvent.wheelDeltaX * -1;
        }

        // Firefox < 17 horizontal scrolling related to DOMMouseScroll event
        if ("axis" in orgEvent && orgEvent.axis === orgEvent.HORIZONTAL_AXIS) {
          deltaX = deltaY * -1;
          deltaY = 0;
        }

        // Set delta to be deltaY or deltaX if deltaY is 0 for backwards compatabilitiy
        delta = deltaY === 0 ? deltaX : deltaY;

        // New school wheel delta (wheel event)
        if ("deltaY" in orgEvent) {
          deltaY = orgEvent.deltaY * -1;
          delta = deltaY;
        }
        if ("deltaX" in orgEvent) {
          deltaX = orgEvent.deltaX;
          if (deltaY === 0) {
            delta = deltaX * -1;
          }
        }

        // No change actually happened, no reason to go any further
        if (deltaY === 0 && deltaX === 0) {
          return;
        }

        // Need to convert lines and pages to pixels if we aren't already in pixels
        // There are three delta modes:
        //   * deltaMode 0 is by pixels, nothing to do
        //   * deltaMode 1 is by lines
        //   * deltaMode 2 is by pages
        if (orgEvent.deltaMode === 1) {
          var lineHeight = $.data(this, "mousewheel-line-height");
          delta *= lineHeight;
          deltaY *= lineHeight;
          deltaX *= lineHeight;
        } else if (orgEvent.deltaMode === 2) {
          var pageHeight = $.data(this, "mousewheel-page-height");
          delta *= pageHeight;
          deltaY *= pageHeight;
          deltaX *= pageHeight;
        }

        // Store lowest absolute delta to normalize the delta values
        absDelta = Math.max(Math.abs(deltaY), Math.abs(deltaX));

        if (!lowestDelta || absDelta < lowestDelta) {
          lowestDelta = absDelta;

          // Adjust older deltas if necessary
          if (shouldAdjustOldDeltas(orgEvent, absDelta)) {
            lowestDelta /= 40;
          }
        }

        // Adjust older deltas if necessary
        if (shouldAdjustOldDeltas(orgEvent, absDelta)) {
          // Divide all the things by 40!
          delta /= 40;
          deltaX /= 40;
          deltaY /= 40;
        }

        // Get a whole, normalized value for the deltas
        delta = Math[delta >= 1 ? "floor" : "ceil"](delta / lowestDelta);
        deltaX = Math[deltaX >= 1 ? "floor" : "ceil"](deltaX / lowestDelta);
        deltaY = Math[deltaY >= 1 ? "floor" : "ceil"](deltaY / lowestDelta);

        // Normalise offsetX and offsetY properties
        if (special.settings.normalizeOffset && this.getBoundingClientRect) {
          var boundingRect = this.getBoundingClientRect();
          offsetX = event.clientX - boundingRect.left;
          offsetY = event.clientY - boundingRect.top;
        }

        // Add information to the event object
        event.deltaX = deltaX;
        event.deltaY = deltaY;
        event.deltaFactor = lowestDelta;
        event.offsetX = offsetX;
        event.offsetY = offsetY;
        // Go ahead and set deltaMode to 0 since we converted to pixels
        // Although this is a little odd since we overwrite the deltaX/Y
        // properties with normalized deltas.
        event.deltaMode = 0;

        // Add event and delta to the front of the arguments
        args.unshift(event, delta, deltaX, deltaY);

        // Clearout lowestDelta after sometime to better
        // handle multiple device types that give different
        // a different lowestDelta
        // Ex: trackpad = 3 and mouse wheel = 120
        if (nullLowestDeltaTimeout) {
          clearTimeout(nullLowestDeltaTimeout);
        }
        nullLowestDeltaTimeout = setTimeout(nullLowestDelta, 200);

        return ($.event.dispatch || $.event.handle).apply(this, args);
      }

      function nullLowestDelta() {
        lowestDelta = null;
      }

      function shouldAdjustOldDeltas(orgEvent, absDelta) {
        // If this is an older event and the delta is divisable by 120,
        // then we are assuming that the browser is treating this as an
        // older mouse wheel event and that we should divide the deltas
        // by 40 to try and get a more usable deltaFactor.
        // Side note, this actually impacts the reported scroll distance
        // in older browsers and can cause scrolling to be slower than native.
        // Turn this off by setting $.event.special.mousewheel.settings.adjustOldDeltas to false.
        return (
          special.settings.adjustOldDeltas &&
          orgEvent.type === "mousewheel" &&
          absDelta % 120 === 0
        );
      }
    });

    S2.define(
      "jquery.select2",
      [
        "jquery",
        "jquery-mousewheel",

        "./select2/core",
        "./select2/defaults",
        "./select2/utils",
      ],
      function ($, _, Select2, Defaults, Utils) {
        if ($.fn.select2 == null) {
          // All methods that should return the element
          var thisMethods = ["open", "close", "destroy"];

          $.fn.select2 = function (options) {
            options = options || {};

            if (typeof options === "object") {
              this.each(function () {
                var instanceOptions = $.extend(true, {}, options);

                var instance = new Select2($(this), instanceOptions);
              });

              return this;
            } else if (typeof options === "string") {
              var ret;
              var args = Array.prototype.slice.call(arguments, 1);

              this.each(function () {
                var instance = Utils.GetData(this, "select2");

                if (instance == null && window.console && console.error) {
                  console.error(
                    "The select2('" +
                      options +
                      "') method was called on an " +
                      "element that is not using Select2."
                  );
                }

                ret = instance[options].apply(instance, args);
              });

              // Check if we should be returning `this`
              if ($.inArray(options, thisMethods) > -1) {
                return this;
              }

              return ret;
            } else {
              throw new Error("Invalid arguments for Select2: " + options);
            }
          };
        }

        if ($.fn.select2.defaults == null) {
          $.fn.select2.defaults = Defaults;
        }

        return Select2;
      }
    );

    // Return the AMD loader configuration so it can be used outside of this file
    return {
      define: S2.define,
      require: S2.require,
    };
  })();

  // Autoload the jQuery bindings
  // We know that all of the modules exist above this, so we're safe
  var select2 = S2.require("jquery.select2");

  // Hold the AMD module references on the jQuery function that was just loaded
  // This allows Select2 to use the internal loader outside of this file, such
  // as in the language files.
  jQuery.fn.select2.amd = S2;

  // Return the Select2 instance for anyone who is importing it.
  return select2;
});

/*
     _ _      _       _
 ___| (_) ___| | __  (_)___
/ __| | |/ __| |/ /  | / __|
\__ \ | | (__|   < _ | \__ \
|___/_|_|\___|_|\_(_)/ |___/
                   |__/

 Version: 1.8.0
  Author: Ken Wheeler
 Website: http://kenwheeler.github.io
    Docs: http://kenwheeler.github.io/slick
    Repo: http://github.com/kenwheeler/slick
  Issues: http://github.com/kenwheeler/slick/issues

 */
/* global window, document, define, jQuery, setInterval, clearInterval */
(function (factory) {
  "use strict";
  if (typeof define === "function" && define.amd) {
    define(["jquery"], factory);
  } else if (typeof exports !== "undefined") {
    module.exports = factory(require("jquery"));
  } else {
    factory(jQuery);
  }
})(function ($) {
  "use strict";
  var Slick = window.Slick || {};

  Slick = (function () {
    var instanceUid = 0;

    function Slick(element, settings) {
      var _ = this,
        dataSettings;

      _.defaults = {
        accessibility: true,
        adaptiveHeight: false,
        appendArrows: $(element),
        appendDots: $(element),
        arrows: true,
        asNavFor: null,
        prevArrow:
          '<button class="slick-prev" aria-label="Previous" type="button">Previous</button>',
        nextArrow:
          '<button class="slick-next" aria-label="Next" type="button">Next</button>',
        autoplay: false,
        autoplaySpeed: 3000,
        centerMode: false,
        centerPadding: "50px",
        cssEase: "ease",
        customPaging: function (slider, i) {
          return $('<button type="button" />').text(i + 1);
        },
        dots: false,
        dotsClass: "slick-dots",
        draggable: true,
        easing: "linear",
        edgeFriction: 0.35,
        fade: false,
        focusOnSelect: false,
        focusOnChange: false,
        infinite: true,
        initialSlide: 0,
        lazyLoad: "ondemand",
        mobileFirst: false,
        pauseOnHover: true,
        pauseOnFocus: true,
        pauseOnDotsHover: false,
        respondTo: "window",
        responsive: null,
        rows: 1,
        rtl: false,
        slide: "",
        slidesPerRow: 1,
        slidesToShow: 1,
        slidesToScroll: 1,
        speed: 500,
        swipe: true,
        swipeToSlide: false,
        touchMove: true,
        touchThreshold: 5,
        useCSS: true,
        useTransform: true,
        variableWidth: false,
        vertical: false,
        verticalSwiping: false,
        waitForAnimate: true,
        zIndex: 1000,
      };

      _.initials = {
        animating: false,
        dragging: false,
        autoPlayTimer: null,
        currentDirection: 0,
        currentLeft: null,
        currentSlide: 0,
        direction: 1,
        $dots: null,
        listWidth: null,
        listHeight: null,
        loadIndex: 0,
        $nextArrow: null,
        $prevArrow: null,
        scrolling: false,
        slideCount: null,
        slideWidth: null,
        $slideTrack: null,
        $slides: null,
        sliding: false,
        slideOffset: 0,
        swipeLeft: null,
        swiping: false,
        $list: null,
        touchObject: {},
        transformsEnabled: false,
        unslicked: false,
      };

      $.extend(_, _.initials);

      _.activeBreakpoint = null;
      _.animType = null;
      _.animProp = null;
      _.breakpoints = [];
      _.breakpointSettings = [];
      _.cssTransitions = false;
      _.focussed = false;
      _.interrupted = false;
      _.hidden = "hidden";
      _.paused = true;
      _.positionProp = null;
      _.respondTo = null;
      _.rowCount = 1;
      _.shouldClick = true;
      _.$slider = $(element);
      _.$slidesCache = null;
      _.transformType = null;
      _.transitionType = null;
      _.visibilityChange = "visibilitychange";
      _.windowWidth = 0;
      _.windowTimer = null;

      dataSettings = $(element).data("slick") || {};

      _.options = $.extend({}, _.defaults, settings, dataSettings);

      _.currentSlide = _.options.initialSlide;

      _.originalSettings = _.options;

      if (typeof document.mozHidden !== "undefined") {
        _.hidden = "mozHidden";
        _.visibilityChange = "mozvisibilitychange";
      } else if (typeof document.webkitHidden !== "undefined") {
        _.hidden = "webkitHidden";
        _.visibilityChange = "webkitvisibilitychange";
      }

      _.autoPlay = $.proxy(_.autoPlay, _);
      _.autoPlayClear = $.proxy(_.autoPlayClear, _);
      _.autoPlayIterator = $.proxy(_.autoPlayIterator, _);
      _.changeSlide = $.proxy(_.changeSlide, _);
      _.clickHandler = $.proxy(_.clickHandler, _);
      _.selectHandler = $.proxy(_.selectHandler, _);
      _.setPosition = $.proxy(_.setPosition, _);
      _.swipeHandler = $.proxy(_.swipeHandler, _);
      _.dragHandler = $.proxy(_.dragHandler, _);
      _.keyHandler = $.proxy(_.keyHandler, _);

      _.instanceUid = instanceUid++;

      // A simple way to check for HTML strings
      // Strict HTML recognition (must start with <)
      // Extracted from jQuery v1.11 source
      _.htmlExpr = /^(?:\s*(<[\w\W]+>)[^>]*)$/;

      _.registerBreakpoints();
      _.init(true);
    }

    return Slick;
  })();

  Slick.prototype.activateADA = function () {
    var _ = this;

    _.$slideTrack
      .find(".slick-active")
      .attr({
        "aria-hidden": "false",
      })
      .find("a, input, button, select")
      .attr({
        tabindex: "0",
      });
  };

  Slick.prototype.addSlide = Slick.prototype.slickAdd = function (
    markup,
    index,
    addBefore
  ) {
    var _ = this;

    if (typeof index === "boolean") {
      addBefore = index;
      index = null;
    } else if (index < 0 || index >= _.slideCount) {
      return false;
    }

    _.unload();

    if (typeof index === "number") {
      if (index === 0 && _.$slides.length === 0) {
        $(markup).appendTo(_.$slideTrack);
      } else if (addBefore) {
        $(markup).insertBefore(_.$slides.eq(index));
      } else {
        $(markup).insertAfter(_.$slides.eq(index));
      }
    } else {
      if (addBefore === true) {
        $(markup).prependTo(_.$slideTrack);
      } else {
        $(markup).appendTo(_.$slideTrack);
      }
    }

    _.$slides = _.$slideTrack.children(this.options.slide);

    _.$slideTrack.children(this.options.slide).detach();

    _.$slideTrack.append(_.$slides);

    _.$slides.each(function (index, element) {
      $(element).attr("data-slick-index", index);
    });

    _.$slidesCache = _.$slides;

    _.reinit();
  };

  Slick.prototype.animateHeight = function () {
    var _ = this;
    if (
      _.options.slidesToShow === 1 &&
      _.options.adaptiveHeight === true &&
      _.options.vertical === false
    ) {
      var targetHeight = _.$slides.eq(_.currentSlide).outerHeight(true);
      _.$list.animate(
        {
          height: targetHeight,
        },
        _.options.speed
      );
    }
  };

  Slick.prototype.animateSlide = function (targetLeft, callback) {
    var animProps = {},
      _ = this;

    _.animateHeight();

    if (_.options.rtl === true && _.options.vertical === false) {
      targetLeft = -targetLeft;
    }
    if (_.transformsEnabled === false) {
      if (_.options.vertical === false) {
        _.$slideTrack.animate(
          {
            left: targetLeft,
          },
          _.options.speed,
          _.options.easing,
          callback
        );
      } else {
        _.$slideTrack.animate(
          {
            top: targetLeft,
          },
          _.options.speed,
          _.options.easing,
          callback
        );
      }
    } else {
      if (_.cssTransitions === false) {
        if (_.options.rtl === true) {
          _.currentLeft = -_.currentLeft;
        }
        $({
          animStart: _.currentLeft,
        }).animate(
          {
            animStart: targetLeft,
          },
          {
            duration: _.options.speed,
            easing: _.options.easing,
            step: function (now) {
              now = Math.ceil(now);
              if (_.options.vertical === false) {
                animProps[_.animType] = "translate(" + now + "px, 0px)";
                _.$slideTrack.css(animProps);
              } else {
                animProps[_.animType] = "translate(0px," + now + "px)";
                _.$slideTrack.css(animProps);
              }
            },
            complete: function () {
              if (callback) {
                callback.call();
              }
            },
          }
        );
      } else {
        _.applyTransition();
        targetLeft = Math.ceil(targetLeft);

        if (_.options.vertical === false) {
          animProps[_.animType] = "translate3d(" + targetLeft + "px, 0px, 0px)";
        } else {
          animProps[_.animType] = "translate3d(0px," + targetLeft + "px, 0px)";
        }
        _.$slideTrack.css(animProps);

        if (callback) {
          setTimeout(function () {
            _.disableTransition();

            callback.call();
          }, _.options.speed);
        }
      }
    }
  };

  Slick.prototype.getNavTarget = function () {
    var _ = this,
      asNavFor = _.options.asNavFor;

    if (asNavFor && asNavFor !== null) {
      asNavFor = $(asNavFor).not(_.$slider);
    }

    return asNavFor;
  };

  Slick.prototype.asNavFor = function (index) {
    var _ = this,
      asNavFor = _.getNavTarget();

    if (asNavFor !== null && typeof asNavFor === "object") {
      asNavFor.each(function () {
        var target = $(this).slick("getSlick");
        if (!target.unslicked) {
          target.slideHandler(index, true);
        }
      });
    }
  };

  Slick.prototype.applyTransition = function (slide) {
    var _ = this,
      transition = {};

    if (_.options.fade === false) {
      transition[_.transitionType] =
        _.transformType + " " + _.options.speed + "ms " + _.options.cssEase;
    } else {
      transition[_.transitionType] =
        "opacity " + _.options.speed + "ms " + _.options.cssEase;
    }

    if (_.options.fade === false) {
      _.$slideTrack.css(transition);
    } else {
      _.$slides.eq(slide).css(transition);
    }
  };

  Slick.prototype.autoPlay = function () {
    var _ = this;

    _.autoPlayClear();

    if (_.slideCount > _.options.slidesToShow) {
      _.autoPlayTimer = setInterval(
        _.autoPlayIterator,
        _.options.autoplaySpeed
      );
    }
  };

  Slick.prototype.autoPlayClear = function () {
    var _ = this;

    if (_.autoPlayTimer) {
      clearInterval(_.autoPlayTimer);
    }
  };

  Slick.prototype.autoPlayIterator = function () {
    var _ = this,
      slideTo = _.currentSlide + _.options.slidesToScroll;

    if (!_.paused && !_.interrupted && !_.focussed) {
      if (_.options.infinite === false) {
        if (_.direction === 1 && _.currentSlide + 1 === _.slideCount - 1) {
          _.direction = 0;
        } else if (_.direction === 0) {
          slideTo = _.currentSlide - _.options.slidesToScroll;

          if (_.currentSlide - 1 === 0) {
            _.direction = 1;
          }
        }
      }

      _.slideHandler(slideTo);
    }
  };

  Slick.prototype.buildArrows = function () {
    var _ = this;

    if (_.options.arrows === true) {
      _.$prevArrow = $(_.options.prevArrow).addClass("slick-arrow");
      _.$nextArrow = $(_.options.nextArrow).addClass("slick-arrow");

      if (_.slideCount > _.options.slidesToShow) {
        _.$prevArrow
          .removeClass("slick-hidden")
          .removeAttr("aria-hidden tabindex");
        _.$nextArrow
          .removeClass("slick-hidden")
          .removeAttr("aria-hidden tabindex");

        if (_.htmlExpr.test(_.options.prevArrow)) {
          _.$prevArrow.prependTo(_.options.appendArrows);
        }

        if (_.htmlExpr.test(_.options.nextArrow)) {
          _.$nextArrow.appendTo(_.options.appendArrows);
        }

        if (_.options.infinite !== true) {
          _.$prevArrow.addClass("slick-disabled").attr("aria-disabled", "true");
        }
      } else {
        _.$prevArrow
          .add(_.$nextArrow)

          .addClass("slick-hidden")
          .attr({
            "aria-disabled": "true",
            tabindex: "-1",
          });
      }
    }
  };

  Slick.prototype.buildDots = function () {
    var _ = this,
      i,
      dot;

    if (_.options.dots === true && _.slideCount > _.options.slidesToShow) {
      _.$slider.addClass("slick-dotted");

      dot = $("<ul />").addClass(_.options.dotsClass);

      for (i = 0; i <= _.getDotCount(); i += 1) {
        dot.append($("<li />").append(_.options.customPaging.call(this, _, i)));
      }

      _.$dots = dot.appendTo(_.options.appendDots);

      _.$dots.find("li").first().addClass("slick-active");
    }
  };

  Slick.prototype.buildOut = function () {
    var _ = this;

    _.$slides = _.$slider
      .children(_.options.slide + ":not(.slick-cloned)")
      .addClass("slick-slide");

    _.slideCount = _.$slides.length;

    _.$slides.each(function (index, element) {
      $(element)
        .attr("data-slick-index", index)
        .data("originalStyling", $(element).attr("style") || "");
    });

    _.$slider.addClass("slick-slider");

    _.$slideTrack =
      _.slideCount === 0
        ? $('<div class="slick-track"/>').appendTo(_.$slider)
        : _.$slides.wrapAll('<div class="slick-track"/>').parent();

    _.$list = _.$slideTrack.wrap('<div class="slick-list"/>').parent();
    _.$slideTrack.css("opacity", 0);

    if (_.options.centerMode === true || _.options.swipeToSlide === true) {
      _.options.slidesToScroll = 1;
    }

    $("img[data-lazy]", _.$slider).not("[src]").addClass("slick-loading");

    _.setupInfinite();

    _.buildArrows();

    _.buildDots();

    _.updateDots();

    _.setSlideClasses(typeof _.currentSlide === "number" ? _.currentSlide : 0);

    if (_.options.draggable === true) {
      _.$list.addClass("draggable");
    }
  };

  Slick.prototype.buildRows = function () {
    var _ = this,
      a,
      b,
      c,
      newSlides,
      numOfSlides,
      originalSlides,
      slidesPerSection;

    newSlides = document.createDocumentFragment();
    originalSlides = _.$slider.children();

    if (_.options.rows > 0) {
      slidesPerSection = _.options.slidesPerRow * _.options.rows;
      numOfSlides = Math.ceil(originalSlides.length / slidesPerSection);

      for (a = 0; a < numOfSlides; a++) {
        var slide = document.createElement("div");
        for (b = 0; b < _.options.rows; b++) {
          var row = document.createElement("div");
          for (c = 0; c < _.options.slidesPerRow; c++) {
            var target =
              a * slidesPerSection + (b * _.options.slidesPerRow + c);
            if (originalSlides.get(target)) {
              row.appendChild(originalSlides.get(target));
            }
          }
          slide.appendChild(row);
        }
        newSlides.appendChild(slide);
      }

      _.$slider.empty().append(newSlides);
      _.$slider
        .children()
        .children()
        .children()
        .css({
          width: 100 / _.options.slidesPerRow + "%",
          display: "inline-block",
        });
    }
  };

  Slick.prototype.checkResponsive = function (initial, forceUpdate) {
    var _ = this,
      breakpoint,
      targetBreakpoint,
      respondToWidth,
      triggerBreakpoint = false;
    var sliderWidth = _.$slider.width();
    var windowWidth = window.innerWidth || $(window).width();

    if (_.respondTo === "window") {
      respondToWidth = windowWidth;
    } else if (_.respondTo === "slider") {
      respondToWidth = sliderWidth;
    } else if (_.respondTo === "min") {
      respondToWidth = Math.min(windowWidth, sliderWidth);
    }

    if (
      _.options.responsive &&
      _.options.responsive.length &&
      _.options.responsive !== null
    ) {
      targetBreakpoint = null;

      for (breakpoint in _.breakpoints) {
        if (_.breakpoints.hasOwnProperty(breakpoint)) {
          if (_.originalSettings.mobileFirst === false) {
            if (respondToWidth < _.breakpoints[breakpoint]) {
              targetBreakpoint = _.breakpoints[breakpoint];
            }
          } else {
            if (respondToWidth > _.breakpoints[breakpoint]) {
              targetBreakpoint = _.breakpoints[breakpoint];
            }
          }
        }
      }

      if (targetBreakpoint !== null) {
        if (_.activeBreakpoint !== null) {
          if (targetBreakpoint !== _.activeBreakpoint || forceUpdate) {
            _.activeBreakpoint = targetBreakpoint;
            if (_.breakpointSettings[targetBreakpoint] === "unslick") {
              _.unslick(targetBreakpoint);
            } else {
              _.options = $.extend(
                {},
                _.originalSettings,
                _.breakpointSettings[targetBreakpoint]
              );
              if (initial === true) {
                _.currentSlide = _.options.initialSlide;
              }
              _.refresh(initial);
            }
            triggerBreakpoint = targetBreakpoint;
          }
        } else {
          _.activeBreakpoint = targetBreakpoint;
          if (_.breakpointSettings[targetBreakpoint] === "unslick") {
            _.unslick(targetBreakpoint);
          } else {
            _.options = $.extend(
              {},
              _.originalSettings,
              _.breakpointSettings[targetBreakpoint]
            );
            if (initial === true) {
              _.currentSlide = _.options.initialSlide;
            }
            _.refresh(initial);
          }
          triggerBreakpoint = targetBreakpoint;
        }
      } else {
        if (_.activeBreakpoint !== null) {
          _.activeBreakpoint = null;
          _.options = _.originalSettings;
          if (initial === true) {
            _.currentSlide = _.options.initialSlide;
          }
          _.refresh(initial);
          triggerBreakpoint = targetBreakpoint;
        }
      }

      // only trigger breakpoints during an actual break. not on initialize.
      if (!initial && triggerBreakpoint !== false) {
        _.$slider.trigger("breakpoint", [_, triggerBreakpoint]);
      }
    }
  };

  Slick.prototype.changeSlide = function (event, dontAnimate) {
    var _ = this,
      $target = $(event.currentTarget),
      indexOffset,
      slideOffset,
      unevenOffset;

    // If target is a link, prevent default action.
    if ($target.is("a")) {
      event.preventDefault();
    }

    // If target is not the <li> element (ie: a child), find the <li>.
    if (!$target.is("li")) {
      $target = $target.closest("li");
    }

    unevenOffset = _.slideCount % _.options.slidesToScroll !== 0;
    indexOffset = unevenOffset
      ? 0
      : (_.slideCount - _.currentSlide) % _.options.slidesToScroll;

    switch (event.data.message) {
      case "previous":
        slideOffset =
          indexOffset === 0
            ? _.options.slidesToScroll
            : _.options.slidesToShow - indexOffset;
        if (_.slideCount > _.options.slidesToShow) {
          _.slideHandler(_.currentSlide - slideOffset, false, dontAnimate);
        }
        break;

      case "next":
        slideOffset =
          indexOffset === 0 ? _.options.slidesToScroll : indexOffset;
        if (_.slideCount > _.options.slidesToShow) {
          _.slideHandler(_.currentSlide + slideOffset, false, dontAnimate);
        }
        break;

      case "index":
        var index =
          event.data.index === 0
            ? 0
            : event.data.index || $target.index() * _.options.slidesToScroll;

        _.slideHandler(_.checkNavigable(index), false, dontAnimate);
        $target.children().trigger("focus");
        break;

      default:
        return;
    }
  };

  Slick.prototype.checkNavigable = function (index) {
    var _ = this,
      navigables,
      prevNavigable;

    navigables = _.getNavigableIndexes();
    prevNavigable = 0;
    if (index > navigables[navigables.length - 1]) {
      index = navigables[navigables.length - 1];
    } else {
      for (var n in navigables) {
        if (index < navigables[n]) {
          index = prevNavigable;
          break;
        }
        prevNavigable = navigables[n];
      }
    }

    return index;
  };

  Slick.prototype.cleanUpEvents = function () {
    var _ = this;

    if (_.options.dots && _.$dots !== null) {
      $("li", _.$dots)
        .off("click.slick", _.changeSlide)
        .off("mouseenter.slick", $.proxy(_.interrupt, _, true))
        .off("mouseleave.slick", $.proxy(_.interrupt, _, false));

      if (_.options.accessibility === true) {
        _.$dots.off("keydown.slick", _.keyHandler);
      }
    }

    _.$slider.off("focus.slick blur.slick");

    if (_.options.arrows === true && _.slideCount > _.options.slidesToShow) {
      _.$prevArrow && _.$prevArrow.off("click.slick", _.changeSlide);
      _.$nextArrow && _.$nextArrow.off("click.slick", _.changeSlide);

      if (_.options.accessibility === true) {
        _.$prevArrow && _.$prevArrow.off("keydown.slick", _.keyHandler);
        _.$nextArrow && _.$nextArrow.off("keydown.slick", _.keyHandler);
      }
    }

    _.$list.off("touchstart.slick mousedown.slick", _.swipeHandler);
    _.$list.off("touchmove.slick mousemove.slick", _.swipeHandler);
    _.$list.off("touchend.slick mouseup.slick", _.swipeHandler);
    _.$list.off("touchcancel.slick mouseleave.slick", _.swipeHandler);

    _.$list.off("click.slick", _.clickHandler);

    $(document).off(_.visibilityChange, _.visibility);

    _.cleanUpSlideEvents();

    if (_.options.accessibility === true) {
      _.$list.off("keydown.slick", _.keyHandler);
    }

    if (_.options.focusOnSelect === true) {
      $(_.$slideTrack).children().off("click.slick", _.selectHandler);
    }

    $(window).off(
      "orientationchange.slick.slick-" + _.instanceUid,
      _.orientationChange
    );

    $(window).off("resize.slick.slick-" + _.instanceUid, _.resize);

    $("[draggable!=true]", _.$slideTrack).off("dragstart", _.preventDefault);

    $(window).off("load.slick.slick-" + _.instanceUid, _.setPosition);
  };

  Slick.prototype.cleanUpSlideEvents = function () {
    var _ = this;

    _.$list.off("mouseenter.slick", $.proxy(_.interrupt, _, true));
    _.$list.off("mouseleave.slick", $.proxy(_.interrupt, _, false));
  };

  Slick.prototype.cleanUpRows = function () {
    var _ = this,
      originalSlides;

    if (_.options.rows > 0) {
      originalSlides = _.$slides.children().children();
      originalSlides.removeAttr("style");
      _.$slider.empty().append(originalSlides);
    }
  };

  Slick.prototype.clickHandler = function (event) {
    var _ = this;

    if (_.shouldClick === false) {
      event.stopImmediatePropagation();
      event.stopPropagation();
      event.preventDefault();
    }
  };

  Slick.prototype.destroy = function (refresh) {
    var _ = this;

    _.autoPlayClear();

    _.touchObject = {};

    _.cleanUpEvents();

    $(".slick-cloned", _.$slider).detach();

    if (_.$dots) {
      _.$dots.remove();
    }

    if (_.$prevArrow && _.$prevArrow.length) {
      _.$prevArrow
        .removeClass("slick-disabled slick-arrow slick-hidden")
        .removeAttr("aria-hidden aria-disabled tabindex")
        .css("display", "");

      if (_.htmlExpr.test(_.options.prevArrow)) {
        _.$prevArrow.remove();
      }
    }

    if (_.$nextArrow && _.$nextArrow.length) {
      _.$nextArrow
        .removeClass("slick-disabled slick-arrow slick-hidden")
        .removeAttr("aria-hidden aria-disabled tabindex")
        .css("display", "");

      if (_.htmlExpr.test(_.options.nextArrow)) {
        _.$nextArrow.remove();
      }
    }

    if (_.$slides) {
      _.$slides
        .removeClass(
          "slick-slide slick-active slick-center slick-visible slick-current"
        )
        .removeAttr("aria-hidden")
        .removeAttr("data-slick-index")
        .each(function () {
          $(this).attr("style", $(this).data("originalStyling"));
        });

      _.$slideTrack.children(this.options.slide).detach();

      _.$slideTrack.detach();

      _.$list.detach();

      _.$slider.append(_.$slides);
    }

    _.cleanUpRows();

    _.$slider.removeClass("slick-slider");
    _.$slider.removeClass("slick-initialized");
    _.$slider.removeClass("slick-dotted");

    _.unslicked = true;

    if (!refresh) {
      _.$slider.trigger("destroy", [_]);
    }
  };

  Slick.prototype.disableTransition = function (slide) {
    var _ = this,
      transition = {};

    transition[_.transitionType] = "";

    if (_.options.fade === false) {
      _.$slideTrack.css(transition);
    } else {
      _.$slides.eq(slide).css(transition);
    }
  };

  Slick.prototype.fadeSlide = function (slideIndex, callback) {
    var _ = this;

    if (_.cssTransitions === false) {
      _.$slides.eq(slideIndex).css({
        zIndex: _.options.zIndex,
      });

      _.$slides.eq(slideIndex).animate(
        {
          opacity: 1,
        },
        _.options.speed,
        _.options.easing,
        callback
      );
    } else {
      _.applyTransition(slideIndex);

      _.$slides.eq(slideIndex).css({
        opacity: 1,
        zIndex: _.options.zIndex,
      });

      if (callback) {
        setTimeout(function () {
          _.disableTransition(slideIndex);

          callback.call();
        }, _.options.speed);
      }
    }
  };

  Slick.prototype.fadeSlideOut = function (slideIndex) {
    var _ = this;

    if (_.cssTransitions === false) {
      _.$slides.eq(slideIndex).animate(
        {
          opacity: 0,
          zIndex: _.options.zIndex - 2,
        },
        _.options.speed,
        _.options.easing
      );
    } else {
      _.applyTransition(slideIndex);

      _.$slides.eq(slideIndex).css({
        opacity: 0,
        zIndex: _.options.zIndex - 2,
      });
    }
  };

  Slick.prototype.filterSlides = Slick.prototype.slickFilter = function (
    filter
  ) {
    var _ = this;

    if (filter !== null) {
      _.$slidesCache = _.$slides;

      _.unload();

      _.$slideTrack.children(this.options.slide).detach();

      _.$slidesCache.filter(filter).appendTo(_.$slideTrack);

      _.reinit();
    }
  };

  Slick.prototype.focusHandler = function () {
    var _ = this;

    _.$slider
      .off("focus.slick blur.slick")
      .on("focus.slick blur.slick", "*", function (event) {
        event.stopImmediatePropagation();
        var $sf = $(this);

        setTimeout(function () {
          if (_.options.pauseOnFocus) {
            _.focussed = $sf.is(":focus");
            _.autoPlay();
          }
        }, 0);
      });
  };

  Slick.prototype.getCurrent = Slick.prototype.slickCurrentSlide = function () {
    var _ = this;
    return _.currentSlide;
  };

  Slick.prototype.getDotCount = function () {
    var _ = this;

    var breakPoint = 0;
    var counter = 0;
    var pagerQty = 0;

    if (_.options.infinite === true) {
      if (_.slideCount <= _.options.slidesToShow) {
        ++pagerQty;
      } else {
        while (breakPoint < _.slideCount) {
          ++pagerQty;
          breakPoint = counter + _.options.slidesToScroll;
          counter +=
            _.options.slidesToScroll <= _.options.slidesToShow
              ? _.options.slidesToScroll
              : _.options.slidesToShow;
        }
      }
    } else if (_.options.centerMode === true) {
      pagerQty = _.slideCount;
    } else if (!_.options.asNavFor) {
      pagerQty =
        1 +
        Math.ceil(
          (_.slideCount - _.options.slidesToShow) / _.options.slidesToScroll
        );
    } else {
      while (breakPoint < _.slideCount) {
        ++pagerQty;
        breakPoint = counter + _.options.slidesToScroll;
        counter +=
          _.options.slidesToScroll <= _.options.slidesToShow
            ? _.options.slidesToScroll
            : _.options.slidesToShow;
      }
    }

    return pagerQty - 1;
  };

  Slick.prototype.getLeft = function (slideIndex) {
    var _ = this,
      targetLeft,
      verticalHeight,
      verticalOffset = 0,
      targetSlide,
      coef;

    _.slideOffset = 0;
    verticalHeight = _.$slides.first().outerHeight(true);

    if (_.options.infinite === true) {
      if (_.slideCount > _.options.slidesToShow) {
        _.slideOffset = _.slideWidth * _.options.slidesToShow * -1;
        coef = -1;

        if (_.options.vertical === true && _.options.centerMode === true) {
          if (_.options.slidesToShow === 2) {
            coef = -1.5;
          } else if (_.options.slidesToShow === 1) {
            coef = -2;
          }
        }
        verticalOffset = verticalHeight * _.options.slidesToShow * coef;
      }
      if (_.slideCount % _.options.slidesToScroll !== 0) {
        if (
          slideIndex + _.options.slidesToScroll > _.slideCount &&
          _.slideCount > _.options.slidesToShow
        ) {
          if (slideIndex > _.slideCount) {
            _.slideOffset =
              (_.options.slidesToShow - (slideIndex - _.slideCount)) *
              _.slideWidth *
              -1;
            verticalOffset =
              (_.options.slidesToShow - (slideIndex - _.slideCount)) *
              verticalHeight *
              -1;
          } else {
            _.slideOffset =
              (_.slideCount % _.options.slidesToScroll) * _.slideWidth * -1;
            verticalOffset =
              (_.slideCount % _.options.slidesToScroll) * verticalHeight * -1;
          }
        }
      }
    } else {
      if (slideIndex + _.options.slidesToShow > _.slideCount) {
        _.slideOffset =
          (slideIndex + _.options.slidesToShow - _.slideCount) * _.slideWidth;
        verticalOffset =
          (slideIndex + _.options.slidesToShow - _.slideCount) * verticalHeight;
      }
    }

    if (_.slideCount <= _.options.slidesToShow) {
      _.slideOffset = 0;
      verticalOffset = 0;
    }

    if (
      _.options.centerMode === true &&
      _.slideCount <= _.options.slidesToShow
    ) {
      _.slideOffset =
        (_.slideWidth * Math.floor(_.options.slidesToShow)) / 2 -
        (_.slideWidth * _.slideCount) / 2;
    } else if (_.options.centerMode === true && _.options.infinite === true) {
      _.slideOffset +=
        _.slideWidth * Math.floor(_.options.slidesToShow / 2) - _.slideWidth;
    } else if (_.options.centerMode === true) {
      _.slideOffset = 0;
      _.slideOffset += _.slideWidth * Math.floor(_.options.slidesToShow / 2);
    }

    if (_.options.vertical === false) {
      targetLeft = slideIndex * _.slideWidth * -1 + _.slideOffset;
    } else {
      targetLeft = slideIndex * verticalHeight * -1 + verticalOffset;
    }

    if (_.options.variableWidth === true) {
      if (
        _.slideCount <= _.options.slidesToShow ||
        _.options.infinite === false
      ) {
        targetSlide = _.$slideTrack.children(".slick-slide").eq(slideIndex);
      } else {
        targetSlide = _.$slideTrack
          .children(".slick-slide")
          .eq(slideIndex + _.options.slidesToShow);
      }

      if (_.options.rtl === true) {
        if (targetSlide[0]) {
          targetLeft =
            (_.$slideTrack.width() -
              targetSlide[0].offsetLeft -
              targetSlide.width()) *
            -1;
        } else {
          targetLeft = 0;
        }
      } else {
        targetLeft = targetSlide[0] ? targetSlide[0].offsetLeft * -1 : 0;
      }

      if (_.options.centerMode === true) {
        if (
          _.slideCount <= _.options.slidesToShow ||
          _.options.infinite === false
        ) {
          targetSlide = _.$slideTrack.children(".slick-slide").eq(slideIndex);
        } else {
          targetSlide = _.$slideTrack
            .children(".slick-slide")
            .eq(slideIndex + _.options.slidesToShow + 1);
        }

        if (_.options.rtl === true) {
          if (targetSlide[0]) {
            targetLeft =
              (_.$slideTrack.width() -
                targetSlide[0].offsetLeft -
                targetSlide.width()) *
              -1;
          } else {
            targetLeft = 0;
          }
        } else {
          targetLeft = targetSlide[0] ? targetSlide[0].offsetLeft * -1 : 0;
        }

        targetLeft += (_.$list.width() - targetSlide.outerWidth()) / 2;
      }
    }

    return targetLeft;
  };

  Slick.prototype.getOption = Slick.prototype.slickGetOption = function (
    option
  ) {
    var _ = this;

    return _.options[option];
  };

  Slick.prototype.getNavigableIndexes = function () {
    var _ = this,
      breakPoint = 0,
      counter = 0,
      indexes = [],
      max;

    if (_.options.infinite === false) {
      max = _.slideCount;
    } else {
      breakPoint = _.options.slidesToScroll * -1;
      counter = _.options.slidesToScroll * -1;
      max = _.slideCount * 2;
    }

    while (breakPoint < max) {
      indexes.push(breakPoint);
      breakPoint = counter + _.options.slidesToScroll;
      counter +=
        _.options.slidesToScroll <= _.options.slidesToShow
          ? _.options.slidesToScroll
          : _.options.slidesToShow;
    }

    return indexes;
  };

  Slick.prototype.getSlick = function () {
    return this;
  };

  Slick.prototype.getSlideCount = function () {
    var _ = this,
      slidesTraversed,
      swipedSlide,
      centerOffset;

    centerOffset =
      _.options.centerMode === true
        ? _.slideWidth * Math.floor(_.options.slidesToShow / 2)
        : 0;

    if (_.options.swipeToSlide === true) {
      _.$slideTrack.find(".slick-slide").each(function (index, slide) {
        if (
          slide.offsetLeft - centerOffset + $(slide).outerWidth() / 2 >
          _.swipeLeft * -1
        ) {
          swipedSlide = slide;
          return false;
        }
      });

      slidesTraversed =
        Math.abs($(swipedSlide).attr("data-slick-index") - _.currentSlide) || 1;

      return slidesTraversed;
    } else {
      return _.options.slidesToScroll;
    }
  };

  Slick.prototype.goTo = Slick.prototype.slickGoTo = function (
    slide,
    dontAnimate
  ) {
    var _ = this;

    _.changeSlide(
      {
        data: {
          message: "index",
          index: parseInt(slide),
        },
      },
      dontAnimate
    );
  };

  Slick.prototype.init = function (creation) {
    var _ = this;

    if (!$(_.$slider).hasClass("slick-initialized")) {
      $(_.$slider).addClass("slick-initialized");

      _.buildRows();
      _.buildOut();
      _.setProps();
      _.startLoad();
      _.loadSlider();
      _.initializeEvents();
      _.updateArrows();
      _.updateDots();
      _.checkResponsive(true);
      _.focusHandler();
    }

    if (creation) {
      _.$slider.trigger("init", [_]);
    }

    if (_.options.accessibility === true) {
      _.initADA();
    }

    if (_.options.autoplay) {
      _.paused = false;
      _.autoPlay();
    }
  };

  Slick.prototype.initADA = function () {
    var _ = this,
      numDotGroups = Math.ceil(_.slideCount / _.options.slidesToShow),
      tabControlIndexes = _.getNavigableIndexes().filter(function (val) {
        return val >= 0 && val < _.slideCount;
      });

    _.$slides
      .add(_.$slideTrack.find(".slick-cloned"))
      .attr({
        "aria-hidden": "true",
        tabindex: "-1",
      })
      .find("a, input, button, select")
      .attr({
        tabindex: "-1",
      });

    if (_.$dots !== null) {
      _.$slides.not(_.$slideTrack.find(".slick-cloned")).each(function (i) {
        var slideControlIndex = tabControlIndexes.indexOf(i);

        $(this).attr({
          role: "tabpanel",
          id: "slick-slide" + _.instanceUid + i,
          tabindex: -1,
        });

        if (slideControlIndex !== -1) {
          var ariaButtonControl =
            "slick-slide-control" + _.instanceUid + slideControlIndex;
          if ($("#" + ariaButtonControl).length) {
            $(this).attr({
              "aria-describedby": ariaButtonControl,
            });
          }
        }
      });

      _.$dots
        .attr("role", "tablist")
        .find("li")
        .each(function (i) {
          var mappedSlideIndex = tabControlIndexes[i];

          $(this).attr({
            role: "presentation",
          });

          $(this)
            .find("button")
            .first()
            .attr({
              role: "tab",
              id: "slick-slide-control" + _.instanceUid + i,
              "aria-controls": "slick-slide" + _.instanceUid + mappedSlideIndex,
              "aria-label": i + 1 + " of " + numDotGroups,
              "aria-selected": null,
              tabindex: "-1",
            });
        })
        .eq(_.currentSlide)
        .find("button")
        .attr({
          "aria-selected": "true",
          tabindex: "0",
        })
        .end();
    }

    for (
      var i = _.currentSlide, max = i + _.options.slidesToShow;
      i < max;
      i++
    ) {
      if (_.options.focusOnChange) {
        _.$slides.eq(i).attr({ tabindex: "0" });
      } else {
        _.$slides.eq(i).removeAttr("tabindex");
      }
    }

    _.activateADA();
  };

  Slick.prototype.initArrowEvents = function () {
    var _ = this;

    if (_.options.arrows === true && _.slideCount > _.options.slidesToShow) {
      _.$prevArrow.off("click.slick").on(
        "click.slick",
        {
          message: "previous",
        },
        _.changeSlide
      );
      _.$nextArrow.off("click.slick").on(
        "click.slick",
        {
          message: "next",
        },
        _.changeSlide
      );

      if (_.options.accessibility === true) {
        _.$prevArrow.on("keydown.slick", _.keyHandler);
        _.$nextArrow.on("keydown.slick", _.keyHandler);
      }
    }
  };

  Slick.prototype.initDotEvents = function () {
    var _ = this;

    if (_.options.dots === true && _.slideCount > _.options.slidesToShow) {
      $("li", _.$dots).on(
        "click.slick",
        {
          message: "index",
        },
        _.changeSlide
      );

      if (_.options.accessibility === true) {
        _.$dots.on("keydown.slick", _.keyHandler);
      }
    }

    if (
      _.options.dots === true &&
      _.options.pauseOnDotsHover === true &&
      _.slideCount > _.options.slidesToShow
    ) {
      $("li", _.$dots)
        .on("mouseenter.slick", $.proxy(_.interrupt, _, true))
        .on("mouseleave.slick", $.proxy(_.interrupt, _, false));
    }
  };

  Slick.prototype.initSlideEvents = function () {
    var _ = this;

    if (_.options.pauseOnHover) {
      _.$list.on("mouseenter.slick", $.proxy(_.interrupt, _, true));
      _.$list.on("mouseleave.slick", $.proxy(_.interrupt, _, false));
    }
  };

  Slick.prototype.initializeEvents = function () {
    var _ = this;

    _.initArrowEvents();

    _.initDotEvents();
    _.initSlideEvents();

    _.$list.on(
      "touchstart.slick mousedown.slick",
      {
        action: "start",
      },
      _.swipeHandler
    );
    _.$list.on(
      "touchmove.slick mousemove.slick",
      {
        action: "move",
      },
      _.swipeHandler
    );
    _.$list.on(
      "touchend.slick mouseup.slick",
      {
        action: "end",
      },
      _.swipeHandler
    );
    _.$list.on(
      "touchcancel.slick mouseleave.slick",
      {
        action: "end",
      },
      _.swipeHandler
    );

    _.$list.on("click.slick", _.clickHandler);

    $(document).on(_.visibilityChange, $.proxy(_.visibility, _));

    if (_.options.accessibility === true) {
      _.$list.on("keydown.slick", _.keyHandler);
    }

    if (_.options.focusOnSelect === true) {
      $(_.$slideTrack).children().on("click.slick", _.selectHandler);
    }

    $(window).on(
      "orientationchange.slick.slick-" + _.instanceUid,
      $.proxy(_.orientationChange, _)
    );

    $(window).on("resize.slick.slick-" + _.instanceUid, $.proxy(_.resize, _));

    $("[draggable!=true]", _.$slideTrack).on("dragstart", _.preventDefault);

    $(window).on("load.slick.slick-" + _.instanceUid, _.setPosition);
    $(_.setPosition);
  };

  Slick.prototype.initUI = function () {
    var _ = this;

    if (_.options.arrows === true && _.slideCount > _.options.slidesToShow) {
      _.$prevArrow.show();
      _.$nextArrow.show();
    }

    if (_.options.dots === true && _.slideCount > _.options.slidesToShow) {
      _.$dots.show();
    }
  };

  Slick.prototype.keyHandler = function (event) {
    var _ = this;
    //Dont slide if the cursor is inside the form fields and arrow keys are pressed
    if (!event.target.tagName.match("TEXTAREA|INPUT|SELECT")) {
      if (event.keyCode === 37 && _.options.accessibility === true) {
        _.changeSlide({
          data: {
            message: _.options.rtl === true ? "next" : "previous",
          },
        });
      } else if (event.keyCode === 39 && _.options.accessibility === true) {
        _.changeSlide({
          data: {
            message: _.options.rtl === true ? "previous" : "next",
          },
        });
      }
    }
  };

  Slick.prototype.lazyLoad = function () {
    var _ = this,
      loadRange,
      cloneRange,
      rangeStart,
      rangeEnd;

    function loadImages(imagesScope) {
      $("img[data-lazy]", imagesScope).each(function () {
        var image = $(this),
          imageSource = $(this).attr("data-lazy"),
          imageSrcSet = $(this).attr("data-srcset"),
          imageSizes =
            $(this).attr("data-sizes") || _.$slider.attr("data-sizes"),
          imageToLoad = document.createElement("img");

        imageToLoad.onload = function () {
          image.animate({ opacity: 0 }, 100, function () {
            if (imageSrcSet) {
              image.attr("srcset", imageSrcSet);

              if (imageSizes) {
                image.attr("sizes", imageSizes);
              }
            }

            image
              .attr("src", imageSource)
              .animate({ opacity: 1 }, 200, function () {
                image
                  .removeAttr("data-lazy data-srcset data-sizes")
                  .removeClass("slick-loading");
              });
            _.$slider.trigger("lazyLoaded", [_, image, imageSource]);
          });
        };

        imageToLoad.onerror = function () {
          image
            .removeAttr("data-lazy")
            .removeClass("slick-loading")
            .addClass("slick-lazyload-error");

          _.$slider.trigger("lazyLoadError", [_, image, imageSource]);
        };

        imageToLoad.src = imageSource;
      });
    }

    if (_.options.centerMode === true) {
      if (_.options.infinite === true) {
        rangeStart = _.currentSlide + (_.options.slidesToShow / 2 + 1);
        rangeEnd = rangeStart + _.options.slidesToShow + 2;
      } else {
        rangeStart = Math.max(
          0,
          _.currentSlide - (_.options.slidesToShow / 2 + 1)
        );
        rangeEnd = 2 + (_.options.slidesToShow / 2 + 1) + _.currentSlide;
      }
    } else {
      rangeStart = _.options.infinite
        ? _.options.slidesToShow + _.currentSlide
        : _.currentSlide;
      rangeEnd = Math.ceil(rangeStart + _.options.slidesToShow);
      if (_.options.fade === true) {
        if (rangeStart > 0) rangeStart--;
        if (rangeEnd <= _.slideCount) rangeEnd++;
      }
    }

    loadRange = _.$slider.find(".slick-slide").slice(rangeStart, rangeEnd);

    if (_.options.lazyLoad === "anticipated") {
      var prevSlide = rangeStart - 1,
        nextSlide = rangeEnd,
        $slides = _.$slider.find(".slick-slide");

      for (var i = 0; i < _.options.slidesToScroll; i++) {
        if (prevSlide < 0) prevSlide = _.slideCount - 1;
        loadRange = loadRange.add($slides.eq(prevSlide));
        loadRange = loadRange.add($slides.eq(nextSlide));
        prevSlide--;
        nextSlide++;
      }
    }

    loadImages(loadRange);

    if (_.slideCount <= _.options.slidesToShow) {
      cloneRange = _.$slider.find(".slick-slide");
      loadImages(cloneRange);
    } else if (_.currentSlide >= _.slideCount - _.options.slidesToShow) {
      cloneRange = _.$slider
        .find(".slick-cloned")
        .slice(0, _.options.slidesToShow);
      loadImages(cloneRange);
    } else if (_.currentSlide === 0) {
      cloneRange = _.$slider
        .find(".slick-cloned")
        .slice(_.options.slidesToShow * -1);
      loadImages(cloneRange);
    }
  };

  Slick.prototype.loadSlider = function () {
    var _ = this;

    _.setPosition();

    _.$slideTrack.css({
      opacity: 1,
    });

    _.$slider.removeClass("slick-loading");

    _.initUI();

    if (_.options.lazyLoad === "progressive") {
      _.progressiveLazyLoad();
    }
  };

  Slick.prototype.next = Slick.prototype.slickNext = function () {
    var _ = this;

    _.changeSlide({
      data: {
        message: "next",
      },
    });
  };

  Slick.prototype.orientationChange = function () {
    var _ = this;

    _.checkResponsive();
    _.setPosition();
  };

  Slick.prototype.pause = Slick.prototype.slickPause = function () {
    var _ = this;

    _.autoPlayClear();
    _.paused = true;
  };

  Slick.prototype.play = Slick.prototype.slickPlay = function () {
    var _ = this;

    _.autoPlay();
    _.options.autoplay = true;
    _.paused = false;
    _.focussed = false;
    _.interrupted = false;
  };

  Slick.prototype.postSlide = function (index) {
    var _ = this;

    if (!_.unslicked) {
      _.$slider.trigger("afterChange", [_, index]);

      _.animating = false;

      if (_.slideCount > _.options.slidesToShow) {
        _.setPosition();
      }

      _.swipeLeft = null;

      if (_.options.autoplay) {
        _.autoPlay();
      }

      if (_.options.accessibility === true) {
        _.initADA();

        if (_.options.focusOnChange) {
          var $currentSlide = $(_.$slides.get(_.currentSlide));
          $currentSlide.attr("tabindex", 0).focus();
        }
      }
    }
  };

  Slick.prototype.prev = Slick.prototype.slickPrev = function () {
    var _ = this;

    _.changeSlide({
      data: {
        message: "previous",
      },
    });
  };

  Slick.prototype.preventDefault = function (event) {
    event.preventDefault();
  };

  Slick.prototype.progressiveLazyLoad = function (tryCount) {
    tryCount = tryCount || 1;

    var _ = this,
      $imgsToLoad = $("img[data-lazy]", _.$slider),
      image,
      imageSource,
      imageSrcSet,
      imageSizes,
      imageToLoad;

    if ($imgsToLoad.length) {
      image = $imgsToLoad.first();
      imageSource = image.attr("data-lazy");
      imageSrcSet = image.attr("data-srcset");
      imageSizes = image.attr("data-sizes") || _.$slider.attr("data-sizes");
      imageToLoad = document.createElement("img");

      imageToLoad.onload = function () {
        if (imageSrcSet) {
          image.attr("srcset", imageSrcSet);

          if (imageSizes) {
            image.attr("sizes", imageSizes);
          }
        }

        image
          .attr("src", imageSource)
          .removeAttr("data-lazy data-srcset data-sizes")
          .removeClass("slick-loading");

        if (_.options.adaptiveHeight === true) {
          _.setPosition();
        }

        _.$slider.trigger("lazyLoaded", [_, image, imageSource]);
        _.progressiveLazyLoad();
      };

      imageToLoad.onerror = function () {
        if (tryCount < 3) {
          /**
           * try to load the image 3 times,
           * leave a slight delay so we don't get
           * servers blocking the request.
           */
          setTimeout(function () {
            _.progressiveLazyLoad(tryCount + 1);
          }, 500);
        } else {
          image
            .removeAttr("data-lazy")
            .removeClass("slick-loading")
            .addClass("slick-lazyload-error");

          _.$slider.trigger("lazyLoadError", [_, image, imageSource]);

          _.progressiveLazyLoad();
        }
      };

      imageToLoad.src = imageSource;
    } else {
      _.$slider.trigger("allImagesLoaded", [_]);
    }
  };

  Slick.prototype.refresh = function (initializing) {
    var _ = this,
      currentSlide,
      lastVisibleIndex;

    lastVisibleIndex = _.slideCount - _.options.slidesToShow;

    // in non-infinite sliders, we don't want to go past the
    // last visible index.
    if (!_.options.infinite && _.currentSlide > lastVisibleIndex) {
      _.currentSlide = lastVisibleIndex;
    }

    // if less slides than to show, go to start.
    if (_.slideCount <= _.options.slidesToShow) {
      _.currentSlide = 0;
    }

    currentSlide = _.currentSlide;

    _.destroy(true);

    $.extend(_, _.initials, { currentSlide: currentSlide });

    _.init();

    if (!initializing) {
      _.changeSlide(
        {
          data: {
            message: "index",
            index: currentSlide,
          },
        },
        false
      );
    }
  };

  Slick.prototype.registerBreakpoints = function () {
    var _ = this,
      breakpoint,
      currentBreakpoint,
      l,
      responsiveSettings = _.options.responsive || null;

    if ($.type(responsiveSettings) === "array" && responsiveSettings.length) {
      _.respondTo = _.options.respondTo || "window";

      for (breakpoint in responsiveSettings) {
        l = _.breakpoints.length - 1;

        if (responsiveSettings.hasOwnProperty(breakpoint)) {
          currentBreakpoint = responsiveSettings[breakpoint].breakpoint;

          // loop through the breakpoints and cut out any existing
          // ones with the same breakpoint number, we don't want dupes.
          while (l >= 0) {
            if (_.breakpoints[l] && _.breakpoints[l] === currentBreakpoint) {
              _.breakpoints.splice(l, 1);
            }
            l--;
          }

          _.breakpoints.push(currentBreakpoint);
          _.breakpointSettings[currentBreakpoint] =
            responsiveSettings[breakpoint].settings;
        }
      }

      _.breakpoints.sort(function (a, b) {
        return _.options.mobileFirst ? a - b : b - a;
      });
    }
  };

  Slick.prototype.reinit = function () {
    var _ = this;

    _.$slides = _.$slideTrack.children(_.options.slide).addClass("slick-slide");

    _.slideCount = _.$slides.length;

    if (_.currentSlide >= _.slideCount && _.currentSlide !== 0) {
      _.currentSlide = _.currentSlide - _.options.slidesToScroll;
    }

    if (_.slideCount <= _.options.slidesToShow) {
      _.currentSlide = 0;
    }

    _.registerBreakpoints();

    _.setProps();
    _.setupInfinite();
    _.buildArrows();
    _.updateArrows();
    _.initArrowEvents();
    _.buildDots();
    _.updateDots();
    _.initDotEvents();
    _.cleanUpSlideEvents();
    _.initSlideEvents();

    _.checkResponsive(false, true);

    if (_.options.focusOnSelect === true) {
      $(_.$slideTrack).children().on("click.slick", _.selectHandler);
    }

    _.setSlideClasses(typeof _.currentSlide === "number" ? _.currentSlide : 0);

    _.setPosition();
    _.focusHandler();

    _.paused = !_.options.autoplay;
    _.autoPlay();

    _.$slider.trigger("reInit", [_]);
  };

  Slick.prototype.resize = function () {
    var _ = this;

    if ($(window).width() !== _.windowWidth) {
      clearTimeout(_.windowDelay);
      _.windowDelay = window.setTimeout(function () {
        _.windowWidth = $(window).width();
        _.checkResponsive();
        if (!_.unslicked) {
          _.setPosition();
        }
      }, 50);
    }
  };

  Slick.prototype.removeSlide = Slick.prototype.slickRemove = function (
    index,
    removeBefore,
    removeAll
  ) {
    var _ = this;

    if (typeof index === "boolean") {
      removeBefore = index;
      index = removeBefore === true ? 0 : _.slideCount - 1;
    } else {
      index = removeBefore === true ? --index : index;
    }

    if (_.slideCount < 1 || index < 0 || index > _.slideCount - 1) {
      return false;
    }

    _.unload();

    if (removeAll === true) {
      _.$slideTrack.children().remove();
    } else {
      _.$slideTrack.children(this.options.slide).eq(index).remove();
    }

    _.$slides = _.$slideTrack.children(this.options.slide);

    _.$slideTrack.children(this.options.slide).detach();

    _.$slideTrack.append(_.$slides);

    _.$slidesCache = _.$slides;

    _.reinit();
  };

  Slick.prototype.setCSS = function (position) {
    var _ = this,
      positionProps = {},
      x,
      y;

    if (_.options.rtl === true) {
      position = -position;
    }
    x = _.positionProp == "left" ? Math.ceil(position) + "px" : "0px";
    y = _.positionProp == "top" ? Math.ceil(position) + "px" : "0px";

    positionProps[_.positionProp] = position;

    if (_.transformsEnabled === false) {
      _.$slideTrack.css(positionProps);
    } else {
      positionProps = {};
      if (_.cssTransitions === false) {
        positionProps[_.animType] = "translate(" + x + ", " + y + ")";
        _.$slideTrack.css(positionProps);
      } else {
        positionProps[_.animType] = "translate3d(" + x + ", " + y + ", 0px)";
        _.$slideTrack.css(positionProps);
      }
    }
  };

  Slick.prototype.setDimensions = function () {
    var _ = this;

    if (_.options.vertical === false) {
      if (_.options.centerMode === true) {
        _.$list.css({
          padding: "0px " + _.options.centerPadding,
        });
      }
    } else {
      _.$list.height(
        _.$slides.first().outerHeight(true) * _.options.slidesToShow
      );
      if (_.options.centerMode === true) {
        _.$list.css({
          padding: _.options.centerPadding + " 0px",
        });
      }
    }

    _.listWidth = _.$list.width();
    _.listHeight = _.$list.height();

    if (_.options.vertical === false && _.options.variableWidth === false) {
      _.slideWidth = Math.ceil(_.listWidth / _.options.slidesToShow);
      _.$slideTrack.width(
        Math.ceil(_.slideWidth * _.$slideTrack.children(".slick-slide").length)
      );
    } else if (_.options.variableWidth === true) {
      _.$slideTrack.width(5000 * _.slideCount);
    } else {
      _.slideWidth = Math.ceil(_.listWidth);
      _.$slideTrack.height(
        Math.ceil(
          _.$slides.first().outerHeight(true) *
            _.$slideTrack.children(".slick-slide").length
        )
      );
    }

    var offset = _.$slides.first().outerWidth(true) - _.$slides.first().width();
    if (_.options.variableWidth === false)
      _.$slideTrack.children(".slick-slide").width(_.slideWidth - offset);
  };

  Slick.prototype.setFade = function () {
    var _ = this,
      targetLeft;

    _.$slides.each(function (index, element) {
      targetLeft = _.slideWidth * index * -1;
      if (_.options.rtl === true) {
        $(element).css({
          position: "relative",
          right: targetLeft,
          top: 0,
          zIndex: _.options.zIndex - 2,
          opacity: 0,
        });
      } else {
        $(element).css({
          position: "relative",
          left: targetLeft,
          top: 0,
          zIndex: _.options.zIndex - 2,
          opacity: 0,
        });
      }
    });

    _.$slides.eq(_.currentSlide).css({
      zIndex: _.options.zIndex - 1,
      opacity: 1,
    });
  };

  Slick.prototype.setHeight = function () {
    var _ = this;

    if (
      _.options.slidesToShow === 1 &&
      _.options.adaptiveHeight === true &&
      _.options.vertical === false
    ) {
      var targetHeight = _.$slides.eq(_.currentSlide).outerHeight(true);
      _.$list.css("height", targetHeight);
    }
  };

  Slick.prototype.setOption = Slick.prototype.slickSetOption = function () {
    /**
     * accepts arguments in format of:
     *
     *  - for changing a single option's value:
     *     .slick("setOption", option, value, refresh )
     *
     *  - for changing a set of responsive options:
     *     .slick("setOption", 'responsive', [{}, ...], refresh )
     *
     *  - for updating multiple values at once (not responsive)
     *     .slick("setOption", { 'option': value, ... }, refresh )
     */

    var _ = this,
      l,
      item,
      option,
      value,
      refresh = false,
      type;

    if ($.type(arguments[0]) === "object") {
      option = arguments[0];
      refresh = arguments[1];
      type = "multiple";
    } else if ($.type(arguments[0]) === "string") {
      option = arguments[0];
      value = arguments[1];
      refresh = arguments[2];

      if (arguments[0] === "responsive" && $.type(arguments[1]) === "array") {
        type = "responsive";
      } else if (typeof arguments[1] !== "undefined") {
        type = "single";
      }
    }

    if (type === "single") {
      _.options[option] = value;
    } else if (type === "multiple") {
      $.each(option, function (opt, val) {
        _.options[opt] = val;
      });
    } else if (type === "responsive") {
      for (item in value) {
        if ($.type(_.options.responsive) !== "array") {
          _.options.responsive = [value[item]];
        } else {
          l = _.options.responsive.length - 1;

          // loop through the responsive object and splice out duplicates.
          while (l >= 0) {
            if (_.options.responsive[l].breakpoint === value[item].breakpoint) {
              _.options.responsive.splice(l, 1);
            }

            l--;
          }

          _.options.responsive.push(value[item]);
        }
      }
    }

    if (refresh) {
      _.unload();
      _.reinit();
    }
  };

  Slick.prototype.setPosition = function () {
    var _ = this;

    _.setDimensions();

    _.setHeight();

    if (_.options.fade === false) {
      _.setCSS(_.getLeft(_.currentSlide));
    } else {
      _.setFade();
    }

    _.$slider.trigger("setPosition", [_]);
  };

  Slick.prototype.setProps = function () {
    var _ = this,
      bodyStyle = document.body.style;

    _.positionProp = _.options.vertical === true ? "top" : "left";

    if (_.positionProp === "top") {
      _.$slider.addClass("slick-vertical");
    } else {
      _.$slider.removeClass("slick-vertical");
    }

    if (
      bodyStyle.WebkitTransition !== undefined ||
      bodyStyle.MozTransition !== undefined ||
      bodyStyle.msTransition !== undefined
    ) {
      if (_.options.useCSS === true) {
        _.cssTransitions = true;
      }
    }

    if (_.options.fade) {
      if (typeof _.options.zIndex === "number") {
        if (_.options.zIndex < 3) {
          _.options.zIndex = 3;
        }
      } else {
        _.options.zIndex = _.defaults.zIndex;
      }
    }

    if (bodyStyle.OTransform !== undefined) {
      _.animType = "OTransform";
      _.transformType = "-o-transform";
      _.transitionType = "OTransition";
      if (
        bodyStyle.perspectiveProperty === undefined &&
        bodyStyle.webkitPerspective === undefined
      )
        _.animType = false;
    }
    if (bodyStyle.MozTransform !== undefined) {
      _.animType = "MozTransform";
      _.transformType = "-moz-transform";
      _.transitionType = "MozTransition";
      if (
        bodyStyle.perspectiveProperty === undefined &&
        bodyStyle.MozPerspective === undefined
      )
        _.animType = false;
    }
    if (bodyStyle.webkitTransform !== undefined) {
      _.animType = "webkitTransform";
      _.transformType = "-webkit-transform";
      _.transitionType = "webkitTransition";
      if (
        bodyStyle.perspectiveProperty === undefined &&
        bodyStyle.webkitPerspective === undefined
      )
        _.animType = false;
    }
    if (bodyStyle.msTransform !== undefined) {
      _.animType = "msTransform";
      _.transformType = "-ms-transform";
      _.transitionType = "msTransition";
      if (bodyStyle.msTransform === undefined) _.animType = false;
    }
    if (bodyStyle.transform !== undefined && _.animType !== false) {
      _.animType = "transform";
      _.transformType = "transform";
      _.transitionType = "transition";
    }
    _.transformsEnabled =
      _.options.useTransform && _.animType !== null && _.animType !== false;
  };

  Slick.prototype.setSlideClasses = function (index) {
    var _ = this,
      centerOffset,
      allSlides,
      indexOffset,
      remainder;

    allSlides = _.$slider
      .find(".slick-slide")
      .removeClass("slick-active slick-center slick-current")
      .attr("aria-hidden", "true");

    _.$slides.eq(index).addClass("slick-current");

    if (_.options.centerMode === true) {
      var evenCoef = _.options.slidesToShow % 2 === 0 ? 1 : 0;

      centerOffset = Math.floor(_.options.slidesToShow / 2);

      if (_.options.infinite === true) {
        if (index >= centerOffset && index <= _.slideCount - 1 - centerOffset) {
          _.$slides
            .slice(index - centerOffset + evenCoef, index + centerOffset + 1)
            .addClass("slick-active")
            .attr("aria-hidden", "false");
        } else {
          indexOffset = _.options.slidesToShow + index;
          allSlides
            .slice(
              indexOffset - centerOffset + 1 + evenCoef,
              indexOffset + centerOffset + 2
            )
            .addClass("slick-active")
            .attr("aria-hidden", "false");
        }

        if (index === 0) {
          allSlides
            .eq(allSlides.length - 1 - _.options.slidesToShow)
            .addClass("slick-center");
        } else if (index === _.slideCount - 1) {
          allSlides.eq(_.options.slidesToShow).addClass("slick-center");
        }
      }

      _.$slides.eq(index).addClass("slick-center");
    } else {
      if (index >= 0 && index <= _.slideCount - _.options.slidesToShow) {
        _.$slides
          .slice(index, index + _.options.slidesToShow)
          .addClass("slick-active")
          .attr("aria-hidden", "false");
      } else if (allSlides.length <= _.options.slidesToShow) {
        allSlides.addClass("slick-active").attr("aria-hidden", "false");
      } else {
        remainder = _.slideCount % _.options.slidesToShow;
        indexOffset =
          _.options.infinite === true ? _.options.slidesToShow + index : index;

        if (
          _.options.slidesToShow == _.options.slidesToScroll &&
          _.slideCount - index < _.options.slidesToShow
        ) {
          allSlides
            .slice(
              indexOffset - (_.options.slidesToShow - remainder),
              indexOffset + remainder
            )
            .addClass("slick-active")
            .attr("aria-hidden", "false");
        } else {
          allSlides
            .slice(indexOffset, indexOffset + _.options.slidesToShow)
            .addClass("slick-active")
            .attr("aria-hidden", "false");
        }
      }
    }

    if (
      _.options.lazyLoad === "ondemand" ||
      _.options.lazyLoad === "anticipated"
    ) {
      _.lazyLoad();
    }
  };

  Slick.prototype.setupInfinite = function () {
    var _ = this,
      i,
      slideIndex,
      infiniteCount;

    if (_.options.fade === true) {
      _.options.centerMode = false;
    }

    if (_.options.infinite === true && _.options.fade === false) {
      slideIndex = null;

      if (_.slideCount > _.options.slidesToShow) {
        if (_.options.centerMode === true) {
          infiniteCount = _.options.slidesToShow + 1;
        } else {
          infiniteCount = _.options.slidesToShow;
        }

        for (i = _.slideCount; i > _.slideCount - infiniteCount; i -= 1) {
          slideIndex = i - 1;
          $(_.$slides[slideIndex])
            .clone(true)
            .attr("id", "")
            .attr("data-slick-index", slideIndex - _.slideCount)
            .prependTo(_.$slideTrack)
            .addClass("slick-cloned");
        }
        for (i = 0; i < infiniteCount + _.slideCount; i += 1) {
          slideIndex = i;
          $(_.$slides[slideIndex])
            .clone(true)
            .attr("id", "")
            .attr("data-slick-index", slideIndex + _.slideCount)
            .appendTo(_.$slideTrack)
            .addClass("slick-cloned");
        }
        _.$slideTrack
          .find(".slick-cloned")
          .find("[id]")
          .each(function () {
            $(this).attr("id", "");
          });
      }
    }
  };

  Slick.prototype.interrupt = function (toggle) {
    var _ = this;

    if (!toggle) {
      _.autoPlay();
    }
    _.interrupted = toggle;
  };

  Slick.prototype.selectHandler = function (event) {
    var _ = this;

    var targetElement = $(event.target).is(".slick-slide")
      ? $(event.target)
      : $(event.target).parents(".slick-slide");

    var index = parseInt(targetElement.attr("data-slick-index"));

    if (!index) index = 0;

    if (_.slideCount <= _.options.slidesToShow) {
      _.slideHandler(index, false, true);
      return;
    }

    _.slideHandler(index);
  };

  Slick.prototype.slideHandler = function (index, sync, dontAnimate) {
    var targetSlide,
      animSlide,
      oldSlide,
      slideLeft,
      targetLeft = null,
      _ = this,
      navTarget;

    sync = sync || false;

    if (_.animating === true && _.options.waitForAnimate === true) {
      return;
    }

    if (_.options.fade === true && _.currentSlide === index) {
      return;
    }

    if (sync === false) {
      _.asNavFor(index);
    }

    targetSlide = index;
    targetLeft = _.getLeft(targetSlide);
    slideLeft = _.getLeft(_.currentSlide);

    _.currentLeft = _.swipeLeft === null ? slideLeft : _.swipeLeft;

    if (
      _.options.infinite === false &&
      _.options.centerMode === false &&
      (index < 0 || index > _.getDotCount() * _.options.slidesToScroll)
    ) {
      if (_.options.fade === false) {
        targetSlide = _.currentSlide;
        if (dontAnimate !== true && _.slideCount > _.options.slidesToShow) {
          _.animateSlide(slideLeft, function () {
            _.postSlide(targetSlide);
          });
        } else {
          _.postSlide(targetSlide);
        }
      }
      return;
    } else if (
      _.options.infinite === false &&
      _.options.centerMode === true &&
      (index < 0 || index > _.slideCount - _.options.slidesToScroll)
    ) {
      if (_.options.fade === false) {
        targetSlide = _.currentSlide;
        if (dontAnimate !== true && _.slideCount > _.options.slidesToShow) {
          _.animateSlide(slideLeft, function () {
            _.postSlide(targetSlide);
          });
        } else {
          _.postSlide(targetSlide);
        }
      }
      return;
    }

    if (_.options.autoplay) {
      clearInterval(_.autoPlayTimer);
    }

    if (targetSlide < 0) {
      if (_.slideCount % _.options.slidesToScroll !== 0) {
        animSlide = _.slideCount - (_.slideCount % _.options.slidesToScroll);
      } else {
        animSlide = _.slideCount + targetSlide;
      }
    } else if (targetSlide >= _.slideCount) {
      if (_.slideCount % _.options.slidesToScroll !== 0) {
        animSlide = 0;
      } else {
        animSlide = targetSlide - _.slideCount;
      }
    } else {
      animSlide = targetSlide;
    }

    _.animating = true;

    _.$slider.trigger("beforeChange", [_, _.currentSlide, animSlide]);

    oldSlide = _.currentSlide;
    _.currentSlide = animSlide;

    _.setSlideClasses(_.currentSlide);

    if (_.options.asNavFor) {
      navTarget = _.getNavTarget();
      navTarget = navTarget.slick("getSlick");

      if (navTarget.slideCount <= navTarget.options.slidesToShow) {
        navTarget.setSlideClasses(_.currentSlide);
      }
    }

    _.updateDots();
    _.updateArrows();

    if (_.options.fade === true) {
      if (dontAnimate !== true) {
        _.fadeSlideOut(oldSlide);

        _.fadeSlide(animSlide, function () {
          _.postSlide(animSlide);
        });
      } else {
        _.postSlide(animSlide);
      }
      _.animateHeight();
      return;
    }

    if (dontAnimate !== true && _.slideCount > _.options.slidesToShow) {
      _.animateSlide(targetLeft, function () {
        _.postSlide(animSlide);
      });
    } else {
      _.postSlide(animSlide);
    }
  };

  Slick.prototype.startLoad = function () {
    var _ = this;

    if (_.options.arrows === true && _.slideCount > _.options.slidesToShow) {
      _.$prevArrow.hide();
      _.$nextArrow.hide();
    }

    if (_.options.dots === true && _.slideCount > _.options.slidesToShow) {
      _.$dots.hide();
    }

    _.$slider.addClass("slick-loading");
  };

  Slick.prototype.swipeDirection = function () {
    var xDist,
      yDist,
      r,
      swipeAngle,
      _ = this;

    xDist = _.touchObject.startX - _.touchObject.curX;
    yDist = _.touchObject.startY - _.touchObject.curY;
    r = Math.atan2(yDist, xDist);

    swipeAngle = Math.round((r * 180) / Math.PI);
    if (swipeAngle < 0) {
      swipeAngle = 360 - Math.abs(swipeAngle);
    }

    if (swipeAngle <= 45 && swipeAngle >= 0) {
      return _.options.rtl === false ? "left" : "right";
    }
    if (swipeAngle <= 360 && swipeAngle >= 315) {
      return _.options.rtl === false ? "left" : "right";
    }
    if (swipeAngle >= 135 && swipeAngle <= 225) {
      return _.options.rtl === false ? "right" : "left";
    }
    if (_.options.verticalSwiping === true) {
      if (swipeAngle >= 35 && swipeAngle <= 135) {
        return "down";
      } else {
        return "up";
      }
    }

    return "vertical";
  };

  Slick.prototype.swipeEnd = function (event) {
    var _ = this,
      slideCount,
      direction;

    _.dragging = false;
    _.swiping = false;

    if (_.scrolling) {
      _.scrolling = false;
      return false;
    }

    _.interrupted = false;
    _.shouldClick = _.touchObject.swipeLength > 10 ? false : true;

    if (_.touchObject.curX === undefined) {
      return false;
    }

    if (_.touchObject.edgeHit === true) {
      _.$slider.trigger("edge", [_, _.swipeDirection()]);
    }

    if (_.touchObject.swipeLength >= _.touchObject.minSwipe) {
      direction = _.swipeDirection();

      switch (direction) {
        case "left":
        case "down":
          slideCount = _.options.swipeToSlide
            ? _.checkNavigable(_.currentSlide + _.getSlideCount())
            : _.currentSlide + _.getSlideCount();

          _.currentDirection = 0;

          break;

        case "right":
        case "up":
          slideCount = _.options.swipeToSlide
            ? _.checkNavigable(_.currentSlide - _.getSlideCount())
            : _.currentSlide - _.getSlideCount();

          _.currentDirection = 1;

          break;

        default:
      }

      if (direction != "vertical") {
        _.slideHandler(slideCount);
        _.touchObject = {};
        _.$slider.trigger("swipe", [_, direction]);
      }
    } else {
      if (_.touchObject.startX !== _.touchObject.curX) {
        _.slideHandler(_.currentSlide);
        _.touchObject = {};
      }
    }
  };

  Slick.prototype.swipeHandler = function (event) {
    var _ = this;

    if (
      _.options.swipe === false ||
      ("ontouchend" in document && _.options.swipe === false)
    ) {
      return;
    } else if (
      _.options.draggable === false &&
      event.type.indexOf("mouse") !== -1
    ) {
      return;
    }

    _.touchObject.fingerCount =
      event.originalEvent && event.originalEvent.touches !== undefined
        ? event.originalEvent.touches.length
        : 1;

    _.touchObject.minSwipe = _.listWidth / _.options.touchThreshold;

    if (_.options.verticalSwiping === true) {
      _.touchObject.minSwipe = _.listHeight / _.options.touchThreshold;
    }

    switch (event.data.action) {
      case "start":
        _.swipeStart(event);
        break;

      case "move":
        _.swipeMove(event);
        break;

      case "end":
        _.swipeEnd(event);
        break;
    }
  };

  Slick.prototype.swipeMove = function (event) {
    var _ = this,
      edgeWasHit = false,
      curLeft,
      swipeDirection,
      swipeLength,
      positionOffset,
      touches,
      verticalSwipeLength;

    touches =
      event.originalEvent !== undefined ? event.originalEvent.touches : null;

    if (!_.dragging || _.scrolling || (touches && touches.length !== 1)) {
      return false;
    }

    curLeft = _.getLeft(_.currentSlide);

    _.touchObject.curX =
      touches !== undefined ? touches[0].pageX : event.clientX;
    _.touchObject.curY =
      touches !== undefined ? touches[0].pageY : event.clientY;

    _.touchObject.swipeLength = Math.round(
      Math.sqrt(Math.pow(_.touchObject.curX - _.touchObject.startX, 2))
    );

    verticalSwipeLength = Math.round(
      Math.sqrt(Math.pow(_.touchObject.curY - _.touchObject.startY, 2))
    );

    if (!_.options.verticalSwiping && !_.swiping && verticalSwipeLength > 4) {
      _.scrolling = true;
      return false;
    }

    if (_.options.verticalSwiping === true) {
      _.touchObject.swipeLength = verticalSwipeLength;
    }

    swipeDirection = _.swipeDirection();

    if (event.originalEvent !== undefined && _.touchObject.swipeLength > 4) {
      _.swiping = true;
      event.preventDefault();
    }

    positionOffset =
      (_.options.rtl === false ? 1 : -1) *
      (_.touchObject.curX > _.touchObject.startX ? 1 : -1);
    if (_.options.verticalSwiping === true) {
      positionOffset = _.touchObject.curY > _.touchObject.startY ? 1 : -1;
    }

    swipeLength = _.touchObject.swipeLength;

    _.touchObject.edgeHit = false;

    if (_.options.infinite === false) {
      if (
        (_.currentSlide === 0 && swipeDirection === "right") ||
        (_.currentSlide >= _.getDotCount() && swipeDirection === "left")
      ) {
        swipeLength = _.touchObject.swipeLength * _.options.edgeFriction;
        _.touchObject.edgeHit = true;
      }
    }

    if (_.options.vertical === false) {
      _.swipeLeft = curLeft + swipeLength * positionOffset;
    } else {
      _.swipeLeft =
        curLeft +
        swipeLength * (_.$list.height() / _.listWidth) * positionOffset;
    }
    if (_.options.verticalSwiping === true) {
      _.swipeLeft = curLeft + swipeLength * positionOffset;
    }

    if (_.options.fade === true || _.options.touchMove === false) {
      return false;
    }

    if (_.animating === true) {
      _.swipeLeft = null;
      return false;
    }

    _.setCSS(_.swipeLeft);
  };

  Slick.prototype.swipeStart = function (event) {
    var _ = this,
      touches;

    _.interrupted = true;

    if (
      _.touchObject.fingerCount !== 1 ||
      _.slideCount <= _.options.slidesToShow
    ) {
      _.touchObject = {};
      return false;
    }

    if (
      event.originalEvent !== undefined &&
      event.originalEvent.touches !== undefined
    ) {
      touches = event.originalEvent.touches[0];
    }

    _.touchObject.startX = _.touchObject.curX =
      touches !== undefined ? touches.pageX : event.clientX;
    _.touchObject.startY = _.touchObject.curY =
      touches !== undefined ? touches.pageY : event.clientY;

    _.dragging = true;
  };

  Slick.prototype.unfilterSlides = Slick.prototype.slickUnfilter = function () {
    var _ = this;

    if (_.$slidesCache !== null) {
      _.unload();

      _.$slideTrack.children(this.options.slide).detach();

      _.$slidesCache.appendTo(_.$slideTrack);

      _.reinit();
    }
  };

  Slick.prototype.unload = function () {
    var _ = this;

    $(".slick-cloned", _.$slider).remove();

    if (_.$dots) {
      _.$dots.remove();
    }

    if (_.$prevArrow && _.htmlExpr.test(_.options.prevArrow)) {
      _.$prevArrow.remove();
    }

    if (_.$nextArrow && _.htmlExpr.test(_.options.nextArrow)) {
      _.$nextArrow.remove();
    }

    _.$slides
      .removeClass("slick-slide slick-active slick-visible slick-current")
      .attr("aria-hidden", "true")
      .css("width", "");
  };

  Slick.prototype.unslick = function (fromBreakpoint) {
    var _ = this;
    _.$slider.trigger("unslick", [_, fromBreakpoint]);
    _.destroy();
  };

  Slick.prototype.updateArrows = function () {
    var _ = this,
      centerOffset;

    centerOffset = Math.floor(_.options.slidesToShow / 2);

    if (
      _.options.arrows === true &&
      _.slideCount > _.options.slidesToShow &&
      !_.options.infinite
    ) {
      _.$prevArrow.removeClass("slick-disabled").attr("aria-disabled", "false");
      _.$nextArrow.removeClass("slick-disabled").attr("aria-disabled", "false");

      if (_.currentSlide === 0) {
        _.$prevArrow.addClass("slick-disabled").attr("aria-disabled", "true");
        _.$nextArrow
          .removeClass("slick-disabled")
          .attr("aria-disabled", "false");
      } else if (
        _.currentSlide >= _.slideCount - _.options.slidesToShow &&
        _.options.centerMode === false
      ) {
        _.$nextArrow.addClass("slick-disabled").attr("aria-disabled", "true");
        _.$prevArrow
          .removeClass("slick-disabled")
          .attr("aria-disabled", "false");
      } else if (
        _.currentSlide >= _.slideCount - 1 &&
        _.options.centerMode === true
      ) {
        _.$nextArrow.addClass("slick-disabled").attr("aria-disabled", "true");
        _.$prevArrow
          .removeClass("slick-disabled")
          .attr("aria-disabled", "false");
      }
    }
  };

  Slick.prototype.updateDots = function () {
    var _ = this;

    if (_.$dots !== null) {
      _.$dots.find("li").removeClass("slick-active").end();

      _.$dots
        .find("li")
        .eq(Math.floor(_.currentSlide / _.options.slidesToScroll))
        .addClass("slick-active");
    }
  };

  Slick.prototype.visibility = function () {
    var _ = this;

    if (_.options.autoplay) {
      if (document[_.hidden]) {
        _.interrupted = true;
      } else {
        _.interrupted = false;
      }
    }
  };

  $.fn.slick = function () {
    var _ = this,
      opt = arguments[0],
      args = Array.prototype.slice.call(arguments, 1),
      l = _.length,
      i,
      ret;
    for (i = 0; i < l; i++) {
      if (typeof opt == "object" || typeof opt == "undefined")
        _[i].slick = new Slick(_[i], opt);
      else ret = _[i].slick[opt].apply(_[i].slick, args);
      if (typeof ret != "undefined") return ret;
    }
    return _;
  };
});

/*!
 * smooth-scroll v12.1.5: Animate scrolling to anchor links
 * (c) 2017 Chris Ferdinandi
 * MIT License
 * http://github.com/cferdinandi/smooth-scroll
 */

(function (root, factory) {
  if (typeof define === "function" && define.amd) {
    define([], function () {
      return factory(root);
    });
  } else if (typeof exports === "object") {
    module.exports = factory(root);
  } else {
    root.SmoothScroll = factory(root);
  }
})(
  typeof global !== "undefined"
    ? global
    : typeof window !== "undefined"
    ? window
    : this,
  function (window) {
    "use strict";

    //
    // Feature Test
    //

    var supports =
      "querySelector" in document &&
      "addEventListener" in window &&
      "requestAnimationFrame" in window &&
      "closest" in window.Element.prototype;

    //
    // Default settings
    //

    var defaults = {
      // Selectors
      ignore: "[data-scroll-ignore]",
      header: null,

      // Speed & Easing
      speed: 500,
      offset: 0,
      easing: "easeInOutCubic",
      customEasing: null,

      // Callback API
      before: function () {},
      after: function () {},
    };

    //
    // Utility Methods
    //

    /**
     * Merge two or more objects. Returns a new object.
     * @param {Object}   objects  The objects to merge together
     * @returns {Object}          Merged values of defaults and options
     */
    var extend = function () {
      // Variables
      var extended = {};
      var deep = false;
      var i = 0;
      var length = arguments.length;

      // Merge the object into the extended object
      var merge = function (obj) {
        for (var prop in obj) {
          if (obj.hasOwnProperty(prop)) {
            extended[prop] = obj[prop];
          }
        }
      };

      // Loop through each object and conduct a merge
      for (; i < length; i++) {
        var obj = arguments[i];
        merge(obj);
      }

      return extended;
    };

    /**
     * Get the height of an element.
     * @param  {Node} elem The element to get the height of
     * @return {Number}    The element's height in pixels
     */
    var getHeight = function (elem) {
      return parseInt(window.getComputedStyle(elem).height, 10);
    };

    /**
     * Escape special characters for use with querySelector
     * @param {String} id The anchor ID to escape
     * @author Mathias Bynens
     * @link https://github.com/mathiasbynens/CSS.escape
     */
    var escapeCharacters = function (id) {
      // Remove leading hash
      if (id.charAt(0) === "#") {
        id = id.substr(1);
      }

      var string = String(id);
      var length = string.length;
      var index = -1;
      var codeUnit;
      var result = "";
      var firstCodeUnit = string.charCodeAt(0);
      while (++index < length) {
        codeUnit = string.charCodeAt(index);
        // Note: there’s no need to special-case astral symbols, surrogate
        // pairs, or lone surrogates.

        // If the character is NULL (U+0000), then throw an
        // `InvalidCharacterError` exception and terminate these steps.
        if (codeUnit === 0x0000) {
          throw new InvalidCharacterError(
            "Invalid character: the input contains U+0000."
          );
        }

        if (
          // If the character is in the range [\1-\1F] (U+0001 to U+001F) or is
          // U+007F, […]
          (codeUnit >= 0x0001 && codeUnit <= 0x001f) ||
          codeUnit == 0x007f ||
          // If the character is the first character and is in the range [0-9]
          // (U+0030 to U+0039), […]
          (index === 0 && codeUnit >= 0x0030 && codeUnit <= 0x0039) ||
          // If the character is the second character and is in the range [0-9]
          // (U+0030 to U+0039) and the first character is a `-` (U+002D), […]
          (index === 1 &&
            codeUnit >= 0x0030 &&
            codeUnit <= 0x0039 &&
            firstCodeUnit === 0x002d)
        ) {
          // http://dev.w3.org/csswg/cssom/#escape-a-character-as-code-point
          result += "\\" + codeUnit.toString(16) + " ";
          continue;
        }

        // If the character is not handled by one of the above rules and is
        // greater than or equal to U+0080, is `-` (U+002D) or `_` (U+005F), or
        // is in one of the ranges [0-9] (U+0030 to U+0039), [A-Z] (U+0041 to
        // U+005A), or [a-z] (U+0061 to U+007A), […]
        if (
          codeUnit >= 0x0080 ||
          codeUnit === 0x002d ||
          codeUnit === 0x005f ||
          (codeUnit >= 0x0030 && codeUnit <= 0x0039) ||
          (codeUnit >= 0x0041 && codeUnit <= 0x005a) ||
          (codeUnit >= 0x0061 && codeUnit <= 0x007a)
        ) {
          // the character itself
          result += string.charAt(index);
          continue;
        }

        // Otherwise, the escaped character.
        // http://dev.w3.org/csswg/cssom/#escape-a-character
        result += "\\" + string.charAt(index);
      }

      return "#" + result;
    };

    /**
     * Calculate the easing pattern
     * @link https://gist.github.com/gre/1650294
     * @param {String} type Easing pattern
     * @param {Number} time Time animation should take to complete
     * @returns {Number}
     */
    var easingPattern = function (settings, time) {
      var pattern;

      // Default Easing Patterns
      if (settings.easing === "easeInQuad") pattern = time * time; // accelerating from zero velocity
      if (settings.easing === "easeOutQuad") pattern = time * (2 - time); // decelerating to zero velocity
      if (settings.easing === "easeInOutQuad")
        pattern = time < 0.5 ? 2 * time * time : -1 + (4 - 2 * time) * time; // acceleration until halfway, then deceleration
      if (settings.easing === "easeInCubic") pattern = time * time * time; // accelerating from zero velocity
      if (settings.easing === "easeOutCubic")
        pattern = --time * time * time + 1; // decelerating to zero velocity
      if (settings.easing === "easeInOutCubic")
        pattern =
          time < 0.5
            ? 4 * time * time * time
            : (time - 1) * (2 * time - 2) * (2 * time - 2) + 1; // acceleration until halfway, then deceleration
      if (settings.easing === "easeInQuart")
        pattern = time * time * time * time; // accelerating from zero velocity
      if (settings.easing === "easeOutQuart")
        pattern = 1 - --time * time * time * time; // decelerating to zero velocity
      if (settings.easing === "easeInOutQuart")
        pattern =
          time < 0.5
            ? 8 * time * time * time * time
            : 1 - 8 * --time * time * time * time; // acceleration until halfway, then deceleration
      if (settings.easing === "easeInQuint")
        pattern = time * time * time * time * time; // accelerating from zero velocity
      if (settings.easing === "easeOutQuint")
        pattern = 1 + --time * time * time * time * time; // decelerating to zero velocity
      if (settings.easing === "easeInOutQuint")
        pattern =
          time < 0.5
            ? 16 * time * time * time * time * time
            : 1 + 16 * --time * time * time * time * time; // acceleration until halfway, then deceleration

      // Custom Easing Patterns
      if (!!settings.customEasing) pattern = settings.customEasing(time);

      return pattern || time; // no easing, no acceleration
    };

    /**
     * Determine the document's height
     * @returns {Number}
     */
    var getDocumentHeight = function () {
      return Math.max(
        document.body.scrollHeight,
        document.documentElement.scrollHeight,
        document.body.offsetHeight,
        document.documentElement.offsetHeight,
        document.body.clientHeight,
        document.documentElement.clientHeight
      );
    };

    /**
     * Calculate how far to scroll
     * @param {Element} anchor The anchor element to scroll to
     * @param {Number} headerHeight Height of a fixed header, if any
     * @param {Number} offset Number of pixels by which to offset scroll
     * @returns {Number}
     */
    var getEndLocation = function (anchor, headerHeight, offset) {
      var location = 0;
      if (anchor.offsetParent) {
        do {
          location += anchor.offsetTop;
          anchor = anchor.offsetParent;
        } while (anchor);
      }
      location = Math.max(location - headerHeight - offset, 0);
      return location;
    };

    /**
     * Get the height of the fixed header
     * @param  {Node}   header The header
     * @return {Number}        The height of the header
     */
    var getHeaderHeight = function (header) {
      return !header ? 0 : getHeight(header) + header.offsetTop;
    };

    /**
     * Bring the anchored element into focus
     * @param {Node}     anchor      The anchor element
     * @param {Number}   endLocation The end location to scroll to
     * @param {Boolean}  isNum       If true, scroll is to a position rather than an element
     */
    var adjustFocus = function (anchor, endLocation, isNum) {
      // Don't run if scrolling to a number on the page
      if (isNum) return;

      // Otherwise, bring anchor element into focus
      anchor.focus();
      if (document.activeElement.id !== anchor.id) {
        anchor.setAttribute("tabindex", "-1");
        anchor.focus();
        anchor.style.outline = "none";
      }
      window.scrollTo(0, endLocation);
    };

    /**
     * Check to see if user prefers reduced motion
     * @param  {Object} settings Script settings
     */
    var reduceMotion = function (settings) {
      if (
        "matchMedia" in window &&
        window.matchMedia("(prefers-reduced-motion)").matches
      ) {
        return true;
      }
      return false;
    };

    //
    // SmoothScroll Constructor
    //

    var SmoothScroll = function (selector, options) {
      //
      // Variables
      //

      var smoothScroll = {}; // Object for public APIs
      var settings,
        anchor,
        toggle,
        fixedHeader,
        headerHeight,
        eventTimeout,
        animationInterval;

      //
      // Methods
      //

      /**
       * Cancel a scroll-in-progress
       */
      smoothScroll.cancelScroll = function () {
        // clearInterval(animationInterval);
        cancelAnimationFrame(animationInterval);
      };

      /**
       * Start/stop the scrolling animation
       * @param {Node|Number} anchor  The element or position to scroll to
       * @param {Element}     toggle  The element that toggled the scroll event
       * @param {Object}      options
       */
      smoothScroll.animateScroll = function (anchor, toggle, options) {
        // Local settings
        var animateSettings = extend(settings || defaults, options || {}); // Merge user options with defaults

        // Selectors and variables
        var isNum =
          Object.prototype.toString.call(anchor) === "[object Number]"
            ? true
            : false;
        var anchorElem = isNum || !anchor.tagName ? null : anchor;
        if (!isNum && !anchorElem) return;
        var startLocation = window.pageYOffset; // Current location on the page
        if (animateSettings.header && !fixedHeader) {
          // Get the fixed header if not already set
          fixedHeader = document.querySelector(animateSettings.header);
        }
        if (!headerHeight) {
          // Get the height of a fixed header if one exists and not already set
          headerHeight = getHeaderHeight(fixedHeader);
        }
        var endLocation = isNum
          ? anchor
          : getEndLocation(
              anchorElem,
              headerHeight,
              parseInt(
                typeof animateSettings.offset === "function"
                  ? animateSettings.offset()
                  : animateSettings.offset,
                10
              )
            ); // Location to scroll to
        var distance = endLocation - startLocation; // distance to travel
        var documentHeight = getDocumentHeight();
        var timeLapsed = 0;
        var start, percentage, position;

        /**
         * Stop the scroll animation when it reaches its target (or the bottom/top of page)
         * @param {Number} position Current position on the page
         * @param {Number} endLocation Scroll to location
         * @param {Number} animationInterval How much to scroll on this loop
         */
        var stopAnimateScroll = function (position, endLocation) {
          // Get the current location
          var currentLocation = window.pageYOffset;

          // Check if the end location has been reached yet (or we've hit the end of the document)
          if (
            position == endLocation ||
            currentLocation == endLocation ||
            (startLocation < endLocation &&
              window.innerHeight + currentLocation) >= documentHeight
          ) {
            // Clear the animation timer
            smoothScroll.cancelScroll();

            // Bring the anchored element into focus
            adjustFocus(anchor, endLocation, isNum);

            // Run callback after animation complete
            animateSettings.after(anchor, toggle);

            // Reset start
            start = null;

            return true;
          }
        };

        /**
         * Loop scrolling animation
         */
        var loopAnimateScroll = function (timestamp) {
          if (!start) {
            start = timestamp;
          }
          timeLapsed += timestamp - start;
          percentage = timeLapsed / parseInt(animateSettings.speed, 10);
          percentage = percentage > 1 ? 1 : percentage;
          position =
            startLocation +
            distance * easingPattern(animateSettings, percentage);
          window.scrollTo(0, Math.floor(position));
          if (!stopAnimateScroll(position, endLocation)) {
            window.requestAnimationFrame(loopAnimateScroll);
            start = timestamp;
          }
        };

        /**
         * Reset position to fix weird iOS bug
         * @link https://github.com/cferdinandi/smooth-scroll/issues/45
         */
        if (window.pageYOffset === 0) {
          window.scrollTo(0, 0);
        }

        // Run callback before animation starts
        animateSettings.before(anchor, toggle);

        // Start scrolling animation
        smoothScroll.cancelScroll();
        window.requestAnimationFrame(loopAnimateScroll);
      };

      /**
       * Handle has change event
       */
      var hashChangeHandler = function (event) {
        // Only run if there's an anchor element to scroll to
        if (!anchor) return;

        // Reset the anchor element's ID
        anchor.id = anchor.getAttribute("data-scroll-id");

        // Scroll to the anchored content
        smoothScroll.animateScroll(anchor, toggle);

        // Reset anchor and toggle
        anchor = null;
        toggle = null;
      };

      /**
       * If smooth scroll element clicked, animate scroll
       */
      var clickHandler = function (event) {
        // Don't run if the user prefers reduced motion
        if (reduceMotion(settings)) return;

        // Don't run if right-click or command/control + click
        if (event.button !== 0 || event.metaKey || event.ctrlKey) return;

        // Check if a smooth scroll link was clicked
        toggle = event.target.closest(selector);
        if (
          !toggle ||
          toggle.tagName.toLowerCase() !== "a" ||
          event.target.closest(settings.ignore)
        )
          return;

        // Only run if link is an anchor and points to the current page
        if (
          toggle.hostname !== window.location.hostname ||
          toggle.pathname !== window.location.pathname ||
          !/#/.test(toggle.href)
        )
          return;

        // Get the sanitized hash
        var hash;
        try {
          hash = escapeCharacters(decodeURIComponent(toggle.hash));
        } catch (e) {
          hash = escapeCharacters(toggle.hash);
        }

        // If the hash is empty, scroll to the top of the page
        if (hash === "#") {
          // Prevent default link behavior
          event.preventDefault();

          // Set the anchored element
          anchor = document.body;

          // Save or create the ID as a data attribute and remove it (prevents scroll jump)
          var id = anchor.id ? anchor.id : "smooth-scroll-top";
          anchor.setAttribute("data-scroll-id", id);
          anchor.id = "";

          // If no hash change event will happen, fire manually
          // Otherwise, update the hash
          if (window.location.hash.substring(1) === id) {
            hashChangeHandler();
          } else {
            window.location.hash = id;
          }

          return;
        }

        // Get the anchored element
        anchor = document.querySelector(hash);

        // If anchored element exists, save the ID as a data attribute and remove it (prevents scroll jump)
        if (!anchor) return;
        anchor.setAttribute("data-scroll-id", anchor.id);
        anchor.id = "";

        // If no hash change event will happen, fire manually
        if (toggle.hash === window.location.hash) {
          event.preventDefault();
          hashChangeHandler();
        }
      };

      /**
       * On window scroll and resize, only run events at a rate of 15fps for better performance
       */
      var resizeThrottler = function (event) {
        if (!eventTimeout) {
          eventTimeout = setTimeout(function () {
            eventTimeout = null; // Reset timeout
            headerHeight = getHeaderHeight(fixedHeader); // Get the height of a fixed header if one exists
          }, 66);
        }
      };

      /**
       * Destroy the current initialization.
       */
      smoothScroll.destroy = function () {
        // If plugin isn't already initialized, stop
        if (!settings) return;

        // Remove event listeners
        document.removeEventListener("click", clickHandler, false);
        window.removeEventListener("resize", resizeThrottler, false);

        // Cancel any scrolls-in-progress
        smoothScroll.cancelScroll();

        // Reset variables
        settings = null;
        anchor = null;
        toggle = null;
        fixedHeader = null;
        headerHeight = null;
        eventTimeout = null;
        animationInterval = null;
      };

      /**
       * Initialize Smooth Scroll
       * @param {Object} options User settings
       */
      smoothScroll.init = function (options) {
        // feature test
        if (!supports) return;

        // Destroy any existing initializations
        smoothScroll.destroy();

        // Selectors and variables
        settings = extend(defaults, options || {}); // Merge user options with defaults
        fixedHeader = settings.header
          ? document.querySelector(settings.header)
          : null; // Get the fixed header
        headerHeight = getHeaderHeight(fixedHeader);

        // When a toggle is clicked, run the click handler
        document.addEventListener("click", clickHandler, false);

        // Listen for hash changes
        window.addEventListener("hashchange", hashChangeHandler, false);

        // If window is resized and there's a fixed header, recalculate its size
        if (fixedHeader) {
          window.addEventListener("resize", resizeThrottler, false);
        }
      };

      //
      // Initialize plugin
      //

      smoothScroll.init(options);

      //
      // Public APIs
      //

      return smoothScroll;
    };

    return SmoothScroll;
  }
);
