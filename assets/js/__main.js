if (!console) console = { log: function() {} };
var debug = true;

/*if (window.location.hostname != "localhost") {
  debug = false;
}*/

var cl = function cl(msg) {
  if (debug) {
    console.log(msg);
  }
};

if (!debug) {
  console = { log: function() {} };
}

var app = (function($) {

  var init = function init($) {
    cl('Application initializing...');

    banner.init();
    header.init();
    plans.init();
    extras.init();
    timeline.init();
    contact.init();

    var scroll = new SmoothScroll('a[href*="#"]');

    cl('All application components initiated.');

    $('.ga').on('click', function() {
      var cat = $(this).data('cat');
      var act = $(this).data('act');
      var lbl = $(this).data('lbl');

      send(cat, act, lbl);
    });

    // class="ga" data-cat="" data-act="" data-lbl=""

    //player on modal for video gallery
    const player = new Plyr('#modal-player', {});

    $('.gallery-item').on('click', function() {
      var id = $(this).attr('video-id');
      player.source = {
        type: 'video',
        sources: [{
          src: id,
          provider: 'youtube',
        }, ],
      };
      player.on('ready', function(event) {
        player.play();
      });
      $('.modal-overlay').toggleClass('active');
    });
    
      $('.video-layer').on('click', function() {
      var id = $(this).attr('video-id');
      player.source = {
        type: 'video',
        sources: [{
          src: id,
          provider: 'youtube',
        }, ],
      };
      player.on('ready', function(event) {
        player.play();
      });
      $('.modal-overlay').toggleClass('active');
    });
    
        $('.new-slider').on('click', function() {
      var id = $(this).attr('video-id');
      player.source = {
        type: 'video',
        sources: [{
          src: id,
          provider: 'youtube',
        }, ],
      };
      player.on('ready', function(event) {
        player.play();
      });
      $('.modal-overlay').toggleClass('active');
    });

    $('.video-trigger').on('click', function() {
      var id = $(this).attr('video-id');
      player.source = {
        type: 'video',
        sources: [{
          src: id,
          provider: 'youtube',
        }, ],
      };
      player.on('ready', function(event) {
        player.play();
      });
      $('.modal-overlay').toggleClass('active');
    });

    $('#close-modal').on('click', function() {
      $('.modal-overlay').toggleClass('active');
      player.stop();
    });

    player.on('pause', function() {
      $('.videoplayer.vwrapper').removeClass('player-playing').addClass('player-paused');
    });

    player.on('play', function() {
      $('.videoplayer.vwrapper').removeClass('player-paused').addClass('player-playing');
    });

    $(document).on('click', '.modal-overlay > .videoplayer > .play-button', function() {
      $('.slider-player-wrapper').removeClass('player-paused').addClass('player-playing');
      player.play();
    });

    $(document).on('click', '.modal-overlay > .videoplayer > .pause-button', function() {
      $('.slider-player-wrapper').removeClass('player-playing').addClass('player-paused');
      player.pause();
    });
  };

  var send = function send(cat, act, lbl) {
    gtag('event', act, {
        'event_category': cat,
        'event_label': lbl,
        'transport_type': 'beacon',
        'event_callback': function(){}
      });
  };

  return {
    init: init,
    send: send
  }
})();

jQuery(document).ready(function($) {
  app.init($);
});

$(window).scroll(function() {    
    var scroll = $(window).scrollTop();
    var objectSelect = $("body");
    var objectPosition = objectSelect.offset().top;
    var objectSelectFim = $("#nossos-planos");
    var objectPositionFim = objectSelectFim.offset().top;
    
    if ( scroll < objectPositionFim ) {
        $("#float-btn").removeClass("negative");
    } else {
        $("#float-btn").addClass("negative");
    }
});

var banner = (function() {

  var init = function init() {
    cl('[banner] initializing...');
    //button VEJA MAIS
    $('.veja-mais').on('click', function() {
      if ($('.hidden-row').length > 1) {
        var target = $('.hidden-row').eq(0);
        $(target).removeClass('hidden-row');
      } else if ($('.hidden-row').length = 1) {
        var target = $('.hidden-row').eq(0);
        $(target).removeClass('hidden-row');
        $('.veja-mais').addClass('hidden-btn');
      }
    });

    //player for sliders
    const playerSlider = new Plyr('#slider-player', {});

    $('#video-in .top-slider').on('init', function(event, slick) {
      $('.slick-current').addClass('center-slide');
      setTimeout(function() {
        $('#video-in .top-slider').css('opacity', 1);
      }, 2000);
    });

    $('#video-in .top-slider').on('beforeChange', function(event, slick, currentSlide, nextSlide) {
      $('.slider-player-wrapper').hide();
      playerSlider.stop();
      $('.slick-current').removeClass('center-slide');
    });

    $('#video-in .top-slider').on('afterChange', function(event, slick, currentSlide, nextSlide) {
      $('.slick-current').addClass('center-slide');
    });

    function getSliderSettings() {
      return {
        dots: false,
        slidesToScroll: 3,
        slidesToShow: 1,
        speed: 400,
        centerMode: true,
        centerPadding: 0,
        swipeToSlide: false,
        draggable: false,
        variableWidth: true,
        prevArrow: '<img class="prev-button" src="assets/images/prev-arrow.png"/>',
        nextArrow: '<img class="next-button" src="assets/images/next-arrow.png"/>',
        responsive: [{
          breakpoint: 768,
          settings: {
            slidesToShow: 1,
            centerMode: false,
            swipeToSlide: false,
            draggable: true,
            variableWidth: false,
          }
        }]
      }
    }

    $('#video-in .top-slider').slick(getSliderSettings());

    $(document).on('click', '.center-slide', function(){
      var id = $(this).find('.slide-play-button').attr('video-id');

      var left = $('.center-slide').offset().left;
      var top = $('.center-slide').offset().top;
      if ($(window).width() <= (767)) {
        top = top - (top * 0.1);
      }

      //get correct scale from css
      var matrixRegex = /matrix\((-?\d*\.?\d+),\s*0,\s*0,\s*(-?\d*\.?\d+),\s*0,\s*0\)/;
      var matches = $('.center-slide').css('transform').match(matrixRegex);

      var width = $('.center-slide').width() * matches[1];
      var height = $('.center-slide').height() * matches[1];

      playerSlider.source = {
        type: 'video',
        sources: [{
          src: id,
          provider: 'youtube',
        }, ],
      };

      $('.slider-player-wrapper').css({ 'left': left, 'top': top, 'width': width, 'height': height }).fadeIn(function() {
        playerSlider.on('ready', function(event) {
          playerSlider.play();
        });
      });

    });

    playerSlider.on('pause', function(){
      $('.slider-player-wrapper').removeClass('player-playing').addClass('player-paused');
    });

    playerSlider.on('play', function(){
      $('.slider-player-wrapper').removeClass('player-paused').addClass('player-playing');
    });

    $(document).on('click', '.play-button', function(){
      $('.slider-player-wrapper').removeClass('player-paused').addClass('player-playing');
      playerSlider.play();
    });

    $(document).on('click', '.pause-button', function(){
      $('.slider-player-wrapper').removeClass('player-playing').addClass('player-paused');
      playerSlider.pause();
    });

    var rtime;
    var timeout = false;
    var delta = 200;
    var slickWasInitialized = null;

    var slickBlocks = function() {
      if ($(window).width() <= (767 - 17)) {
        $('.video-gallery').slick({
          dots: true,
          arrows: false,
          infinite: false,
          slidesToShow: 1,
          slidesToScroll: 1,
          speed: 300,
        });
        slickWasInitialized = true;
      } else {
        if (slickWasInitialized) {
          $('.video-gallery').slick('unslick');
        }
      }
    }

    var resizeMainSlider = function() {
      //resize slick slider 
      $('#video-in .top-slider').slick('unslick');
      $('#video-in .top-slider').slick(getSliderSettings());

      setTimeout(function() {
        //check slider position
        var left = $('.center-slide').offset().left;
        var top = $('.center-slide').offset().top;
        if ($(window).width() <= (767)) {
          top = top - (top * 0.1);
        }

        var matrixRegex = /matrix\((-?\d*\.?\d+),\s*0,\s*0,\s*(-?\d*\.?\d+),\s*0,\s*0\)/;
        var matches = $('.center-slide').css('transform').match(matrixRegex);

        var width = $('.center-slide').width() * matches[1];
        var height = $('.center-slide').height() * matches[1];

        $('.slider-player-wrapper').css({ 'left': left, 'top': top, 'width': width, 'height': height });
      }, 300);
    }

    slickBlocks();

    $(window).resize(function() {
      rtime = new Date();
      if (timeout === false) {
        timeout = true;
        setTimeout(resizeend, delta);
      }
    });

    function resizeend() {
      if (new Date() - rtime < delta) {
        setTimeout(resizeend, delta);
      } else {
        timeout = false;
        slickBlocks();
        resizeMainSlider();
      }
    }
    cl('[banner] initiated.');
  };

  return {
    init: init
  }
})();
function isValidDate(dateString)
{
    // First check for the pattern
    if(!/^\d{1,2}\/\d{1,2}\/\d{4}$/.test(dateString))
        return false;

    // Parse the date parts to integers
    var parts = dateString.split("/");
    var day = parseInt(parts[0], 10);
    var month = parseInt(parts[1], 10);
    var year = parseInt(parts[2], 10);

    // Check the ranges of month and year
    if(year < 1000 || year > (new Date()).getFullYear() || month == 0 || month > 12)
        return false;

    var monthLength = [ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ];

    // Adjust for leap years
    if(year % 400 == 0 || (year % 100 != 0 && year % 4 == 0))
        monthLength[1] = 29;

    // Check the range of the day
    return day > 0 && day <= monthLength[month - 1];
};

function isValidEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

var contact = (function(){

	var fields = {
		"fname" : $('input#name'),
		"fphone" : $('input#phone'),
		"fbirthday" : $('input#birthday'),
		"fstate" : $('select#state'),
		"femail" : $('input#email'),
		'fplan' : $('select#plan'),
	}

	var init = function init(){
		cl('[Contact] initializing...');

			$('.close-modal-trigger').on('click', function() {
				closeSucessModal();
			})

	    var SPMaskBehavior = function (val) {
	      return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
	    },
	    spOptions = {
	      onKeyPress: function(val, e, field, options) {
	          field.mask(SPMaskBehavior.apply({}, arguments), options);
	        }
	    };

		$('select').select2({

		});	   

		$( window ).resize(function() {
			$('select').select2({});	   	
		});

	    $('.date input').mask('00/00/0000');
	    $('.phone input').mask(SPMaskBehavior, spOptions);   

	    bindValidation(); 

	    $('#sendForm').off().on('click', function(){
	    	$(this).addClass('sending');
	    	$('#formfeedback').html("<p>Verificando dados...</p>");
	    	if(validate()){
	    		// cl("The form has been submitted");
		    	// $(this).removeClass('sending');
		    	sendForm();
	    	} else {
		    	$('#formfeedback').html("<p class='error'>Existem erros no formulário, por favor verifique os campos e tente novamente.</p>");
	    		cl("The form contains errors.");
		    	$(this).removeClass('sending');
	    	}
	    });

	    console.log(fields);

		cl('[Contact] initiated.');
	};

	var bindValidation = function bindValidation(){
		cl('Binding validation events...');
		$('#contact-form #name').on('blur', function(){
			if($(this).val().length > 0){
				validateName($(this));
			}
		});

		$('#contact-form #phone').on('blur', function(){
			if($(this).val().length > 0){
			validatePhone($(this), false);
			}
		});

		$('#contact-form #birthday').on('blur', function(){
			if($(this).val().length > 0){
				validateDate($(this));
			}
		});

		$('#contact-form #state').on('change', function(){
			// if($(this).val().length > 0){
				validateState($(this));
			// }
		});

		$('#contact-form #email').on('blur', function(){
			if($(this).val().length > 0){
				validateEmail($(this));
			}
		});

		$('#contact-form #plan').on('change', function(){
			// if($(this).val().length > 0){
				validatePlan($(this));
			// }
		});
	};

	var validate = function validate(){
		var valid = true;
		console.log(fields);

		if(!validateName(fields.fname)){valid = false;}
		if(!validatePhone(fields.fphone, false)){valid = false;}
		if(!validateDate(fields.fbirthday)){valid = false;}
		if(!validateState(fields.fstate)){valid = false;}
		if(!validateEmail(fields.femail)){valid = false;}
		if(!validatePlan(fields.fplan)){valid = false;}

		return valid;
	};

	var validateName = function validateName(fld) {
		var valid = true;
		var fldval = fld.val().trim();
		cl('------------');
		cl('Validating Name');
		cl(fldval);

		fld.parent().removeClass('error success');
		if(fldval.length == "") {valid = false;}

		if(valid) {
			fld.parent().addClass('success');
		} else {
			fld.parent().addClass('error');
		}
		cl(valid);
		cl('------------');
		return valid;
	}

	var validatePhone = function validatePhone(fld, req) {
		var valid = true;
		var fldval = fld.val().trim();
		cl('------------');
		cl('Validating Phone Number');
		cl(fldval);

		fld.parent().removeClass('error success');
		if(req){
			if(fldval.length < 14) {valid = false;}
		} else {
			if(fldval.length > 0){
				req = true;
				if(fldval.length < 14) {valid = false;}
			}
		}

		if(req) {
			if(valid) {
				fld.parent().addClass('success');
			} else {
				fld.parent().addClass('error');
			}
		}
		cl(valid);
		cl('------------');
		return valid;
	}

	var validateDate = function validateDate(fld) {
		var valid = true;
		var fldval = fld.val().trim();
		cl('------------');
		cl('Validating Name');
		cl(fldval);

		fld.parent().removeClass('error success');
		if(fldval.length < 10) {valid = false;}
		if(!isValidDate(fldval)) {valid = false;}

		if(valid) {
			fld.parent().addClass('success');
		} else {
			fld.parent().addClass('error');
		}
		cl(valid);
		cl('------------');
		return valid;		
	};

	var validateState = function validateState(fld) {
		var valid = true;
		var fldval = fld.val().trim();
		cl('------------');
		cl('Validating Name');
		cl(fldval);

		fld.parent().removeClass('error success');
		if(fldval.length == 0) {valid = false;}

		if(valid) {
			fld.parent().addClass('success');
		} else {
			fld.parent().addClass('error');
		}
		cl(valid);
		cl('------------');
		return valid;		
	};

	var validateEmail = function validateDate(fld, confirmation) {
		var valid = true;
		var fldval = fld.val().trim();
		cl('------------');
		cl('Validating Name');
		cl(fldval);

		fld.parent().removeClass('error success');
		if(fldval.length == 0) {valid = false;}
		if(!isValidEmail(fldval)) {valid = false;}

		if(confirmation){
			if(fldval != $('#email').val().trim()){valid = false;}
		} else {
			$('#reemail').blur();
		}

		if(valid) {
			fld.parent().addClass('success');
		} else {
			fld.parent().addClass('error');
		}
		cl(valid);
		cl('------------');
		return valid;		
	};

	var validatePlan = function validatePlan(fld) {
		var valid = true;
		var fldval = fld.val().trim();
		cl('------------');
		cl('Validating Plan');
		cl(fldval);

		fld.parent().removeClass('error success');
		if(fldval.length == 0) {valid = false;}

		if(valid) {
			fld.parent().addClass('success');
		} else {
			fld.parent().addClass('error');
		}
		cl(valid);
		cl('------------');
		return valid;		
	};

  var sendForm = function sendForm(){
 	  $('#formfeedback').html("<p class=''>Enviando. Aguarde um momento.</p>");
    // var formData = new FormData(this);
    //var endpoint = "/sendmail/index.php";

    var formData = {};

    formData.fullname = $('#name').val();
    formData.phone = $('#phone').val();
    formData.birthday = $('#birthday').val();
    formData.state = $('#state').val();
    formData.email = $('#email').val();
    formData.plan = $('#plan').val();


    console.log('Data to be sent:');
    console.log(formData);

    sendToSalesForce(formData);


    // var success = "<p class='success'>Seus dados foram enviados com sucesso.</p>";
    $('#formfeedback').html("");

    // $.ajax({
    //     url: endpoint,
    //     type: 'POST',
    //     data: JSON.stringify(formData),
    //     success: function () {
    //   var success = "<p class='success'>Seus dados foram enviados com sucesso.</p>";
    //       $('#formfeedback').html(success);
    //       // $('#sendForm').removeClass('sending');

    //     },
    //     error: function () {
    //         console.log('error!');
    //       $('#sendForm').removeClass('sending');
    //     },
    //     cache: false,
    //     contentType: 'application/json',
    //     processData: false
    // });          

  }

  	var sendToSalesForce = function(data){

  	var firstName = '';
  	var lastName = '';

  	if(data.fullname && data.fullname.indexOf !== ' ' >= 0) {
  		var names = data.fullname.split(' ');
  		firstName = names[0];
  		names.splice(0, 1);
  		lastName = names.join(' ');
  	} else {
  		firstName = data.fullname;
  	}

  	var postData = {
  		"encoding": "UTF-8",
  		"oid": "00D1I000003r5oB",
  		"first_name": firstName,
  		"last_name": lastName,
  		"phone": data.phone,
  		"plan": data.plan,
  		"00N1I00000N4jL7": data.birthday,
  		"00N1I00000N4jL8": data.state,
  		"email": data.email,
  		"00N1I00000N4jL6": data.email,
  		"lead_source": "Nenhum"            
  	}

  	$.ajax({
  		url: "https://webto.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8",
  		method: 'post',
  		data: postData,
  		complete: function(res) {
  		  showSucessModal();
  		} 
  	});
  }  

  var showSucessModal = function() {
  	$('.success-modal').fadeIn();
  }

  var closeSucessModal = function() {
  	$('.success-modal').fadeOut();
  }

	return {
		init: init
	}
})();
var rtime;
var timeout = false;
var delta = 200;
var slickWasInitialized = null;

var extras = (function(){

	var init = function init(){
		cl('[extras] initializing...');

		$('.tab-trigger').on('click', function() {
			var target = $(this).attr('target');
			if(!$(this).hasClass('active')) {
				$(this).siblings().removeClass('active');
				$(this).addClass('active');

				$('.tab-content.active').fadeOut(function() {
					$('.tab-content.active').removeClass('active');
					$('#'+target).fadeIn().addClass('active');
				});
				
			};
		});
		
		cl('[extras] initiated.');
	};

	return {
		init: init
	}
})();
var header = (function(){

	var init = function init(){
		cl('[Header] initializing...');

		$('.hamburger').on('click', function(){
			$(this).toggleClass('is-active');
			$('header').toggleClass('menu-open');
			$('html').toggleClass('menu-open');
		})

		$('header a').on('click', function(){
			$('.hamburger').removeClass('is-active');
			$('header').removeClass('menu-open');
			$('html').removeClass('menu-open');
		})

		cl('[Header] initiated.');
	};

	return {
		init: init
	}
})();
var plans = (function(){

	var init = function init(){
		cl('[Plans] initializing...');

		$('#plan-selector a').off().on('click', function(e){
			e.preventDefault();
			e.stopPropagation();
			var act = $(this).attr('href');
			$(this).closest('ul').attr('class', act);
			$('#plan-list li').removeClass('active');
			$('#'+act).addClass('active');
		});

		cl('[Plans] initiated.');
	};

	return {
		init: init
	}
})();
var timeline = (function(){

	var init = function init(){
		cl('[Timeline] initializing...');

		$('#timeline-filter a').off().on('click', function(e){
			e.preventDefault();
			e.stopPropagation();
			$(this).toggleClass('active');

			filterTimeline();
		});


		$('#timeline').slick({
			dots: true,
			arrows: false,
			infinite: false,
			slidesToShow: 5,
			slidesToScroll: 5,
		    responsive: [
		    	{
			      breakpoint: 768,
			      settings: {
			        slidesToShow: 1,
			        slidesToScroll: 1
			      }
			    }
		    ]
		});		


		cl('[Timeline] initiated.');
	};

	var filterTimeline = function filterTimeline(){
		var fts = $('#timeline-filter .active').length;
		var filter = 'unfilter';
		var tmp;

		if(fts == 1) {
			filter = "."+$('#timeline-filter .active').eq(0).data('filter');
		}

		if(fts == 2) {
			filter = ":not('."+$('#timeline-filter a:not(.active)').eq(0).data('filter')+"')";
		}

		cl(filter);

	    $('#timeline').slick('slickUnfilter');
	    $('#timeline article>ul>li').show();
	    if(filter != "unfilter"){
    		$('#timeline').slick('slickFilter',filter);
		    $('#timeline article>ul>li').hide();
	    	$('#timeline article>ul>li'+filter).show();
	    }
	}

	return {
		init: init
	}
})();