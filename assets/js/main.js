$(function () {
  $(".grid-luz-sob-sombras").slick({
    dots: false,
    slidesToShow: 4,
    slidesToScroll: 1,
    speed: 300,
    prevArrow: $(".prev"),
    nextArrow: $(".next"),
    infinite: false,
    responsive: [
      {
        breakpoint: 1440,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 651,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  });
    $('.grid-comportamento').slick(
        {
            dots: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            speed: 300,
            infinite: false,
            arrows:false,
            appendDots: $('.dots-round'),
            asNavFor: '.dots-mobile .slick-dots',
        }
    );
    $('.dots-mobile .slick-dots').slick({
        infinite: false,
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '.grid-comportamento',
        arrows: false,
        dots: false,
        centerMode: true,
        focusOnSelect: true,
        centerPadding: '40px',
    });
});


$(function () {
    $('.grid-conversa-msg').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        speed: 300,
        prevArrow: $(".wprev"),
        nextArrow: $(".wnext"),
        dots:false,
        autoplay: true,
        autoplaySpeed:10000,

    })
  });

const menu = document.querySelector("#menu-mobile");
menu.addEventListener("click", toggleMenu);

function toggleMenu() {
  const menuwrapper = document.querySelector("#menu-mobile-wrapper");
  if (menuwrapper.classList.contains("open")) {
    menuwrapper.classList.remove("open");
    return;
  }

  menuwrapper.classList.add("open");
}

const linkmenu = document.querySelectorAll(".menu-mobile-wrapper a");
[].forEach.call(linkmenu, function (link) {
  link.addEventListener("click", function () {
    const menuwrapper = document.querySelector("#menu-mobile-wrapper");
    if (menuwrapper.classList.contains("open")) {
      menuwrapper.classList.remove("open");
    }
  });
});

// Select all links with hashes
$('a[href*="#"]')
  // Remove links that don't actually link to anything
  .not('[href="#"]')
  .not('[href="#0"]')
  .click(function (event) {
    event.preventDefault();
    // On-page links
    if (
      location.pathname.replace(/^\//, "") ==
        this.pathname.replace(/^\//, "") &&
      location.hostname == this.hostname
    ) {
      // Figure out element to scroll to
      const header = document.querySelector(".menu-header");

      var target = $(this.hash);
      target = target.length ? target : $("[name=" + this.hash.slice(1) + "]");
      // Does a scroll target exist?
      if (target.length) {
        // Only prevent default if animation is actually gonna happen
        event.preventDefault();
        $("html, body").animate(
          {
            scrollTop: target.offset().top - header.offsetHeight,
          },
          1000,
          function () {
            // Callback after animation
            // Must change focus!
          }
        );
      }
    }
  });

  var debug = true;
  var cl = function cl(msg) {
      if (debug) {
          console.log(msg);
      }
  };
  
  $(document).ready(function () {
      $("#menu-open,#menu-close").click(function () {
          $("#menu-in").slideToggle();
      });
  
      $('#horario-contato').select2({
  
          placeholder: 'Horário para contato'
      });
      $('#dia-contato').select2({

          placeholder: 'Dia para contato'
      });
  

  });
  
  var contact = (function () {
  
      var fields = {
          "fname": $('input#name'),
          "fphone": $('input#phone'),
          "femail": $('input#email'),
          //"fdob" : $('#dob'),
          'fcnpj': $('input#cnpj'),
          'fstate': $('input#id-estado'),
          'fcheckbox01': $('input#msg-autorization01'),
          //'fplano': $('input#plano'
      }
  
      var init = function init() {
  
  
          $('.close-modal-trigger').on('click', function () {
              closeSucessModal();
          })
  
          var SPMaskBehavior = function (val) {
              return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
          },
              spOptions = {
                  onKeyPress: function (val, e, field, options) {
                      field.mask(SPMaskBehavior.apply({}, arguments), options);
                  }
              };
  
          $('select').select2({
  
          });
  
          $(window).resize(function () {
              $('select').select2({});
          });
  
          $('.field-fone').mask(SPMaskBehavior, spOptions);
          $('#dob').mask('00/00/0000');
          bindValidation();
  
          $('.cnpj').mask('000.000.000-00', { reverse: false, clearIfNotMatch: true });
          $('#has_cnpj').on('change', function () {
              if ($(this).is(':checked')) {
                  $('#cnpj').attr('placeholder', 'CNPJ');
                  $('.cnpj').mask('00.000.000/0000-00', { reverse: false, clearIfNotMatch: true });
              } else {
                  $('#cnpj').attr('placeholder', 'CPF');
                  $('.cnpj').mask('000.000.000-00', { reverse: false, clearIfNotMatch: true });
  
              };
              $('#cnpj').val('');
          });
  
          $('#sendForm').on('click', function (e) {
              $(this).addClass('sending');
              e.preventDefault();
              $('#formfeedback').html("<p>Verificando dados...</p>");
              if (validate ()) {
  
                  //console.log("The form has been submitted");
                  // $(this).removeClass('sending');

                  sendForm();
              } else {
                  $('#formfeedback').html("<p class='error'>Existem erros no formulÃ¡rio ou algum campo sem marção, por favor verifique os campos e tente novamente.</p>");
                  console.log("The form contains errors.");
                  $(this).removeClass('sending');
              }
          });
  
  
  
          console.log('[Contact] initiated.');
      };
  
      var bindValidation = function bindValidation() {
          console.log('Binding validation events...');

          $('#name').on('blur', function () {
              validateName($(this));
          });
  
          $('#phone').on('blur', function () {
              if ($(this).val().length > 0) {
                  validatePhone($(this), false);
              }
          });
  
          $('#email').on('blur', function () {
              if ($(this).val().length > 0) {
                  validateEmail($(this), false);
              }
          });
          $('#dob').on('blur', function () {
              if ($(this).val().length > 0) {
                  validateDate($(this), false);
              }
          });
  
          $('#contact-form #cnpj').on('blur', function () {
              validateCNPJ($(this));
          });

          $('#contact-form #msg-autorization01').is('checked', function () {
                validateCheckbox01($(this));
            });
         
          $('#id-estado').on('change', function () {
              // if($(this).val().length > 0){
              validateName($(this));
              // }
          });

          $('#plano').on('change', function () {
              // if($(this).val().length > 0){
              validateName($(this));
              // }
          });
      };

      let checkbox = document.getElementById('#msg-autorization01');
  
      var validate = function validate() {

          var valid = true;
  
  
          if (!validateName(fields.fname)) { valid = false; }
          if (!validatePhone(fields.fphone, false)) { valid = false; }
          if (!validateEmail(fields.femail)) { valid = false; }
          //if(!validateDate(fields.fdob)){valid = false;}
          if (!validateName($(fields.fstate))) { valid = false; }
          //if (!validateName($(fields.fplano))) { valid = false; }
          if (!validateCNPJ(fields.fcnpj)) { valid = false; }
          if (!validateCheckbox(fields.fcheckbox01)) { valid = false; }

          return valid;
      };

      var validateCheckbox = function validateCheckbo01(fld) {
        var valid = true;
        var fldval = fld.val().trim();
        console.log('------------');
        console.log('Validating Checkbox01');
        console.log(fldval);

        fld.parent().removeClass('error success');
        if(fld.is(":checked")){
            valid=true;
        }else{
            valid=false;
        }

        if (valid) {
            fld.parent().addClass('success');
        } else {
            fld.parent().addClass('error');
        }
        console.log(valid);
        console.log('------------');
        return valid;
    }
  
      var validateName = function validateName(fld) {
          var valid = true;
          var fldval = fld.val().trim();
          console.log('------------');
          console.log('Validating Name');
          console.log(fldval);
  
          fld.parent().removeClass('error success');
          if (fldval.length == "") { valid = false; }
  
          if (valid) {
              fld.parent().addClass('success');
          } else {
              fld.parent().addClass('error');
          }
          console.log(valid);
          console.log('------------');
          return valid;
      }
      var validateEmail = function validateDate(fld, confirmation) {
          var valid = true;
          var fldval = fld.val().trim();
          console.log('------------');
          console.log('Validating Name');
          console.log(fldval);
  
          fld.parent().removeClass('error success');
          if (fldval.length == 0) { valid = false; }
          if (!isValidEmail(fldval)) { valid = false; }
  
          if (confirmation) {
              if (fldval != $('#email').val().trim()) { valid = false; }
          } else {
              $('#reemail').blur();
          }
  
          if (valid) {
              fld.parent().addClass('success');
          } else {
              fld.parent().addClass('error');
          }
          cl(valid);
          cl('------------');
          return valid;
      };
  
      /* var validateDate = function validateDate(fld) {
          var valid = true;
          var fldval = fld.val().trim();
          cl('------------');
          cl('Validating Name');
          cl(fldval);
  
          fld.parent().removeClass('error success');
          if (fldval.length < 10) { valid = false; }
          if (!isValidDate(fldval)) { valid = false; }
  
          if (valid) {
              fld.parent().addClass('success');
          } else {
              fld.parent().addClass('error');
          }
          cl(valid);
          cl('------------');
          return valid;
      }; */
  
      var validatePhone = function validatePhone(fld, req) {
          var valid = true;
          var fldval = fld.val().trim();
          console.log('------------');
          console.log('Validating Phone Number');
          console.log(fldval);
  
          fld.parent().removeClass('error success');
          if (req) {
              if (fldval.length < 14) { valid = false; }
          } else {
              if (fldval.length > 0) {
                  req = true;
                  if (fldval.length < 14) { valid = false; }
              }
          }
  
          if (req) {
              if (valid) {
                  fld.parent().addClass('success');
              } else {
                  fld.parent().addClass('error');
              }
          }
          console.log(valid);
          console.log('------------');
          return valid;
      }
  
      var validateCNPJ = function validateCNPJ(fld) {
          var valid = true;
          var fldval = fld.val().trim();
          cl('------------');
          cl('Validating Name');
          cl(fldval);
  
          fld.parent().removeClass('error success');
          if (fldval.length == 0) { valid = false; }
          if ($('#has_cnpj').is(':checked')) {
              if (!validarCNPJ(fldval)) {
                  valid = false;
              }
          } else {
              if (!validarCPF(fldval)) {
                  valid = false;
              }
          }
          console.log(valid);
  
          if (valid) {
              fld.parent().addClass('success');
          } else {
              fld.parent().addClass('error');
          }
  
          cl(valid);
          cl('------------');
          return valid;
      };

    

  
      var sendForm = function sendForm() {
          $('#formfeedback').html("<p class=''>Enviando. Aguarde um momento.</p>");
          // var formData = new FormData(this);
          //var endpoint = "/sendmail/index.php";
  
          var formData = {};
  
          formData.fullname = $('#name').val();
          formData.phone = $('#phone').val();
          formData.state = $('#state').val();
          formData.email = $('#email').val();
  
  
          console.log('Data to be sent:');
          console.log(formData);
  
          sendToSalesForce(formData);
  
  
      }
  
      var sendToSalesForce = function (data) {
          var serializedData = $('#contact-form').serialize();
          $.ajax({
              url: "post_lead.php",
              method: 'post',
              data: serializedData,
              complete: function(res) {
                  console.log(res);
                  showSucessModal();
              }
          });
      }
  
      function setCookie(cname, cvalue, exdays) {
          var d = new Date();
          d.setTime(d.getTime() + (exdays*24*60*60*1000));
          var expires = "expires="+ d.toUTCString();
          document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
        }
        
        $(document).ready(function(){
            if(getCookie("__utmzz")) {
                var utms = getCookie("__utmzz");
                var utmsA = utms.split('|');
                var utmsFinal = [];
                utmsA.forEach(function(el) {
                  var splitedEl = el.split('=');
                  utmsFinal[splitedEl[0]] = splitedEl[1];
                })
                
                $("#input-campaign").val(utmsFinal['utmccn']);
                $("#input-medium").val(utmsFinal['utmcmd']);
                $("#input-origin").val(utmsFinal['utmcsr']);
           }
           var urlPage = location.protocol + '//' + location.host + location.pathname;
                $("#url-page").val(urlPage);
          var htmlOut="";
          
          var selectEstado = $("#state");
          var selectCidades = $("#select-cidades");
          var selectPlano = $("#plan");
        
          $.ajax({
              url: "https://www.amilcuidadocerto.com.br/get_info.php",
              type: "GET",     
        
          }).done(function(resposta) {
            $('#select-cidades').parent().fadeOut();
            $.ajax({
                url: "get_states.php",
                type: "GET",  
                dataType: "json"
        
            }).done(function(resposta) {
                var htmlOutEstado ="<option value=''>Selecione seu estado</option>";
                var arrData = resposta.data
                arrData.forEach(function(el) {
                  htmlOutEstado += '<option value="'+el.uf+'" data-idEstado='+el._id+'>'+el.name+'</option>';
                })
                selectEstado.html(htmlOutEstado);
            });
            $('#plan').parent().fadeOut();
             $.ajax({
                url: "get_plans.php",
                type: "GET",  
                dataType: "json"
        
            }).done(function(resposta) {
                htmlOut = "";
                htmlOut +="<option value=''>Selecione seu Plano</option>";
                var arrData = resposta.data
                arrData.forEach(function(el) {
                  
                  htmlOut += '<option value="'+el._id+'">'+el.name+'</option>';
                })
                selectPlano.html(htmlOut);
            });    
          });
        
         
          selectEstado.on('change',function(){    
              var htmlOut="<option value=''>Selecione a cidade</option>";
              $("#id-estado").val($(this).find(':selected').data('idestado'));
              $.ajax({
                  url: "get_cities.php",
                  type: "POST",  
                  dataType: "json",
                  data: {uf: $(this).val()}
        
              }).done(function(resposta) {
                  var arrData = resposta.data
                  console.log(arrData.length);
                  if(arrData.length > 0) {
                    $('#select-cidades').parent().fadeIn()
                    console.log('teste');
                  } else {
                    $('#select-cidades').parent().fadeOut()
                  }
                  arrData.forEach(function(el) {           
                    htmlOut += '<option value="'+el._id+'">'+el.name+'</option>';
                  })
                  selectCidades.html(htmlOut);
        
              });
          });
        });
        
  
      var showSucessModal = function () {
          $('.success-modal').fadeIn();
      }
  
      var closeSucessModal = function () {
          $('.success-modal').fadeOut();
      }
  
      return {
          init: init
      }
  })();
  
  contact.init();
  
  
  document.querySelectorAll(".__range-step").forEach(function (ctrl) {
      var el = ctrl.querySelector('input');
      var output = ctrl.querySelector('output');
      var newPoint, newPlace, offset;
      el.oninput = function () {
          // colorize step options
          ctrl.querySelectorAll("option").forEach(function (opt) {
              if (opt.value <= el.valueAsNumber)
                  opt.style.backgroundColor = 'green';
              else
                  opt.style.backgroundColor = '#aaa';
          });
          // colorize before and after
          var valPercent = (el.valueAsNumber - parseInt(el.min)) / (parseInt(el.max) - parseInt(el.min));
          var style = 'background-image: -webkit-gradient(linear, 0% 0%, 100% 0%, color-stop(' +
              valPercent + ', green), color-stop(' +
              valPercent + ', #aaa));';
          el.style = style;
  
          // Popup
          if ((' ' + ctrl.className + ' ').indexOf(' ' + '__range-step-popup' + ' ') > -1) {
              var selectedOpt = ctrl.querySelector('option[value="' + el.value + '"]');
              output.innerText = selectedOpt.text;
              output.style.left = "50%";
              output.style.left = ((selectedOpt.offsetLeft + selectedOpt.offsetWidth / 2) - output.offsetWidth / 2) + 'px';
          }
      };
      el.oninput();
  });
  
  function isValidEmail(email) {
      var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(String(email).toLowerCase());
  }
  /*
  function allowDrop(ev) {
      ev.preventDefault();
    }
    
    function drag(ev) {
      ev.dataTransfer.setData("text", ev.target.id);
      this.style.opacity = '0.9';    
      dragSrcEl = this; 
      this.sty
    }
    
    function drop(ev) {
      ev.preventDefault();
      var data = ev.dataTransfer.getData("text");
      ev.target.appendChild(document.getElementById(data));
      let target = ev.target.dataset.target;
      $('.active').removeClass('active');
      $("#"+target).addClass('active');
      $(".target-step").hide();
      $("#target-"+target).fadeIn();
    }*/
  
  function moveChoiceTo(elem_choice, direction) {
  
      var span = elem_choice,
          td = span.parentNode;
  
      if (direction === -1 && span.previousElementSibling) {
          td.insertBefore(span, span.previousElementSibling);
  
      } else if (direction === 1 && span.nextElementSibling) {
  
          td.insertBefore(span.nextElementSibling, span.nextElementSibling)
      }
  }
  
  
  function isValidDate(dateString) {
      // First check for the pattern
      if (!/^\d{1,2}\/\d{1,2}\/\d{4}$/.test(dateString))
          return false;
  
      // Parse the date parts to integers
      var parts = dateString.split("/");
      var day = parseInt(parts[0], 10);
      var month = parseInt(parts[1], 10);
      var year = parseInt(parts[2], 10);
  
      // Check the ranges of month and year
      if (year < 1000 || year > (new Date()).getFullYear() || month == 0 || month > 12)
          return false;
  
      var monthLength = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
  
      // Adjust for leap years
      if (year % 400 == 0 || (year % 100 != 0 && year % 4 == 0))
          monthLength[1] = 29;
  
      // Check the range of the day
      return day > 0 && day <= monthLength[month - 1];
  };
  
  function setCookie(cname, cvalue, exdays) {
      var d = new Date();
      d.setTime(d.getTime() + (exdays*24*60*60*1000));
      var expires = "expires="+ d.toUTCString();
      document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }
    
    $(document).ready(function(){
        if(getCookie("__utmzz")) {
            var utms = getCookie("__utmzz");
            var utmsA = utms.split('|');
            var utmsFinal = [];
            utmsA.forEach(function(el) {
              var splitedEl = el.split('=');
              utmsFinal[splitedEl[0]] = splitedEl[1];
            })
            
            $("#input-campaign").val(utmsFinal['utmccn']);
            $("#input-medium").val(utmsFinal['utmcmd']);
            $("#input-origin").val(utmsFinal['utmcsr']);
       }
       var urlPage = location.protocol + '//' + location.host + location.pathname;
            $("#url-page").val(urlPage);
      var htmlOut="";
      
      var selectEstado = $("#state");
      var selectCidades = $("#select-cidades");
      var selectPlano = $("#plan");
    
      $.ajax({
          url: "get_info.php",
          type: "GET",     
    
      }).done(function(resposta) {
        $('#select-cidades').parent().fadeOut();
        $.ajax({
            url: "get_states.php",
            type: "GET",  
            dataType: "json"
    
        }).done(function(resposta) {
            var htmlOutEstado ="<option>Selecione seu estado</option>";
            var arrData = resposta.data
            arrData.forEach(function(el) {
              htmlOutEstado += '<option value="'+el.uf+'" data-idEstado='+el._id+'>'+el.name+'</option>';
            })
            selectEstado.html(htmlOutEstado);
        });
        $('#plan').parent().fadeOut();
         $.ajax({
            url: "get_plans.php",
            type: "GET",  
            dataType: "json"
    
        }).done(function(resposta) {
            htmlOut = "";
            htmlOut +="<option>Selecione seu Plano</option>";
            var arrData = resposta.data
            arrData.forEach(function(el) {
              
              htmlOut += '<option value="'+el._id+'">'+el.name+'</option>';
            })
            selectPlano.html(htmlOut);
        });    
      });
    
     
      selectEstado.on('change',function(){    
          var htmlOut="<option>Selecione a cidade</option>";
          $("#id-estado").val($(this).find(':selected').data('idestado'));
          $.ajax({
              url: "get_cities.php",
              type: "POST",  
              dataType: "json",
              data: {uf: $(this).val()}
    
          }).done(function(resposta) {
              var arrData = resposta.data
              console.log(arrData.length);
              if(arrData.length > 0) {
                $('#select-cidades').parent().fadeIn()
                console.log('teste');
              } else {
                $('#select-cidades').parent().fadeOut()
              }
              arrData.forEach(function(el) {           
                htmlOut += '<option value="'+el._id+'">'+el.name+'</option>';
              })
              selectCidades.html(htmlOut);
    
          });
      });
    });
    
  
  function validarCNPJ(cnpj) {
  
      cnpj = cnpj.replace(/[^\d]+/g, '');
  
      if (cnpj == '') return false;
  
      if (cnpj.length != 14)
          return false;
  
      // Elimina CNPJs invalidos conhecidos
      if (cnpj == "00000000000000" ||
          cnpj == "11111111111111" ||
          cnpj == "22222222222222" ||
          cnpj == "33333333333333" ||
          cnpj == "44444444444444" ||
          cnpj == "55555555555555" ||
          cnpj == "66666666666666" ||
          cnpj == "77777777777777" ||
          cnpj == "88888888888888" ||
          cnpj == "99999999999999")
          return false;
  
      // Valida DVs
      tamanho = cnpj.length - 2
      numeros = cnpj.substring(0, tamanho);
      digitos = cnpj.substring(tamanho);
      soma = 0;
      pos = tamanho - 7;
      for (i = tamanho; i >= 1; i--) {
          soma += numeros.charAt(tamanho - i) * pos--;
          if (pos < 2)
              pos = 9;
      }
      resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
      if (resultado != digitos.charAt(0))
          return false;
  
      tamanho = tamanho + 1;
      numeros = cnpj.substring(0, tamanho);
      soma = 0;
      pos = tamanho - 7;
      for (i = tamanho; i >= 1; i--) {
          soma += numeros.charAt(tamanho - i) * pos--;
          if (pos < 2)
              pos = 9;
      }
      resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
      if (resultado != digitos.charAt(1))
          return false;
  
      return true;
  
  }
  
  function validarCPF(strCPF) {
      var Soma;
      var Resto;
      strCPF = strCPF.replace(/[^\d]+/g, '');
      Soma = 0;
      if (strCPF == "00000000000") return false;
  
      for (i = 1; i <= 9; i++) Soma = Soma + parseInt(strCPF.substring(i - 1, i)) * (11 - i);
      Resto = (Soma * 10) % 11;
  
      if ((Resto == 10) || (Resto == 11)) Resto = 0;
      if (Resto != parseInt(strCPF.substring(9, 10))) return false;
  
      Soma = 0;
      for (i = 1; i <= 10; i++) Soma = Soma + parseInt(strCPF.substring(i - 1, i)) * (12 - i);
      Resto = (Soma * 10) % 11;
  
      if ((Resto == 10) || (Resto == 11)) Resto = 0;
      if (Resto != parseInt(strCPF.substring(10, 11))) return false;
      return true;
  }
  
  function getCookie(cname) {
      var name = cname + "=";
      var decodedCookie = decodeURIComponent(document.cookie);
      var ca = decodedCookie.split(';');
      for(var i = 0; i <ca.length; i++) {
          var c = ca[i];
          while (c.charAt(0) == ' ') {
              c = c.substring(1);
          }
          if (c.indexOf(name) == 0) {
              return c.substring(name.length, c.length);
          }
      }
      return "";
  }

