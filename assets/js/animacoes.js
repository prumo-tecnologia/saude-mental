$(function () { // wait for document ready
    var controller = new ScrollMagic.Controller();
    function init() {
        var isMobile = (function (a) {
            return /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4));
        })(navigator.userAgent || navigator.vendor || window.opera);

        if ($(window).width() < 769) {
            var wipeAnimation = new TimelineMax()
                .from("#box-depoimento p:first-child", 1, {
                    transform: "matrix3d(0.631517, -0.416881, 0, -0.000456567, 4.229144, -0.749892, 0, 0, 0, 0, 1, 0, 28.7394, 0, 0, 1)",
                    duration: 0.5,
                    opacity: 0,
                    scale: 2,
                    y: 80,
                    rotationX: 180,
                    transformOrigin: "0% 50% -50",
                    ease: "back",
                    stagger: 0.01
                })  // in from left
                .from("#box-depoimento p:nth-child(2)", 1, {
                    transform: "matrix3d(0.631517, 0.416881, 0, -0.000456567, 4.229144, 0.749892, 0, -0.00143939, 0, 0, 1, 0, 28.7394, -30.3039, 0, 1)",
                    duration: 0.5,
                    opacity: 0,
                    scale: 2,
                    y: 80,
                    rotationX: 180,
                    transformOrigin: "0% 50% -50",
                    ease: "back",
                    stagger: 0.01
                })  // in from left
            var scene = new ScrollMagic.Scene({
                triggerElement: "#box-depoimento"
            })
                .setTween(wipeAnimation )

                .addTo(controller);
            var tween0 = new TimelineMax()
                .to(".img-top-mobile", 1, {y: "-100%"}, 0, 0)

            var scene0 = new ScrollMagic.Scene({triggerElement: ".titulo-banner", duration: 1500, offset: 0})
                .setTween(tween0)
                .triggerHook(0)
                .addTo(controller);
            var tween2 = new TimelineMax()
                .from("#luz-sob-sombras-item1", 8, {x: "100%"}, "+=2")
                .from("#luz-sob-sombras-item1 path", 8, {fill: "#461BFF"}, "+=2")
                .from("#luz-sob-sombras-item1 p", 8, {opacity: 0}, "+=2")
                .to("#luz-sob-sombras-item1", 8, {x: "-300%"}, "+=2")
                .from("#luz-sob-sombras-item2", 8, {x: "100%"}, "+=2")
                .from("#luz-sob-sombras-item2 path", 8, {fill: "#461BFF"}, "+=2")
                .from("#luz-sob-sombras-item2 p", 8, {opacity: 0}, "+=2")
                .to("#luz-sob-sombras-item2", 8, {x: "-300%"}, "+=2")
                .from("#luz-sob-sombras-item3", 8, {x: "100%"}, "+=2")
                .from("#luz-sob-sombras-item3 path", 8, {fill: "#461BFF"}, "+=2")
                .from("#luz-sob-sombras-item3 p", 8, {opacity: 0}, "+=2")
                .to("#luz-sob-sombras-item3", 8, {x: "-300%"}, "+=2")
                .from("#luz-sob-sombras-item4", 8, {x: "100%"}, "+=2")
                .from("#luz-sob-sombras-item4 path", 8, {fill: "#461BFF"}, "+=2")
                .from("#luz-sob-sombras-item4 p", 8, {opacity: 0}, "+=2")
                .to("#luz-sob-sombras-item4", 8, {x: "-300%"}, "+=5")


            var scene2 = new ScrollMagic.Scene({triggerElement: "#luz-sob-sombras", duration: 5000, offset: 50})
                .setTween(tween2)
                .setPin('#luz-sob-sombras')
                .triggerHook(0)
                .addTo(controller);


            var tween3 = new TimelineMax()
                .from("#mapa-balao-1 ", 0.9, {width: 0, height: 0}, 0, 0)
                .from("#mapa-balao-1 p", 0.9, {opacity: 0}, 1, 0)
                .from("#mapa-balao2", 0.9, {width: 0, height: 0}, 2, 0)
                .from("#mapa-balao2 p", 0.9, {opacity: 0}, 3, 0)
                .from("#mapa-balao-3", 0.9, {width: 0, height: 0}, 4, 0)
                .from("#mapa-balao-3 p", 0.9, {opacity: 0}, 5, 0)

            var scene3 = new ScrollMagic.Scene({triggerElement: "#stats-brasil", duration: 1500, offset: 0})
                .setTween(tween3)
                .setPin('#stats-brasil')
                .triggerHook(0)
                .addTo(controller);

            var scene4 = new ScrollMagic.Scene({triggerElement: "#box-linha-do-tempo", duration: 800, offset: -100})
                .setPin('#box-linha-do-tempo')
                .triggerHook(0)
                .addTo(controller)
                .on('enter', function (e) {
                    const obj = document.getElementById("pin-percent");
                    const pin = document.getElementById("pin");
                    pin.classList.add('moved');

                    animateValue(obj, 0, 40, 700);
                })
                .on('leave', function (e) {
                    if (e.state == "BEFORE") {
                        const pin = document.getElementById("pin");
                        pin.classList.remove('moved');
                        const obj = document.getElementById("pin-percent");
                        animateValue(obj, 40, 0, 700);
                    }
                })

            var tween5 = new TimelineMax()
                .from("#frase-saude-mental p", 1, {y: "100%", opacity: 0.2});


            // build scene
            var scene5 = new ScrollMagic.Scene({
                triggerElement: "#frase-saude-mental",
                duration: $("#frase-saude-mental").height()
            })
                .setTween(tween5)
                .addTo(controller);


            var tween6 = new TimelineMax()
                .from("#citacao-1 img", 1, {x: "-200%"})
                .from("#citacao-2 img", 1, {x: "+200%"});


            // build scene
            var scene6 = new ScrollMagic.Scene({triggerElement: "#citacoes", duration: 600})
                .setTween(tween6)
                .addTo(controller);

            var tween7 = new TimelineMax()
                .from("#historia svg", 2, {y: "100px"})
                .call(
                    function () {
                        const obj = document.getElementById("video-gatilhos-mobile");
                        obj.currentTime = 0;
                        obj.play();
                    }
                )


            // build scene
            var scene7 = new ScrollMagic.Scene({triggerElement: "#historia", duration: 800, offset: -300})
                .setTween(tween7)
                .addTo(controller);

            var tween8 = new TimelineMax()
                .from("#oquee .container", 0.1, {y: "50%"}, "0")
                .from(".afinal", 2, {scale: 0, opacity: 0}, "+=2")
                .from(".afinal .container", 2, {scale: 0, opacity: 0}, "+=2")
                .from(".psicologia-gatilho", 2, {scale: 0, opacity: 0}, "+=2")
                .from(".psicologia-gatilho .container", 1, {scale: 0, opacity: 0}, "+=2")
                .from(".e-como", 2, {scale: 0, opacity: 0}, "+=2")
                .from(".e-como .container", 1, {scale: 0, opacity: 0}, "+=2")
                .from(".identificar", 2, {scale: 0, opacity: 0}, "+=2")
                .from(".identificar .container", 1, {scale: 0, opacity: 0}, "+=2")
                .from(".auto-observacao", 2, {scale: 0, opacity: 0}, "+=2")
                .from(".auto-observacao .container", 1, {scale: 0, opacity: 0}, "+=2")
                .from(".nao-ha-motivo", 2, {scale: 0, opacity: 0}, "+=2")
                .from(".nao-ha-motivo .container", 1, {scale: 0, opacity: 0}, "+=2")
                .from(".sofrer", 2, {scale: 0, opacity: 0}, "+=2")
                .from(".sofrer .container", 1, {scale: 0, opacity: 0}, "+=2")
                .from(".busque-ajuda", 2, {scale: 0, opacity: 0}, "+=6")
                .from(".busque-ajuda .container", 6, {scale: 0, opacity: 0}, "+=0")

            // build scene
            var scene8 = new ScrollMagic.Scene({triggerElement: "#pin-blocks", duration: "940%"})
                .setPin("#pin-blocks")
                .setTween(tween8)
                .triggerHook(0)
                .addTo(controller);


            var tween9 = new TimelineMax()
                .from(".grid-a-gente-sabe .grid-item:nth-child(5)", 1, {y: "500%"})
                .from(".grid-a-gente-sabe .grid-item:nth-child(4)", 2, {y: "500%"})
                .from(".grid-a-gente-sabe .grid-item:nth-child(3)", 2, {y: "500%"})
                .from(".grid-a-gente-sabe .grid-item:nth-child(2)", 2, {y: "500%"})
                .from(".grid-a-gente-sabe .grid-item:nth-child(1)", 2, {y: "500%"})


            // build scene
            var scene9 = new ScrollMagic.Scene({triggerElement: ".a-gente-sabe", duration: 1500, offset: 620})
                .setPin(".a-gente-sabe")
                .setTween(tween9)
                .triggerHook(0)
                .addTo(controller);
            var tween10 = new TimelineMax()
                .from(".mais-ajudam .grid-item:nth-child(1)", 4, {x: "200%"}, "+=2")
                .call(function () {
                    $(".dots-mais-ajudam-item:nth-child(1)").addClass('active');
                    $(".dots-mais-ajudam-item:nth-child(2)").removeClass('active');
                    $(".dots-mais-ajudam-item:nth-child(3)").removeClass('active');
                    $(".dots-mais-ajudam-item:nth-child(4)").removeClass('active');

                })
                .from(".mais-ajudam .grid-item:nth-child(2)", 4, {x: "200%"}, "+=2")
                .call(function () {
                    $(".dots-mais-ajudam-item:nth-child(2)").addClass('active');
                    $(".dots-mais-ajudam-item:nth-child(1)").removeClass('active');
                    $(".dots-mais-ajudam-item:nth-child(3)").removeClass('active');
                    $(".dots-mais-ajudam-item:nth-child(4)").removeClass('active');

                })

                .from(".mais-ajudam .grid-item:nth-child(3)", 4, {x: "200%"}, "+=2")
                .call(function () {
                    $(".dots-mais-ajudam-item:nth-child(3)").addClass('active');
                    $(".dots-mais-ajudam-item:nth-child(1)").removeClass('active');
                    $(".dots-mais-ajudam-item:nth-child(2)").removeClass('active');
                    $(".dots-mais-ajudam-item:nth-child(4)").removeClass('active');

                })
                .from(".mais-ajudam .grid-item:nth-child(4)", 4, {x: "200%"}, "+=2")
                .call(function () {
                    $(".dots-mais-ajudam-item:nth-child(4)").addClass('active');
                    $(".dots-mais-ajudam-item:nth-child(1)").removeClass('active');
                    $(".dots-mais-ajudam-item:nth-child(2)").removeClass('active');
                    $(".dots-mais-ajudam-item:nth-child(3)").removeClass('active');

                })

            // build scene
            var scene10 = new ScrollMagic.Scene({triggerElement: ".mais-ajudam", duration: 1500})
                .setPin(".mais-ajudam")
                .setTween(tween10)
                .triggerHook(0)
                .addTo(controller);

            var tween11 = new TimelineMax()
                .from(".flex-amil-funciona:nth-child(2)", 8, {opacity: 0}, "+=2")
                .from(".flex-amil-funciona:nth-child(2)+svg", 2, {opacity: 0}, "+=2")
                .from(".flex-amil-funciona:nth-child(4)", 2, {opacity: 0}, "+=2")
                .from(".flex-amil-funciona:nth-child(4)+svg", 2, {opacity: 0}, "+=4")
                .from(".flex-amil-funciona:nth-child(6)", 2, {opacity: 0}, "+=6")
                .from(".flex-amil-funciona:nth-child(6)+svg", 2, {opacity: 0}, "+=4")
                .from(".flex-amil-funciona:nth-child(8)", 2, {opacity: 0}, "+=6")
                .from(".flex-amil-funciona:nth-child(8)+svg", 2, {opacity: 0}, "+=6")
                .from(".flex-amil-funciona:nth-child(10)", 2, {opacity: 0}, "+=6")

            // build scene
            var scene11 = new ScrollMagic.Scene({triggerElement: ".amil-funciona", duration: 2700, offset: 300})
                .setTween(tween11)
                .addTo(controller);


        } else {
            var wipeAnimation = new TimelineMax()
                .from("#box-depoimento p:first-child", 1, {
                    transform: "matrix3d(0.631517, -0.416881, 0, -0.000456567, 4.229144, -0.749892, 0, 0, 0, 0, 1, 0, 28.7394, 0, 0, 1)",
                    duration: 0.5,
                    opacity: 0,
                    scale: 2,
                    y: 80,
                    rotationX: 180,
                    transformOrigin: "0% 50% -50",
                    ease: "back",
                    stagger: 0.01
                })  // in from left
                .from("#box-depoimento p:nth-child(2)", 1, {
                    transform: "matrix3d(0.631517, 0.416881, 0, -0.000456567, 4.229144, 0.749892, 0, -0.00143939, 0, 0, 1, 0, 28.7394, -30.3039, 0, 1)",
                    duration: 0.5,
                    opacity: 0,
                    scale: 2,
                    y: 80,
                    rotationX: 180,
                    transformOrigin: "0% 50% -50",
                    ease: "back",
                    stagger: 0.01
                })  // in from left
            var scene = new ScrollMagic.Scene({
                triggerElement: "#box-depoimento"
            })
                .setTween(wipeAnimation )

                .addTo(controller);
            /*var wipeAnimation = new TimelineMax();

            const box = document.getElementById("box-depoimento");
            const firstText = box.querySelector('p:first-child').innerHTML.split('');
            const secondText = box.querySelector('p:nth-child(2)').innerHTML.split('');

            var p1 = new Promise(
                // a função resolve() é chamada com a capacidade para resolver ou
                // rejeitar a promise
                function(resolve, reject) {
                    var stringText = '<span class="first-paragraph">';
                    i=0;
                    [].forEach.call(firstText,function(el) {
                        if(el == " ") {
                            stringText += "&nbsp;";
                        } else if( el == "|") {
                            stringText += "<br>";
                        } else {
                            stringText +="<span class='paragraph-letter-"+i+"'>";
                            stringText += el;
                            stringText +="</span>";
                        }
                        i++;
                    });
                    stringText +="</span>";
                    box.querySelector('p:first-child').innerHTML = stringText;
                    const boxp1 = document.querySelector(".first-paragraph");

                    j = 0;
                    [].forEach.call(boxp1.querySelectorAll("span"), function(elm){

                        const elitem = ".first-paragraph paragraph-letter-"+j;
                        wipeAnimation.add(
                            [
                                TweenMax.from(elm,0.04,{transform:"matrix3d("+Math.random()+", "+ Math.random()*-1 +", 0, "+ Math.random()*-1 +", "+Math.random()+4+", -0.749892, 0, 0, 0, 0, 1, 0, 28.7394, 0, 0, 1)",opacity:0,duration: 0.1, scale:2, y:80, rotationX:180, transformOrigin: Math.random()*100+"% "+Math.random()*100+"% "+Math.random()*100+"%",  ease:"back", stagger: 0.01},0)
                            ]
                        )
                        j++;
                    });


                    var stringText2 = '<span class="second-paragraph">';
                    i=0;
                   var strongOpened = false;
                    [].forEach.call(secondText,function(el) {
                        if(el == " ") {
                            stringText2 += "&nbsp;";
                        } else if( el == "|") {
                            stringText2 += "<br>";
                        } else if( el == "\\" && strongOpened == false) {
                            console.log('opened');
                            strongOpened = true;
                            stringText2 += "<strong>";
                        } else if( el == "\\" && strongOpened == true) {
                            strongOpened = false;
                            stringText2 += "</strong><br>";
                        }
                        else {
                            stringText2 +="<span class='paragraph-letter-"+i+"'>";
                            stringText2 += el;
                            stringText2 +="</span>";
                        }
                        i++;
                    });
                    stringText2 +="</span>";
                    wipeAnimation.add(
                        [
                            TweenMax.to(".first-paragraph",1,{opacity:0})
                        ]
                    )
                    box.querySelector('p:nth-child(2)').innerHTML = stringText2;
                    const boxp2 = document.querySelector(".second-paragraph");

                    [].forEach.call(boxp2.querySelectorAll("span"), function(elm){
                      wipeAnimation.add(
                            [
                                TweenMax.from(elm,0.08,{transform:"matrix3d("+Math.random()+", "+ Math.random()*-1 +", 0, "+ Math.random()*-1 +", "+Math.random()+4+", -0.749892, 0, 0, 0, 0, 1, 0, 28.7394, 0, 0, 1)",opacity:0,duration: 0.1, scale:2, y:80, rotationX:180, transformOrigin: Math.random()*100+"% "+Math.random()*100+"% "+Math.random()*100+"%",  ease:"back", stagger: 0.01},0)
                            ]
                        )

                    });


                    resolve(true);
                });


            p1.then(

                // apenas logamos a mensagem e o valor
                function(val) {

                    var scene = new ScrollMagic.Scene({
                        triggerElement: "#box-depoimento"
                    })
                        .setTween(wipeAnimation )
                        .addIndicators({name: "1 (duration: 123123)"})
                        .addTo(controller);
                }
            );
            */


            var tween0 = new TimelineMax()
                .to(".banner", 0.9, {backgroundPositionY: "400px"}, 0, 0)

            var scene0 = new ScrollMagic.Scene({triggerElement: ".banner", duration: 1500, offset: 0})
                .setTween(tween0)
                .triggerHook(0)
                .addTo(controller);


            var tween2 = new TimelineMax()
                .from("#luz-sob-sombras-item1 path", 0.9, {fill: "#461BFF"}, 0, 0)
                .from("#luz-sob-sombras-item1 p", 0.9, {opacity: 0}, 0, 0)
                .from("#luz-sob-sombras-item2 path", 0.9, {fill: "#461BFF"}, 1, 0)
                .from("#luz-sob-sombras-item2 p", 0.9, {opacity: 0}, 1, 0)
                .from("#luz-sob-sombras-item3 path", 0.9, {fill: "#461BFF"}, 2, 0)
                .from("#luz-sob-sombras-item3 p", 0.9, {opacity: 0}, 2, 0)
                .from("#luz-sob-sombras-item4 path", 0.9, {fill: "#461BFF"}, 3, 0)
                .from("#luz-sob-sombras-item4 p", 0.9, {opacity: 0}, 3, 0)


            var scene2 = new ScrollMagic.Scene({triggerElement: "#luz-sob-sombras", duration: 1500, offset: 0})
                .setTween(tween2)
                .setPin('#luz-sob-sombras')
                .triggerHook(0)
                .addTo(controller);

            var tween3 = new TimelineMax()
                .from("#mapa-balao-1 ", 0.9, {width: 0, height: 0}, 0, 0)
                .from("#mapa-balao-1 p", 0.9, {opacity: 0}, 1, 0)
                .from("#mapa-balao2", 0.9, {width: 0, height: 0}, 2, 0)
                .from("#mapa-balao2 p", 0.9, {opacity: 0}, 3, 0)
                .from("#mapa-balao-3", 0.9, {width: 0, height: 0}, 4, 0)
                .from("#mapa-balao-3 p", 0.9, {opacity: 0}, 5, 0)

            var scene3 = new ScrollMagic.Scene({triggerElement: "#stats-brasil", duration: 1500, offset: 0})
                .setTween(tween3)
                .setPin('#stats-brasil')
                .triggerHook(0)
                .addTo(controller);


            var scene4 = new ScrollMagic.Scene({triggerElement: "#box-linha-do-tempo", duration: 800, offset: -100})
                .setPin('#box-linha-do-tempo')
                .triggerHook(0)
                .addTo(controller)
                .on('enter', function (e) {
                    const obj = document.getElementById("pin-percent");
                    const pin = document.getElementById("pin");
                    pin.classList.add('moved');

                    animateValue(obj, 0, 40, 700);
                })
                .on('leave', function (e) {
                    if (e.state == "BEFORE") {
                        const pin = document.getElementById("pin");
                        pin.classList.remove('moved');
                        const obj = document.getElementById("pin-percent");
                        animateValue(obj, 40, 0, 700);
                    }
                })


            var tween5 = new TimelineMax()
                .from("#frase-saude-mental p", 1, {y: "100%", opacity: 0.2});


            // build scene
            var scene5 = new ScrollMagic.Scene({
                triggerElement: "#frase-saude-mental",
                duration: $("#frase-saude-mental").height()
            })
                .setTween(tween5)
                .addTo(controller);


            var tween6 = new TimelineMax()
                .from("#citacao-1 img", 1, {x: "-200%"})
                .from("#citacao-2 img", 1, {x: "+200%"});


            // build scene
            var scene6 = new ScrollMagic.Scene({triggerElement: "#citacoes", duration: 900})
                .setTween(tween6)
                .addTo(controller);


            var tween7 = new TimelineMax()
                .from("#historia svg", 4, {y: "500px"})
                .call(
                    function () {
                        const obj = document.getElementById("video-gatilhos");
                        obj.currentTime = 0;
                        obj.play();
                    }
                )


            // build scene
            var scene7 = new ScrollMagic.Scene({triggerElement: "#historia", duration: 800, offset: -300})
                .setTween(tween7)
                .addTo(controller);


            var tween8 = new TimelineMax()
                .from("#oquee .container", 0.1, {y: "50%"}, "0")
                .from(".afinal", 2, {scale: 0, opacity: 0}, "+=4")
                .from(".afinal .container", 1, {scale: 0, opacity: 0}, "+=3")
                .from(".psicologia-gatilho", 2, {scale: 0, opacity: 0}, "+=3")
                .from(".psicologia-gatilho .container", 1, {scale: 0, opacity: 0}, "+=3")
                .from(".e-como", 2, {scale: 0, opacity: 0}, "+=2")
                .from(".e-como .container", 1, {scale: 0, opacity: 0}, "+=3")
                .from(".identificar", 2, {scale: 0, opacity: 0}, "+=3")
                .from(".identificar .container", 1, {scale: 0, opacity: 0}, "+=3")
                .from(".auto-observacao", 2, {scale: 0, opacity: 0}, "+=3")
                .from(".auto-observacao .container", 1, {scale: 0, opacity: 0}, "+=3")
                .from(".nao-ha-motivo", 2, {scale: 0, opacity: 0}, "+=3")
                .from(".nao-ha-motivo .container", 1, {scale: 0, opacity: 0}, "+=3")
                .from(".sofrer", 2, {scale: 0, opacity: 0}, "+=3")
                .from(".sofrer .container", 1, {scale: 0, opacity: 0}, "+=3")
                .from(".busque-ajuda", 2, {scale: 0, opacity: 0}, "+=6")
                .from(".busque-ajuda .container", 6, {scale: 0, opacity: 0}, "+=3")

            // build scene
            var scene8 = new ScrollMagic.Scene({triggerElement: "#pin-blocks", duration: "940%"})
                .setPin("#pin-blocks")
                .setTween(tween8)
                .triggerHook(0)
                .addTo(controller);


            var tween9 = new TimelineMax()
                .from(".grid-a-gente-sabe .grid-item:nth-child(5)", 1, {y: "200%"})
                .from(".grid-a-gente-sabe .grid-item:nth-child(4)", 2, {y: "200%"})
                .from(".grid-a-gente-sabe .grid-item:nth-child(3)", 2, {y: "200%"})
                .from(".grid-a-gente-sabe .grid-item:nth-child(2)", 2, {y: "200%"})
                .from(".grid-a-gente-sabe .grid-item:nth-child(1)", 2, {y: "200%"})
            var $offset = 0;
            // build scene
            var scene9 = new ScrollMagic.Scene({triggerElement: ".a-gente-sabe", duration: 1500, offset: $offset})
                .setPin(".a-gente-sabe")
                .setTween(tween9)
                .triggerHook(0)
                .addTo(controller);


            var tween10 = new TimelineMax()
                .from(".mais-ajudam .grid-item:nth-child(1)", 4, {y: "400%"}, "+=2")
                .call(function () {
                    $(".dots-mais-ajudam-item:nth-child(1)").addClass('active');
                    $(".dots-mais-ajudam-item:nth-child(2)").removeClass('active');
                    $(".dots-mais-ajudam-item:nth-child(3)").removeClass('active');
                    $(".dots-mais-ajudam-item:nth-child(4)").removeClass('active');

                })
                .from(".mais-ajudam .grid-item:nth-child(2)", 4, {y: "400%"}, "+=2")
                .call(function () {
                    $(".dots-mais-ajudam-item:nth-child(2)").addClass('active');
                    $(".dots-mais-ajudam-item:nth-child(1)").removeClass('active');
                    $(".dots-mais-ajudam-item:nth-child(3)").removeClass('active');
                    $(".dots-mais-ajudam-item:nth-child(4)").removeClass('active');

                })

                .from(".mais-ajudam .grid-item:nth-child(3)", 4, {y: "400%"}, "+=2")
                .call(function () {
                    $(".dots-mais-ajudam-item:nth-child(3)").addClass('active');
                    $(".dots-mais-ajudam-item:nth-child(1)").removeClass('active');
                    $(".dots-mais-ajudam-item:nth-child(2)").removeClass('active');
                    $(".dots-mais-ajudam-item:nth-child(4)").removeClass('active');

                })
                .from(".mais-ajudam .grid-item:nth-child(4)", 4, {y: "400%"}, "+=2")
                .call(function () {
                    $(".dots-mais-ajudam-item:nth-child(4)").addClass('active');
                    $(".dots-mais-ajudam-item:nth-child(1)").removeClass('active');
                    $(".dots-mais-ajudam-item:nth-child(2)").removeClass('active');
                    $(".dots-mais-ajudam-item:nth-child(3)").removeClass('active');

                })

            // build scene
            var scene10 = new ScrollMagic.Scene({triggerElement: ".mais-ajudam", duration: 1500})
                .setPin(".mais-ajudam")
                .setTween(tween10)
                .triggerHook(0)
                .addTo(controller);


            var tween11 = new TimelineMax()
                .from(".flex-amil-funciona:nth-child(2)", 1, {opacity: 0}, 1)
                .from(".flex-amil-funciona:nth-child(2)+svg", 1, {opacity: 0}, 2)
                .from(".flex-amil-funciona:nth-child(4)", 1, {opacity: 0}, 2)
                .from(".flex-amil-funciona:nth-child(4)+svg", 1, {opacity: 0}, 3)
                .from(".flex-amil-funciona:nth-child(6)", 1, {opacity: 0}, 3)
                .from(".flex-amil-funciona:nth-child(6)+svg", 1, {opacity: 0}, 4)
                .from(".flex-amil-funciona:nth-child(8)", 1, {opacity: 0}, 4)
                .from(".flex-amil-funciona:nth-child(8)+svg", 1, {opacity: 0}, 5)
                .from(".flex-amil-funciona:nth-child(10)", 1, {opacity: 0}, 5)

            // build scene
            var scene11 = new ScrollMagic.Scene({triggerElement: ".amil-funciona", duration: 1600})
                .setTween(tween11)
                .addTo(controller);


        }

        function animateValue(obj, start, end, duration) {
            let startTimestamp = null;
            const step = (timestamp) => {
                if (!startTimestamp) startTimestamp = timestamp;
                const progress = Math.min((timestamp - startTimestamp) / duration, 1);
                obj.innerHTML = Math.floor(progress * (end - start) + start);
                if (progress < 1) {
                    window.requestAnimationFrame(step);
                }

            };
            window.requestAnimationFrame(step);
        }
    }
    $(window).on('orientationchange', reloadPage);
    function reloadPage() {
        location.reload();
    }
    init(false);
});