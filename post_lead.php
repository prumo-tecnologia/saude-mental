<?php
	session_start();
	$ch = curl_init('https://api.nt.digital/lead/register');
	$params = array(
		"name"=> $_POST['name'],
		"email"=> $_POST['email'],
		"phone"=> $_POST['phone'],
		"state_id"=> $_POST['state'],
		"city_id"=> $_POST['city'],
		"plan_id"=> $_POST['plan'],
		"campaign"=> $_POST['campaign'],
		"midia"=> $_POST['medium'],
		"urlPage"=> $_POST['url-page'],
		"origin"=> $_POST['origin'],
        "document"=> $_POST['cnpj']
	);	
	
	curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json","charset:UTF-8","Authorization: Bearer ". $_SESSION['token_login']));
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_TIMEOUT, 30);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLINFO_HEADER_OUT, false);
	curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params)); 	
	
	$return = curl_exec($ch);

	echo $return;
	curl_close($ch);
